Feature: Lending Chanelling

  @dataDummy
  Scenario Outline: Generate Data Dummy Individu
    Given generate data "<Case1>""<Case2>"

    Examples:
      | Case1      | Case2      |
      | APPFILEIDV | REAFILEIDV |

  @approvalForm
  Scenario: Recomendation data Individu
    Given open browser and usermaker login application
    When akses dan verifikasi tampilan list approval form
    Then verifikasi semua data yang ada di list approval form
#3-5
    And verifikasi filter dropdown source approval form
      | klik drop down |
      | all dan los    |
      | nikel          |
#6-12
    And verifikasi filter dropdown status klik field dropdown status
      | klik drop down     |
      | all                |
      | waiting for review |
      | waiting approval   |
      | approved           |
      | rejected           |
      | approval expired   |
#13
    And verifikasi filter dropdown partner klik field dropdown partner
      | klik drop down partner |
      | all                    |
      | pilih partner          |
#16
    And verifikasi fungsi search approval form
    And verifikasi fungsi next page
    And verifikasi fungsi prev page
    And verifikasi fungsi specific page
    And verifikasi fungsi sort debtor name
    And verifikasi fungsi sort partner
    And verifikasi fungsi sort date
    And verifikasi fungsi sort loan amount
    And verifikasi fungsi sort status
#25
    And verifikasi data approval form source
#26-31
    And verifikasi data approval form
      | no aplikasi |
      | debtor name |
      | partner     |
      | date        |
      | loan amount |
      | rate        |
#32
    And verifikasi data approval form detail channeling
      | partner                    |
      | nama debitur               |
      | status                     |
      | no applikasi               |
      | tanggal pengajuan approval |
      | jenis debitur              |
      | plafon                     |
      | jangka waktu               |
      | interest rate              |
      | nik                        |
      | npwp                       |
      | mobile phone number        |
      | remark terisi              |
#45-46
    And verifikasi data detail remark approval form
      | detail remark (data kosong pada file approval)  |
      | detail dokumen pendukung (tidak upload dokumen) |
#47-48
    Then klik bulk recommendation
      | klik check pada beberapa data status waiting for review   |
      | klik uncheck pada beberapa data status waiting for review |
#49
    And klik button back cancel selection approval form
    And klik bulk recommendation klik check all
    And klik bulk recommendation klik check pada data status approved
    And klik bulk recommendation klik check pada data status rejected
    And klik bulk recommendation klik check pada data status waiting approval
#54
    And klik bulk recommendation klik check pada beberapa data status waiting for review klik recommendation
#55
    And klik bulk recommendation klik check pada beberapa data status waiting for review klik not recommendation
#56
    And klik debtor name dengan status waiting for review
#57
    And klik debtor name dengan status waiting for review tidak mengisi rekomendasi dan catatan klik submit
#58
    And klik debtor name dengan status waiting for review mengisi rekomendasi tidak mengisi catatan klik submit
#59
    And klik debtor name dengan status waiting for review tidak mengisi rekomendasi mengisi catatan klik submit
#60
    And klik debtor name dengan status waiting for review mengisi rekomendasi recommended mengisi catatan 5000karakter klik submit
#61
    And klik debtor name dengan status waiting for review mengisi rekomendasi not recommended mengisi catatan 4999karakter klik submit
#62
    And verifikasi data approval form detail dokumen pendukung upload dokumen> 1file zip
#63
    And verifikasi data approval form detail dokumen pendukung upload dokumen2x isi file berbeda
#64
    And klik bulk recommendation klik check pada data status approval expired
#65-66
    Then klik bulk recommendation check
      | klik check all klik recommendation     |
      | klik check all klik not recommendation |
#67
    And login approver klik bulk approve klik check pada beberapa data status waiting approval
#68
    And klik bulk approve klik uncheck pada beberapa data status waiting approval
#69
    And klik bulk button back cancel selection approval form
#70
    And klik bulk approve klik check all
#71
    And klik bulk approve klik check pada data status approved
#72
    And klik bulk approve klik check pada data status rejected
#73
    And klik bulk approve klik check pada data status waiting approval
#74
    And klik bulk approve klik check pada data status approval expired
#75
    And klik bulk approve klik check pada beberapa data status waiting approval klik reject
#76
    And klik bulk approve klik check pada beberapa data status waiting approval klik approve recomendation
#77
    And klik debtor name dengan status waiting approval approval form
#78
    And klik debtor name dengan status approval expired
#79
    And klik debtor name dengan status approved approval form
#80
    And klik debtor name dengan status rejected approval form
#81
    And klik debtor name dengan status waiting approval tidak mengisi keputusan dan keterangan klik submit
#82
    And klik debtor name dengan status waiting approval mengisi keputusan reject tidak mengisi keterangan klik submit
#83
    And klik debtor name dengan status waiting approval mengisi keputusan approve tidak mengisi keterangan klik submit
#84
    And klik debtor name dengan status waiting approval tidak mengisi keputusan mengisi keterangan klik submit
#85-86
    Then klik debtor name dengan status waiting approval mengisi keputusan approve mengisi keterangan
      | 5000karakter klik submit |
      | 4999karakter klik submit |
#87-88
    And verifikasi total
      | nominal approve                             |
      | select all verifikasi total nominal approve |
#89-90-91
    And klik bulk recommendation klik check pada beberapa data waiting review
      | verifikasi total nominal approve                                         |
      | uncheck pada 2 data yang sudah di check verifikasi total nominal approve |
      | cancel selection verifikasi total nominal approve                        |
#92-93-94
    And verifikasi klik bulk approve
      | select all verifikasi total nominal approve                                                           |
      | check pada beberapa data waiting approval verifikasi total nominal approve                            |
      | klik check pada beberapa data waiting approval klik cancel selection verifikasi total nominal approve |
#95
    And klik bulk approve check beberapa data waiting approval klik cancel verifikasi total nominal approve

#96-97
    And klik bulk approve klik check all klik
      | reject  |
      | approve |


  @realizationForm
  Scenario: Realisai Form Individu
#1
    When user akses dan verifikasi tampilan list Realization Form
#2
    Then verifikasi data yang ada di list realization form
#3
    And verifikasi filter dropdown source
#4-5
    And pilih source
#6
    When verifikasi filter dropdown status
#7-10
    Then pilih filter status
      | all              |
      | waiting approval |
      | approved         |
      | rejected         |
#11
    And verifikasi filter dropdown partner
#12-13
    Then pilih filter partner
#14
    When verifikasi fungsi search
#15-17
    And klik paging
#18-22
    Then sorting kolom
#23-29
    And verifikasi data realization form
      | source         |
      | application no |
      | debtor name    |
      | partner        |
      | date           |
      | loan amount    |
      | rate           |
#30-86
    Then verifikasi data realization form detail
      | status                       |
      | nomor aplikasi               |
      | nama debitur                 |
      | jenis debitur                |
      | gender code                  |
      | nik                          |
      | npwp                         |
      | alamat berdasarkan identitas |
      | kelurahan                    |
      | kecamatan                    |
      | kode dati II                 |
      | province                     |
      | kode pos                     |
      | kewarganegaraan debitur      |
      | no telpon                    |
      | mobile phone number          |
      | email                        |
      | place of birth               |
      | date of birth                |
      | last education code          |
      | employer                     |
      | employer address             |
      | plafon                       |
      | interest rate                |
      | nomor PK                     |
      | tanggal akad                 |
      | tanggal angsuran 1           |
      | jenis penggunaan             |
      | sektor ekonomi               |
      | omzet                        |
      | go public                    |
      | sandi golongan debitur       |
      | penghasilan kotor per tahun  |
      | bentuk badan usaha           |
      | tempat berdiri badan usaha   |
      | original loant amount        |
      | disbursment date             |
      | tenor                        |
      | segmentasi debitur           |
      | kode pekerjaan               |
      | mother maiden name           |
      | religion                     |
      | penghasilan kotor per bulan  |
      | marital status               |
      | jumlah tanggungan            |
      | nama pasangan                |
      | tanggal lahir pasangan       |
      | perjanjian pisah harta       |
      | no akte                      |
      | tanggal berdiri              |
      | no akte terahir              |
      | tanggal akte tarahir         |
      | bidang usaha                 |
      | jangka waktu                 |
      | jenis kredit                 |
      | debtor category              |
      | income source                |
#87
    And klik bulk approve and klik check pada beberapa data status waiting approval
#88
    And klik bulk approve and klik uncheck pada beberapa data status waiting approval
#89
    Then klik button back cancel selection
#90
    Then klik bulk approve and klik check all
#91-94
    When klik bulk approve then verifikasi data status
      | approved         |
      | rejected         |
      | pending          |
      | waiting approval |
#95
    And klik bulk approve klik check pada beberapa data status waiting approval klik approve
#96
    And klik bulk approve klik check all klik approve
#97
    And klik debtor name dengan status waiting approval
#98
    And klik debtor name dengan status approved
#99
    And klik debtor name dengan status rejected
#100
    When klik debtor name dengan status waiting approval klik approve popup
#101
    Then klik debtor name dengan status waiting approval Klik approve klik tidak
#102
    And klik debtor name dengan status waiting approval Klik approve klik ya
#103
    And klik debtor name dengan status waiting approval klik reject
#104-106
    When klik debtor name dengan status Waiting approval klik reject pilih type reject reason dokumen pendukung tidak sesuai description diisi karakter
#107
    Then verifikasi Notifikasi setelah file masuk
#108
    Then verifikasi Total Nominal Approve
#109-112
    When klik bulk approve verifikasi total nominal approve
#113
    And consume reafile dengan no aplikasi yang sudah ada di LOS dan terdapat field lain yang beda mis alamat status waiting approval
#114-116
    And verifikasi data realization expired
#117
    And consume reafile dengan no aplikasi yang sudah ada di LOS dan terdapat field lain yang beda mis alamat status approval Expired
#118
    And consume reafile dengan no aplikasi yang sudah ada di LOS dan terdapat field lain yang beda mis alamat status rejected
##119
    And consume reafile dengan no aplikasi yang sudah ada di LOS dan terdapat field lain yang beda mis alamat status pending batch run
##120
    And consume reafile dengan no aplikasi yang sudah ada di LOS dan terdapat field lain yang beda mis alamat status approved
#121-123
    And verifikasi data realization pending
#124
    And consume reafile dengan no aplikasi yang sudah ada di LOS dan terdapat field lain yang beda mis alamat status pending

  @validationApprovalFile
  Scenario: Validation Approval File Individu
    Given user akses LOS application
    When input no aplikasi 17karakter varchar
    And input NIK 16karakter numerik
    And input NPWP
    And input jenis debitur
    And input nama debitur
    And input jangka waktu kredit
    And input interest rate
    And input plafon
    And input phone number
    And input remarks
    And input 2data approval NIK sama di 1partner
    And input 2data approval NIK sama di 2partner
    Then verifikasi notif setelah proses file approval

  @validationRealizationFile
  Scenario: Validation Realization File Individu
    Given user akses realisasi LOS application
    When input realisasi no aplikasi 17karakter varchar
    And input realisasi nama debitur
    And input jenis debitur P
    And input gender code
    And input realisasi NIK 16karakter numerik
    And input realsiasi NPWP
    And input alamat
    And input alamat kelurahan
    And input alamat kecamatan
    And input alamat kode pos
    And input alamat kode dati II
    And input provinsi
    And input no telepon
    And input mobile phone number
    And input email
    And input place of birth
    And input tanggal lahir debitur
    And input last education
    And input employer
    And input employer address
    And input mother maiden
    And input religion satu karakter numerik
    And input penghasilan kotor per bulan
    And input marital status
    And input nama pasangan
    And input tanggal lahir pasangan
    And input perjanjian pisah harta
    And input no akte
    And input tanggal berdiri
    And input no akte terahir
    And input tanggal akte terahir
    And input bidang usaha
    And input jangka waktu
    And input jenis kredit
    And input realisasi plafon
    And input realisasi interest rate
    And input no pk
    And input tanggal akad
    And input tanggal angsuran pertama
    And input jenis penggunaan
    And input sektor ekonomi
    And input omzet
    And input go public
    And input sandi golongan
    And input penghasilan kotor per tahun
    And input bentuk badan usaha
    And input tempat berdiri badan usaha
    And input disbursement date
    And input tenor
    And input segmentasi debitur
    And input kode pekerjaan
    And input debtor category
    And input income source
    Then input jumlah tanggungan