package com.extentreports;

import com.aventstack.extentreports.ExtentReports;
import com.aventstack.extentreports.reporter.ExtentSparkReporter;
import com.aventstack.extentreports.reporter.configuration.Theme;
import com.aventstack.extentreports.reporter.configuration.ViewName;
import elementsPage.Const;
import testData.ReadTestData;

import java.text.SimpleDateFormat;
import java.util.Date;

public class ExtentReportsFile {
    static ReadTestData readTestData = new ReadTestData();
    static ExtentReports extent;
    public static String folderReport;

    public static ExtentReports report() {
        if (extent == null) {
            readTestData.testData();
            //date now
            SimpleDateFormat formatDate = new SimpleDateFormat(Const.pPattern);
            String fodlerTagName = formatDate.format(new Date());
            folderReport = Const.pthFileImage + readTestData.tagName + readTestData.companyName + fodlerTagName;

            ExtentSparkReporter sparkReporter = new ExtentSparkReporter(folderReport + "//chanelling_individu_" + readTestData.tagName + ".html")
                    .viewConfigurer()
                    .viewOrder()
                    .as(new ViewName[]{
                            ViewName.DASHBOARD,
                            ViewName.TEST,
                            ViewName.AUTHOR,
                            ViewName.DEVICE,
                            ViewName.EXCEPTION,
                            ViewName.LOG

                    })
                    .apply();
            sparkReporter.config().enableOfflineMode(true);
            sparkReporter.config().setTheme(Theme.DARK);
            sparkReporter.config().setDocumentTitle("INDIVIDU - " + readTestData.companyName);
            sparkReporter.config().setReportName("Extent Report Chanelling");

            extent = new ExtentReports();
            extent.attachReporter(sparkReporter);
        }
        return extent;
    }
}
