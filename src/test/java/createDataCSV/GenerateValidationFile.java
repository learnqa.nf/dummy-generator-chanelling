package createDataCSV;

import randomValue.GenerateRandomDataApp;
import testData.ReadTestData;
import writeFile.WriteTestData;

import java.io.IOException;
import java.util.List;

public class GenerateValidationFile {
    static GenerateRandomDataApp generateRandomDataApp = new GenerateRandomDataApp();
    static ReadTestData readTestData = new ReadTestData();
    static WriteTestData writeTestData = new WriteTestData();
    static String nik;
    static String nikDeferentPartner;

    public static void dataAPP(int irow, List<String[]> dataApp, String nama_debitur, String code_company, String name_company, String kode_produk, String genderCode, String remark, String keyword) throws IOException {
        readTestData.testData();

        switch (irow) {
            case 1:
                dataApp.add(new String[]{code_company + generateRandomDataApp.nomorAplikasi(name_company, keyword), generateRandomDataApp.nik(genderCode), ""
                        , generateRandomDataApp.jenisDebitur(name_company), nama_debitur + " " + generateRandomDataApp.namaDebitur() + "MJIOPLKMNBVCXMNJKLOPUHNJKIOLMNJKIP60", "012"
                        , "00800", "000004999999900", "62834689591634567820", "40Karakter40Karakter40Karakter40Karakter"});
                break;
            case 2:
                dataApp.add(new String[]{code_company + generateRandomDataApp.nomorAplikasi(name_company, keyword), generateRandomDataApp.nik(genderCode), generateRandomDataApp.npwp()
                        , generateRandomDataApp.jenisDebitur(name_company), nama_debitur + " " + generateRandomDataApp.namaDebitur() + "MJIOPLKMNBVCXMNJKLOPUHNJKIOLMNJKI59", "060"
                        , "00975", "000005000000000", "6283468959163456719", ""});
                break;
            case 3:
                String noApplication = code_company + generateRandomDataApp.nomorAplikasi(name_company, keyword);
                nikDeferentPartner = generateRandomDataApp.nik(genderCode);

                dataApp.add(new String[]{noApplication, nikDeferentPartner, generateRandomDataApp.npwp()
                        , generateRandomDataApp.jenisDebitur(name_company), nama_debitur + " " + generateRandomDataApp.namaDebitur(), "120"
                        , "01100", "000010000000000", "628" + generateRandomDataApp.phoneNumber(), remark});

                writeTestData.writeData(noApplication, 8);
                writeTestData.writeData(nikDeferentPartner, 9);
                break;
            case 4:
                nik = generateRandomDataApp.nik(genderCode);
                dataApp.add(new String[]{code_company + generateRandomDataApp.nomorAplikasi(name_company, keyword), nik, generateRandomDataApp.npwp()
                        , generateRandomDataApp.jenisDebitur(name_company), nama_debitur + " " + generateRandomDataApp.namaDebitur(), generateRandomDataApp.jangkaWaktu(code_company)
                        , generateRandomDataApp.interestRate(code_company, kode_produk), "000005900000000", "628" + generateRandomDataApp.phoneNumber(), remark});
                break;
            case 5:
                dataApp.add(new String[]{code_company + generateRandomDataApp.nomorAplikasi(name_company, keyword), nik, generateRandomDataApp.npwp()
                        , generateRandomDataApp.jenisDebitur(name_company), nama_debitur + " " + generateRandomDataApp.namaDebitur(), generateRandomDataApp.jangkaWaktu(code_company)
                        , generateRandomDataApp.interestRate(code_company, kode_produk), "000005300000000", "628" + generateRandomDataApp.phoneNumber(), remark});
                break;
            case 6:
                dataApp.add(new String[]{code_company + generateRandomDataApp.nomorAplikasi(name_company, keyword), generateRandomDataApp.nik(genderCode), ""
                        , generateRandomDataApp.jenisDebitur(name_company), nama_debitur + " " + generateRandomDataApp.namaDebitur(), generateRandomDataApp.jangkaWaktu(code_company)
                        , generateRandomDataApp.interestRate(code_company, kode_produk), "000000100000000", "628" + generateRandomDataApp.phoneNumber(), remark});
                break;
            case 7:
                dataApp.add(new String[]{code_company + generateRandomDataApp.nomorAplikasi(name_company, keyword), generateRandomDataApp.nik("L"), generateRandomDataApp.npwp()
                        , generateRandomDataApp.jenisDebitur(name_company), nama_debitur + " " + generateRandomDataApp.namaDebitur(), generateRandomDataApp.jangkaWaktu(code_company)
                        , generateRandomDataApp.interestRate(code_company, kode_produk), generateRandomDataApp.plafonKredit(code_company, kode_produk), "628" + generateRandomDataApp.phoneNumber(), remark});
                break;
            case 8:
                dataApp.add(new String[]{code_company + generateRandomDataApp.nomorAplikasi(name_company, keyword), generateRandomDataApp.nik("P"), generateRandomDataApp.npwp()
                        , generateRandomDataApp.jenisDebitur(name_company), nama_debitur + " " + generateRandomDataApp.namaDebitur(), generateRandomDataApp.jangkaWaktu(code_company)
                        , generateRandomDataApp.interestRate(code_company, kode_produk), generateRandomDataApp.plafonKredit(code_company, kode_produk), "628" + generateRandomDataApp.phoneNumber(), remark});
                break;
            case 9:
            case 10:
            case 11:
                dataApp.add(new String[]{code_company + generateRandomDataApp.nomorAplikasi(name_company, keyword), generateRandomDataApp.nik(genderCode), generateRandomDataApp.npwp()
                        , generateRandomDataApp.jenisDebitur(name_company), nama_debitur + " " + generateRandomDataApp.namaDebitur() + "MJIOPLKMNBVCXMNJKLOPUHNJKIOLMNJKI59", generateRandomDataApp.jangkaWaktu(code_company)
                        , generateRandomDataApp.interestRate(code_company, kode_produk), generateRandomDataApp.plafonKredit(code_company, kode_produk), "628" + generateRandomDataApp.phoneNumber(), remark});
                break;
        }
    }

    public static void dataAPPDifferentPartner(int irow, List<String[]> dataApp, String nama_debitur, String code_company, String name_company, String kode_produk, String genderCode, String remark, String keyword) throws IOException {
        readTestData.testData();

        if (irow == 1) {
            dataApp.add(new String[]{code_company + generateRandomDataApp.nomorAplikasi(name_company, keyword), readTestData.nik, generateRandomDataApp.npwp()
                    , generateRandomDataApp.jenisDebitur(name_company), nama_debitur + " " + generateRandomDataApp.namaDebitur() + "MJIOPLKMNBVCXSAD", generateRandomDataApp.jangkaWaktu(code_company)
                    , generateRandomDataApp.interestRate(code_company, kode_produk), "000001200000000", "628" + generateRandomDataApp.phoneNumber(), remark});
        }
    }

    public static void main(String[] args) throws IOException {
        DataValidationCSVFile dataValidationCSVFile =new DataValidationCSVFile();
        dataValidationCSVFile.dataDummy("APPFILEIDV", "REAFILEIDV", "10");

//        String a = "3273254303954714";
//        System.out.println(a.charAt(6));
    }
}
