package createDataCSV;

import elementsPage.Const;
import randomValue.GenerateRandomDataApp;
import randomValue.GenerateRandomDataPengurus;
import randomValue.GenerateRandomDataRea;
import testData.ReadTestData;
import com.opencsv.CSVWriter;
import formatFile.CSVFileName;
import writeFile.UtilsFile;
import writeFile.WriteTestData;

import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.*;

public class DataValidationCSVFile {
    private String pthResultDataDummyFile = Const.resultDataDummyFile;
    ArrayList<String[]> arrayData = new ArrayList<>();
    String jw;
    String keyword;

    public void dataDummy(String case1, String case2, String input_jumlah_data) throws IOException {

        String nama_debitur,
                name_company,
                code_company,
                kode_produk,
                tanggal,
                jumlahData;

        File fileApp;
        File fileRea;
        String resultDataDummyFile = Const.resultDataDummyFile;
        // call class generate random data
        GenerateRandomDataApp generateRandomDataApp = new GenerateRandomDataApp();
        UtilsFile utilsFile = new UtilsFile();
        ReadTestData readTestData = new ReadTestData();

        readTestData.testData();

        //date now
        String pattern = Const.glPattern;
        SimpleDateFormat simpleDateFormat = new SimpleDateFormat(pattern);
        String fdate = simpleDateFormat.format(new Date());

        String pattern2 = "HHmmss";
        SimpleDateFormat simpleDateFormat2 = new SimpleDateFormat(pattern2);
        keyword = simpleDateFormat2.format(new Date());

        String environment = String.valueOf(readTestData.cellDataValue[0]);
        nama_debitur = String.valueOf(readTestData.cellDataValue[1]);
        name_company = String.valueOf(readTestData.cellDataValue[2]);
        code_company = String.valueOf(readTestData.cellDataValue[3]);
        kode_produk = String.valueOf(readTestData.cellDataValue[4]);
//        jumlahData = String.valueOf(readTestData.cellDataValue[5]);
        jumlahData = input_jumlah_data;
        tanggal = fdate;

        final boolean b = name_company.equals(Const.typ_NikelICL) || name_company.equals(Const.typ_NikelIPL) || name_company.equals(Const.typ_NikelMora);
        // first create file object for file placed at location
        // specified by filepath
        if (b) {
            fileApp = new File(resultDataDummyFile + case1.substring(0, 7) + Const.extCSV);
            fileRea = new File(resultDataDummyFile + case2.substring(0, 7) + Const.extCSV);
        } else {
            fileApp = new File(resultDataDummyFile + case1 + Const.extCSV);
            fileRea = new File(resultDataDummyFile + case2 + Const.extCSV);
        }

        try {
            // create FileWriter object with file as parameter
            FileWriter outputfileApp = new FileWriter(fileApp);
            FileWriter outputfileRea = new FileWriter(fileRea);

            // create CSVWriter with '|' as separator
            CSVWriter writerApp = new CSVWriter(outputfileApp, '|',
                    CSVWriter.NO_QUOTE_CHARACTER,
                    CSVWriter.DEFAULT_ESCAPE_CHARACTER,
                    CSVWriter.DEFAULT_LINE_END);
            CSVWriter writerRea = new CSVWriter(outputfileRea, '|',
                    CSVWriter.NO_QUOTE_CHARACTER,
                    CSVWriter.DEFAULT_ESCAPE_CHARACTER,
                    CSVWriter.DEFAULT_LINE_END);

            // create a List which contains String array
            ArrayList<String[]> dataApp = new ArrayList<>();
            dataApp.add(new String[]{"Nomor_Aplikasi", "NIK", "NPWP", "Jenis_Debitur", "Nama_Debitur", "Jangka_Waktu_Kredit", "Interest_Rate", "Plafon_Kredit", "Phone_Number", "Remark"});

            List<String[]> dataRea = new ArrayList<>();
            dataRea.add(new String[]{"Nomor Aplikasi", "Nama Debitur", "Jenis Debitur", "Gender Code", "NIK ", "Nomor NPWP", "Alamat", "Alamat_Kelurahan",
                    "Alamat_Kecamatan", "Alamat_KodePos", "Alamat_KodeDATI II", "Province", "No Telepon ", "Mobile Phone Number", "Email", "Place Of Birth",
                    "Tanggal lahir debitur", "Last Education Code", "Employer", "Employer Address", "Mother Maiden Name", "Religion", "Penghasilan Kotor per Bulan",
                    "Marital Status", "Nama Pasangan", "Tanggal Lahir Pasangan", "Perjanjian Pisah Harta", "No Akte", "Tanggal Berdiri", "No Akte Terakhir",
                    "Tanggal Akte Terakhir", "Bidang Usaha", "Jangka Waktu", "Jenis Kredit", "Plafon", "Interest Rate", "Nomor PK", "Tanggal Akad",
                    "Tanggal Angsuran I", "Jenis Penggunaan", "Sektor Ekonomi", "Omzet", "Go Public", "Sandi Golongan Debitur", "Penghasilan Kotor per Tahun",
                    "Bentuk Badan Usaha", "Tempat berdiri badan usaha", "Original Loan Amount", "Disbursement Date", "Tenor", "Segmentasi Debitur", "Pekerjaan",
                    "Debtor Category", "Income Source", "Jumlah_Tanggungan"});


            String[] dataPengurusHeader = {"Nomor_Aplikasi", "Nomor_Urut_Pengurus", "Jumlah_Pengrus", "Sandi_Jabatan_BI",
                    "Pangsa_Kepemilikan", "Bentuk_Pengurus", "Modal_Dasar", "Modal_Disetor", "Modal_Ditempatkan", "NPWP_Pengurus",
                    "Nama_Pengurus", "Alamat_Pengurus", "Alamat_Kelurahan", "Alamat_Kecamatan", "Alamat_Dati_11", "No_KTP", "No_Akte",
                    "Tanggal_Lahir", "Tanggal_Akte", "Dati_11_Tempat_Lahir", "Jenis_Kelamin"};

            GenerateRandomDataRea generateRandomDataRea = new GenerateRandomDataRea();
            // CONDITIONAL NIKEL
            if (b) {
                // write data csv approval
                for (int idxApp = 1; idxApp <= Integer.parseInt(jumlahData); idxApp++) {
                    dataApp.add(new String[]{code_company + generateRandomDataApp.nomorAplikasi(name_company, keyword), "", generateRandomDataApp.npwp()
                            , generateRandomDataApp.jenisDebitur(name_company), "PT " + nama_debitur + " " + generateRandomDataApp.namaDebitur(), generateRandomDataApp.jangkaWaktu(code_company)
                            , generateRandomDataApp.interestRate(code_company, kode_produk), generateRandomDataApp.plafonKredit(code_company, kode_produk), "628" + generateRandomDataApp.phoneNumber(), "Tanggal Akad: " + fdate});
                }
                // write data csv realisasi
                arrayData = dataApp;

                int ix = 1;
                for (String[] reaData : arrayData) {
                    String ft = String.valueOf(System.nanoTime());
                    String genderCode = generateRandomDataRea.genderCode();
                    if (reaData[0] != "Nomor_Aplikasi" && reaData[1] != "NIK" && reaData[2] != "NPWP" && reaData[3] != "Jenis_Debitur" && reaData[4] != "Nama_Debitur"
                            && reaData[5] != "Jangka_Waktu" && reaData[6] != "Interest_Rate" && reaData[7] != "Plafon_Kredit" && reaData[8] != "Phone_Number" && reaData[9] != "Remark") {

                        if (name_company.equals(Const.typ_NikelMora)) {
                            dataRea.add(new String[]{reaData[0], reaData[4], reaData[3], "", reaData[1], reaData[2], "Jln Bougenvile Raya " + generateRandomDataApp.rdmAlamat(), "Kelurahan Kemuning " + generateRandomDataApp.rdmAlamat(),
                                    "Kecamatan Amarilis " + generateRandomDataApp.rdmAlamat(), "40614", "0191", "Jawa Barat", generateRandomDataRea.noTelepon(code_company), "628" + generateRandomDataRea.mobilePhoneNumber(), nama_debitur + generateRandomDataApp.rdmAlamat() + "@gmail.com", "",
                                    "", "", "", "", "", "", "",
                                    "", "", "", "", "NOAKTE" + ft, "17081980", "NOAKTE" + ft, "17081980", generateRandomDataRea.bidangUsaha() + "00000", reaData[5], "P0" + generateRandomDataRea.jenisKredit(), reaData[7], reaData[6], "PK0" + generateRandomDataRea.noPK(), fdate,
                                    generateRandomDataRea.angsuranPertama(code_company, reaData[5]), generateRandomDataRea.jenisPenggunaan(), generateRandomDataRea.sektorEkonomi(environment, name_company), "0000001" + generateRandomDataRea.omzet() + "000000000", "1", "S14", "",
                                    "99", "Bumi Putra " + generateRandomDataApp.rdmAlamat(), reaData[7], fdate, reaData[5], "0" + generateRandomDataRea.segmentasiDebitur(), "",
                                    "UM", "", ""});

                            System.out.println("Nama Debitur                : " + reaData[4]);
                        } else {
                            dataRea.add(new String[]{reaData[0], reaData[4].replace("PT ", ""), generateRandomDataApp.jenisDebitur(name_company), genderCode, generateRandomDataApp.nik(genderCode), reaData[2], "Jln Bougenvile Raya " + generateRandomDataApp.rdmAlamat(), "Kelurahan Kemuning " + generateRandomDataApp.rdmAlamat(),
                                    "Kecamatan Amarilis " + generateRandomDataApp.rdmAlamat(), "40614", "0191", "Jawa Barat", generateRandomDataRea.noTelepon(code_company), "628" + generateRandomDataRea.mobilePhoneNumber(), nama_debitur + generateRandomDataApp.rdmAlamat() + "@gmail.com", "Bandung",
                                    "17081995", "0" + generateRandomDataRea.lastEducation(), "Company " + generateRandomDataRea.employer(), "Jln Asoka " + generateRandomDataApp.rdmAlamat(), "Maharani " + generateRandomDataRea.motherMaiden(), generateRandomDataRea.religion(), generateRandomDataRea.penghasilanKotor(name_company),
                                    "1", "Dharma putra " + generateRandomDataRea.namaPasangan(), "17081990", "", "", "", "", "", "", reaData[5], "P0" + generateRandomDataRea.jenisKredit(), reaData[7], reaData[6], "PK0" + generateRandomDataRea.noPK(), fdate,
                                    generateRandomDataRea.angsuranPertama(code_company, reaData[5]), generateRandomDataRea.jenisPenggunaan(), generateRandomDataRea.sektorEkonomi(readTestData.environment, name_company), "", "", "S14", generateRandomDataRea.penghasilanKotorPertahun(name_company),
                                    "", "", reaData[7], fdate, reaData[5], "0" + generateRandomDataRea.segmentasiDebitur(), "00" + generateRandomDataRea.kodePekerjaan(),
                                    "UM", generateRandomDataRea.incomeSource(), generateRandomDataRea.jumlahTanggungan()});

                            System.out.println("Nama Debitur                : " + reaData[4].replace("PT ", ""));
                        }


                        System.out.println("Plafon Kredit               : " + reaData[7]);
                        System.out.println("Interest Rate               : " + generateRandomDataApp.interestRate(code_company, kode_produk));
                        System.out.println("Jangka Waktu                : " + generateRandomDataApp.jangkaWaktu(code_company));
                        System.out.println("Angsuran Pertama            : " + generateRandomDataRea.angsuranPertama(code_company, generateRandomDataApp.jangkaWaktu(code_company)));

                        System.out
                                .println("========================================================");

                    }
                    ix++;
                }
                if (name_company.equals(Const.typ_NikelMora)) {
                    //pengurus nikel
                    GenerateRandomDataPengurus generateRandomDataPengurus = new GenerateRandomDataPengurus();
                    //write data csv pengurus
                    for (String[] pengurusData : dataRea) {
                        if (!pengurusData[0].equals("Nomor Aplikasi")) {
                            try {
                                File file = new File(pthResultDataDummyFile + "PENGURUS_" + code_company + "_" + kode_produk + "_" + pengurusData[0] + "_" + tanggal + Const.extCSV);
                                // create FileWriter object with file as parameter
                                FileWriter outputfile = new FileWriter(file);

                                // create CSVWriter object filewriter object as parameter
                                CSVWriter writer = new CSVWriter(outputfile, '|', CSVWriter.NO_QUOTE_CHARACTER, CSVWriter.DEFAULT_ESCAPE_CHARACTER, CSVWriter.DEFAULT_LINE_END);

                                // adding header to csv
                                writer.writeNext(dataPengurusHeader);

                                // add data to csv
                                String[] dataPengurus = {pengurusData[0], "0" + generateRandomDataPengurus.nomorUrutPengurus(), "0" + generateRandomDataPengurus.jumlahPengurus(),
                                        generateRandomDataPengurus.sandiJabatanBI(), generateRandomDataPengurus.pangsaKepemilikan() + "0000",
                                        generateRandomDataPengurus.bentukPengurus(), generateRandomDataPengurus.modalDasar(), generateRandomDataPengurus.modalDisetor(),
                                        generateRandomDataPengurus.modalDitempatkan(), generateRandomDataPengurus.npwpPengurus(),
                                        "Winter " + generateRandomDataPengurus.namaPengurus(), "Jalan Sumenep " + generateRandomDataPengurus.alamatPengurus(),
                                        "Jalan Bahyangs " + generateRandomDataPengurus.alamatKelurahan(), "Jalan Bougenvile " + generateRandomDataPengurus.alamatKecamatan(),
                                        "011" + generateRandomDataPengurus.alamatDatiII(), generateRandomDataPengurus.noKTP(code_company),
                                        "000000000000000000000000000" + generateRandomDataPengurus.noAkte(), "06062001", "06062001", "011" + generateRandomDataPengurus.DatiIITempatLahr(),
                                        generateRandomDataPengurus.jenisKelamin()};

                                writer.writeNext(dataPengurus);
                                // closing writer connection
                                writer.close();
                            } catch (IOException e) {
                                e.printStackTrace();
                            }
                        }
                    }
                }

            } else {

                String genderCode;
                String oRide = null;

                // write data csv approval file
                String remark;

                for (int idxApp = 1; idxApp <= Integer.parseInt(jumlahData); idxApp++) {
                    genderCode = generateRandomDataRea.genderCode();
                    if (name_company.contains("INDODANA")) {
                        remark = GenerateRandomDataApp.rndmNumber();
                    } else {// kopnus
                        remark = "Tanggal Akad: " + tanggal + "-" + System.nanoTime() + " Auto Generate";
                    }
                    GenerateValidationFile.dataAPP(idxApp, dataApp, nama_debitur, code_company, name_company, kode_produk, genderCode, remark, keyword);
                    oRide = oRide + "|" + genderCode + "|";
                }

                // write data csv realisasi
                arrayData = dataApp;

                int ix = 0;
                int iGenderCd = 1;
                String genderCodeNew = null;
                String cd_datiII, alamat, no_akte, tanggal_berdiri, no_akte_terahir, tanggal_akte_terahir, no_pk, omzet, go_public, bentuk_badan_usaha, tempat_berdiri_badan_usaha, disbursement_date, jumlah_tanggungan;
                String alamat_kelurahan, employer, employer_address, mother_maiden, marital_status, nama_pasangan, tanggal_lahir_pasangan, perjanjian_pisah_harta;
                String alamat_kecamatan, province, no_telepon, mobile_phone_number, email, place_of_birth, tanggal_lahir_debitur, last_education_code;
                String[] temp = oRide.split("\\|");
                for (String[] reaData : arrayData) {
                    if (reaData[0] != "Nomor_Aplikasi" && reaData[1] != "NIK" && reaData[2] != "NPWP" && reaData[3] != "Jenis_Debitur" && reaData[4] != "Nama_Debitur"
                            && reaData[5] != "Jangka_Waktu" && reaData[6] != "Interest_Rate" && reaData[7] != "Plafon_Kredit" && reaData[8] != "Phone_Number" && reaData[9] != "Remark") {

                        genderCodeNew = temp[iGenderCd];
                        switch (iGenderCd) {
                            case 11:
                                genderCodeNew = "L";
                                break;
                            case 13:
                                genderCodeNew = "P";
                                break;
                        }

                        String nik = reaData[1];
                        cd_datiII = generateRandomDataRea.datiIICode();
                        alamat = "Jln Bougenvile Raya " + generateRandomDataApp.rdmAlamat();
                        alamat_kelurahan = "Kelurahan Kemuning " + generateRandomDataApp.rdmAlamat();
                        alamat_kecamatan = "Kecamatan Amarilis " + generateRandomDataApp.rdmAlamat();
                        province = "Jawa Barat " + generateRandomDataApp.rdmAlamat();
                        no_telepon = "08" + generateRandomDataRea.noTelepon(code_company);
                        mobile_phone_number = "628" + generateRandomDataRea.mobilePhoneNumber();
                        email = nama_debitur + generateRandomDataApp.rdmAlamat() + "@gmail.com";
                        place_of_birth = "Bandung" + generateRandomDataApp.rdmAlamat();
                        tanggal_lahir_debitur = "17081995";
                        last_education_code = "0" + generateRandomDataRea.lastEducation();
                        employer = "Company " + generateRandomDataRea.employer();
                        employer_address = "Jln Asoka " + generateRandomDataApp.rdmAlamat();
                        mother_maiden = "Maharani " + generateRandomDataRea.motherMaiden();
                        marital_status = generateRandomDataRea.maritalStatusCode();
                        nama_pasangan = "Dharma putra " + generateRandomDataRea.namaPasangan();
                        tanggal_lahir_pasangan = "17081990";
                        perjanjian_pisah_harta = generateRandomDataRea.perjanjianPisahHarta();
                        no_akte = "";
                        tanggal_berdiri = "";
                        no_akte_terahir = "";
                        tanggal_akte_terahir = "";
                        no_pk = "PK0" + generateRandomDataRea.noPK();
                        omzet = "0000001" + generateRandomDataRea.omzet() + "000000000";
                        go_public = "1";
                        bentuk_badan_usaha = "99";
                        tempat_berdiri_badan_usaha = "Bumi Putra " + generateRandomDataApp.rdmAlamat();
                        disbursement_date = fdate;
                        jumlah_tanggungan = generateRandomDataRea.jumlahTanggungan();
                        switch (ix) {
                            case 1:
                                alamat = "Jln Bougenvile Raya " + generateRandomDataApp.rdmAlamat() + "MKLNJHBGVFCDXZSAFRTY50" + generateRandomDataApp.rdmAlamat();
                                alamat_kelurahan = "Kelurahan Kemuning " + "MKLNJHBGZSAFRTY40" + generateRandomDataApp.rdmAlamat();
                                alamat_kecamatan = "Kecamatan Amarilis " + "MKLNJHBGZSAFRTY40" + generateRandomDataApp.rdmAlamat();
                                province = "Jawa Barat " + "MKLNJHBG25" + generateRandomDataApp.rdmAlamat();
                                no_telepon = "08" + generateRandomDataRea.noTelepon(code_company);
                                mobile_phone_number = "628" + generateRandomDataRea.mobilePhoneNumber() + "1234520";
                                tanggal_lahir_debitur = "17081995";
                                last_education_code = "0" + generateRandomDataRea.lastEducation();

                                break;
                            case 4:
                            case 5:
                                if (nik.charAt(6) == '4') {
                                    genderCodeNew = "P";
                                } else {
                                    genderCodeNew = "L";
                                }
                                break;
                            case 6:
                                break;
                            case 7:
                                alamat = "Jln Bougenvile Raya " + generateRandomDataApp.rdmAlamat() + "MKLNJHBGVFCDXZSAFRT49" + generateRandomDataApp.rdmAlamat();
                                alamat_kelurahan = "Kelurahan Kemuning " + "MKLNJHBGZSAFRT39" + generateRandomDataApp.rdmAlamat();
                                alamat_kecamatan = "Kecamatan Amarilis " + "MKLNJHBGZSAFRT39" + generateRandomDataApp.rdmAlamat();
                                province = "Jawa Barat " + "MKLNJHB24" + generateRandomDataApp.rdmAlamat();
                                no_telepon = "0" + generateRandomDataRea.noTelepon(code_company);
                                mobile_phone_number = "628" + generateRandomDataRea.mobilePhoneNumber() + "123419";
                                last_education_code = "";

                                employer_address = "Jln Asoka " + "ASDFGHJKLMNBVCHDURYEHDFNVJFHSDJWIFUF199" + generateRandomDataApp.rndmText150();
                                marital_status = "2";
                                nama_pasangan = "";
                                tanggal_lahir_pasangan = "";
                                perjanjian_pisah_harta = "";
                                no_pk = "PK0" + generateRandomDataRea.noPK() + "678938742100" + generateRandomDataApp.rndmNumber80();
                                tempat_berdiri_badan_usaha = "Bumi Putra " + "KMNJHBGVFDSER30" + generateRandomDataApp.rdmAlamat();
                                jumlah_tanggungan = "";
                                bentuk_badan_usaha = "";
                                place_of_birth = "Bandung" + generateRandomDataApp.rdmAlamat() + "MKLWERTGHNMNHGFDNJHBGVFCDXZSAFRTY50" + generateRandomDataApp.rdmAlamat();
                                email = nama_debitur + generateRandomDataApp.rdmAlamat() + "PADFGHJNKLOWOPMNV50" + generateRandomDataApp.rdmAlamat() + "@gmail.com";
                                break;
                            case 8:
                                mother_maiden = "Maharani " + generateRandomDataApp.rdmAlamat() + "MKLNJHBGSWERTYUIOPVFCDXZSXAFRTYASDFGHJTYU60" + generateRandomDataApp.rdmAlamat();
                                no_pk = "PK0" + generateRandomDataRea.noPK() + "67893874299" + generateRandomDataApp.rndmNumber80();
                                tempat_berdiri_badan_usaha = "Bumi Putra " + "KMNJHBGVFDSE29" + generateRandomDataApp.rdmAlamat();
                                nama_pasangan = "Dharma putra " + generateRandomDataRea.namaPasangan() + "MKOPLNJHBGZSAFRTASXSDCDERFDSWERFGTRW59" + generateRandomDataRea.namaPasangan();
                                employer_address = "Jln Asoka " + "ASDFGHJKLMNBVCHDURYEHDFNVJFHSDJWIFUFY200" + generateRandomDataApp.rndmText150();
                                place_of_birth = "Bandung" + generateRandomDataApp.rdmAlamat() + "MKLWERTGHNMNHGFDNJHBGVFCDXZSAFRT49" + generateRandomDataApp.rdmAlamat();
                                email = nama_debitur + generateRandomDataApp.rdmAlamat() + "PADFGHJNKLOWOPMN49" + generateRandomDataApp.rdmAlamat() + "@gmail.com";
                                break;
                            case 9:
                                mother_maiden = "Maharani " + generateRandomDataApp.rdmAlamat() + "MKLNJHBGSWERTYUIOPVFCDXCFVBGNHYTRZSXAFRY59" + generateRandomDataApp.rdmAlamat();
                                no_akte = "";
                                tanggal_berdiri = "";
                                no_akte_terahir = "";
                                tanggal_akte_terahir = "";
                                tempat_berdiri_badan_usaha = "";
//                                disbursement_date = generateRandomDataRea.datePlus("1");
                                nama_pasangan = "Dharma putra " + generateRandomDataRea.namaPasangan() + "MKOPLNJHBGZSSWEDFRTGHTREDFGREWDVAFRTY60" + generateRandomDataRea.namaPasangan();
                                break;
                            case 10:
                                no_akte = "17-AKTA PERUSAHAAN02900000017";
                                tanggal_berdiri = "17081990";
                                no_akte_terahir = "17-AKTA PERUSAHAAN02900000017";
                                tanggal_akte_terahir = "17081990";
                                employer = "Company " + generateRandomDataRea.employer() + "ASDKMHJFBVNCMKSJDFKLSSDCVFRGHTYJMKNJBHVG60" + generateRandomDataRea.employer();
                                break;
                            case 11:
                                no_akte = "17-AKTA PERUSAHAAN030000000017";
                                tanggal_berdiri = "17081990";
                                no_akte_terahir = "17-AKTA PERUSAHAAN030000000017";
                                tanggal_akte_terahir = "17081990";
                                employer = "Company " + generateRandomDataRea.employer() + "ASDKMHJFBVNCMKSJDFFGRTHNGDSFKLSJMKNJBHV59" + generateRandomDataRea.employer();
                                omzet = "";
                                go_public = "";
                                break;

                        }
                        dataRea.add(new String[]{reaData[0], reaData[4], reaData[3], genderCodeNew, nik, reaData[2], alamat, alamat_kelurahan,
                                alamat_kecamatan, "40614", cd_datiII, province, no_telepon, mobile_phone_number, email, place_of_birth,
                                tanggal_lahir_debitur, last_education_code, employer, employer_address, mother_maiden, generateRandomDataRea.religion(), generateRandomDataRea.penghasilanKotor(name_company),
                                marital_status, nama_pasangan, tanggal_lahir_pasangan, perjanjian_pisah_harta, no_akte, tanggal_berdiri, no_akte_terahir, tanggal_akte_terahir, generateRandomDataRea.bidangUsaha() + "00000", reaData[5], "P0" + generateRandomDataRea.jenisKredit(), reaData[7], reaData[6], no_pk, fdate,
                                generateRandomDataRea.angsuranPertama(code_company, reaData[5]), generateRandomDataRea.jenisPenggunaan(), generateRandomDataRea.sektorEkonomi(environment, name_company), omzet, go_public, generateRandomDataRea.sandiGolongan(), generateRandomDataRea.penghasilanKotorPertahun(name_company),
                                bentuk_badan_usaha, tempat_berdiri_badan_usaha, reaData[7], disbursement_date, reaData[5], "0" + generateRandomDataRea.segmentasiDebitur(), "00" + generateRandomDataRea.kodePekerjaan(),
                                generateRandomDataRea.debtorCategory(), generateRandomDataRea.incomeSource(), jumlah_tanggungan});

                        iGenderCd = iGenderCd + 2;
                        System.out.println("Nama Debitur                : " + reaData[4]);
                        System.out.println("Plafon Kredit               : " + reaData[7]);

                        System.out
                                .println("========================================================");
                    }
                    ix++;
                }
            }
            WriteTestData writeTestData = new WriteTestData();
            writeTestData.writeData(keyword, 6);
            readTestData.testData();

            System.out.println("Short Name          : " + name_company);
            System.out.println("Jumlah Data         : " + jumlahData);
            System.out.println("Company Code        : " + code_company);
            System.out.println("Product Code        : " + kode_produk);
            System.out.println("Interest Rate       : " + generateRandomDataApp.interestRate(code_company, kode_produk));
            System.out.println("Jangka Waktu        : " + generateRandomDataApp.jangkaWaktu(code_company));
            System.out.println("Angsuran Pertama    : " + generateRandomDataRea.angsuranPertama(code_company, generateRandomDataApp.jangkaWaktu(code_company)));
            System.out.println("Keyword             : " + readTestData.timeStamp);
            System.out
                    .println("========================================================");

            writerApp.writeAll(dataApp);
            writerRea.writeAll(dataRea);
            // closing writer connection
            writerApp.close();
            writerRea.close();

        } catch (IOException e) {
            e.printStackTrace();
        }

        // call class format file
        CSVFileName csvFileName = new CSVFileName();
        csvFileName.parseData(case1, case2, code_company, kode_produk, tanggal, nama_debitur, name_company);

        if (b) {
            utilsFile.dltAppFile();
        }
        csvFileName.zipDataDummy(name_company, arrayData);
    }

    public static void validateApprovalFile() throws IOException {
//public static void main(String[] args) throws IOException {


        UtilsFile utilsFile = new UtilsFile();
        utilsFile.dltDummyExist();

        DataValidationCSVFile dataValidationCSVFile = new DataValidationCSVFile();
        dataValidationCSVFile.dataDummy("APPFILEIDV", "REAFILEIDV", "5");
    }
}


