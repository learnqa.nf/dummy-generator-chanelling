package individu;

import applos.ConsumeData;
import createDataCSV.DataCSV;
import elementsPage.Const;
import org.openqa.selenium.WebDriver;
import runner.FormApprovalRunner;
import writeFile.UtilsFile;

import java.io.File;
import java.io.IOException;

import static applos.ExecutionJob.jobAppExpired;
import static elementsPage.ApprovalFormApprover.loginApprover;
import static elementsPage.ApprovalFormMaker.loginMaker;
import static elementsPage.ApprovalFormMaker.logout;
import static elementsPage.Const.*;
import static individu.ChangesStatus.changedApproveAsApprover;
import static individu.ChangesStatus.changedRecomendationAsMaker;
import static writeFile.UtilsFile.unzip;

public class AdditionalProcess {

    private static final WebDriver driver = FormApprovalRunner.driver;
    static UtilsFile cleanFolder = new UtilsFile();
    static DataCSV dataCSV = new DataCSV();
    static ConsumeData consumeData = new ConsumeData();

    public static void provideDataExpiredApprovalForm() throws IOException {

        cleanFolder.dltDummyExist();
        dataCSV.dataDummy(Const.appFile, Const.reaFile, "2");
        consumeData.serverAkses();
        jobAppExpired();
    }

    public static void provideDataApprovalFormUserApprover() {
        try {

            UtilsFile utilsFile = new UtilsFile();
            utilsFile.dltDummyExist();

            DataCSV dataCSV = new DataCSV();
            dataCSV.dataDummy(Const.appFile, Const.reaFile, "20");

            ConsumeData connServer = new ConsumeData();
            connServer.serverAkses();
            logout(driver);
            loginMaker(driver);
            changedRecomendationAsMaker(driver, 15);
            loginApprover(driver, Const.e2e_user_ldap_approveRecomend, Const.e2e_pwd_ldap_approveRecomend);
            changedApproveAsApprover(driver, 5);
            loginApprover(driver, Const.e2e_user_ldap_approveRecomend, Const.e2e_pwd_ldap_approveRecomend);

        } catch (Exception e) {
          e.printStackTrace();
        }
    }
}
