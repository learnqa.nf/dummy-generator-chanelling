package individu;

import elementsPage.BaseAction;
import elementsPage.Const;
import org.openqa.selenium.Alert;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import readFile.ReadCSVFormRealization;
import testData.ReadTestData;

import java.io.IOException;

import static elementsPage.ApprovalFormMaker.*;

public class ChangesStatus extends BaseAction {

    public static int jumlahData;
    public static ReadTestData readTestData = new ReadTestData();
    public static String debtor_name;
    public static String coll_debtor_name = null;
    static ReadCSVFormRealization readCSVFormRealization = new ReadCSVFormRealization();

    public static void changedRecomendationAsMaker(WebDriver driver, int jml_changes) throws InterruptedException {
        jumlahData = jml_changes;

        readTestData.testData();
        try {
            Thread.sleep(Const.delay);
            click(driver, menuApproval);
            LibUtils.waitElementVisible(driver, btnBulkRecomend, 30);
            click(driver, btnBulkRecomend);
            Thread.sleep(3000);
            writeText(driver, txtSearch, readTestData.timeStamp);
            driver.findElement(bulk_statusForReview).click();
            LibUtils.waitElementVisible(driver, radioButtonFirst, Const.seconds);
            coll_debtor_name = null;
            for (int i = 0; i < jumlahData; i++) {
                int idx = i + 1;
                driver.findElement(By.xpath("//*[@id='channelingApprovalDataTable']/tbody/tr[" + idx + "]/td[1]/div/label")).click();
                if (i == 7) {
                    LibUtils.scrollIntoView(driver, By.xpath("//*[@id='channelingApprovalDataTable']/tbody/tr['" + i + 9 + "']/td[1]/div/label"));
                } else if (i == 12) {
                    LibUtils.scrollIntoView(driver, tableBulk_nextPage);
                }
                debtor_name = driver.findElement(By.xpath("//*[@id='channelingApprovalDataTable']/tbody/tr[" + idx + "]/td[3]/a")).getText();
                coll_debtor_name = coll_debtor_name + "|" + debtor_name;
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        LibUtils.scrollIntoView(driver, recomended);
        click(driver, recomended);
        Thread.sleep(7000);
//        LibUtils.waitElementVisible(driver, bulkFilter_rwDatafirst1, Const.seconds);
        click(driver, btnProfile);
        Thread.sleep(3000);
        click(driver, btnLogout);
        Thread.sleep(3000);

    }

    public static void changedApproveAsApprover(WebDriver driver, int jml_approve) {
        String[] str_debtorname = coll_debtor_name.split("\\|");

        try {
            for (int i = 1; i < 3; i++) {
                Thread.sleep(Const.delay);
                click(driver, menuApproval);
                LibUtils.waitElementVisible(driver, btnBulkApprove, Const.seconds);
                click(driver, btnBulkApprove);
                LibUtils.waitElementVisible(driver, txtSearch, Const.seconds);
                writeText(driver, txtSearch, str_debtorname[i]);
                LibUtils.waitElementVisible(driver, bulkFilter_radioFirstData, Const.seconds);
                click(driver, bulkFilter_radioFirstData);
                Thread.sleep(Const.delay);
                if (i == 1) {
                    click(driver, recomended);
                } else {
                    driver.findElement(notRecomended).click();
                }
                Thread.sleep(Const.delay);
            }
            Thread.sleep(Const.delay);
            Thread.sleep(Const.delay);
            click(driver, btnProfile);
            Thread.sleep(Const.delay);
            Thread.sleep(Const.delay);
            click(driver, btnLogout);

        } catch (Exception e) {
            e.getCause();
        }
    }

    public static void changedRecomendationAsMaker_rea(WebDriver driver, int jml_changes) throws InterruptedException {
        jumlahData = jml_changes;
        readTestData.testData();
        try {
            Thread.sleep(Const.delay);
            click(driver, menuApproval);
            LibUtils.waitElementVisible(driver, btnBulkRecomend, 30);
            click(driver, btnBulkRecomend);
            Thread.sleep(3000);
            writeText(driver, txtSearch, readTestData.timeStamp);
            click(driver, bulk_statusForReview);
            LibUtils.waitElementVisible(driver, radioButtonFirst, Const.seconds);
            coll_debtor_name = null;
            for (int i = 0; i < jumlahData; i++) {
                int idx = i + 1;
                driver.findElement(By.xpath("//*[@id='channelingApprovalDataTable']/tbody/tr[" + idx + "]/td[1]/div/label")).click();
                if (i == 7) {
                    LibUtils.scrollIntoView(driver, By.xpath("//*[@id='channelingApprovalDataTable']/tbody/tr['" + i + 9 + "']/td[1]/div/label"));
                } else if (i == 12) {
                    LibUtils.scrollIntoView(driver, tableBulk_nextPage);
                }
                debtor_name = driver.findElement(By.xpath("//*[@id='channelingApprovalDataTable']/tbody/tr[" + idx + "]/td[3]/a")).getText();
                coll_debtor_name = coll_debtor_name + "|" + debtor_name;
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        LibUtils.scrollIntoView(driver, recomended);
        click(driver, recomended);
        Thread.sleep(5000);
        try {
            Alert alert = driver.switchTo().alert();
            alert.accept();
        } catch (Exception ignored) {
        }
        Thread.sleep(3000);
        click(driver, btnProfile);
        Thread.sleep(3000);
        click(driver, btnLogout);
        Thread.sleep(3000);

    }

    public static void changedApproveAsApprover_rea(WebDriver driver, int jml_approve) {
        String[] str_debtorname = coll_debtor_name.split("\\|");

        try {
            for (int i = 1; i <= jml_approve; i++) {
                Thread.sleep(Const.delay);
                click(driver, menuApproval);
                LibUtils.waitElementVisible(driver, btnBulkApprove, Const.seconds);
                click(driver, btnBulkApprove);
                LibUtils.waitElementVisible(driver, txtSearch, Const.seconds);
                writeText(driver, txtSearch, str_debtorname[i]);
                LibUtils.waitElementVisible(driver, bulkFilter_radioFirstData, Const.seconds);
                click(driver, bulkFilter_radioFirstData);
                Thread.sleep(Const.delay);
                click(driver, recomended);
                Thread.sleep(Const.delay);
            }
            Thread.sleep(Const.delay);
            Thread.sleep(Const.delay);
            click(driver, btnProfile);
            Thread.sleep(Const.delay);
            Thread.sleep(Const.delay);
            click(driver, btnLogout);

        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public static void changedApproveAsApprover_reaFile(WebDriver driver, int jml_approve) throws IOException {
//        String[] str_debtorname = coll_debtor_name.split("\\|");
        String[] arr = readCSVFormRealization.fileCSVRea();
        int iArr = 55;
        try {
            for (int i = 1; i <= jml_approve; i++) {
                Thread.sleep(Const.delay);
                click(driver, menuApproval);
                LibUtils.waitElementVisible(driver, btnBulkApprove, Const.seconds);
                click(driver, btnBulkApprove);
                LibUtils.waitElementVisible(driver, txtSearch, Const.seconds);
                writeText(driver, txtSearch, arr[iArr]);
                LibUtils.waitElementVisible(driver, bulkFilter_radioFirstData, Const.seconds);
                click(driver, bulkFilter_radioFirstData);
                Thread.sleep(Const.delay);
                if (iArr == 110 || iArr == 165 || iArr == 220 || iArr == 275) {
                    driver.findElement(notRecomended).click();
                    try {
                        Alert alert = driver.switchTo().alert();
                        alert.accept();
                    } catch (Exception ignored) {
                    }
                } else {
                    click(driver, recomended);
                }
                Thread.sleep(Const.delay);
                iArr = iArr + 55;
            }

            Thread.sleep(Const.delay);
            Thread.sleep(Const.delay);
            click(driver, btnProfile);
            Thread.sleep(Const.delay);
            Thread.sleep(Const.delay);
            click(driver, btnLogout);

        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public static void changedRejectAsApprover_rea(WebDriver driver, int jml_approve) {
        String[] str_debtorname = coll_debtor_name.split("\\|");

        try {
            for (int i = 1; i <= jml_approve; i++) {
                Thread.sleep(Const.delay);
                click(driver, menuApproval);
                LibUtils.waitElementVisible(driver, btnBulkApprove, Const.seconds);
                click(driver, btnBulkApprove);
                LibUtils.waitElementVisible(driver, txtSearch, Const.seconds);
                writeText(driver, txtSearch, str_debtorname[i]);
                LibUtils.waitElementVisible(driver, bulkFilter_radioFirstData, Const.seconds);
                click(driver, bulkFilter_radioFirstData);
                Thread.sleep(Const.delay);
                click(driver, notRecomended);
                Thread.sleep(Const.delay);
            }
            Thread.sleep(Const.delay);
            Thread.sleep(Const.delay);
            click(driver, btnProfile);
            Thread.sleep(Const.delay);
            Thread.sleep(Const.delay);
            click(driver, btnLogout);

        } catch (Exception e) {
            e.printStackTrace();
        }
    }
}
