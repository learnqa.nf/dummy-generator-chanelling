package individu;

import applos.ConsumeData;
import capture.ImageExcel;
import com.aventstack.extentreports.ExtentReports;
import com.aventstack.extentreports.ExtentTest;
import com.aventstack.extentreports.Status;
import com.aventstack.extentreports.markuputils.MarkupHelper;
import createDataCSV.DataCSV;
import elementsPage.BaseAction;
import elementsPage.Const;
import elementsPage.DebtorDetailPage;
import org.openqa.selenium.*;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.testng.Assert;
import org.testng.asserts.SoftAssert;
import testData.ReadTestData;
import writeFile.UtilsFile;

import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.Arrays;
import java.util.Collections;
import java.util.Date;
import java.util.List;

import static elementsPage.ApprovalFormApprover.loginApprover;
import static elementsPage.ApprovalFormMaker.*;
import static individu.ChangesStatus.changedApproveAsApprover_rea;
import static individu.ChangesStatus.changedRecomendationAsMaker_rea;

public class LibUtils extends BaseAction {
    public static ExtentTest capture;
    static ReadTestData readTestData = new ReadTestData();
    static ConsumeData consumeData = new ConsumeData();
    static ImageExcel imageExcel = new ImageExcel();

    private ExtentTest processRecomend, childRecomend, processApprove, childApprove;

    public static void waitElementVisible(WebDriver driver, By locator, int seconds) {

        WebDriverWait wait = new WebDriverWait(driver, seconds);
        WebElement element = wait.until(ExpectedConditions.visibilityOfElementLocated(locator));

    }

    public static boolean _boleanWaitElementVisible(WebDriver driver, By locator, int seconds) {

        WebDriverWait wait = new WebDriverWait(driver, seconds);
        WebElement element = wait.until(ExpectedConditions.visibilityOfElementLocated(locator));
        return element.isDisplayed();
    }

    public static WebElement waitElementClickAble(WebDriver driver, By locator, int seconds) {

        WebDriverWait wait = new WebDriverWait(driver, seconds);
        WebElement element = wait.until(ExpectedConditions.elementToBeClickable(locator));

        return element;
    }

    public static void scrollIntoView(WebDriver driver, By locator) {
        try {
            for (int i = 1; i <= 2; i++) {
                JavascriptExecutor js = (JavascriptExecutor) driver;
                js.executeScript(Const.scrollIntoview, driver.findElement(locator));
                Thread.sleep(500);
            }
        } catch (Exception e) {
            e.printStackTrace();
        }

    }

    public static void moveTo(WebDriver driver, By locator) {
        try {
            driver.findElement(locator).sendKeys(Keys.PAGE_DOWN);
            Thread.sleep(Const.delay);
        } catch (Exception ee) {
            ee.printStackTrace();
        }
    }

    public static void moveToEnd(WebDriver driver, By locator) {
        try {
            driver.findElement(locator).sendKeys(Keys.CONTROL, Keys.END);
            Thread.sleep(Const.delay);
        } catch (Exception ee) {
            ee.printStackTrace();
        }
    }

    public static void status_testCase(int iRPN, boolean status, String notes) {
        readTestData.testData();
        switch (readTestData.tagName) {
            case "approvalForm":
                if (status) {
                    imageExcel.insertStatusExcel(iRPN, Const.attachmentName, Const.tStatus, notes);
                } else {
                    imageExcel.insertStatusExcel(iRPN, Const.attachmentName, Const.fStatus, notes);
                }
                break;
            case "realizationForm":
                if (status) {
                    imageExcel.insertStatusExcel(iRPN, Const.attachmentNameRea, Const.tStatus, notes);
                } else {
                    imageExcel.insertStatusExcel(iRPN, Const.attachmentNameRea, Const.fStatus, notes);
                }
                break;
            case "validationApprovalFile":
                if (status) {
                    imageExcel.insertStatusExcel(iRPN, Const.attachmentNameValidateAppFile, Const.tStatus, notes);
                } else {
                    imageExcel.insertStatusExcel(iRPN, Const.attachmentNameValidateAppFile, Const.fStatus, notes);
                }
                break;
            case "validationRealizationFile":
                if (status) {
                    imageExcel.insertStatusExcel(iRPN, Const.attachmentNameValidateReaFile, Const.tStatus, notes);
                } else {
                    imageExcel.insertStatusExcel(iRPN, Const.attachmentNameValidateReaFile, Const.fStatus, notes);
                }
                break;
        }
    }

    public static boolean verifyValue(String value_expected, String value_actual) {

        try {
            Assert.assertEquals(value_actual, value_expected);
            capture.log(Status.PASS, MarkupHelper.createUnorderedList(Arrays.asList("Expected CSV: " + value_expected, "Actual UI: " + value_actual)).getMarkup());
            System.out.println("AssertTrue >>>>>>>>>> Expected : [" + value_expected + "]  -  " + "Actual : [" + value_actual + "]");
            return true;
        } catch (AssertionError ee) {
            capture.log(Status.FAIL, MarkupHelper.createUnorderedList(Arrays.asList("Expected CSV: " + value_expected, "Actual UI: " + value_actual)).getMarkup());
            System.out.println("AssertFalse >>>>>>>>> Expected : [" + value_expected + "]  -  " + "Actual : [" + value_actual + "]");
            return false;
        }
    }

    public static boolean boolean_isDisplayed(WebDriver driver, By locator) {
        try {
            Thread.sleep(2000);
            return driver.findElements(locator).size() > 0;
        } catch (Exception e) {
            return false;
        }
    }

    public static void verifyElementExist(WebDriver driver, By locator) {
        if (driver.findElements(locator).size() > 0) {
            String getValue = getText(driver, locator);
            capture.log(Status.PASS, MarkupHelper.createUnorderedList(List.of("Actual result UI: " + getValue)).getMarkup());

        } else {
            LibUtils.capture.log(Status.FAIL, "Not Exist");
        }
    }

    public static String timestamp() {
        String pattern = "ddMMMyyyyHHmmss";
        SimpleDateFormat simpleDateFormat = new SimpleDateFormat(pattern);
        String fdate = simpleDateFormat.format(new Date());
        return fdate;
    }

    public static void provideDataToLOS(WebDriver driver, String jumlahData) throws IOException, InterruptedException {
        // provide data dummy
        UtilsFile cleanFolder = new UtilsFile();
        cleanFolder.dltDummyExist();

        DataCSV dataCSV = new DataCSV();
        dataCSV.dataDummy(Const.appFile, Const.reaFile, jumlahData);

        ConsumeData consumeData = new ConsumeData();
        consumeData.serverAkses();
        logout(driver);
        loginMaker(driver);
        changedRecomendationAsMaker_rea(driver, Integer.parseInt(jumlahData));
        loginApprover(driver, "surya.simarmata", "Surya2022");// approval file
        changedApproveAsApprover_rea(driver, Integer.parseInt(jumlahData));
        consumeData.serverAksesRea();

        loginApprover(driver, "weike.phinjaya", "Weike1234");
    }

    public static void realisasiReject(WebDriver driver) throws InterruptedException {

        loginMaker(driver);
        changedRecomendationAsMaker_rea(driver, 1);
        loginApprover(driver, Const.e2e_user_ldap_approveRecomend, Const.e2e_pwd_ldap_approveRecomend);// approval file
        changedApproveAsApprover_rea(driver, 1);
        consumeData.serverAksesRea();
        loginApprover(driver, Const.e2e_user_ldap_approveRealisasi, Const.e2e_pwd_ldap_approveRealisasi);// realisasi file
        DebtorDetailPage debtorDetailPage = new DebtorDetailPage();
        debtorDetailPage.reject();
        logout(driver);

    }

    public static void createTest(int iRowPictName, ExtentTest extent_test_case, ExtentReports extent) {

        readTestData.testData();
        String tc = null;
        switch (readTestData.tagName) {
            case "approvalForm":
                tc = "TC_AF_IDV";
                break;
            case "realizationForm":
                tc = "TC_RF_IDV";
                break;
            case "validationApprovalFile":
                tc = "TC_VAF_IDV";
                break;
            case "validationRealizationFile":
                tc = "TC_VRF_IDV";
                break;
        }

        extent_test_case = extent.createTest(tc + " " + iRowPictName + " : " + ReadTestData.testCaseName(iRowPictName)).assignAuthor(System.getProperty("user.name")).assignCategory("Regression");
        capture = extent_test_case.info("Expected Result: " + ReadTestData.expectedResult(iRowPictName));

    }

    public static void codeBlockText(String texts) {
        capture.log(Status.PASS, MarkupHelper.createCodeBlock(texts));

    }

    public static void actualLogTrue(String result) {
        try {
            Assert.assertEquals(result, "true");
            capture.log(Status.PASS, MarkupHelper.createUnorderedList(List.of("Actual result UI: " + result)).getMarkup());

        } catch (AssertionError e) {
            capture.log(Status.FAIL, MarkupHelper.createUnorderedList(List.of("Actual result UI: " + result)).getMarkup());

        }
    }

    public static void actualLogDoubleOrder(String result, String v) {
        try {
            Assert.assertEquals(result, "true");
            capture.log(Status.PASS, MarkupHelper.createUnorderedList(List.of("Actual result UI: " + result, v)).getMarkup());

        } catch (AssertionError e) {
            capture.log(Status.FAIL, MarkupHelper.createUnorderedList(List.of("Actual result UI: " + result, v)).getMarkup());

        }
    }

    public static void expectedLogTrue(String result) {
        capture.log(Status.PASS, MarkupHelper.createUnorderedList(List.of("Expected result CSV: " + result)).getMarkup());
    }

    public static void logInfo(String result) {
        capture.log(Status.INFO, MarkupHelper.createUnorderedList(List.of(result)).getMarkup());

    }

    public static boolean verifyAscending(String strDataColl) {
        String[] arrData = strDataColl.split("\\|");
        List<String> listActual = Arrays.asList(arrData);
        List<String> listExpected = Arrays.asList(arrData);
        Collections.sort(listExpected);

        try {
            Assert.assertEquals(listActual, listExpected);
            return true;
        } catch (AssertionError e) {
            return false;
        }
    }

    public static boolean verifyDescending(String strDataColl) {
        String[] arrData2 = strDataColl.split("\\|");
        List<String> listActual2 = Arrays.asList(arrData2);
        List<String> listExpected2 = Arrays.asList(arrData2);
        Collections.reverse(listExpected2);

        try {
            Assert.assertEquals(listActual2, listExpected2);
            return true;
        } catch (AssertionError e) {
            return false;
        }
    }

    public static void main(String[] args) {
        System.out.println(verifyValue("", "1"));
    }
}