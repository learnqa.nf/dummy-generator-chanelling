package testData;

import elementsPage.Const;
import org.apache.poi.openxml4j.util.ZipSecureFile;
import org.apache.poi.xssf.usermodel.XSSFSheet;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;

public class ReadTestData {
    public String[] cellDataValue;
    public String environment;
    public String namaDebitur;
    public String companyName;
    public String cd_company;
    public String kode_produk;
    public String jumlahData;
    public String tagName;
    public String jReject;
    public String timeStamp;
    public String noAppp;
    public String nik;

    static ReadTestData readTestData = new ReadTestData();

    public String[] testData() {

        try {
            ZipSecureFile.setMinInflateRatio(0);
            XSSFWorkbook workbook = new XSSFWorkbook(Const.pthTestData);
            XSSFSheet sheet = workbook.getSheet(Const.sheetTestData);

            environment = sheet.getRow(1).getCell(0).getStringCellValue();
            namaDebitur = sheet.getRow(1).getCell(1).getStringCellValue();
            companyName = sheet.getRow(1).getCell(2).getStringCellValue();
            cd_company = sheet.getRow(1).getCell(3).getStringCellValue();
            kode_produk = sheet.getRow(1).getCell(4).getStringCellValue();
            jumlahData = sheet.getRow(1).getCell(5).getStringCellValue();
            timeStamp = sheet.getRow(1).getCell(6).getStringCellValue();
            tagName = sheet.getRow(1).getCell(7).getStringCellValue();
            noAppp = sheet.getRow(1).getCell(8).getStringCellValue();
            nik = sheet.getRow(1).getCell(9).getStringCellValue();

            cellDataValue = new String[]{environment, namaDebitur, companyName, cd_company, kode_produk, jumlahData};

            workbook.close();
        } catch (Exception exp) {
            exp.printStackTrace();
        }
        return cellDataValue;
    }

    public static String testCaseID(int i) {
        String _case = null;
        readTestData.testData();

        try {
            XSSFSheet sheet = null;
            XSSFWorkbook workbook = new XSSFWorkbook(Const.pthFileValidation + Const.fileNMScenario);

            switch (readTestData.tagName){

                case "approvalForm":
                    sheet = workbook.getSheet(Const.sheetApprovalFormIDV);
                    break;
                case "realizationForm":
                    sheet = workbook.getSheet(Const.sheetRealizationFormIDV);
                    break;
                case "validationApprovalFile":
                    sheet = workbook.getSheet(Const.sheetApprovalFileIDV);
                    break;
                case "validationRealizationFile":
                    sheet = workbook.getSheet(Const.sheetRealisasiFileIDV);
                    break;
            }
            assert sheet != null;
            _case = sheet.getRow(i + 1).getCell(1).getStringCellValue();

            workbook.close();
            return _case;
        } catch (Exception e) {
            e.printStackTrace();
            return _case;
        }
    }
    public static String testCaseName(int i) {
        String _case = null;
        readTestData.testData();

        try {
            XSSFSheet sheet = null;
            XSSFWorkbook workbook = new XSSFWorkbook(Const.pthFileValidation + Const.fileNMScenario);

            switch (readTestData.tagName){

                case "approvalForm":
                    sheet = workbook.getSheet(Const.sheetApprovalFormIDV);
                    break;
                case "realizationForm":
                    sheet = workbook.getSheet(Const.sheetRealizationFormIDV);
                    break;
                case "validationApprovalFile":
                    sheet = workbook.getSheet(Const.sheetApprovalFileIDV);
                    break;
                case "validationRealizationFile":
                    sheet = workbook.getSheet(Const.sheetRealisasiFileIDV);
                    break;
            }
            assert sheet != null;
            _case = sheet.getRow(i + 1).getCell(4).getStringCellValue();

            workbook.close();
            return _case;
        } catch (Exception e) {
            e.printStackTrace();
            return _case;
        }
    }

    public static String expectedResult(int i) {
        String _expectedResult = null;
        XSSFSheet sheet = null;
        readTestData.testData();
        try {
            ZipSecureFile.setMinInflateRatio(0);
            XSSFWorkbook workbook = new XSSFWorkbook(Const.pthFileValidation + Const.fileNMScenario);
            switch (readTestData.tagName){

                case "approvalForm":
                    sheet = workbook.getSheet(Const.sheetApprovalFormIDV);
                    break;
                case "realizationForm":
                    sheet = workbook.getSheet(Const.sheetRealizationFormIDV);
                    break;
                case "validationApprovalFile":
                    sheet = workbook.getSheet(Const.sheetApprovalFileIDV);
                    break;
                case "validationRealizationFile":
                    sheet = workbook.getSheet(Const.sheetRealisasiFileIDV);
                    break;
            }

            _expectedResult = sheet.getRow(i + 1).getCell(5).getStringCellValue();

            workbook.close();
        } catch (Exception e) {
            e.printStackTrace();
        }
        return _expectedResult;
    }
}
