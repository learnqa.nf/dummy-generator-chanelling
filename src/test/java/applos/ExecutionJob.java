package applos;

import com.jcraft.jsch.*;
import elementsPage.Const;
import testData.ReadTestData;

import java.io.InputStream;
import java.util.Properties;

public class ExecutionJob {
    static String user = "qalos";
    static String pass = "P@ssw0rd";
    public static String hit_job;
    static String getName = null;
    public int chmodPermission = 747;
    static ReadTestData readTestData = new ReadTestData();

    public static void jobPBR() {

        String pthApprovalFile;
        String host;
        String e2e_host = "10.27.62.156";
        String uat_host = "10.27.62.157";

        String hit_job = "cd /bdb/app/los-source/job-execute && ./realization_pbr.sh";


        ConsumeData.hit_job = hit_job;
        readTestData.testData();
        if (readTestData.environment.equals(Const.e2e_environment)) {
            host = e2e_host;
            System.out.println("HOST : " + host);
        } else {
            host = uat_host;
            System.out.println("HOST : " + host);
        }

        try {
            Properties config = new Properties();
            config.put("StrictHostKeyChecking", "no");

            JSch jSch = new JSch();
            Session session = jSch.getSession(user, host);
            session.setPassword(pass);
            session.setConfig(config);
            session.connect();
            ChannelSftp channelSftp = (ChannelSftp) session.openChannel("sftp");
            channelSftp.connect();
            System.out
                    .println("Session connected: " + session.isConnected());

            Channel channel = session.openChannel("exec");
            System.out.println("");
            System.out
                    .println("======================Run Job=======================");
            System.out.println("");

            ((ChannelExec) channel).setCommand(hit_job);
            channel.setInputStream(null);
            ((ChannelExec) channel).setErrStream(System.err);

            InputStream in = channel.getInputStream();
            channel.connect();
            byte[] tmp = new byte[1024];
            while (true) {
                while (in.available() > 0) {
                    int i = in.read(tmp, 0, 1024);
                    if (i < 0) break;
                    System.out.print(new String(tmp, 0, i));
                }
                if (channel.isClosed()) {
                    System.out.println("exit-status: " + channel.getExitStatus());
                    break;
                }
                try {
                    Thread.sleep(1000);
                } catch (Exception ee) {
                }
            }
            channel.disconnect();
            session.disconnect();
            System.out.println("DONE");
            System.out
                    .println("===================Run Job Success==================");
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public static void jobReaExpired() {

        String pthApprovalFile;
        String host;
        String e2e_host = "10.27.62.156";
        String uat_host = "10.27.62.157";

        String hit_job = "cd /bdb/app/los-source/job-execute && ./update-realization-expired.sh";


        ConsumeData.hit_job = hit_job;
        readTestData.testData();
        if (readTestData.environment.equals(Const.e2e_environment)) {
            host = e2e_host;
            System.out.println("HOST : " + host);
        } else {
            host = uat_host;
            System.out.println("HOST : " + host);
        }

        try {
            Properties config = new Properties();
            config.put("StrictHostKeyChecking", "no");

            JSch jSch = new JSch();
            Session session = jSch.getSession(user, host);
            session.setPassword(pass);
            session.setConfig(config);
            session.connect();
            ChannelSftp channelSftp = (ChannelSftp) session.openChannel("sftp");
            channelSftp.connect();
            System.out
                    .println("Session connected: " + session.isConnected());

            Channel channel = session.openChannel("exec");
            System.out.println("");
            System.out
                    .println("======================Run Job=======================");
            System.out.println("");

            ((ChannelExec) channel).setCommand(hit_job);
            channel.setInputStream(null);
            ((ChannelExec) channel).setErrStream(System.err);

            InputStream in = channel.getInputStream();
            channel.connect();
            byte[] tmp = new byte[1024];
            while (true) {
                while (in.available() > 0) {
                    int i = in.read(tmp, 0, 1024);
                    if (i < 0) break;
                    System.out.print(new String(tmp, 0, i));
                }
                if (channel.isClosed()) {
                    System.out.println("exit-status: " + channel.getExitStatus());
                    break;
                }
                try {
                    Thread.sleep(1000);
                } catch (Exception ee) {
                }
            }
            channel.disconnect();
            session.disconnect();
            System.out.println("DONE");
            System.out
                    .println("===================Run Job Success==================");
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
    public static void jobAppExpired() {

        String pthApprovalFile;
        String host;
        String e2e_host = "10.27.62.156";
        String uat_host = "10.27.62.157";

        String hit_job = "cd /bdb/app/los-source/job-execute && ./update-approval-expired.sh";


        ConsumeData.hit_job = hit_job;
        readTestData.testData();
        if (readTestData.environment.equals(Const.e2e_environment)) {
            host = e2e_host;
            System.out.println("HOST : " + host);
        } else {
            host = uat_host;
            System.out.println("HOST : " + host);
        }

        try {
            Properties config = new Properties();
            config.put("StrictHostKeyChecking", "no");

            JSch jSch = new JSch();
            Session session = jSch.getSession(user, host);
            session.setPassword(pass);
            session.setConfig(config);
            session.connect();
            ChannelSftp channelSftp = (ChannelSftp) session.openChannel("sftp");
            channelSftp.connect();
            System.out
                    .println("Session connected: " + session.isConnected());

            Channel channel = session.openChannel("exec");
            System.out.println("");
            System.out
                    .println("======================Run Job=======================");
            System.out.println("");

            ((ChannelExec) channel).setCommand(hit_job);
            channel.setInputStream(null);
            ((ChannelExec) channel).setErrStream(System.err);

            InputStream in = channel.getInputStream();
            channel.connect();
            byte[] tmp = new byte[1024];
            while (true) {
                while (in.available() > 0) {
                    int i = in.read(tmp, 0, 1024);
                    if (i < 0) break;
                    System.out.print(new String(tmp, 0, i));
                }
                if (channel.isClosed()) {
                    System.out.println("exit-status: " + channel.getExitStatus());
                    break;
                }
                try {
                    Thread.sleep(1000);
                } catch (Exception ee) {
                }
            }
            channel.disconnect();
            session.disconnect();
            System.out.println("DONE");
            System.out
                    .println("===================Run Job Success==================");
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

}
