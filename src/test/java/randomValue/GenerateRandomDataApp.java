package randomValue;

import elementsPage.Const;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Random;

public class GenerateRandomDataApp {
    public String timeStamp;

    public String nomorAplikasi(String nameCompany, String keyword) {


        timeStamp = keyword;
        String chars = "123456789ABCDEFGHIJKLMNOPQRSTUVWXYZ";


        if (nameCompany.contains("SEAMONEY") || nameCompany.equals(Const.typ_NikelMora) || nameCompany.equals(Const.typ_NikelICL) || nameCompany.equals(Const.typ_NikelIPL)) {
            int len = 17;
            Random rnd = new Random();
            StringBuilder nomoraplikasi = new StringBuilder(len);

            for (int i = 0; i < len; i++)
                nomoraplikasi.append(chars.charAt(rnd.nextInt(chars.length())));
            return nomoraplikasi + timeStamp;
        } else {
            int len = 4;
            Random rnd = new Random();
            StringBuilder nomoraplikasi = new StringBuilder(len);

            for (int i = 0; i < len; i++)
                nomoraplikasi.append(chars.charAt(rnd.nextInt(chars.length())));
            return nomoraplikasi + timeStamp;
        }
    }

    public String nik(String genderCode) {
        String chars = "123456789";
        int len = 1;
        Random rnd = new Random();
        StringBuilder bulan = new StringBuilder(len);

        for (int i = 0; i < len; i++)
            bulan.append(chars.charAt(rnd.nextInt(chars.length())));

        String ychars = "6789";
        int ylen = 1;
        Random yrnd = new Random();
        StringBuilder y = new StringBuilder(len);

        for (int i = 0; i < ylen; i++)
            y.append(ychars.charAt(yrnd.nextInt(ychars.length())));

        String chars1 = "123456789";
        int len1 = 5;
        Random rnd1 = new Random();
        StringBuilder nik1 = new StringBuilder(len);

        for (int i = 0; i < len1; i++)
            nik1.append(chars1.charAt(rnd1.nextInt(chars1.length())));


        if (genderCode.equals("P")) {
            return "3273254" + bulan + "0" + bulan + y + nik1;
        } else {
            return "3273251" + bulan + "0" + bulan + y + nik1;
        }
    }


    public String npwp() {
        String chars = "1234567891111213141516171819234";
        int len = 13;
        Random rnd = new Random();
        StringBuilder npwp = new StringBuilder(len);

        for (int i = 0; i < len; i++)
            npwp.append(chars.charAt(rnd.nextInt(chars.length())));
        return "26" + npwp;

    }

    public String rdmAlamat() {
        String chars = "ABCDEFGHIJKLMNOPQRSTUVWXYZ1234567890";
        int len = 4;
        Random rnd = new Random();
        StringBuilder n = new StringBuilder(len);

        for (int i = 0; i < len; i++)
            n.append(chars.charAt(rnd.nextInt(chars.length())));
        return n.toString();
    }

    public String jenisDebitur(String company_name) {
        if (company_name.equals(Const.typ_NikelMora)) {
            return "B";
        } else {
            return "P";
        }
    }

    public String namaDebitur() {

        String chars = "12345ABCDEFGHIJKLMNOPQRSTUVWXYZ";
        int len = 4;
        Random rnd = new Random();
        StringBuilder namadebitur = new StringBuilder(len);

        for (int i = 0; i < len; i++)
            namadebitur.append(chars.charAt(rnd.nextInt(chars.length())));
        return namadebitur + timeStamp;
    }

    public String jangkaWaktu(String v) {
        String jangkaWaktu;
        switch (v) {
            case "L000007":
            case "L000008":
            case "L000009":
            case "L000010":
            case "L000054":
            case "L000055":
                jangkaWaktu = "006";
                return jangkaWaktu;
            case "L000133":
            case "L000004":
                jangkaWaktu = "036";
                return jangkaWaktu;
            case "L000005":
                String chars = "01234567";
                int len = 2;
                Random rnd = new Random();
                StringBuilder n = new StringBuilder(len);

                for (int i = 0; i < len; i++)
                    n.append(chars.charAt(rnd.nextInt(chars.length())));
                return "1" + n;
        }
        return null;
    }

    public String interestRate(String cd_company, String cd_product) {
        String interestRate;
        switch (cd_company) {
            case "L000030":
            case "L000132":
            case "L000040":
            case "L000001":
            case "L000058":
                interestRate = "01025";
                return interestRate;
            case "L000004":
                interestRate = "01015";
                return interestRate;
            case "L000133":
                interestRate = "01075";
                return interestRate;
            case "L000005":
            case "L000136":
            case "L000137":
            case "L000007":
            case "L000008":
            case "L000009":
            case "L000010":
            case "L000054":
            case "L000055":
                interestRate = "01115";
                return interestRate;

        }
        return null;
    }

    public String plafonKredit(String cd_company, String cd_product) {
        String chars = "1234567";
        int len = 1;
        Random rnd = new Random();
        StringBuilder plafonkredit = new StringBuilder(len);

        for (int i = 0; i < len; i++)
            plafonkredit.append(chars.charAt(rnd.nextInt(chars.length())));


        switch (cd_company) {
            case "L000133":
            case "L000004":
                return "000000" + "1" + plafonkredit + "0000000";
            case "L000005":
                if (cd_product.equals("24Bl047")) {
                    return "000010000000000";
                } else {
                    return "000010000000000";
                }

            case "L000136":
                return "000000100000000";
            case "L000137":
            case "L000007":
            case "L000008":
            case "L000009":
                return "00000001" + plafonkredit + "000000";
            case "L000010":
            case "L000054":
            case "L000055":
                return "00000012" + plafonkredit + "000000";


        }
        return null;
    }

    public String phoneNumber() {
        String chars = "81998765432";
        int len = 10;
        Random rnd = new Random();
        StringBuilder phoneNumber = new StringBuilder(len);

        for (int i = 0; i < len; i++)
            phoneNumber.append(chars.charAt(rnd.nextInt(chars.length())));
        return phoneNumber.toString();
    }

    public static String rndmText() {
        String chars = "123 456 7890 ABC DEFG HIJ KLMN OPQ RST UVW XYZ";
        int len = 4986;
        Random rnd = new Random();
        StringBuilder rndmText = new StringBuilder(len);

        for (int i = 0; i < len; i++)
            rndmText.append(chars.charAt(rnd.nextInt(chars.length())));
        return rndmText.toString();
    }

    public String rndmText150() {
        String chars = "123 456 7890 ABCDE FGHI JKL MNO PQRS TUV WXYZ";
        int len = 150;
        Random rnd = new Random();
        StringBuilder rndmText = new StringBuilder(len);

        for (int i = 0; i < len; i++)
            rndmText.append(chars.charAt(rnd.nextInt(chars.length())));

        return rndmText.toString();
    }

    public static String rndmText2() {
        String chars = "1234 567890A BCDE FGHIJK LMNOPQRS TUVWXYZ";
        int len = 47;
        Random rnd = new Random();
        StringBuilder rndmText = new StringBuilder(len);

        for (int i = 0; i < len; i++)
            rndmText.append(chars.charAt(rnd.nextInt(chars.length())));

        String chars2 = "123 4567 890A BCDEFGH IJKLM NOPQR STUV WXYZ";
        int len2 = 40;
        Random rnd2 = new Random();
        StringBuilder rndmText2 = new StringBuilder(len2);

        for (int i = 0; i < len2; i++)
            rndmText2.append(chars2.charAt(rnd2.nextInt(chars2.length())));

        return rndmText + rndmText2.toString();
    }

    public static String rndmNumber() {
        String chars = "3456789";
        int len = 1;
        Random rnd = new Random();
        StringBuilder rndmText = new StringBuilder(len);

        for (int i = 0; i < len; i++)
            rndmText.append(chars.charAt(rnd.nextInt(chars.length())));

        String chars2 = "789";
        int len2 = 1;
        Random rnd2 = new Random();
        StringBuilder rndmText2 = new StringBuilder(len2);

        for (int i = 0; i < len; i++)
            rndmText2.append(chars2.charAt(rnd2.nextInt(chars2.length())));

        String chars3 = "23456789";
        int len3 = 1;
        Random rnd3 = new Random();
        StringBuilder rndmText3 = new StringBuilder(len3);

        for (int i = 0; i < len; i++)
            rndmText3.append(chars3.charAt(rnd3.nextInt(chars3.length())));
        return rndmText + rndmText2.toString() + rndmText3;
    }

    public String rndmNumber80() {
        String chars = "12345678901234567890";
        int len = 80;
        Random rnd = new Random();
        StringBuilder rndmText = new StringBuilder(len);

        for (int i = 0; i < len; i++)
            rndmText.append(chars.charAt(rnd.nextInt(chars.length())));

        return rndmText.toString();
    }
    public static String dateNow(String pattern){
        SimpleDateFormat simpleDateFormat = new SimpleDateFormat(pattern);
        String fdate = simpleDateFormat.format(new Date());
        return fdate;
    }
}
