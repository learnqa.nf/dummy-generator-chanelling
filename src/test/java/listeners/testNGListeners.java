package listeners;

import org.testng.ITestContext;
import org.testng.ITestListener;
import org.testng.ITestResult;

public class testNGListeners implements ITestListener {
    @Override
    public void onTestStart(ITestResult result) {
        System.out.println("Start test.........." + result.getName());
    }

    @Override
    public void onTestSuccess(ITestResult result) {
        System.out.println("Test success........" + result.getName());

    }

    @Override
    public void onTestFailure(ITestResult result) {
        System.out.println("Test Fail............" + result.getName());
    }

    @Override
    public void onTestSkipped(ITestResult result) {
        System.out.println("Test Skip............" + result.getName());

    }

    @Override
    public void onTestFailedWithTimeout(ITestResult result) {
        System.out.println("Test fail timeout...." + result.getName());
    }

    @Override
    public void onStart(ITestContext context) {
        System.out.println("OnStart.............." + context.getName());
    }

    @Override
    public void onFinish(ITestContext context) {
        System.out.println("OnFinish............." + context.getName());
    }
}
