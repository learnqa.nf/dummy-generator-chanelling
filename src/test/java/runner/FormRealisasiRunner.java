package runner;

import applos.ConsumeData;
import com.aventstack.extentreports.ExtentReports;
import com.aventstack.extentreports.ExtentTest;
import createDataCSV.DataCSV;
import elementsPage.Const;
import individu.LibUtils;
import io.cucumber.testng.*;
import io.github.bonigarcia.wdm.WebDriverManager;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.edge.EdgeDriver;
import org.testng.annotations.*;
import writeFile.UtilsFile;

import java.io.IOException;
import java.util.concurrent.TimeUnit;

import static com.extentreports.ExtentReportsFile.report;
import static elementsPage.ApprovalFormApprover.loginApprover;
import static elementsPage.ApprovalFormMaker.loginMaker;
import static individu.ChangesStatus.*;

@CucumberOptions(
        plugin = {"pretty",
                "html:target/cucumber-report/cucumber.html",
                "json:target/cucumber-report/cucumber.json",
                "com.aventstack.extentreports.cucumber.adapter.ExtentCucumberAdapter:"
        },
        tags = "@realizationForm",
        features = {"src/test/resources/Features/realisasi.feature"},
        glue = {"chanellingDefinitions"}
)

public class FormRealisasiRunner extends AbstractTestNGCucumberTests {
    private TestNGCucumberRunner testNGCucumberRunner;
    public static ExtentReports extent;
    public static ExtentTest extent_test_case;
    UtilsFile cleanFolder = new UtilsFile();
    DataCSV dataCSV = new DataCSV();

    public static WebDriver driver;
    private int jumlahData = 4;
    ConsumeData consumeData = new ConsumeData();



    @BeforeTest
    public void setExtentReport() {
        extent = report();

    }

    @BeforeClass
    public void setup() throws IOException {
//
//        TempImages cleanSheetPict = new TempImages();
//        cleanSheetPict.cleanSheetImages(Const.sheetCaptureRealization);

    }

    @BeforeMethod
    void startDriver() throws InterruptedException, IOException {
        testNGCucumberRunner = new TestNGCucumberRunner(this.getClass());

        cleanFolder.dltDummyExist();
        dataCSV.dataDummy(Const.appFile, Const.reaFile, "1");
        consumeData.serverAkses();

        WebDriverManager.edgedriver().setup();
        driver = new EdgeDriver();
        driver.manage().timeouts().implicitlyWait(5, TimeUnit.SECONDS);

        LibUtils.realisasiReject(driver);//reject for tc 10

        cleanFolder.dltDummyExist();
        dataCSV.dataDummy(Const.appFile, Const.reaFile, String.valueOf(jumlahData));
        consumeData.serverAkses();

        loginMaker(driver);
        changedRecomendationAsMaker_rea(driver, jumlahData);
        loginApprover(driver, Const.e2e_user_ldap_approveRecomend, Const.e2e_pwd_ldap_approveRecomend);// approval file
        changedApproveAsApprover_rea(driver, jumlahData);
        consumeData.serverAksesRea();
        loginApprover(driver, Const.e2e_user_ldap_approveRealisasi, Const.e2e_pwd_ldap_approveRealisasi);// realisasi file

    }

    @AfterMethod
    public void close() {
//        driver.close();
        extent.flush();
    }

    @Test(groups = "cucumber", description = "Runs Realization Form Scenarios", dataProvider = "scenarios")
    public void runScenarioReaForm(PickleWrapper pickleWrapper, FeatureWrapper featureWrapper) {
        testNGCucumberRunner.runScenario(pickleWrapper.getPickle());
    }
}