package runner;

import applos.ConsumeData;
import com.aventstack.extentreports.ExtentReports;
import com.aventstack.extentreports.ExtentTest;
import createDataCSV.DataCSV;
import elementsPage.Const;
import individu.AdditionalProcess;
import io.cucumber.testng.*;
import io.github.bonigarcia.wdm.WebDriverManager;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.edge.EdgeDriver;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.Test;
import writeFile.UtilsFile;

import java.io.IOException;

import static com.extentreports.ExtentReportsFile.report;


@CucumberOptions(
        plugin = {"pretty",
                "html:target/cucumber-report/cucumber.html",
                "json:target/cucumber-report/cucumber.json",
                "com.aventstack.extentreports.cucumber.adapter.ExtentCucumberAdapter:"
        },
        tags = "@approvalForm",
        features = {"src/test/resources/Features/realisasi.feature"},
        glue = {"chanellingDefinitions"}
)

public class FormApprovalRunner extends AbstractTestNGCucumberTests {
    public static WebDriver driver;
    private TestNGCucumberRunner testNGCucumberRunner;
    public static ExtentReports extent;
    public static ExtentTest extent_test_case;

    @BeforeTest
    public void setExtentReport(){
        //create extent report
        extent = report();
    }
    @BeforeMethod
    public void startDriver() throws IOException {
        testNGCucumberRunner = new TestNGCucumberRunner(this.getClass());

        AdditionalProcess.provideDataExpiredApprovalForm();

        UtilsFile moveFileScenario = new UtilsFile();
        moveFileScenario.dltDummyExist();

        DataCSV dataCSV = new DataCSV();
        dataCSV.dataDummy(Const.appFile, Const.reaFile, "20");

        ConsumeData connServer = new ConsumeData();
        connServer.serverAkses();

        WebDriverManager.edgedriver().setup();
        driver = new EdgeDriver();

    }
    @Test(groups = "cucumber", description = "Runs Approval Form Scenarios", dataProvider = "scenarios")
    public void runScenarioAppForm(PickleWrapper pickleWrapper, FeatureWrapper featureWrapper) {
        testNGCucumberRunner.runScenario(pickleWrapper.getPickle());
    }
    @AfterMethod
    public void close(){
        extent.flush();
    }
}