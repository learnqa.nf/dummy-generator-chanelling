package runner;


import applos.ConsumeData;
import com.aventstack.extentreports.ExtentReports;
import com.aventstack.extentreports.ExtentTest;
import com.aventstack.extentreports.Status;
import createDataCSV.DataValidationCSVFile;
import elementsPage.Const;
import individu.LibUtils;
import io.cucumber.testng.*;
import io.github.bonigarcia.wdm.WebDriverManager;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.edge.EdgeDriver;
import org.testng.ITestResult;
import org.testng.annotations.*;
import writeFile.UtilsFile;
import writeFile.TempImages;

import java.io.IOException;
import java.util.concurrent.TimeUnit;

import static com.extentreports.ExtentReportsFile.report;
import static elementsPage.ApprovalFormApprover.loginApprover;
import static elementsPage.ApprovalFormMaker.loginMaker;
import static individu.ChangesStatus.*;

@CucumberOptions(
        plugin = {"pretty",
                "html:target/cucumber-report/cucumber.html",
                "json:target/cucumber-report/cucumber.json",
                "com.aventstack.extentreports.cucumber.adapter.ExtentCucumberAdapter:"
        },
        tags = "@validationRealizationFile",
        features = {"src/test/resources/Features/realisasi.feature"},
        glue = {"chanellingDefinitions"}
)
public class ValidateFileRealisasiRunner extends AbstractTestNGCucumberTests {

    private TestNGCucumberRunner testNGCucumberRunner;
    private final int jumlahData = 11;
    public static ExtentReports extent;
    public static ExtentTest extent_test_case;
    public static WebDriver driver;
    ConsumeData consumeData = new ConsumeData();

    @BeforeTest
    public void setExtentReport() {
        extent = report();
    }

    @BeforeClass
    public void setup() throws IOException {
        testNGCucumberRunner = new TestNGCucumberRunner(this.getClass());
        //clean dummy folder
        UtilsFile cleanFolder = new UtilsFile();
        cleanFolder.dltDummyExist();
        //create csv data dummmy
        DataValidationCSVFile dataValidationCSVFile = new DataValidationCSVFile();
        dataValidationCSVFile.dataDummy("APPFILEIDV", "REAFILEIDV", String.valueOf(jumlahData));
        consumeData.serverAkses();
    }

    @BeforeMethod
    public void startDriver() throws InterruptedException, IOException {

        readTestData.testData();
        WebDriverManager.edgedriver().setup();
        driver = new EdgeDriver();
        driver.manage().timeouts().implicitlyWait(5, TimeUnit.SECONDS);

        loginMaker(driver);
        changedRecomendationAsMaker_rea(driver, jumlahData);
        loginApprover(driver, Const.e2e_user_ldap_approveRecomend, Const.e2e_pwd_ldap_approveRecomend);// approval file
        changedApproveAsApprover_reaFile(driver, jumlahData);
        consumeData.serverAksesRea();
    }

    @AfterMethod
    public void close(ITestResult result) {

        if (result.getStatus() == ITestResult.FAILURE) {
            LibUtils.capture.log(Status.FAIL, result.getThrowable());
        }
        testNGCucumberRunner.finish();
        extent.flush();
        driver.close();
    }

    @Test(groups = "cucumber", description = "Runs Realsasi File Scenarios", dataProvider = "scenarios")
    public void runScenarioReaFile(PickleWrapper pickleWrapper) {
        testNGCucumberRunner.runScenario(pickleWrapper.getPickle());
    }
}
