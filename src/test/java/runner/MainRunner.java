package runner;

import applos.ConsumeData;
import createDataCSV.DataCSV;
import createDataCSV.DataValidationCSVFile;
import elementsPage.Const;
import readFile.ReadCSVFormApproval;
import writeFile.UtilsFile;

import java.io.IOException;

public class MainRunner {
    public static void main(String[] args) throws IOException {

        ConsumeData consumeData = new ConsumeData();
        System.out
                .println("========================================================");
        System.out
                .println("====================GENERATE DATA=======================");
        System.out
                .println("========================================================");

        UtilsFile utilsFile = new UtilsFile();
        utilsFile.dltDummyExist();

        DataCSV dataCSV = new DataCSV();
        dataCSV.dataDummy(Const.appFile, Const.reaFile, "1");

//        DataValidationCSVFile dataValidationCSVFile = new DataValidationCSVFile();
//        dataValidationCSVFile.dataDummy(Const.appFile, Const.reaFile, String.valueOf(6));

//        DataValidationCSVFile dataValidationCSVFile = new DataValidationCSVFile();
//        dataValidationCSVFile.dataDummy("APPFILEIDV", "REAFILEIDV", String.valueOf(11));

        ConsumeData connServer = new ConsumeData();
        connServer.serverAkses();

    }
}
