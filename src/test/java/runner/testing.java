package runner;

import capture.TakeSreenshot;
import com.aventstack.extentreports.*;
import com.aventstack.extentreports.annotations.MarkupIgnore;
import com.aventstack.extentreports.markuputils.MarkupHelper;
import com.aventstack.extentreports.reporter.ExtentSparkReporter;
import com.aventstack.extentreports.reporter.JsonFormatter;
import com.aventstack.extentreports.reporter.configuration.Theme;
import elementsPage.Const;
import formatFile.ZipFolder;
import individu.LibUtils;
import io.github.bonigarcia.wdm.WebDriverManager;
import listeners.testNGListeners;
import org.apache.commons.codec.binary.Base64;
import org.apache.commons.io.FileUtils;
import org.apache.commons.io.IOUtils;
import org.openqa.selenium.OutputType;
import org.openqa.selenium.TakesScreenshot;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.edge.EdgeDriver;
import org.testng.Assert;
import org.testng.ITestListener;
import org.testng.ITestResult;
import org.testng.annotations.*;
import readFile.ReadCSVFormApproval;
import testData.ReadTestData;
import writeFile.UtilsFile;

import java.io.*;
import java.nio.file.Paths;
import java.text.SimpleDateFormat;
import java.util.*;
import java.util.zip.ZipEntry;
import java.util.zip.ZipInputStream;

import static com.extentreports.ExtentReportsFile.folderReport;
import static elementsPage.Const.pthFolderDownload;
import static individu.LibUtils.capture;
import static individu.LibUtils.status_testCase;
import static randomValue.GenerateRandomDataApp.dateNow;
import static writeFile.UtilsFile.unzip;

@Listeners(testNGListeners.class)
public class testing {
    //    private WebDriver driver;
//    public ExtentReports extent;
//    public ExtentTest partner;
//    ExtentTest processRecomend, processApprove, childRecomend, childApprove;
//    ReadTestData readTestData = new ReadTestData();
//
//    @BeforeMethod
//    public void setup() throws IOException {
//        ExtentReports extent1;
//
//        ExtentSparkReporter esp = new ExtentSparkReporter(System.getProperty("user.dir") + "/ExtentReport/ExtentReportsSwarupExtentReport.html");
//
//        JsonFormatter json = new JsonFormatter("/ExtentReport/extent.json");
//
//        esp.config().setTheme(Theme.STANDARD);
//        esp.config().setEncoding("utf-8");
//        extent1 = new ExtentReports();
//
//        extent1.createDomainFromJsonArchive("extent.json");
//
//        extent1.attachReporter(json, esp);
//        extent1.setAnalysisStrategy(AnalysisStrategy.SUITE);
//        System.out.println("beofore metod");
//        this.extent = extent1;
//
//
//    }
//
//    public class MyCustomLog {
//        private final List<Object> names = Arrays.asList("Anshoodfsdfgsdfgsdfg", "Extendfgsdfgsdfgt", "Klsdfgsdfgsdfgsdfgsgsdfgsdfgsdfgsdfgsdfgov");
//        private final Object[] favStack = new Object[]{"Java", "C#", "Angular"};
//        private final List<Object> ignored = Arrays.asList("Ignored", "Ignored", "Ignsfdgsdfgsdfgsdfgsdfgsdfgsdfgsdfgored");
//        private final List<Object> names1 = Arrays.asList("Anshoo", "Extsdfgsdfgent", "Ksfdgsfdglov");
//        private final Object[] favStack2 = new Object[]{"Java", "C#", "Angular"};
//        private final List<Object> ignored3 = Arrays.asList("Ignored", "sdfgsdfgsdfgsdfIgnored", "Ignored");
//        private final List<Object> names4 = Arrays.asList("Anshoo", "Extent", "Klov");
//        private final Object[] favStack5 = new Object[]{"Java", "C#", "Angular"};
//        private final List<Object> ignored6 = Arrays.asList("Ignored", "Ignored", "Ignored");
//        private final List<Object> names7 = Arrays.asList("Anshoo", "Extent", "Klov");
//        private final Object[] favStack8 = new Object[]{"Java", "C#", "Angular"};
//        private final List<Object> ignored9 = Arrays.asList("Ignored", "Isdfgsdfgsdgfsdfgnored", "Ignored");
//        private final List<Object> names10 = Arrays.asList("Anshoo", "Extent", "Klov");
//        private final Object[] favStack11 = new Object[]{"Java", "C#", "Agsdfgsdfgsdfgsfgngular"};
//        private final List<Object> ignored12 = Arrays.asList("Ignorsdfgsdfgsdfgsdfgsdfgsdfgsdfgsdfged", "Ignored", "Ignored");
//    }
//
//    @Test(testName = "Chrome browser Testing")
//    public void chromeBrowser() throws IOException {
//        TakeSreenshot takeSreenshot = new TakeSreenshot();
//        System.out.println("main test");
//        WebDriverManager.edgedriver().setup();
//        driver = new EdgeDriver();
//        driver.get("https://www.google.com/");
//        driver.manage().window().maximize();
//        try {
//            partner = extent.createTest("test case 1");
//            partner.log(Status.PASS, "Need to open the URL " + " http://www.icicibank.com");
//            capture2(driver);
//            partner.log(Status.PASS, "Test Case is passed", MediaEntityBuilder.createScreenCaptureFromBase64String(capture(driver)).build());
//            extent.createTest("GeneratedLog")
//                    .generateLog(Status.PASS, MarkupHelper.toTable(new MyCustomLog()));
//
//
////            partner = extent.createTest("test case 2");
////
////            File file = new File("resultDataDummyFile/APPFILEIDV_L000004_24PL004_26122022.csv");
////            String content = FileUtils.readFileToString(file, "UTF-8");
////            partner.log(Status.INFO, MarkupHelper.createCodeBlock(content));
////
////            partner.log(Status.PASS, "<a href='APPFILEIDV_L000004_24PL004_26122022.csv'>click to view text</a>");
////            partner.log(Status.INFO, MarkupHelper.createCodeBlock(content));
////            partner.log(Status.PASS, "Need to open the URL " + " http://www.icicibank.com");
////            partner.log(Status.PASS, "Test Case is passed", MediaEntityBuilder.createScreenCaptureFromPath(capture(driver)).build());
//
//
//        } catch (Exception ee) {
//
//        }
//    }
//
//    @AfterMethod
//    public void getResult(ITestResult result) {
//        try {
//
//            if (result.getStatus() == ITestResult.SUCCESS) {
//                childRecomend.log(Status.PASS, "Test case is passed " + result.getStatus() + " " + result.getTestClass() + "  " + result.getName());
//                childRecomend.log(Status.PASS, "Test case is passed " + result.getTestName());
//
//            } else { // (result.getStatus() == ITestResult.FAILURE) {
//                childRecomend.log(Status.FAIL, "Test case is failed at below location " + result.getThrowable());
//            }
//        } catch (Exception e) {
//
//        }
//        extent.flush();
////        driver.close();
//    }
//
//
//    public static String captureBase64(WebDriver driver) throws IOException {
//        String encodedBase64 = null;
//        FileInputStream fileInputStream = null;
//        File scrFile = ((TakesScreenshot) driver).getScreenshotAs(OutputType.FILE);
//        File Dest = new File("src/../BStackImages/" + System.currentTimeMillis()
//                + ".png");
//        String errflpath = Dest.getAbsolutePath();
//        FileUtils.copyFile(scrFile, Dest);
//
//        try {
//
//            fileInputStream = new FileInputStream(Dest);
//            byte[] bytes = new byte[(int) Dest.length()];
//            fileInputStream.read(bytes);
//            encodedBase64 = new String(Base64.encodeBase64(bytes));
//
//        } catch (FileNotFoundException e) {
//            e.printStackTrace();
//        }
//        return "data:image/png;base64," + encodedBase64;
//
//
//    }
//
//    public static String capture(WebDriver driver) throws IOException {
//
//        File scrFile = ((TakesScreenshot) driver).getScreenshotAs(OutputType.FILE);
//        String ss = "src/../BStackImages/" + System.currentTimeMillis() + ".png";
//        File Dest = new File(ss);
//        String errflpath = Dest.getAbsolutePath();
//        FileUtils.copyFile(scrFile, Dest);
//
//        byte[] imgBytes = IOUtils.toByteArray(new FileInputStream(ss));
//        return Base64.encodeBase64String(imgBytes);
//    }
//
//    public void capture2(WebDriver driver) {
//        readTestData.testData();
//
//        try {
//
//            String pattern = Const.pPattern;
//            SimpleDateFormat simpleDateFormat = new SimpleDateFormat(pattern);
//            String formattedToday = simpleDateFormat.format(new Date());
//
//            String ss = "src/../BStackImages/" + System.currentTimeMillis() + ".png";
//            Thread.sleep(Const.delay);
//            File src_screenshot = ((TakesScreenshot) driver).getScreenshotAs(OutputType.FILE);
//            File Dest = new File(ss);
//
//            FileUtils.copyFile(src_screenshot, Dest);
//            byte[] imgBytes = IOUtils.toByteArray(new FileInputStream(ss));
//            partner.log(Status.INFO, MediaEntityBuilder.createScreenCaptureFromBase64String(Base64.encodeBase64String(imgBytes)).build());
//
//        } catch (InterruptedException | IOException e) {
//            e.printStackTrace();
//        }
//    }
//
//    @Test
//    public void test() throws IOException {
//        ExtentSparkReporter spark = new ExtentSparkReporter("./tes/spark.html");
//        JsonFormatter json = new JsonFormatter("./tes/extent.json");
//        ExtentReports extent = new ExtentReports();
//        extent.createDomainFromJsonArchive("./tes/extent.json");
//        extent.attachReporter(json, spark);
//        partner = extent.createTest("test case 1");
//    }
//
//
    public static void main(String[] args) throws IOException {
//        File directory = new File("C:/Users/" + System.getProperty("user.name") + "/Downloads/");
//        File[] files = directory.listFiles(File::isFile);
//        long lastModifiedTime = Long.MIN_VALUE;
//        File chosenFile = null;
//
//        if (files != null) {
//            for (File file : files) {
//                if (file.lastModified() > lastModifiedTime) {
//                    chosenFile = file;
//                    lastModifiedTime = file.lastModified();
//                }
//            }
//        }
//        assert chosenFile != null;
//        String a = String.valueOf(chosenFile);
        UtilsFile.deleteInFolderContainsFileName(pthFolderDownload, "Test1");
    }
}

