package runner;

import applos.ConsumeData;
import com.aventstack.extentreports.ExtentReports;
import com.aventstack.extentreports.ExtentTest;
import createDataCSV.DataValidationCSVFile;
import elementsPage.Const;
import io.cucumber.testng.*;
import io.github.bonigarcia.wdm.WebDriverManager;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.edge.EdgeDriver;
import org.testng.annotations.*;
import writeFile.UtilsFile;

import java.io.IOException;
import java.net.MalformedURLException;
import java.util.concurrent.TimeUnit;

import static com.extentreports.ExtentReportsFile.report;

@CucumberOptions(
        plugin = {"pretty",
                "html:target/cucumber-report/cucumber.html",
                "json:target/cucumber-report/cucumber.json",
                "com.aventstack.extentreports.cucumber.adapter.ExtentCucumberAdapter:"
        },
        tags = "@validationApprovalFile",
        features = {"src/test/resources/Features/realisasi.feature"},
        glue = {"chanellingDefinitions"}
)

public class ValidateFileApprovalRunner extends AbstractTestNGCucumberTests {
    private TestNGCucumberRunner testNGCucumberRunner;
    private final int jumlahData = 6;
    public static ExtentReports extent;
    public static ExtentTest extent_test_case;
    public static WebDriver driver;
    ConsumeData consumeData = new ConsumeData();

    @BeforeTest
    public void setExtentReport() {
        extent = report();

    }

    @BeforeClass
    public void setup() throws IOException {
        testNGCucumberRunner = new TestNGCucumberRunner(this.getClass());
        //clean dummy folder
        UtilsFile cleanFolder = new UtilsFile();
        cleanFolder.dltDummyExist();
        //create cdv
        DataValidationCSVFile dataValidationCSVFile = new DataValidationCSVFile();
        dataValidationCSVFile.dataDummy(Const.appFile, Const.reaFile, String.valueOf(jumlahData));
        consumeData.serverAkses();//consume file csv
    }

    @BeforeMethod
    public void startDriver() throws InterruptedException, MalformedURLException {

        WebDriverManager.edgedriver().setup();
        driver = new EdgeDriver();
        driver.manage().timeouts().implicitlyWait(5, TimeUnit.SECONDS);
    }

    @AfterMethod
    public void close() {
        testNGCucumberRunner.finish();
        extent.flush();
//        driver.close();
    }

    @Test(groups = "cucumber", description = "Runs Approval File Scenarios", dataProvider = "scenarios")
    public void runScenarioAppFile(PickleWrapper pickleWrapper, FeatureWrapper featureWrapper) {
        testNGCucumberRunner.runScenario(pickleWrapper.getPickle());
    }
}
