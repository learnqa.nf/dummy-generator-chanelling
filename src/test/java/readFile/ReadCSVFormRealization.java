package readFile;

import elementsPage.Const;

import java.io.File;
import java.io.FileReader;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

public class ReadCSVFormRealization {

    public String[] fileCSVRea() throws IOException {

        List<String> listOfStrings = new ArrayList<>();
        File folder = new File(Const.resultDataDummyFile);// folder csv
        File[] listOfFiles = folder.listFiles();
        String fileExcel = null;

        for (int i = 0; i < listOfFiles.length; i++) {
            if (listOfFiles[i].isFile()) {
                //contains REA or onother file
                if (listOfFiles[i].getName().contains(Const.csvRea)) {
                    fileExcel = listOfFiles[i].getName();// file name
                }
            }
        }
        FileReader fr = new FileReader(Const.resultDataDummyFile + fileExcel);// folder and file name
        String s = new String();
        char ch;

        // checking for EOF
        while (fr.ready()) {
            ch = (char) fr.read();
            // Used to specify the delimiters
            if (ch == '|' || ch == '\n') {
                // Storing each string in arraylist
                listOfStrings.add(s.toString());
                // clearing content in string
                s = new String();
            } else {

                s += ch;
            }
        }
        if (s.length() > 0) {
            listOfStrings.add(s.toString());
        }
        String[] array = listOfStrings.toArray(new String[0]);
        fr.close();
        return array;
    }
}
