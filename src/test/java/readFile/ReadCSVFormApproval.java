package readFile;

import elementsPage.Const;

import java.io.File;
import java.io.FileReader;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.Objects;

public class ReadCSVFormApproval {

    public String[] fileCSVApp() throws IOException {

        List<String> listOfStrings = new ArrayList<>();
        File folder = new File(Const.resultDataDummyFile);
        File[] listOfFiles = folder.listFiles();
        String fileExcel = null;

        for (int i = 0; i < Objects.requireNonNull(listOfFiles).length; i++) {
            if (listOfFiles[i].isFile()) {

                if (listOfFiles[i].getName().contains(Const.csvApp)) {
                    fileExcel = listOfFiles[i].getName();
                }
            }
        }
        FileReader fr = new FileReader(Const.resultDataDummyFile + fileExcel);
        // Created a string to store each character to form word
        String s = new String();
        char ch;

        // checking for EOF
        while (fr.ready()) {
            ch = (char) fr.read();
            // Used to specify the delimiters
            if (ch == '|' || ch == '\n') {
                listOfStrings.add(s.toString());
                s = new String();
            } else {
                s += ch;
            }
        }
        if (s.length() > 0) {

            listOfStrings.add(s.toString());
        }

        String[] array = listOfStrings.toArray(new String[0]);
        return array;
    }
}
