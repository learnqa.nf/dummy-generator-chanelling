package readFile;

import com.opencsv.CSVReader;
import com.opencsv.CSVWriter;
import com.opencsv.exceptions.CsvException;
import elementsPage.Const;
import individu.LibUtils;

import java.io.*;
import java.util.ArrayList;
import java.util.List;
import java.util.Objects;

public class UpdateCSV {
    public static void updateFieldApprovalFileCSV(String strData, String newData) throws IOException, CsvException {

        File folder = new File(Const.resultDataDummyFile);
        File[] listOfFiles = folder.listFiles();
        String getFileCSV = null;

        for (int i = 0; i < Objects.requireNonNull(listOfFiles).length; i++) {
            if (listOfFiles[i].isFile()) {

                if (listOfFiles[i].getName().contains(Const.appFile)) {
                    getFileCSV = listOfFiles[i].getName();
                }
            }
        }
        CSVReader reader = new CSVReader(new FileReader(new File(Const.resultDataDummyFile + getFileCSV)));
        List<String[]> csvBody = null;
        ArrayList<String[]> dataApp = null;

        csvBody = reader.readAll();
        dataApp = new ArrayList<>();

        for (int i = 0; i < csvBody.size(); i++) {
            String[] strArray = csvBody.get(i);
            for (int j = 0; j < strArray.length; j++) {
                if (strArray[j].contains(strData)) { //String to be replaced
                    String[] p = new String[0];
                    for (String[] s : csvBody) {
                        for (int index = 0; index < s.length; index++) {
                            p = s[index].split("\\|");

                            // p[2] = field NPWP row 2
                            if (p[2].equals(strData)) {
                                p[2] = newData;
                            }
                            if (p[4].equals(strData)) {
                                p[4] = newData;
                            }
                            if (p[5].equals(strData)) {
                                p[5] = newData;
                            }
                            if (p[6].equals(strData)) {
                                p[6] = newData;
                            }
                            if (p[7].equals(strData)) {
                                p[7] = newData;
                            }
                            if (p[8].equals(strData)) {
                                p[8] = newData;
                            }
//                            if (p[9].equals(strData)) {
//                                p[9] = newData;
//                            }
                            dataApp.add(p);
                        }
                    }
                }
            }
        }
        reader.close();

        File fileApp = new File(Const.resultDataDummyFile + getFileCSV);
        FileWriter outputfileApp = new FileWriter(fileApp);
        CSVWriter writer = new CSVWriter(outputfileApp, '|',
                CSVWriter.NO_QUOTE_CHARACTER,
                CSVWriter.DEFAULT_ESCAPE_CHARACTER,
                CSVWriter.DEFAULT_LINE_END);

        writer.writeAll(dataApp);
        writer.flush();
        writer.close();
    }

    public static void updateFieldCSV(String strData, String newData) throws IOException, CsvException {

        File folder = new File(Const.resultDataDummyFile);
        File[] listOfFiles = folder.listFiles();
        String getFileCSV = null;

        for (int i = 0; i < Objects.requireNonNull(listOfFiles).length; i++) {
            if (listOfFiles[i].isFile()) {

                if (listOfFiles[i].getName().contains(Const.csvRea)) {
                    getFileCSV = listOfFiles[i].getName();
                }
            }
        }
        CSVReader reader = new CSVReader(new FileReader(new File(Const.resultDataDummyFile + getFileCSV)));
        List<String[]> csvBody = reader.readAll();
        ArrayList<String[]> dataApp = new ArrayList<>();

        for (int i = 0; i < csvBody.size(); i++) {
            String[] strArray = csvBody.get(i);
            for (int j = 0; j < strArray.length; j++) {
                if (strArray[j].contains(strData)) { //String to be replaced
                    String[] p = new String[0];
                    for (String[] s : csvBody) {
                        for (int index = 0; index < s.length; index++) {
                            p = s[index].split("\\|");

                            if (p[6].equals(strData)) {
                                p[6] = newData + LibUtils.timestamp();
                            }
                            dataApp.add(p);
                        }
                    }
                }
            }
        }
        reader.close();

        File fileApp = new File(Const.resultDataDummyFile + getFileCSV);
        FileWriter outputfileApp = new FileWriter(fileApp);
        CSVWriter writer = new CSVWriter(outputfileApp, '|',
                CSVWriter.NO_QUOTE_CHARACTER,
                CSVWriter.DEFAULT_ESCAPE_CHARACTER,
                CSVWriter.DEFAULT_LINE_END);

        writer.writeAll(dataApp);
        writer.flush();
        writer.close();
    }

    public static void main(String[] args) throws IOException, CsvException {
//        ReadCSVFormRealization readCSVFormRealization = new ReadCSVFormRealization();
//        String[] arr = readCSVFormRealization.fileCSVRea();
//        String alamat1 = arr[61];
//        updateFieldCSV(alamat1, "update xadnasfbheyridp");

        ReadCSVFormApproval appfile = new ReadCSVFormApproval();
        String[] arr = appfile.fileCSVApp();
        String NPWP = arr[22];
        String namaDebitur1 = arr[14];
        String namadebitur2 = arr[24];
        String JWK1 = arr[15];
        String JWK2 = arr[25];
        String JWK3 = arr[35];
        String intRate1 = arr[16];
        String intRate2 = arr[26];
        String intRate3 = arr[36];
        String palfon1 = arr[17];
        String palfon2 = arr[27];
        String palfon3 = arr[37];
        String phoneNumber1 = arr[18];
        String phoneNumber2 = arr[28];
        String remarks1 = arr[19];
        String remarks2 = arr[29];

        String[] modifData = {NPWP, namaDebitur1, namadebitur2, JWK1, JWK2, JWK3,
                intRate1, intRate2, intRate3, palfon1, palfon2, palfon3,
                phoneNumber1, phoneNumber2, remarks1, remarks2};
        for (int i = 0; i < modifData.length; i++) {
            switch (i) {
                case 0:
                    updateFieldApprovalFileCSV(modifData[i], "");
                    break;
                case 1:
                    updateFieldApprovalFileCSV(modifData[i], "MJIOPLKMNBVCXSAD");
                    break;
                case 2:
                    updateFieldApprovalFileCSV(modifData[i], "MJIOPLKMNBVCXSA");
                    break;
                case 3:
                    updateFieldApprovalFileCSV(modifData[i], "012");
                    break;
                case 4:
                    updateFieldApprovalFileCSV(modifData[i], "060");
                    break;
                case 5:
                    updateFieldApprovalFileCSV(modifData[i], "120");
                    break;
                case 6:
                    updateFieldApprovalFileCSV(modifData[i], "00800");
                    break;
                case 7:
                    updateFieldApprovalFileCSV(modifData[i], "00950");
                    break;
                case 8:
                    updateFieldApprovalFileCSV(modifData[i], "01100");
                    break;
                case 9:
                    updateFieldApprovalFileCSV(modifData[i], "000000100000000");
                    break;
                case 10:
                    updateFieldApprovalFileCSV(modifData[i], "000005000000000");
                    break;
                case 11:
                    updateFieldApprovalFileCSV(modifData[i], "000010000000000");
                    break;
                case 12:
                    updateFieldApprovalFileCSV(modifData[i], "62834689591634567820");
                    break;
                case 13:
                    updateFieldApprovalFileCSV(modifData[i], "6283468959163456719");
                    break;
                case 14:
                    updateFieldApprovalFileCSV(modifData[i], "40Karakter40Karakter40Karakter40Karakter");
                    break;
                case 15:
                    updateFieldApprovalFileCSV(modifData[i], "");
                    break;
            }
        }
    }
}
