package chanellingDefinitions;

import elementsPage.ApprovalFilePage;
import io.cucumber.java.en.And;
import io.cucumber.java.en.Given;
import io.cucumber.java.en.Then;
import io.cucumber.java.en.When;

import java.io.IOException;

public class StepValidationApprovalFile {

    ApprovalFilePage appFile = new ApprovalFilePage();

    @Given("user akses LOS application")
    public void user_akses_LOS_application() {
        appFile.login();

    }

    @When("input no aplikasi 17karakter varchar")
    public void input_no_aplikasi_17karakter_varchar() throws IOException {
        appFile.validationNoApplication17Varchar();

    }

    @And("input NIK 16karakter numerik")
    public void input_NIK_16karakter_numerik() {
        appFile.validationNIK16numerik();

    }

    @And("input NPWP")
    public void input_NPWP() throws IOException {
        appFile.validationNPWP15KarakterNumerik();
        appFile.validationNPWPKosongPlafonKurang50Juta();

    }

    @And("input jenis debitur")
    public void input_jenis_debitur() {
        appFile.validationJenisDebitur();

    }

    @And("input nama debitur")
    public void input_nama_debitur() throws IOException {
        appFile.validationNamaDebitur60Karakter();
        appFile.validationNamaDebitur59Karakter();

    }

    @And("input jangka waktu kredit")
    public void input_jangka_waktu_kredit() throws IOException {
        appFile.validationJangkaWaktuBatasBawah();
        appFile.validationJangkaWaktuBatasTengah();
        appFile.validationJangkaWaktuBatasAtas();

    }

    @And("input interest rate")
    public void input_interest_rate() throws IOException {
        appFile.validationInterestRateBatasBawah();
        appFile.validationInterestRateBatasTengah();
        appFile.validationInterestRateBatasAtas();

    }

    @And("input plafon")
    public void input_plafon() throws IOException {
        appFile.validationPlafonBatasBawah();
        appFile.validationPlafonBatasTengah();
        appFile.validationPlafonBatasAtas();

    }

    @And("input phone number")
    public void input_phone_number() throws IOException {
        appFile.validationPhoneNumber20Karakter();
        appFile.validationPhoneNumber19Karakter();

    }

    @And("input remarks")
    public void input_remarks() throws IOException {
        appFile.validationRemarks40Karakter();
        appFile.validationRemarksKosong();

    }

    @And("input 2data approval NIK sama di 1partner")
    public void input_2data_approval_NIK_sama_di_1partner() {
        appFile.validationDuaDataNIKSatuPartner();

    }

    @And("input 2data approval NIK sama di 2partner")
    public void input_2data_approval_NIK_sama_di_2partner() throws IOException {
        appFile.validationDuaDataNIKDuaPartner();

    }

    @Then("verifikasi notif setelah proses file approval")
    public void verifikasi_notif_setelah_proses_file_approval() throws InterruptedException {
        appFile.validationNotifSetelahProsesApproval();

    }


}
