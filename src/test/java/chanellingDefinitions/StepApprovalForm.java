package chanellingDefinitions;

import elementsPage.ApprovalFormApprover;
import elementsPage.ApprovalFormMaker;
import individu.AdditionalProcess;
import io.cucumber.java.Before;
import io.cucumber.java.Scenario;
import io.cucumber.java.en.And;
import io.cucumber.java.en.Given;
import io.cucumber.java.en.Then;
import io.cucumber.java.en.When;
import org.openqa.selenium.WebDriver;
import runner.FormApprovalRunner;
import writeFile.WriteTestData;

import java.io.IOException;
import java.util.Collection;
import java.util.List;

import static elementsPage.ApprovalFormMaker.*;

public class StepApprovalForm {

    private final WebDriver driver = FormApprovalRunner.driver;
    public String tag;

    ApprovalFormMaker approvalFormMaker = new ApprovalFormMaker();
    ApprovalFormApprover approvalFormApprover = new ApprovalFormApprover();

    @Before
    public void before(Scenario scenario) throws IOException {
        Collection<String> coll_tag = scenario.getSourceTagNames();
        for (String tag_ : coll_tag) {
            tag = tag_.replace("@", "");
        }

        WriteTestData writeTestData = new WriteTestData();
        writeTestData.writeData(tag, 7);

        System.out.println("Scenario Name   : " + scenario.getName());
        System.out.println("Tag Name        : " + tag);

    }

    @Given("open browser and usermaker login application")
    public void open_browser_and_usermaker_login_application() {
        loginMaker(driver);
    }

    @When("akses dan verifikasi tampilan list approval form")
    public void aksesDanVerifikasiTampilanListApprovalForm() throws IOException {
        approvalFormMaker.setApproval();
        approvalFormMaker.verifyTampilanLosApprovalForm();

    }

    @Then("verifikasi semua data yang ada di list approval form")
    public void verifikasiSemuaDataYangAdaDiListApprovalForm() throws InterruptedException {
        approvalFormMaker.verifySemuaDataListApprovalForm();
        approvalFormMaker.changeStatus();

    }

    @And("verifikasi filter dropdown source approval form")
    public void verifikasiFilterDropdownSourceApprovalForm(List<String> list_case) {
        approvalFormMaker.verifyFilterDropDown(list_case);

    }

    @And("verifikasi filter dropdown status klik field dropdown status")
    public void verifikasiFilterDropdownStatusKlikFieldDropdownStatus(List<String> list_case) throws InterruptedException {
        approvalFormMaker.verifyFilterDropdownStatus(list_case);

    }

    @And("verifikasi filter dropdown partner klik field dropdown partner")
    public void verifikasiFilterDropdownPartnerKlikFieldDropdownPartner(List<String> list_case) {
        approvalFormMaker.verifyFilterDropdwonPartner(list_case);

    }

    @And("verifikasi fungsi search approval form")
    public void verifikasiFungsiSearchApprovalForm() throws InterruptedException {
        approvalFormMaker.verifyFungsiSearch();

    }

    @And("verifikasi fungsi next page")
    public void verifikasiFungsiNextPage() throws InterruptedException {
        approvalFormMaker.verifyNextPage();
    }

    @And("verifikasi fungsi prev page")
    public void verifikasiFungsiPrevPage() {
        approvalFormMaker.verifyPrevPage();

    }

    @And("verifikasi fungsi specific page")
    public void verifikasiFungsiSpecificPage() {
        approvalFormMaker.verifySpecificPage();

    }

    @And("verifikasi fungsi sort debtor name")
    public void verifikasiFungsiSortDebtorName() {
        approvalFormMaker.verifySortDebtorName();

    }

    @And("verifikasi fungsi sort partner")
    public void verifikasiFungsiSortPartner() {
        approvalFormMaker.verifySortpartner();

    }

    @And("verifikasi fungsi sort date")
    public void verifikasiFungsiSortDate() {
        approvalFormMaker.verifySortDate();

    }

    @And("verifikasi fungsi sort loan amount")
    public void verifikasiFungsiSortLoanAmount() {
        approvalFormMaker.verifySortLoanAmount();

    }

    @And("verifikasi fungsi sort status")
    public void verifikasiFungsiSortStatus() {
        approvalFormMaker.verifySortStatus();

    }

    @And("verifikasi data approval form source")
    public void verifikasiDataApprovalFormSource() {
        approvalFormMaker.verifyDataSource();

    }

    @And("verifikasi data approval form")
    public void verifikasiDataApprovalForm(List<String> list_case) throws InterruptedException {
        approvalFormMaker.verifyDataApprovalForm(list_case);

    }

    @And("verifikasi data approval form detail channeling")
    public void verifikasiDataApprovalFormdetailChanneling(List<String> list_case) throws InterruptedException {
        approvalFormMaker.verifyDataApprovalFormDetail(list_case);

    }

    @And("verifikasi data detail remark approval form")
    public void verifikasiDataDetailRemarkApprovalForm(List<String> list_case) throws InterruptedException {
        approvalFormMaker.verifyDetailRemark(list_case);

    }

    @Then("klik bulk recommendation")
    public void klikBulkRecommendation(List<String> list_case) throws InterruptedException {
        approvalFormMaker.klikBulkRecomendation(list_case);

    }

    @And("klik button back cancel selection approval form")
    public void klikButtonBackCancelSelectionApprovalForm() throws InterruptedException {
        approvalFormMaker.klikButtonBackCancel();

    }

    @And("klik bulk recommendation klik check all")
    public void klikBulkRecommendationKlikCheckAll() {
        approvalFormMaker.klikBulkCheckAll();

    }

    @And("klik bulk recommendation klik check pada data status approved")
    public void klikBulkRecommendationKlikCheckPadaDataStatusApproved() throws InterruptedException {
        approvalFormMaker.klikCheckPadaStatusApproved();

    }

    @And("klik bulk recommendation klik check pada data status rejected")
    public void klikBulkRecommendationKlikCheckPadaDataStatusRejected() throws InterruptedException {
        approvalFormMaker.klikCheckPadaStatusRejected();

    }

    @And("klik bulk recommendation klik check pada data status waiting approval")
    public void klikBulkRecommendationKlikCheckPadaDataStatusWaitingApproval() throws InterruptedException {
        approvalFormMaker.klikCheckPadaStatusWaitingApproval();

    }

    @And("klik bulk recommendation klik check pada beberapa data status waiting for review klik recommendation")
    public void klikBulkRecommendationKlikCheckPadaBeberapaDataStatusWaitingForReviewKlikRecommendation() throws InterruptedException {
        approvalFormMaker.klikBeberapaWaitingForReviewKlikRecomendation();

    }

    @And("klik bulk recommendation klik check pada beberapa data status waiting for review klik not recommendation")
    public void klikBulkRecommendationKlikCheckPadaBeberapaDataStatusWaitingForReviewKlikNotRecommendation() throws InterruptedException {
        approvalFormMaker.klikBeberapaWaitingForReviewKlikNotRecomendation();

    }

    @And("klik debtor name dengan status waiting for review")
    public void klikDebtorNameDenganStatusWaitingForReview() {
        approvalFormMaker.klikDebtorNameWaitingForReview();

    }

    @And("klik debtor name dengan status waiting for review tidak mengisi rekomendasi dan catatan klik submit")
    public void klikDebtorNameDenganStatusWaitingForReviewTidakMengisiRekomendasiDanCatatanKlikSubmit() throws InterruptedException {
        approvalFormMaker.debtorNameWaitingForReviewTidakIsiRekomenDanCatatan();

    }

    @And("klik debtor name dengan status waiting for review mengisi rekomendasi tidak mengisi catatan klik submit")
    public void klikDebtorNameDenganStatusWaitingForReviewMengisiRekomendasiTidakMengisiCatatanklikSubmit() throws InterruptedException {
        approvalFormMaker.debtorNameWaitingForReviewIsiRekomenDanTidakISiCatatan();

    }

    @And("klik debtor name dengan status waiting for review tidak mengisi rekomendasi mengisi catatan klik submit")
    public void klikDebtorNameDenganStatusWaitingForReviewTidakMengisiRekomendasiMengisiCatatanKlikSubmit() throws InterruptedException {
        approvalFormMaker.debtorNameWaitingForReviewTidakIsiRekomenDanISiCatatan();

    }

    @And("klik debtor name dengan status waiting for review mengisi rekomendasi recommended mengisi catatan 5000karakter klik submit")
    public void klikDebtorNameDenganStatusWaitingForReviewMengisiRekomendasiRecommendedMengisiCatatanKarakterKlikSubmit() throws InterruptedException {
        approvalFormMaker.waitingForReviewIsiRekomendasidanIsiCatatan5000Karakter();

    }

    @And("klik debtor name dengan status waiting for review mengisi rekomendasi not recommended mengisi catatan 4999karakter klik submit")
    public void klikDebtorNameDenganStatusWaitingForReviewMengisiRekomendasiNotRecommendedMengisiCatatanKarakterKlikSubmit() throws InterruptedException {
        approvalFormMaker.waitingForReviewIsiRekomendasidanIsiCatatan4999Karakter();

    }

    @And("verifikasi data approval form detail dokumen pendukung upload dokumen> 1file zip")
    public void verifikasiDataApprovalFormDetaildokumenPendukungUploadDokumenFileZip() throws IOException {
        approvalFormMaker.dataDetailDokumenPendukungUploadFileZip();

    }

    @And("verifikasi data approval form detail dokumen pendukung upload dokumen2x isi file berbeda")
    public void verifikasiDataApprovalFormDetaildokumenPendukungUploadDokumenXIsiFileBerbeda() throws IOException {
        approvalFormMaker.dataDetailDokumenPendukungReupload();

    }

    @And("klik bulk recommendation klik check pada data status approval expired")
    public void klikBulkRecommendationKlikCheckPadaDataStatusApprovalExpired() throws InterruptedException {
        approvalFormMaker.checkDataStatusApprovalExpired();

    }

    @Then("klik bulk recommendation check")
    public void klikBulkRecommendationCheck(List<String> list_case) throws IOException, InterruptedException {
        approvalFormMaker.klikBulkRekomendasiCheck(list_case);

    }

    @And("login approver klik bulk approve klik check pada beberapa data status waiting approval")
    public void loginApproverKlikBulkApproveKlikCheckPadaBeberapaDataStatusWaitingApproval() {
        approvalFormApprover.bulkCheckBeberapaStatusWaitingApproval();

    }

    @And("klik bulk approve klik uncheck pada beberapa data status waiting approval")
    public void klikBulkApproveKlikUncheckPadaBeberapaDataStatusWaitingApproval() {
        approvalFormApprover.bulkKlikUncheckBeberapaStatusWaitingApproval();

    }

    @And("klik bulk button back cancel selection approval form")
    public void klikBulkButtonBackCancelSelectionApprovalForm() {
        approvalFormApprover.klikBackSelection();

    }

    @And("klik bulk approve klik check all")
    public void klikBulkApproveKlikCheckAll() {
        approvalFormApprover.bulkKlikCheckALl();

    }

    @And("klik bulk approve klik check pada data status approved")
    public void klikBulkApproveKlikCheckPadaDataStatusApproved() {
        approvalFormApprover.bulkKlikCheckStatusApproved();

    }

    @And("klik bulk approve klik check pada data status rejected")
    public void klikBulkApproveKlikCheckPadaDataStatusRejected() {
        approvalFormApprover.bulkKlikCheckStatusRejected();
    }

    @And("klik bulk approve klik check pada data status waiting approval")
    public void klikBulkApproveKlikCheckPadaDataStatusWaitingApproval() {
        approvalFormApprover.bulkKlikCheckStatusWaitingApproval();

    }

    @And("klik bulk approve klik check pada data status approval expired")
    public void klikBulkApproveKlikCheckPadaDataStatusApprovalExpired() {
        approvalFormApprover.bulkKlikCheckStatusExpired();
    }

    @And("klik bulk approve klik check pada beberapa data status waiting approval klik reject")
    public void klikBulkApproveKlikCheckPadaBeberapaDataStatusWaitingApprovalKlikReject() {
        approvalFormApprover.bulkKlikCheckStatusWaitingApprovalKlikreject();

    }

    @And("klik bulk approve klik check pada beberapa data status waiting approval klik approve recomendation")
    public void klikBulkApproveKlikCheckPadaBeberapaDataStatusWaitingApprovalKlikApproveRecomendation() {
        approvalFormApprover.bulkKlikCheckStatusWaitingApprovalKlikApproveRecomendation();

    }

    @And("klik debtor name dengan status waiting approval approval form")
    public void klikDebtorNameDenganStatusWaitingApprovalApprovalForm() {
        approvalFormApprover.debtorNameWaitingApproval();

    }

    @And("klik debtor name dengan status approval expired")
    public void klikDebtorNameDenganStatusApprovalExpired() throws InterruptedException {
        approvalFormApprover.debtorNameStatusAppvalExpired();

    }

    @And("klik debtor name dengan status approved approval form")
    public void klikDebtorNameDenganStatusApprovedApprovalForm() {
        approvalFormApprover.debtorNameStatusApprovalApproved();

    }

    @And("klik debtor name dengan status rejected approval form")
    public void klikDebtorNameDenganStatusRejectedApprovalForm() {
        approvalFormApprover.debtorNameStatusRejected();

    }

    @And("klik debtor name dengan status waiting approval tidak mengisi keputusan dan keterangan klik submit")
    public void klikDebtorNameDenganStatusWaitingApprovalTidakMengisiKeputusanDanKeteranganKlikSubmit() {
        approvalFormApprover.statusWaitingApprovalTidakKeputusanDanKeterangan();

    }

    @And("klik debtor name dengan status waiting approval mengisi keputusan reject tidak mengisi keterangan klik submit")
    public void klikDebtorNameDenganStatusWaitingApprovalMengisiKeputusanRejectTidakMengisiKeteranganKlikSubmit() {
        approvalFormApprover.statusWaitingApprovalMengisiKeputusanDanTidakIsiKeterangan();

    }

    @And("klik debtor name dengan status waiting approval mengisi keputusan approve tidak mengisi keterangan klik submit")
    public void klikDebtorNameDenganStatusWaitingApprovalMengisiKeputusanApproveTidakMengisiKeteranganKlikSubmit() {
        approvalFormApprover.statusWaitingApprovalIsiKeputusanDanTidakIsiKeterangan();

    }

    @And("klik debtor name dengan status waiting approval tidak mengisi keputusan mengisi keterangan klik submit")
    public void klikDebtorNameDenganStatusWaitingApprovalTidakMengisiKeputusanMengisiKeteranganKlikSubmit() throws InterruptedException {
        approvalFormApprover.statusWaitingApprovalTidakIsiKeputusanDanIsiKeterangan();

    }

    @Then("klik debtor name dengan status waiting approval mengisi keputusan approve mengisi keterangan")
    public void klikDebtorNameDenganStatusWaitingApprovalMengisiKeputusanApproveMengisiKeterangan(List<String> list_case) throws InterruptedException {
        approvalFormApprover.statusWaitingApprovalIsiKeputusanDanIsiKeterangan(list_case);

    }

    @And("verifikasi total")
    public void verifikasiTotal(List<String> list_case) {
        approvalFormApprover.nominalApproveAndSelectAllVerifyTotalNominalApprove(list_case);

    }

    @And("klik bulk recommendation klik check pada beberapa data waiting review")
    public void klikBulkRecommendationKlikCheckPadaBeberapaDataWaitingReview(List<String> list_case) throws InterruptedException {
        approvalFormApprover.verifyBulkRecomendTotalAndUncheckAndCancelSelection(list_case);

    }

    @And("verifikasi klik bulk approve")
    public void verifikasiKlikBulkApprove(List<String> list_case) {
        approvalFormApprover.verifyBulkApproveSelectAllAndCheckBeberapaAndCancelSelection(list_case);

    }

    @And("klik bulk approve check beberapa data waiting approval klik cancel verifikasi total nominal approve")
    public void klikBulkApproveCheckBeberapaDataWaitingApprovalKlikCancelVerifikasiTotalNominalApprove() throws InterruptedException {
        approvalFormApprover.verifyBulkApproveCheckBeberapaCancel();

    }

    @And("klik bulk approve klik check all klik")
    public void klikBulkApproveKlikCheckAllKlik(List<String> list_case) {
        approvalFormApprover.bulkApproveCheckAllRejectApprove(list_case);

    }
//    @And("user bulk recomendation")
//    public void user_bulk_recomendation() throws IOException, InterruptedException {
//        approvalFormMaker.setApproval();
//    }
//
//    @Then("user maker logout application")
//    public void user_maker_logout_application() throws InterruptedException {
//        logout(driver);
//        driver.close();
//    }
//
//    @Given("provide data dummy for bulk user approver")
//    public void provideDataDummyForBulkUserApprover() throws IOException {
//        MoveFileScenario moveFileScenario = new MoveFileScenario();
//        moveFileScenario.dltDummyExist();
//
//        DataCSV dataCSV = new DataCSV();
//        dataCSV.dataDummy(Const.appFile, Const.reaFile, "20");
//    }
//
//    @When("user hit data to los")
//    public void userHitDataToLos() throws InterruptedException {
//        ConsumeData connServer = new ConsumeData();
//        connServer.serverAkses();
//
//        loginMaker(driver);
//        changedRecomendationAsMaker(driver, 15);
//        loginApprover(driver, Const.e2e_user_ldap_approveRecomend, Const.e2e_pwd_ldap_approveRecomend);
//        changedApproveAsApprover(driver, 5);
//    }
//
//    @And("userapprover login application")
//    public void userapprover_login_application() {
//        loginApprover(driver, Const.e2e_user_ldap_approveRealisasi, Const.e2e_pwd_ldap_approveRealisasi);
//
//    }
//
//    @And("user bulk approver")
//    public void userBulkApprover() {
//        approvalFormApprover.setApprovement(driver);
//    }
//
//    @Then("user approver logout application")
//    public void user_approver_logout_application() throws InterruptedException {
//        logout(driver);
//        driver.close();
//        System.out.println("finish");
//    }
}
