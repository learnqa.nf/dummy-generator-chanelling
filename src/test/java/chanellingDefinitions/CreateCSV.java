package chanellingDefinitions;

import createDataCSV.DataCSV;
import io.cucumber.java.en.Given;
import writeFile.UtilsFile;

import java.io.IOException;

public class CreateCSV {

    @Given("generate data {string}{string}")
    public void generate_data(String case1, String case2) throws IOException {

        System.out
                .println("========================================================");
        System.out
                .println("====================GENERATE DATA=======================");
        System.out
                .println("========================================================");
        UtilsFile utilsFile = new UtilsFile();
        utilsFile.dltDummyExist();

        DataCSV dataCSV = new DataCSV();
        dataCSV.dataDummy(case1, case2, "5");
    }
}

