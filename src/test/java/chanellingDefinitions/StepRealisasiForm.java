package chanellingDefinitions;

import com.opencsv.exceptions.CsvException;
import elementsPage.BulkApprovePage;
import elementsPage.DebtorDetailPage;
import elementsPage.MenuRealizationPage;
import elementsPage.ReconsumeReafile;
import io.cucumber.java.Before;
import io.cucumber.java.Scenario;
import io.cucumber.java.en.And;
import io.cucumber.java.en.Given;
import io.cucumber.java.en.Then;
import io.cucumber.java.en.When;
import writeFile.WriteTestData;

import java.io.IOException;
import java.util.Collection;
import java.util.List;


public class StepRealisasiForm {

    //    public String tag;
    MenuRealizationPage menuRealizationPage = new MenuRealizationPage();
    DebtorDetailPage debtorDetailPage = new DebtorDetailPage();
    BulkApprovePage bulkApprovePage = new BulkApprovePage();
    ReconsumeReafile reconsumeReafile = new ReconsumeReafile();

    @When("user akses dan verifikasi tampilan list Realization Form")
    public void user_akses_dan_verifikasi_tampilan_list_Realization_Form() {
        menuRealizationPage.verifikasiLayarUtamaRealizationForm();
    }

    @Then("verifikasi data yang ada di list realization form")
    public void verifikasi_data_yang_ada_di_list_realization_form() throws IOException {
        menuRealizationPage.dataBaruLolosWaitingApproval();

    }

    @And("verifikasi filter dropdown source")
    public void verifikasi_filter_dropdown_source() {
        menuRealizationPage.verifikasiFilterDropdownSource();

    }

    @And("pilih source")
    public void pilih_source() throws InterruptedException {
        menuRealizationPage.pilihSourceAllAndLos();
        menuRealizationPage.pilihSourceNikel();
    }

    @When("verifikasi filter dropdown status")
    public void verifikasi_filter_dropdown_status() {
        menuRealizationPage.klikFieldDropdownStatus();

    }

    @Then("pilih filter status")
    public void pilih_filter_status(List<String> tbl_status) throws InterruptedException {
        menuRealizationPage.pilihStatus(tbl_status);

    }

    @And("verifikasi filter dropdown partner")
    public void verifikasi_filter_dropdown_partner() {
        menuRealizationPage.klikFieldDropdownPartner();

    }

    @Then("pilih filter partner")
    public void pilih_filter_partner() {
        menuRealizationPage.klikPartnerAll();
        menuRealizationPage.klikSpecificPartner();
    }

    @When("verifikasi fungsi search")
    public void verifikasi_fungsi_search() {
        menuRealizationPage.isiFieldSearch();

    }

    @And("klik paging")
    public void klik_paging() throws InterruptedException {
        menuRealizationPage.klikNextPage();
        menuRealizationPage.klikPreviousPage();
        menuRealizationPage.specificPage();
    }

    @Then("sorting kolom")
    public void sorting_kolom() {
        menuRealizationPage.shortDebtorNameColumn();
        menuRealizationPage.shortPartnerColumn();
        menuRealizationPage.shortDateColumn();
        menuRealizationPage.shortLoanAmountColumn();
        menuRealizationPage.shortStatusColumn();
    }

    @And("verifikasi data realization form")
    public void verifikasi_data_realization_form(List<String> data_realization) throws IOException {
        menuRealizationPage.verifyDataRalizationForm(data_realization);

    }

    @Then("verifikasi data realization form detail")
    public void verifikasi_data_realization_form_detail(List<String> data_detail_realization) throws IOException {
        debtorDetailPage.verifyDataRalizationFormDetail(data_detail_realization);

    }

    @And("klik bulk approve and klik check pada beberapa data status waiting approval")
    public void klik_bulk_approve_and_klik_check_pada_beberapa_data_status_waiting_approval() {
        bulkApprovePage.klikCheckBeberapaDataWaitingApproval();

    }

    @And("klik bulk approve and klik uncheck pada beberapa data status waiting approval")
    public void klik_bulk_approve_and_klik_uncheck_pada_beberapa_data_status_waiting_approval() {
        bulkApprovePage.klikUncheckBeberapaDataWaitingApproval();
    }

    @Then("klik button back cancel selection")
    public void klik_button_back_cancel_selection() throws InterruptedException {
        bulkApprovePage.klikBtnCancelSelection();
    }

    @Then("klik bulk approve and klik check all")
    public void klik_bulk_approve_and_klik_check_all() {
        bulkApprovePage.radioBtnCheckAll();
    }

    @When("klik bulk approve then verifikasi data status")
    public void klik_bulk_approve_then_verifikasi_data_status(List<String> statusData) {
        bulkApprovePage.verifyDataTidakTampilBerdasarkanStatus(statusData);
    }

    @And("klik bulk approve klik check pada beberapa data status waiting approval klik approve")
    public void klik_bulk_approve_klik_check_pada_beberapa_data_status_waiting_approval_klik_approve() throws InterruptedException {
        bulkApprovePage.klikApprovePadaBeberapaDataWaitingApproval();
    }

    @And("klik bulk approve klik check all klik approve")
    public void klik_bulk_approve_klik_check_all_klik_approve() throws InterruptedException, IOException {
        bulkApprovePage.klikRadioBtnAllApprove();
    }

    @And("klik debtor name dengan status waiting approval")
    public void klik_debtor_name_dengan_status_waiting_approval() {
        debtorDetailPage.klikDebtorNameVerifyDataWaitingApproval();
    }


    @And("klik debtor name dengan status approved")
    public void klik_debtor_name_dengan_status_approved() throws InterruptedException {
        debtorDetailPage.klikDebtorNameVerifyDataApproved();
    }

    @And("klik debtor name dengan status rejected")
    public void klik_debtor_name_dengan_status_rejected() throws InterruptedException {
        debtorDetailPage.klikDebtorNameVerifyDataRejected();
    }

    @When("klik debtor name dengan status waiting approval klik approve popup")
    public void klik_debtor_name_dengan_status_waiting_approval_klik_approve_popup() {
        debtorDetailPage.klikDebtorNameApproveVerifyPopUp();
    }

    @Then("klik debtor name dengan status waiting approval Klik approve klik tidak")
    public void klik_debtor_name_dengan_status_waiting_approval_Klik_approve_klik_tidak() {
        debtorDetailPage.klikDebtorNameApproveVerifyPopUpTidak();
    }

    @And("klik debtor name dengan status waiting approval Klik approve klik ya")
    public void klik_debtor_name_dengan_status_waiting_approval_Klik_approve_klik_ya() {
        debtorDetailPage.klikDebtorNameApproveVerifyPopUpYa();
    }

    @And("klik debtor name dengan status waiting approval klik reject")
    public void klik_debtor_name_dengan_status_waiting_approval_klik_reject() throws InterruptedException {
        debtorDetailPage.klikDebtorNameRejectVerifyPopUp();
    }

    @When("klik debtor name dengan status Waiting approval klik reject pilih type reject reason dokumen pendukung tidak sesuai description diisi karakter")
    public void klik_debtor_name_dengan_status_Waiting_approval_klik_reject_pilih_type_reject_reason_dokumen_pendukung_tidak_sesuai_description_diisi_karakter() throws InterruptedException {
        debtorDetailPage.klikDebtorNameReject100Char();
        debtorDetailPage.klikDebtorNameReject99Char();
        debtorDetailPage.klikDebtorNameRejectEmptyChar();
    }

    @Then("verifikasi Notifikasi setelah file masuk")
    public void verifikasi_Notifikasi_setelah_file_masuk() {
        debtorDetailPage.verifyNotification();

    }

    @Then("verifikasi Total Nominal Approve")
    public void verifikasi_Total_Nominal_Approve() throws InterruptedException {
        menuRealizationPage.verifyTotalNominalApproved();
    }

    @When("klik bulk approve verifikasi total nominal approve")
    public void klikBulkApproveVerifikasiTotalNominalApprove() throws InterruptedException {
        bulkApprovePage.verifyBulkTotalNominalApproved();
        bulkApprovePage.verifyBulkCheckBeberapaWaitingApproval();
        bulkApprovePage.verifyBulkCheckAndUncheckWaitingApproval();
        bulkApprovePage.verifyTotalNominalApprovedAfterCancel();
    }


    @And("consume reafile dengan no aplikasi yang sudah ada di LOS dan terdapat field lain yang beda mis alamat status waiting approval")
    public void consume_reafile_dengan_no_aplikasi_yang_sudah_ada_di_LOS_dan_terdapat_field_lain_yang_beda_mis_alamat_status_waiting_approval() {
        reconsumeReafile.reconsumeReafileUpdateAlamatWaitingApproval();
    }

    @And("verifikasi data realization expired")
    public void verifikasiDataRealizationExpired() throws InterruptedException {
        menuRealizationPage.pilihFilterStatusExpired();
        debtorDetailPage.klikDebtorNameVerifyDataApprovalExpired();
        menuRealizationPage.dataBaruLolosMelebihiWaktuStatusExpired();

    }

    @And("consume reafile dengan no aplikasi yang sudah ada di LOS dan terdapat field lain yang beda mis alamat status approval Expired")
    public void consume_reafile_dengan_no_aplikasi_yang_sudah_ada_di_LOS_dan_terdapat_field_lain_yang_beda_mis_alamat_status_approval_Expired() {
        reconsumeReafile.reconsumeReafileUpdateAlamatApprovalExpired();

    }

    @And("consume reafile dengan no aplikasi yang sudah ada di LOS dan terdapat field lain yang beda mis alamat status rejected")
    public void consumeReafileDenganNoAplikasiYangSudahAdaDiLOSDanTerdapatFieldLainYangBedaMisAlamatStatusRejected() {
        reconsumeReafile.reconsumeReafileUpdateAlamatRejected();
    }

    @And("consume reafile dengan no aplikasi yang sudah ada di LOS dan terdapat field lain yang beda mis alamat status pending batch run")
    public void consume_reafile_dengan_no_aplikasi_yang_sudah_ada_di_LOS_dan_terdapat_field_lain_yang_beda_mis_alamat_status_pending_batch_run() {
        reconsumeReafile.reconsumeReafileUpdateAlamatPendingBatchRun();
    }

    @And("consume reafile dengan no aplikasi yang sudah ada di LOS dan terdapat field lain yang beda mis alamat status approved")
    public void consume_reafile_dengan_no_aplikasi_yang_sudah_ada_di_LOS_dan_terdapat_field_lain_yang_beda_mis_alamat_status_approved() {
        reconsumeReafile.reconsumeReafileUpdateAlamatApproved();
    }


    @And("verifikasi data realization pending")
    public void verifikasiDataRealizationPending() throws InterruptedException {
        menuRealizationPage.pilihFilterStatusPending();
        debtorDetailPage.klikDebtorNameVerifyDataPending();
        menuRealizationPage.dataBaruLolosMelebihiLimitStatusPending();

    }

    @And("consume reafile dengan no aplikasi yang sudah ada di LOS dan terdapat field lain yang beda mis alamat status pending")
    public void consume_reafile_dengan_no_aplikasi_yang_sudah_ada_di_LOS_dan_terdapat_field_lain_yang_beda_mis_alamat_status_pending() {
        reconsumeReafile.reconsumeReafileUpdateAlamatPending();

    }


}
