package chanellingDefinitions;


import elementsPage.RealisasiFilePage;
import io.cucumber.java.en.And;
import io.cucumber.java.en.Given;
import io.cucumber.java.en.Then;
import io.cucumber.java.en.When;

import java.io.IOException;

public class StepValidationRealisasilFile {

    RealisasiFilePage reaFile = new RealisasiFilePage();

    @Given("user akses realisasi LOS application")
    public void userAksesRealisasiLOSApplication() throws IOException {
        reaFile.login();
    }

    @When("input realisasi no aplikasi 17karakter varchar")
    public void inputRealisasiNoAplikasi17KarakterVarchar() {
        reaFile.noAplikasi17karakter();

    }

    @And("input realisasi nama debitur")
    public void input_realisasi_nama_debitur() {
        reaFile.namaDebitur40Karakter();
        reaFile.namaDebitur39Karakter();
    }

    @And("input jenis debitur P")
    public void input_jenis_debitur_P() {
        reaFile.jenisDebitur();
    }

    @And("input gender code")
    public void input_gender_code() {
        reaFile.genderCodeL();
        reaFile.genderCodeP();
    }

    @And("input realisasi NIK 16karakter numerik")
    public void input_realisasi_NIK_16karakter_numerik() {
        reaFile.nik16Karakter();
    }

    @And("input realsiasi NPWP")
    public void input_realsiasi_NPWP() {
        reaFile.npwp15Karakter();
        reaFile.npwpKosong();
    }

    @And("input alamat")
    public void input_alamat() {
        reaFile.alamat50Karakter();
        reaFile.alamat49Karakter();
    }

    @And("input alamat kelurahan")
    public void input_alamat_kelurahan() {
        reaFile.alamatKelurahan40Karakter();
        reaFile.alamatKelurahan39Karakter();
    }

    @And("input alamat kecamatan")
    public void input_alamat_kecamatan() {
        reaFile.alamatKecamatan40Karakter();
        reaFile.alamatKecamatan39Karakter();
    }

    @And("input alamat kode pos")
    public void input_alamat_kode_pos() {
        reaFile.alamatKodepos();

    }

    @And("input alamat kode dati II")
    public void input_alamat_kode_dati_II() {
        reaFile.alamatDatiII();

    }

    @And("input provinsi")
    public void input_provinsi() {
        reaFile.province25Karakter();
        reaFile.province24Karakter();
    }

    @And("input no telepon")
    public void input_no_telepon() {
        reaFile.noTelepon13Karakter();
        reaFile.noTelepon12Karakter();

    }

    @And("input mobile phone number")
    public void input_mobile_phone_number() {
        reaFile.MobilePhoneNumber20Karakter();
        reaFile.mobilePhoneNumber19Karakter();
    }

    @And("input email")
    public void input_email() {
        reaFile.email50Karakter();
        reaFile.email49Karakter();
    }

    @And("input place of birth")
    public void input_place_of_birth() {
        reaFile.placeOfBirth50Karakter();
        reaFile.placeOfBirth49Karakter();
    }

    @And("input tanggal lahir debitur")
    public void input_tanggal_lahir_debitur() {
        reaFile.tanggalLahirDebitur();
    }

    @And("input last education")
    public void input_last_education() {
        reaFile.lastEducation2Karakter();
        reaFile.lastEducationKosong();
    }

    @And("input employer")
    public void input_employer() {
        reaFile.employer50Karakter();
        reaFile.employer49Karakter();
    }

    @And("input employer address")
    public void input_employer_address() {
        reaFile.employerAddress200Karakter();
        reaFile.employerAddress199Karakter();
    }

    @And("input mother maiden")
    public void input_mother_maiden() {
        reaFile.motherMaiden50Karakter();
        reaFile.motherMaiden49Karakter();
    }

    @And("input religion satu karakter numerik")
    public void input_religion_satu_karakter_numerik() {
        reaFile.religion1Karakter();
    }

    @And("input penghasilan kotor per bulan")
    public void input_penghasilan_kotor_per_bulan() {
        reaFile.penghasilanKotorPerBulan();
    }

    @And("input marital status")
    public void input_marital_status() {
        reaFile.maritalStatus1Karakter();
    }

    @And("input nama pasangan")
    public void input_nama_pasangan() {
        reaFile.namaPasangan40Karakter();
        reaFile.namaPasangan39Karakter();
        reaFile.namaPasanganKosong();
    }

    @And("input tanggal lahir pasangan")
    public void input_tanggal_lahir_pasangan() {
        reaFile.tanggalLahirPasangan8Karakter();
        reaFile.tanggalLahirPasanganKosong();
    }

    @And("input perjanjian pisah harta")
    public void input_perjanjian_pisah_harta() {
        reaFile.perjanjianPisahHarta1Karakter();
        reaFile.perjanjianPisahHarta1Kosong();
    }

    @And("input no akte")
    public void input_no_akte() {
        reaFile.noAkte30Karakter();
        reaFile.noAkte29Karakter();
        reaFile.noAkteKosong();
    }

    @And("input tanggal berdiri")
    public void input_tanggal_berdiri() {
        reaFile.tanggalBerdiri8Karakter();
        reaFile.tanggalBerdiriKosong();
    }

    @And("input no akte terahir")
    public void input_no_akte_terahir() {
        reaFile.noAkteTerakhir30Karakter();
        reaFile.noAkteTerakhir29Karakter();
        reaFile.noAkteTerakhirKosong();
    }

    @And("input tanggal akte terahir")
    public void input_tanggal_akte_terahir() {
        reaFile.tanggalAkteTerakhir8Karakter();
        reaFile.tanggalAkteTerakhirKosong();
    }

    @And("input bidang usaha")
    public void input_bidang_usaha() {
        reaFile.bidangUsaha6Karakter();
    }

    @And("input jangka waktu")
    public void input_jangka_waktu() {
        reaFile.jangkaWaktu3Karakter();
    }

    @And("input jenis kredit")
    public void input_jenis_kredit() {
        reaFile.jenisKredit3Karakter();
    }

    @And("input realisasi plafon")
    public void input_realisasi_plafon() {
        reaFile.plafon15Karakter();
    }

    @And("input realisasi interest rate")
    public void input_realisasi_interest_rate() {
        reaFile.interestRate5Karakter();
    }

    @And("input no pk")
    public void input_no_pk() {
        reaFile.noPk100Karakter();
        reaFile.noPk99Karakter();
    }

    @And("input tanggal akad")
    public void input_tanggal_akad() {
        reaFile.tanggalAkad8Karakter();
    }

    @And("input tanggal angsuran pertama")
    public void input_tanggal_angsuran_pertama() {
        reaFile.tanggalAngsuran1_8Karakter();
    }

    @And("input jenis penggunaan")
    public void input_jenis_penggunaan() {
        reaFile.jenisPenggunaan1Karakter();
    }

    @And("input sektor ekonomi")
    public void input_sektor_ekonomi() {
        reaFile.sektorEkonomi6Karakter();
    }

    @And("input omzet")
    public void input_omzet() {
        reaFile.omzet17Karakter();
        reaFile.omzetKosong();
    }

    @And("input go public")
    public void input_go_public() {
        reaFile.goPublic1Karakter();
        reaFile.goPublicKosong();
    }

    @And("input sandi golongan")
    public void input_sandi_golongan() {
        reaFile.sandiGolongan();
    }

    @And("input penghasilan kotor per tahun")
    public void input_penghasilan_kotor_per_tahun() {
        reaFile.penghasilanKotorPerTahun();
    }

    @And("input bentuk badan usaha")
    public void input_bentuk_badan_usaha() {
        reaFile.bentukBadanUsahaSesuaiList();
        reaFile.bentukBadanUsahaKosong();
    }

    @And("input tempat berdiri badan usaha")
    public void input_tempat_berdiri_badan_usaha() {
        reaFile.tempatBerdiriBadanUsaha30Karakter();
        reaFile.tempatBerdiriBadanUsaha29Karakter();
        reaFile.tempatBerdiriBadanUsahaKosong();
    }

    @And("input disbursement date")
    public void input_disbursement_date() {
        reaFile.disbursement8KarakterCurrentDate();
        reaFile.disbursement8KarakterPlus1Hari();
    }

    @And("input tenor")
    public void input_tenor() {
        reaFile.tenor3Karakter();
    }

    @And("input segmentasi debitur")
    public void input_segmentasi_debitur() {
        reaFile.segmentasiDebitur2Karakter();
    }

    @And("input kode pekerjaan")
    public void input_kode_pekerjaan() {
        reaFile.kodePekerjaan3Karakter();
    }

    @And("input debtor category")
    public void input_debtor_category() {
        reaFile.debtorCategory2Karakter();
    }

    @And("input income source")
    public void input_income_source() {
        reaFile.incomeSource1Karakter();
    }

    @Then("input jumlah tanggungan")
    public void input_jumlah_tanggungan() {
        reaFile.jumlahTanggungan2Karakter();
        reaFile.jumlahTanggunganKosong();
    }
}
