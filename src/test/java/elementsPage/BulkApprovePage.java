package elementsPage;


import capture.TakeSreenshot;
import com.aventstack.extentreports.Status;
import com.aventstack.extentreports.markuputils.MarkupHelper;
import individu.LibUtils;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import runner.FormRealisasiRunner;
import testData.ReadTestData;

import java.io.IOException;
import java.util.List;

import static individu.LibUtils.*;
import static runner.FormRealisasiRunner.extent;
import static runner.FormRealisasiRunner.extent_test_case;

public class BulkApprovePage extends BaseAction {

    public static final By menuRealization = By.xpath("//div[normalize-space()='Realization']");
    public static final By tbl_realizationForm_debtorname = By.xpath("//*[@id='channelingTable']/tr[1]/td[4]/a");
    public static final By bulk_search = By.xpath("//*[@id=\"searchInput\"]");
    public static final By txt_Search_form = By.xpath("//*[@id='myInput']");
    public static final By txt_search_bulk = By.xpath("//*[@id=\"searchInput\"]");
    public static final By btnBulkApprove = By.linkText("Bulk Approve");
    public static final By list_partner = By.xpath("//*[@id=\"select2-filterSource-container\"]");
    public static final By list_partnerKopnus = By.xpath("//*[starts-with(@id, 'select2-filterSource-result-') and contains(@id,'-KOPNUS')]");
    public static final By list_status = By.xpath("//*[@id='select2-filterStatus-container']");
    public static final By list_statusPending = By.xpath("//*[starts-with(@id, 'select2-filterStatus-result-') and contains(@id,'-Pending')]");
    public static final By list_statusWaitingApproval = By.xpath("//*[starts-with(@id, 'select2-filterStatus-result-') and contains(@id,'-Waiting Approval')]");
    public static final By list_statusApproved = By.xpath("//*[starts-with(@id, 'select2-filterStatus-result-') and contains(@id,'-Approved')]");
    public static final By list_statusRejected = By.xpath("//*[starts-with(@id, 'select2-filterStatus-result-') and contains(@id,'-Rejected')]");
    public static final By tbl_realizationForm_applicationNo = By.xpath("//*[@id=\"channelingTable\"]/tr[1]/td[3]/a");
    public static final By radio_check_all = By.xpath("//label[@for='selectAll']");
    public static final By btn_cancelSelection = By.xpath("/html/body/div[2]/div[2]/div/div[1]/div/div[1]/div/a/img");
    public static final By btn_bulkApprove = By.xpath("//*[@id=\"btnSubmitRealization\"]");
    public static final By bulk_linkDebtorName = By.xpath("//*[@id=\"channelingTable\"]/tr[1]/td[5]/a");
    public static final By next_page = By.xpath("//*[@id='realizationTable_next']/a");
    public static final By noDataTable = By.xpath("//td[contains(.,'No matching records found')]");

    DebtorDetailPage debtorDetailPage = new DebtorDetailPage();

    int iRowPictName;

    WebDriver driver = FormRealisasiRunner.driver;
    ReadTestData readTestData = new ReadTestData();
    TakeSreenshot takeSreenshot = new TakeSreenshot();

    public void klikCheckBeberapaDataWaitingApproval() {
        iRowPictName = 87;
        createTest(iRowPictName, extent_test_case, extent);
        readTestData.testData();

        click(driver, menuRealization);
        driver.findElement(btnBulkApprove).click();
        driver.findElement(bulk_search).sendKeys(readTestData.timeStamp);
        driver.findElement(list_partner).click();
        LibUtils.scrollIntoView(driver, list_partnerKopnus);
        driver.findElement(list_partnerKopnus).click();
        driver.findElement(list_status).click();
        driver.findElement(list_statusWaitingApproval).click();

        for (int x = 1; x <= 3; x++) {
            driver.findElement(By.xpath("//*[@id='channelingTable']/tr[" + x + "]/td[1]/div/label")).click();

            if (x == 3) {
                LibUtils.actualLogTrue(String.valueOf(LibUtils.boolean_isDisplayed(driver, By.xpath("//*[@id='channelingTable']/tr[" + x + "]/td[1]/div/label"))));
            }
        }

        takeSreenshot.capture(driver);
        status_testCase(iRowPictName, true, "");
    }

    public void klikUncheckBeberapaDataWaitingApproval() {
        iRowPictName = 88;
        createTest(iRowPictName, extent_test_case, extent);
        takeSreenshot.capture(driver);

        for (int x = 3; x > 1; x--) {
            driver.findElement(By.xpath("//*[@id='channelingTable']/tr[" + x + "]/td[1]/div/label")).click();
            if (x == 2) {
                LibUtils.actualLogTrue(String.valueOf(LibUtils.boolean_isDisplayed(driver, By.xpath("//*[@id='channelingTable']/tr[" + x + "]/td[1]/div/label"))));
            }
        }
        takeSreenshot.capture(driver);
        status_testCase(iRowPictName, true, "");
    }

    public void klikBtnCancelSelection() throws InterruptedException {
        iRowPictName = 89;
        createTest(iRowPictName, extent_test_case, extent);
        driver.findElement(btn_cancelSelection).click();
        Thread.sleep(4000);
        LibUtils.actualLogTrue(String.valueOf(LibUtils.boolean_isDisplayed(driver, tbl_realizationForm_debtorname)));

        takeSreenshot.capture(driver);
        status_testCase(iRowPictName, true, "");
    }

    public void radioBtnCheckAll() {
        iRowPictName = 90;
        createTest(iRowPictName, extent_test_case, extent);

        driver.findElement(btnBulkApprove).click();
        driver.findElement(bulk_search).sendKeys(readTestData.timeStamp);
        driver.findElement(list_status).click();
        driver.findElement(list_statusWaitingApproval).click();
        LibUtils.waitElementVisible(driver, bulk_linkDebtorName, Const.seconds);
        driver.findElement(radio_check_all).click();
        LibUtils.actualLogTrue(String.valueOf(LibUtils.boolean_isDisplayed(driver, radio_check_all)));
        takeSreenshot.capture(driver);
        status_testCase(iRowPictName, true, "");
    }

    public void verifyDataTidakTampilBerdasarkanStatus(List<String> statusData) {


        driver.navigate().refresh();
        iRowPictName = 90;
        int iSeq = 1;
        for (int i = 0; i < statusData.size(); i++) {
            iRowPictName = iRowPictName + iSeq;
            createTest(iRowPictName, extent_test_case, extent);

            String _sdata = statusData.get(i);
            switch (i) {
                case 0:

                    driver.findElement(bulk_search).sendKeys(readTestData.timeStamp);
                    driver.findElement(list_status).click();
                    LibUtils.waitElementVisible(driver, list_statusApproved, Const.seconds);
                    driver.findElement(list_statusApproved).click();
                    LibUtils.verifyElementExist(driver, noDataTable);
                    takeSreenshot.capture(driver);
                    status_testCase(iRowPictName, true, "");
                    break;
                case 1:

                    driver.findElement(list_status).click();
                    LibUtils.scrollIntoView(driver, list_statusRejected);
                    LibUtils.waitElementVisible(driver, list_statusRejected, Const.seconds);
                    driver.findElement(list_statusRejected).click();
                    LibUtils.verifyElementExist(driver, noDataTable);
                    takeSreenshot.capture(driver);
                    status_testCase(iRowPictName, true, "");
                    break;
                case 2:

                    driver.findElement(list_status).click();
                    LibUtils.waitElementVisible(driver, list_statusPending, Const.seconds);
                    driver.findElement(list_statusPending).click();
                    LibUtils.verifyElementExist(driver, noDataTable);
                    takeSreenshot.capture(driver);
                    status_testCase(iRowPictName, true, "");
                    break;
                case 3:

                    driver.findElement(list_status).click();
                    LibUtils.waitElementVisible(driver, list_statusWaitingApproval, Const.seconds);
                    driver.findElement(list_statusWaitingApproval).click();
                    LibUtils.actualLogTrue(String.valueOf(LibUtils.boolean_isDisplayed(driver, bulk_linkDebtorName)));
                    takeSreenshot.capture(driver);
                    status_testCase(iRowPictName, true, "");
                    break;
            }
        }
    }

    public void klikApprovePadaBeberapaDataWaitingApproval() throws InterruptedException {
        iRowPictName = 95;
        createTest(iRowPictName, extent_test_case, extent);

        driver.navigate().refresh();
        Thread.sleep(3000);
        driver.findElement(list_status).click();
        driver.findElement(list_statusWaitingApproval).click();
        driver.findElement(bulk_search).sendKeys(readTestData.timeStamp);
        // waiting for object
        String coll_debtorName = null;
        String debtorName;
        for (int x = 1; x < 3; x++) {
            driver.findElement(By.xpath("//*[@id='channelingTable']/tr[" + x + "]/td[1]/div/label")).click();
            debtorName = driver.findElement(By.xpath(" //*[@id='channelingTable']/tr[" + x + "]/td[4]")).getText();
            coll_debtorName = coll_debtorName + "|" + debtorName;
        }

        takeSreenshot.capture(driver);
        driver.findElement(btn_bulkApprove).click();
        Thread.sleep(5000);
        takeSreenshot.capture(driver);
        String[] str_debtorname = coll_debtorName.split("\\|");
        for (int i = 1; i <= 2; i++) {
            writeText(driver, txt_search_bulk, str_debtorname[i]);
            LibUtils.actualLogTrue(str_debtorname[i]);
            takeSreenshot.capture(driver);
            driver.findElement(txt_search_bulk).clear();
        }

        status_testCase(iRowPictName, true, "");
    }

    public void klikRadioBtnAllApprove() throws InterruptedException, IOException {
        iRowPictName = 96;
        createTest(iRowPictName, extent_test_case, extent);

        click(driver, menuRealization);
        driver.findElement(btnBulkApprove).click();
        LibUtils.waitElementVisible(driver, bulk_linkDebtorName, Const.seconds);
        driver.findElement(radio_check_all).click();
        //span[@id='selectedItem']
        Thread.sleep(Const.delay);
        takeSreenshot.capture(driver);

        driver.findElement(btn_bulkApprove).click();

        //waiting for object
        Thread.sleep(6000);
        driver.findElement(list_status).click();
        driver.findElement(list_statusWaitingApproval).click();
        LibUtils.verifyElementExist(driver, noDataTable);// get text
        takeSreenshot.capture(driver);
        status_testCase(iRowPictName, true, "");

        provideDataToLOS(driver, "10");// reconsume data to los

    }

    public void verifyBulkTotalNominalApproved() throws InterruptedException {

        iRowPictName = 109;
        createTest(iRowPictName, extent_test_case, extent);


        click(driver, menuRealization);
        driver.findElement(btnBulkApprove).click();
        driver.findElement(list_status).click();
        driver.findElement(list_statusWaitingApproval).click();
        LibUtils.waitElementVisible(driver, bulk_linkDebtorName, Const.seconds);
        driver.findElement(radio_check_all).click();
        String totalNominalApproved = driver.findElement(By.id("totalNominalApproved")).getAttribute("value").replace("Rp.", "").replace(".", "").trim();

        int totalLoanAmount = 0;
        String loanAmount;
        String coll_loanAmount = null;
        try {
            while (true) {
                for (int idx = 1; idx <= 25; idx++) {
                    loanAmount = driver.findElement(By.xpath("//*[@id='channelingTable']/tr[" + idx + "]/td[8]")).getText();
                    coll_loanAmount = coll_loanAmount + "|" + loanAmount;
                    takeSreenshot.capture(driver);
                    if (idx == 25) {

                        takeSreenshot.capture(driver);
                        Thread.sleep(500);
                        LibUtils.scrollIntoView(driver, next_page);

                        takeSreenshot.capture(driver);
                        driver.findElement(next_page).click();
                    }
                }
            }
        } catch (Exception ignored) {
            String[] str_loanAmount = coll_loanAmount.split("\\|");
            LibUtils.codeBlockText("Actual status Approved " + coll_loanAmount.replace("null", ""));
            for (int x = 1; x < str_loanAmount.length; x++) {
                totalLoanAmount = totalLoanAmount + Integer.parseInt(str_loanAmount[x].replace("Rp ", "").replace(".", "").replace(",00", "").trim());
            }
        }
        boolean _sts = verifyValue(totalNominalApproved, String.valueOf(totalLoanAmount));
        LibUtils.capture.log(Status.PASS, MarkupHelper.createUnorderedList(List.of(
                "Total nominal approved: " + totalNominalApproved,
                "Jumlah status approved: " + String.valueOf(totalLoanAmount),
                "Status validasi: " + String.valueOf(_sts))).getMarkup());

        status_testCase(iRowPictName, _sts, "Total Nominal Approved: " + totalNominalApproved + " >>>>> " + "Total penjumlahan loan amount di table: " + totalLoanAmount);

        System.out.println("Total Nominal Approver >>>>>>>>>>>>>>>>>>>> " + totalNominalApproved);
        System.out.println("Total penjumlahan loan amount di table >>>> " + totalLoanAmount);
    }

    public void verifyBulkCheckBeberapaWaitingApproval() {
        iRowPictName = 110;
        createTest(iRowPictName, extent_test_case, extent);

        click(driver, menuRealization);
        driver.findElement(btnBulkApprove).click();
        driver.findElement(list_status).click();
        driver.findElement(list_statusWaitingApproval).click();
        LibUtils.waitElementVisible(driver, bulk_linkDebtorName, Const.seconds);

        String totalNominalApproved;
        int totalLoanAmount = 0;
        String loanAmount;
        String coll_loanAmount = null;

        for (int idx = 1; idx <= 2; idx++) {
            driver.findElement(By.xpath("//*[@id='channelingTable']/tr[" + idx + "]/td[1]/div/label")).click();
            loanAmount = driver.findElement(By.xpath("//*[@id='channelingTable']/tr[" + idx + "]/td[8]")).getText();
            coll_loanAmount = coll_loanAmount + "|" + loanAmount;
        }
//        LibUtils.scrollIntoView(driver, next_page);

        takeSreenshot.capture(driver);
        totalNominalApproved = driver.findElement(By.id("totalNominalApproved")).getAttribute("value").replace("Rp.", "").replace(".", "").trim();

        String[] str_loanAmount = coll_loanAmount.split("\\|");
        for (int x = 1; x < str_loanAmount.length; x++) {
            totalLoanAmount = totalLoanAmount + Integer.parseInt(str_loanAmount[x].replace("Rp ", "").replace(".", "").replace(",00", "").trim());
        }
        boolean _sts = verifyValue(totalNominalApproved, String.valueOf(totalLoanAmount));
        LibUtils.codeBlockText("Actual status waiting approval " + coll_loanAmount.replace("null", ""));
        LibUtils.capture.log(Status.PASS, MarkupHelper.createUnorderedList(List.of(
                "Total nominal approved: " + totalNominalApproved,
                "Jumlah status approved: " + String.valueOf(totalLoanAmount),
                "Status validasi: " + String.valueOf(_sts))).getMarkup());

        status_testCase(iRowPictName, _sts, "Total Nominal Approved: " + totalNominalApproved + " >>>>> " + "Total penjumlahan loan amount di table: " + totalLoanAmount);

        System.out.println("Total Nominal Approver >>>>>>>>>>>>>>>>>>>> " + totalNominalApproved);
        System.out.println("Total penjumlahan loan amount di table >>>> " + totalLoanAmount);
    }

    public void verifyBulkCheckAndUncheckWaitingApproval() {
        iRowPictName = 111;
        createTest(iRowPictName, extent_test_case, extent);

        click(driver, menuRealization);
        driver.findElement(btnBulkApprove).click();
        driver.findElement(list_status).click();
        driver.findElement(list_statusWaitingApproval).click();
        LibUtils.waitElementVisible(driver, bulk_linkDebtorName, Const.seconds);

        String totalNominalApproved;
        int totalLoanAmount = 0;
        String loanAmount;
        String coll_loanAmount = null;
        for (int idx = 1; idx <= 4; idx++) {
            driver.findElement(By.xpath("//*[@id='channelingTable']/tr[" + idx + "]/td[1]/div/label")).click();
        }
        takeSreenshot.capture(driver);

        for (int idx = 1; idx <= 2; idx++) {
            loanAmount = driver.findElement(By.xpath("//*[@id='channelingTable']/tr[" + idx + "]/td[8]")).getText();
            coll_loanAmount = coll_loanAmount + "|" + loanAmount;
        }
        for (int idx = 4; idx >= 3; idx--) {
            driver.findElement(By.xpath("//*[@id='channelingTable']/tr[" + idx + "]/td[1]/div/label")).click();
        }

        takeSreenshot.capture(driver);
        totalNominalApproved = driver.findElement(By.id("totalNominalApproved")).getAttribute("value").replace("Rp.", "").replace(".", "").trim();

        String[] str_loanAmount = coll_loanAmount.split("\\|");
        for (int x = 1; x < str_loanAmount.length; x++) {
            totalLoanAmount = totalLoanAmount + Integer.parseInt(str_loanAmount[x].replace("Rp ", "").replace(".", "").replace(",00", "").trim());
        }
        boolean _sts = verifyValue(totalNominalApproved, String.valueOf(totalLoanAmount));
        LibUtils.codeBlockText("Actual status waiting approval " + coll_loanAmount.replace("null", ""));
        LibUtils.capture.log(Status.PASS, MarkupHelper.createUnorderedList(List.of(
                "Total nominal approved: " + totalNominalApproved,
                "Jumlah status approved: " + String.valueOf(totalLoanAmount),
                "Status validasi: " + String.valueOf(_sts))).getMarkup());

        status_testCase(iRowPictName, _sts, "Total Nominal Approved: " + totalNominalApproved + " >>>>> " + "Total penjumlahan loan amount di table: " + totalLoanAmount);

        System.out.println("Total Nominal Approver >>>>>>>>>>>>>>>>>>>> " + totalNominalApproved);
        System.out.println("Total penjumlahan loan amount di table >>>> " + totalLoanAmount);
    }

    public void verifyTotalNominalApprovedAfterCancel() {
        iRowPictName = 112;
        createTest(iRowPictName, extent_test_case, extent);

        click(driver, menuRealization);
        LibUtils.waitElementVisible(driver, tbl_realizationForm_debtorname, Const.seconds);
        String totalAllNominalApproved1 = driver.findElement(By.xpath("//input[@id='totalNominalPinjaman']")).getAttribute("value").replace("Rp.", "").replace(".", "").trim();
        takeSreenshot.capture(driver);

        driver.findElement(btnBulkApprove).click();
        driver.findElement(list_status).click();
        driver.findElement(list_statusWaitingApproval).click();
        LibUtils.waitElementVisible(driver, bulk_linkDebtorName, Const.seconds);

        String totalNominalApproved;
        int totalLoanAmount = 0;
        String loanAmount;
        String coll_loanAmount = null;
        for (int idx = 1; idx <= 3; idx++) {
            driver.findElement(By.xpath("//*[@id='channelingTable']/tr[" + idx + "]/td[1]/div/label")).click();
        }
        takeSreenshot.capture(driver);

        driver.findElement(btn_cancelSelection).click();
        LibUtils.waitElementVisible(driver, tbl_realizationForm_debtorname, Const.seconds);
        String totalAllNominalApproved2 = driver.findElement(By.xpath("//input[@id='totalNominalPinjaman']")).getAttribute("value").replace("Rp.", "").replace(".", "").trim();
        takeSreenshot.capture(driver);
        boolean _sts = verifyValue(totalAllNominalApproved1, String.valueOf(totalAllNominalApproved2));
        LibUtils.capture.log(Status.PASS, MarkupHelper.createUnorderedList(List.of(
                "Expectation Total nominal approved: " + totalAllNominalApproved1,
                "Actual Total nominal approved after cancel: " + totalAllNominalApproved2,
                "Status validasi: " + String.valueOf(_sts))).getMarkup());

        status_testCase(iRowPictName, _sts, "Total Nominal Approved Before: " + totalAllNominalApproved1 + " >>>>> " + "Total Nominal Approved After: " + totalAllNominalApproved2);

        System.out.println("Total Nominal Approver >>>>>>>>>>>>>>>>>>>> " + totalAllNominalApproved1);
        System.out.println("Total penjumlahan loan amount di table >>>> " + totalAllNominalApproved2);
    }
}
