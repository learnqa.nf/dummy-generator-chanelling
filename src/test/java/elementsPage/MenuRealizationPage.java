package elementsPage;

import applos.ExecutionJob;
import capture.TakeSreenshot;
import com.aventstack.extentreports.Status;
import com.aventstack.extentreports.markuputils.MarkupHelper;
import individu.LibUtils;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import readFile.ReadCSVFormRealization;
import readFile.ReadTestCase;
import runner.FormRealisasiRunner;
import testData.ReadTestData;

import java.io.IOException;
import java.util.List;
import java.util.Objects;

import static individu.LibUtils.*;
import static runner.FormRealisasiRunner.extent;
import static runner.FormRealisasiRunner.extent_test_case;


public class MenuRealizationPage extends BaseAction {
    private final WebDriver driver = FormRealisasiRunner.driver;
    String[] partnering = new String[]{"KOPNUS", "INDODANA"};
    private boolean elementExist;

    public static final By menuRealization = By.xpath("//div[normalize-space()='Realization']");
    public static final By lbl_filterSource = By.xpath("//label[normalize-space()='Source :']");
    public static final By lbl_filterStatus = By.xpath("//label[normalize-space()='Status :']");
    public static final By lbl_filterPartner = By.xpath("//label[normalize-space()='Partner :']");
    public static final By lbl_no = By.xpath("//th[@aria-label=' No ']");
    public static final By lbl_source = By.xpath("//th[@id='sortApiSource']");
    public static final By lbl_applicationNo = By.xpath("//th[@id='sortAppNumber']");
    public static final By lbl_debtorName = By.xpath("//th[@id='sortProductCode']");
    public static final By lbl_partner = By.xpath("//th[@id='sortCompanyName']");
    public static final By lbl_date = By.xpath("//th[@id='sortCreatedDate']");
    public static final By lbl_loanAmount = By.xpath("//th[@aria-label=' Loan Amount: activate to sort column ascending']");
    public static final By lbl_rate = By.xpath("//th[@id='sortInterestRate']");
    public static final By lbl_status = By.xpath("//th[@aria-label=' Status: activate to sort column ascending']");
    public static final By lbl_reason = By.xpath("//th[@aria-label=' Reason']");


    public static final By txt_Search = By.xpath("//*[@id=\"myInput\"]");
    public static final By list_status = By.xpath("//*[@id='select2-filterStatus-container']");
    public static final By list_statusPending = By.xpath("//*[starts-with(@id, 'select2-filterStatus-result-') and contains(@id,'-Pending')]");
    public static final By list_statusWaitingApproval = By.xpath("//*[starts-with(@id, 'select2-filterStatus-result-') and contains(@id,'-Waiting Approval')]");
    public static final By list_statusApproved = By.xpath("//*[starts-with(@id, 'select2-filterStatus-result-') and contains(@id,'-Approved')]");
    public static final By list_statusRejected = By.xpath("//*[starts-with(@id, 'select2-filterStatus-result-') and contains(@id,'-Rejected')]");
    public static final By list_statusApprovalExpired = By.xpath("//*[starts-with(@id, 'select2-filterStatus-result-') and contains(@id,'-Approval Expired')]");
    public static final By list_source = By.xpath("//*[@id='select2-filterApiSource-container']");
    public static final By list_sourceLOS = By.xpath("//*[starts-with(@id, 'select2-filterApiSource-result-') and contains(@id,'-LOS')]");
    public static final By list_sourceNIKEL = By.xpath("//*[starts-with(@id, 'select2-filterApiSource-result-') and contains(@id,'-NIKEL')]");
    public static final By list_partner = By.xpath("//*[@id=\"select2-filterSource-container\"]");
    public static final By list_partnerKopnus = By.xpath("//*[starts-with(@id, 'select2-filterSource-result-') and contains(@id,'-KOPNUS')]");
    public static final By list_partnerICL = By.xpath("//*[starts-with(@id, 'select2-filterSource-result-') and contains(@id,'-Indodana Cash Loan')]");
    public static final By list_partnerIPL = By.xpath("//*[starts-with(@id, 'select2-filterSource-result-') and contains(@id,'-Indodana Pay Later')]");
    public static final By next_page = By.xpath("//*[@id=\"realizationTable_next\"]/a");
    public static final By prev_page = By.xpath("//*[@id=\"realizationTable_previous\"]/a");
    public static final By specific_number_page = By.xpath("//*[@id=\"realizationTable_paginate\"]/ul/li[3]/a");
    public static final By short_debtorName = By.xpath("//*[@id=\"sortProductCode\"]");
    public static final By short_partner = By.xpath("//*[@id=\"sortCompanyName\"]");
    public static final By short_date = By.xpath("//*[@id=\"sortCreatedDate\"]");
    public static final By short_loan_amount = By.xpath("//*[@id=\"realizationTable\"]/thead/tr/th[7]");
    public static final By short_status = By.xpath("//*[@id=\"realizationTable\"]/thead/tr/th[9]");
    public static final By tbl_realizationForm_debtorname = By.xpath("//*[@id='channelingTable']/tr[1]/td[4]/a");
    public static final By notMatchFound = By.xpath("//td[@class='dataTables_empty']");
    public static final By table_form_status = By.xpath("//*[@id=\"channelingTable\"]/tr/td[9]/a");
    public static final By title_realisasi = By.xpath("/html/body/div[2]/div[1]/div/div[1]/div[1]");

    String status_pending = "Not Test/Pending Test";
    ReadTestData readTestData = new ReadTestData();
    TakeSreenshot takeSreenshot = new TakeSreenshot();
    String[] arr;
    ReadCSVFormRealization readCSVFormRealization = new ReadCSVFormRealization();

    int iSeq = 1;
    public int iShoot; // row number picture
    int iRowCapture = 55; // jarak antar picture
    public int iRowPictName = 1; // row number scenario

    public void verifikasiLayarUtamaRealizationForm() {
        iRowPictName = 1;
        createTest(iRowPictName, extent_test_case, extent);
        click(driver, menuRealization);
        waitElementVisible(driver, tbl_realizationForm_debtorname, Const.seconds);
        verifyElementExist(driver, lbl_filterSource);
        verifyElementExist(driver, lbl_filterStatus);
        verifyElementExist(driver, lbl_filterPartner);
        verifyElementExist(driver, lbl_filterSource);
        verifyElementExist(driver, lbl_no);
        verifyElementExist(driver, lbl_applicationNo);
        verifyElementExist(driver, lbl_debtorName);
        verifyElementExist(driver, lbl_partner);
        verifyElementExist(driver, lbl_date);
        verifyElementExist(driver, lbl_loanAmount);
        verifyElementExist(driver, lbl_rate);
        verifyElementExist(driver, lbl_status);
        verifyElementExist(driver, lbl_reason);

        takeSreenshot.capture(driver);
        status_testCase(1, true, "");
    }

    public void dataBaruLolosWaitingApproval() throws IOException {
        iRowPictName = 2;
        createTest(iRowPictName, extent_test_case, extent);

        readTestData.testData();
        click(driver, list_status);
        click(driver, list_statusWaitingApproval);
        writeText(driver, txt_Search, readTestData.timeStamp);
        waitElementVisible(driver, tbl_realizationForm_debtorname, Const.seconds);

        String[] dummyCSV = readCSVFormRealization.fileCSVRea();
        int x = 55;
        boolean sts = true;
        while (sts) {
            if (x < dummyCSV.length - 1) {
                if (boolean_isDisplayed(driver, By.xpath(" //td[normalize-space()='" + dummyCSV[x] + "']"))) {
                    verifyElementExist(driver, By.xpath(" //td[normalize-space()='" + dummyCSV[x] + "']"));
                    x = x + 55;
                }
            } else {
                sts = false;
            }
        }

        takeSreenshot.capture(driver);
        status_testCase(iRowPictName, true, "");
    }

    public void dataBaruLolosMelebihiLimitStatusPending() {
        iRowPictName = 123;
        createTest(iRowPictName, extent_test_case, extent);

        click(driver, menuRealization);
        driver.findElement(txt_Search).clear();
        readTestData.testData();
        click(driver, list_status);
        scrollIntoView(driver, list_statusPending);
        verifyElementExist(driver, list_statusPending);
        driver.findElement(list_statusPending).click();
        driver.findElement(txt_Search).sendKeys(readTestData.timeStamp);

        LibUtils.capture.log(Status.SKIP, "SKIP");
        takeSreenshot.capture(driver);
        status_testCase(iRowPictName, true, status_pending);
    }

    public void dataBaruLolosMelebihiWaktuStatusExpired() {
        iRowPictName = 116;
        createTest(iRowPictName, extent_test_case, extent);

        click(driver, menuRealization);
        driver.findElement(txt_Search).clear();
        readTestData.testData();
        click(driver, list_status);
        scrollIntoView(driver, list_statusApprovalExpired);
        verifyElementExist(driver, list_statusApprovalExpired);
        driver.findElement(list_statusApprovalExpired).click();
        driver.findElement(txt_Search).sendKeys(readTestData.timeStamp);

        takeSreenshot.capture(driver);
        status_testCase(iRowPictName, true, status_pending);
    }

    public void verifikasiFilterDropdownSource() {
        iRowPictName = 3;
        createTest(iRowPictName, extent_test_case, extent);

        driver.navigate().refresh();
        driver.findElement(list_source).click();

        verifyElementExist(driver, By.xpath("//li[normalize-space()='All']"));
        verifyElementExist(driver, list_sourceLOS);
        verifyElementExist(driver, list_sourceNIKEL);

        takeSreenshot.capture(driver);
        status_testCase(iRowPictName, true, "");
    }

    public void pilihSourceAllAndLos() throws InterruptedException {
        iRowPictName = 4;
        createTest(iRowPictName, extent_test_case, extent);

        driver.navigate().refresh();
        verifyElementExist(driver, list_source);
        Thread.sleep(3000);
        takeSreenshot.capture(driver);

        driver.findElement(list_source).click();
        scrollIntoView(driver, list_sourceLOS);
        verifyElementExist(driver, list_sourceLOS);
        driver.findElement(list_sourceLOS).click();
        takeSreenshot.capture(driver);
        status_testCase(iRowPictName, true, "");
    }

    public void pilihSourceNikel() {
        iRowPictName = 5;
        createTest(iRowPictName, extent_test_case, extent);

        driver.findElement(list_source).click();
        verifyElementExist(driver, list_sourceNIKEL);
        driver.findElement(list_sourceNIKEL).click();
        verifyElementExist(driver, notMatchFound);
        takeSreenshot.capture(driver);
        status_testCase(iRowPictName, true, "");
    }

    public void klikFieldDropdownStatus() {
        iRowPictName = 6;
        createTest(iRowPictName, extent_test_case, extent);

        driver.navigate().refresh();
        driver.findElement(list_status).click();
        verifyElementExist(driver, list_statusWaitingApproval);
        verifyElementExist(driver, list_statusPending);
        verifyElementExist(driver, list_statusApproved);
        takeSreenshot.capture(driver);
        scrollIntoView(driver, list_statusApprovalExpired);
        verifyElementExist(driver, list_statusRejected);
        verifyElementExist(driver, list_statusApprovalExpired);
        takeSreenshot.capture(driver);
        status_testCase(iRowPictName, true, "");
    }

    public void pilihStatus(List<String> tbl_status) throws InterruptedException {

        for (int i = 0; i < tbl_status.size(); i++) {
            switch (i) {
                case 0:
                    iRowPictName = 7;
                    createTest(iRowPictName, extent_test_case, extent);
                    driver.navigate().refresh();
                    Thread.sleep(3000);
                    verifyElementExist(driver, list_status);
                    takeSreenshot.capture(driver);
                    status_testCase(iRowPictName, true, "");

                    break;
                case 1:
                    iRowPictName = 8;
                    createTest(iRowPictName, extent_test_case, extent);
                    driver.findElement(list_status).click();
                    verifyElementExist(driver, list_status);
                    driver.findElement(list_statusWaitingApproval).click();

                    takeSreenshot.capture(driver);
                    status_testCase(iRowPictName, true, "");
                    break;
                case 2:
                    iRowPictName = 9;
                    createTest(iRowPictName, extent_test_case, extent);
                    driver.findElement(list_status).click();
                    scrollIntoView(driver, list_statusApprovalExpired);
                    verifyElementExist(driver, list_statusApproved);
                    driver.findElement(list_statusApproved).click();

                    takeSreenshot.capture(driver);
                    status_testCase(iRowPictName, true, "");
                    break;
                case 3:
                    iRowPictName = 10;
                    createTest(iRowPictName, extent_test_case, extent);
                    driver.findElement(list_status).click();
                    scrollIntoView(driver, list_statusApprovalExpired);
                    verifyElementExist(driver, list_statusRejected);
                    driver.findElement(list_statusRejected).click();

                    takeSreenshot.capture(driver);
                    status_testCase(iRowPictName, true, "");
                    break;
            }
        }
    }

    public void pilihFilterStatusExpired() {
        iRowPictName = 114;
        createTest(iRowPictName, extent_test_case, extent);
        ExecutionJob.jobReaExpired(); //job realisasi expired

        click(driver, menuRealization);
        click(driver, list_status);
        scrollIntoView(driver, list_statusApprovalExpired);
        verifyElementExist(driver, list_statusApprovalExpired);
        click(driver, list_statusApprovalExpired);

        takeSreenshot.capture(driver);
        status_testCase(iRowPictName, true, "");

    }

    public void pilihFilterStatusPending() {
        iRowPictName = 121;
        createTest(iRowPictName, extent_test_case, extent);

        click(driver, menuRealization);
        click(driver, list_status);
        scrollIntoView(driver, list_statusPending);
        verifyElementExist(driver, list_statusPending);
        click(driver, list_statusPending);

        LibUtils.capture.log(Status.SKIP, "SKIP");
        takeSreenshot.capture(driver);
        status_testCase(iRowPictName, true, "");

    }

    public void klikFieldDropdownPartner() {
        iRowPictName = 11;
        createTest(iRowPictName, extent_test_case, extent);

        driver.findElement(list_partner).click();
        LibUtils.capture.log(Status.PASS, "Actual result: Tampil Partner");
        takeSreenshot.capture(driver);
        status_testCase(iRowPictName, true, "");

    }

    public void klikPartnerAll() {
        iRowPictName = 12;
        createTest(iRowPictName, extent_test_case, extent);
        driver.navigate().refresh();
        verifyElementExist(driver, list_partner);
        waitElementVisible(driver, tbl_realizationForm_debtorname, Const.seconds);
        takeSreenshot.capture(driver);
        status_testCase(iRowPictName, true, "");
    }

    public void klikSpecificPartner() {
        iRowPictName = 13;
        createTest(iRowPictName, extent_test_case, extent);

        driver.findElement(list_partner).click();
        scrollIntoView(driver, list_partnerKopnus);
        readTestData.testData();
        verifyElementExist(driver, list_partnerKopnus);
        driver.findElement(By.xpath("//*[starts-with(@id, 'select2-filterSource-result-') and contains(@id,'-" + readTestData.companyName + "')]")).click();

        takeSreenshot.capture(driver);
        status_testCase(iRowPictName, true, "");
    }

    public void isiFieldSearch() {
        iRowPictName = 14;
        createTest(iRowPictName, extent_test_case, extent);

        driver.navigate().refresh();
        driver.findElement(txt_Search).sendKeys(readTestData.timeStamp);
        LibUtils.capture.log(Status.PASS, "Actual result: search " + readTestData.timeStamp);
        waitElementVisible(driver, tbl_realizationForm_debtorname, Const.seconds);

        takeSreenshot.capture(driver);
        status_testCase(iRowPictName, true, "");
    }

    public void klikNextPage() throws InterruptedException {
        iRowPictName = 15;
        createTest(iRowPictName, extent_test_case, extent);

        driver.navigate().refresh();
        Thread.sleep(Const.delay);
        moveToEnd(driver, prev_page);
        LibUtils.capture.log(Status.INFO, "Before next");
        takeSreenshot.capture(driver);
        try {
            driver.findElement(next_page).click();
            LibUtils.capture.log(Status.PASS, "Actual result: after next");
            takeSreenshot.capture(driver);
            status_testCase(iRowPictName, true, "");
        } catch (Exception ee) {

            takeSreenshot.capture(driver);
            status_testCase(iRowPictName, false, ee.getMessage());
        }
    }

    public void klikPreviousPage() {
        iRowPictName = 16;
        createTest(iRowPictName, extent_test_case, extent);

        try {
            moveToEnd(driver, next_page);
            LibUtils.capture.log(Status.INFO, "Before prev");
            takeSreenshot.capture(driver);
            driver.findElement(prev_page).click();
            LibUtils.capture.log(Status.PASS, "Actual result: after prev");
            takeSreenshot.capture(driver);
            status_testCase(iRowPictName, true, "");
        } catch (Exception ee) {

            takeSreenshot.capture(driver);
            status_testCase(iRowPictName, false, ee.getMessage());
        }
    }


    public void specificPage() {
        iRowPictName = 17;
        createTest(iRowPictName, extent_test_case, extent);

        try {
            LibUtils.moveTo(driver, next_page);
            LibUtils.capture.log(Status.INFO, "Actual result: before specific page");
            takeSreenshot.capture(driver);
            driver.findElement(specific_number_page).click();
            verifyElementExist(driver, specific_number_page);

            takeSreenshot.capture(driver);
            status_testCase(iRowPictName, true, "");
        } catch (Exception ee) {

            takeSreenshot.capture(driver);
            status_testCase(iRowPictName, false, ee.getMessage());
        }
    }

    public void shortDebtorNameColumn() {
        iRowPictName = 18;
        createTest(iRowPictName, extent_test_case, extent);

        driver.navigate().refresh();
        String data;
        String strDataColl = null;
        String[] dataColl;
        click(driver, short_debtorName);// ascending
        try {
            for (int i = 1; i <= 25; i++) {
                data = driver.findElement(By.xpath("//*[@id='channelingTable']/tr[" + i + "]/td[4]/a")).getText();
                strDataColl = strDataColl + "|" + data;
            }
        } catch (Exception ignored) {
        }
        //validasi sort ascending
        takeSreenshot.capture(driver);

        moveToEnd(driver, next_page);
        takeSreenshot.capture(driver);
        codeBlockText("Actual Ascending Debtor Name: " + strDataColl.replace("null", ""));
        actualLogTrue(String.valueOf(LibUtils.verifyAscending(strDataColl)));
        scrollIntoView(driver, title_realisasi);
        click(driver, short_debtorName);// descending
        strDataColl = null;
        try {
            for (int i = 1; i <= 25; i++) {
                data = driver.findElement(By.xpath("//*[@id='channelingTable']/tr[" + i + "]/td[4]/a")).getText();
                strDataColl = strDataColl + "|" + data;
            }
        } catch (Exception ignored) {
        }
        //validasi sort descending
        takeSreenshot.capture(driver);
        moveToEnd(driver, next_page);
        takeSreenshot.capture(driver);
        codeBlockText("Descending Debtor Name: " + strDataColl.replace("null", ""));
        actualLogTrue(String.valueOf(LibUtils.verifyDescending(strDataColl)));
        scrollIntoView(driver, title_realisasi);

        status_testCase(iRowPictName, true, "");
    }

    public void shortPartnerColumn() {
        iRowPictName = 19;
        createTest(iRowPictName, extent_test_case, extent);

        driver.navigate().refresh();
        String data;
        String strDataColl = null;
        String[] dataColl;
        click(driver, short_partner);// ascending
        try {
            for (int i = 1; i <= 25; i++) {
                data = driver.findElement(By.xpath("//*[@id='channelingTable']/tr[" + i + "]/td[5]")).getText();
                strDataColl = strDataColl + "|" + data;
            }
        } catch (Exception ignored) {
        }
        //validasi sort ascending
        

        takeSreenshot.capture(driver);
        moveToEnd(driver, next_page);
        takeSreenshot.capture(driver);
        codeBlockText("Ascending Partner Name: " + strDataColl.replace("null", ""));
        actualLogTrue(String.valueOf(LibUtils.verifyAscending(strDataColl)));
        scrollIntoView(driver, title_realisasi);
        click(driver, short_partner);// descending
        strDataColl = null;
        try {
            for (int i = 1; i <= 25; i++) {
                data = driver.findElement(By.xpath("//*[@id='channelingTable']/tr[" + i + "]/td[5]")).getText();
                strDataColl = strDataColl + "|" + data;
            }
        } catch (Exception ignored) {
        }
        //validasi sort ascending
        

        takeSreenshot.capture(driver);
        moveToEnd(driver, next_page);
        takeSreenshot.capture(driver);
        codeBlockText("Descending Partner Name: " + strDataColl.replace("null", ""));
        actualLogTrue(String.valueOf(LibUtils.verifyDescending(strDataColl)));
        scrollIntoView(driver, title_realisasi);

        status_testCase(iRowPictName, true, "");
    }

    public void shortDateColumn() {
        iRowPictName = 20;
        createTest(iRowPictName, extent_test_case, extent);

        driver.navigate().refresh();
        String data;
        String strDataColl = null;
        String[] dataColl;
        click(driver, short_date);// ascending
        try {
            for (int i = 1; i <= 25; i++) {
                data = driver.findElement(By.xpath("//*[@id='channelingTable']/tr[" + i + "]/td[6]")).getText();
                strDataColl = strDataColl + "|" + data;
            }
        } catch (Exception ignored) {
        }
        //validasi sort ascending
        

        takeSreenshot.capture(driver);
        moveToEnd(driver, next_page);
        takeSreenshot.capture(driver);
        codeBlockText("Ascending Date: " + strDataColl.replace("null", ""));
        actualLogTrue(String.valueOf(LibUtils.verifyAscending(strDataColl)));
        scrollIntoView(driver, title_realisasi);
        click(driver, short_date);// descending
        strDataColl = null;
        try {
            for (int i = 1; i <= 25; i++) {
                data = driver.findElement(By.xpath("//*[@id='channelingTable']/tr[" + i + "]/td[6]")).getText();
                strDataColl = strDataColl + "|" + data;
            }
        } catch (Exception ignored) {
        }
        //validasi sort ascending
        

        takeSreenshot.capture(driver);
        moveToEnd(driver, next_page);
        takeSreenshot.capture(driver);
        codeBlockText("Descending Date: " + strDataColl.replace("null", ""));
        actualLogTrue(String.valueOf(LibUtils.verifyDescending(strDataColl)));
        scrollIntoView(driver, title_realisasi);

        status_testCase(iRowPictName, true, "");
    }

    public void shortLoanAmountColumn() {
        iRowPictName = 21;
        createTest(iRowPictName, extent_test_case, extent);

        driver.navigate().refresh();
        String data;
        String strDataColl = null;
        String[] dataColl;
        click(driver, short_loan_amount);// ascending
        try {
            for (int i = 1; i <= 25; i++) {
                data = driver.findElement(By.xpath("//*[@id='channelingTable']/tr[" + i + "]/td[7]")).getText();
                strDataColl = strDataColl + "|" + data;
            }
        } catch (Exception ignored) {
        }
        //validasi sort ascending
        takeSreenshot.capture(driver);
        moveToEnd(driver, next_page);
        takeSreenshot.capture(driver);
        codeBlockText("Ascending Loan Amount: " + strDataColl.replace("null", ""));
        actualLogTrue(String.valueOf(LibUtils.verifyAscending(strDataColl)));
        scrollIntoView(driver, title_realisasi);
        click(driver, short_loan_amount);// descending
        strDataColl = null;
        try {
            for (int i = 1; i <= 25; i++) {
                data = driver.findElement(By.xpath("//*[@id='channelingTable']/tr[" + i + "]/td[7]")).getText();
                strDataColl = strDataColl + "|" + data;
            }
        } catch (Exception ignored) {
        }
        //validasi sort
        

        takeSreenshot.capture(driver);
        moveToEnd(driver, next_page);
        takeSreenshot.capture(driver);
        codeBlockText("Descending Loan Amount: " + strDataColl.replace("null", ""));
        actualLogTrue(String.valueOf(LibUtils.verifyDescending(strDataColl)));
        scrollIntoView(driver, title_realisasi);

        status_testCase(iRowPictName, true, "");
    }

    public void shortStatusColumn() {
        iRowPictName = 22;
        createTest(iRowPictName, extent_test_case, extent);

        driver.navigate().refresh();
        String data;
        String strDataColl = null;
        String[] dataColl;
        click(driver, short_status);// ascending
        try {
            for (int i = 1; i <= 25; i++) {
                data = driver.findElement(By.xpath("//*[@id='channelingTable']/tr[" + i + "]/td[9]")).getText();
                strDataColl = strDataColl + "|" + data;
            }
        } catch (Exception ignored) {
        }
        //validasi sort ascending
        takeSreenshot.capture(driver);
        moveToEnd(driver, next_page);
        takeSreenshot.capture(driver);
        codeBlockText("Ascending Status: " + strDataColl.replace("null", ""));
        actualLogTrue(String.valueOf(LibUtils.verifyAscending(strDataColl)));
        scrollIntoView(driver, title_realisasi);
        click(driver, short_status);// descending
        strDataColl = null;
        try {
            for (int i = 1; i <= 25; i++) {
                data = driver.findElement(By.xpath("//*[@id='channelingTable']/tr[" + i + "]/td[9]")).getText();
                strDataColl = strDataColl + "|" + data;
            }
        } catch (Exception ignored) {
        }
        //validasi sort
        

        takeSreenshot.capture(driver);
        moveToEnd(driver, next_page);
        takeSreenshot.capture(driver);
        codeBlockText("Descending Status: " + strDataColl.replace("null", ""));
        actualLogTrue(String.valueOf(LibUtils.verifyDescending(strDataColl)));
        scrollIntoView(driver, title_realisasi);

        status_testCase(iRowPictName, true, "");
    }

    public void verifyDataRalizationForm(List<String> dataRealization) throws IOException {
        arr = readCSVFormRealization.fileCSVRea();
        String strData;
        int x = 3;
        int count = 0;
        boolean sts = true;

        /**
         * search data status waiting approval
         */
        while (sts) {
            driver.findElement(txt_Search).sendKeys(arr[55 + count]);
//            boolean exist = _boleanWaitElementVisible(driver, table_form_status, Const.seconds);
            if (driver.getPageSource().contains(Const.noMatch)) {
                driver.findElement(txt_Search).clear();
                count = count + 55;
            } else {
                sts = false;

            }
        }
        boolean _sts = false;
        for (int i = 0; i < dataRealization.size(); i++) {
            switch (i) {
                case 0:
                    iRowPictName = 23;
                    createTest(iRowPictName, extent_test_case, extent);
                    strData = getText(driver, By.xpath("//*[@id='channelingTable']/tr/td[2]"));
                    _sts = LibUtils.verifyValue("LOS", strData);
                    takeSreenshot.capture(driver);
                    status_testCase(iRowPictName, _sts, "Expected: " + "LOS" + " ====================> Actual: " + strData);
                    break;
                case 1:
                    iRowPictName = 24;
                    createTest(iRowPictName, extent_test_case, extent);
                    strData = getText(driver, By.xpath("//*[@id='channelingTable']/tr/td[" + x + "]"));
                    _sts = LibUtils.verifyValue(arr[55 + count], strData);
                    takeSreenshot.capture(driver);
                    status_testCase(iRowPictName, _sts, "Expected: " + arr[55 + count] + " ====================> Actual: " + strData);
                    x++;
                    break;
                case 2:
                    iRowPictName = 25;
                    createTest(iRowPictName, extent_test_case, extent);
                    strData = getText(driver, By.xpath("//*[@id='channelingTable']/tr/td[" + x + "]"));
                    _sts = LibUtils.verifyValue(arr[56 + count], strData);
                    takeSreenshot.capture(driver);
                    status_testCase(iRowPictName, _sts, "Expected: " + arr[56 + count] + " ====================> Actual: " + strData);
                    x++;
                    break;
                case 3:
                    iRowPictName = 26;
                    createTest(iRowPictName, extent_test_case, extent);
                    strData = getText(driver, By.xpath("//*[@id='channelingTable']/tr/td[" + x + "]"));
                    _sts = LibUtils.verifyValue(readTestData.companyName, strData);
                    takeSreenshot.capture(driver);
                    status_testCase(iRowPictName, _sts, "Expected: " + readTestData.companyName + " ====================> Actual: " + strData);
                    x++;
                    break;
                case 4:
                    iRowPictName = 27;
                    createTest(iRowPictName, extent_test_case, extent);
                    strData = getText(driver, By.xpath("//*[@id='channelingTable']/tr/td[" + x + "]"));
                    _sts = LibUtils.verifyValue(strData, strData);
                    takeSreenshot.capture(driver);
                    status_testCase(iRowPictName, _sts, "Expected: " + strData + " ====================> Actual: " + strData);
                    x++;
                    break;
                case 5:
                    iRowPictName = 28;
                    createTest(iRowPictName, extent_test_case, extent);
                    strData = "000000" + getText(driver, By.xpath("//*[@id='channelingTable']/tr/td[" + x + "]")).replace(".", "").replace("Rp", "").replace(",", "").trim();
                    _sts = LibUtils.verifyValue(arr[89 + count], strData);
                    takeSreenshot.capture(driver);
                    status_testCase(iRowPictName, _sts, "Expected: " + arr[89 + count] + " ====================> Actual: " + strData);
                    x++;
                    break;
                case 6:
                    iRowPictName = 29;
                    createTest(iRowPictName, extent_test_case, extent);
                    strData = "0" + getText(driver, By.xpath("//*[@id='channelingTable']/tr/td[" + x + "]")).replace("%", "").replace(".", "");
                    _sts = LibUtils.verifyValue(arr[90 + count], strData);
                    takeSreenshot.capture(driver);
                    status_testCase(iRowPictName, _sts, "Expected: " + arr[90 + count] + " ====================> Actual: " + strData);
                    x++;
                    break;
            }
        }
    }

    public void verifyTotalNominalApproved() throws InterruptedException {

        iRowPictName = 108;
        createTest(iRowPictName, extent_test_case, extent);

        click(driver, menuRealization);
        waitElementVisible(driver, tbl_realizationForm_debtorname, Const.seconds);
        String totalAllNominalApproved = driver.findElement(By.xpath("//input[@id='totalNominalPinjaman']")).getAttribute("value").replace("Rp.", "").replace(".", "").trim();
        int totalLoanAmountApproved = 0;
        int totalLoanAmountPBR = 0;
        for (int i = 1; i <= 2; i++) {
            scrollIntoView(driver, By.xpath("//input[@id='totalNominalPinjaman']"));
            if (i == 1) {
                driver.findElement(list_status).click();
                driver.findElement(list_statusApproved).click();
            } else {
                driver.navigate().refresh();
                Thread.sleep(Const.delay);
                driver.findElement(txt_Search).sendKeys("Pending Batch Run");
            }

            takeSreenshot.capture(driver);

            waitElementVisible(driver, tbl_realizationForm_debtorname, Const.seconds);
            String totalNominalApproved = driver.findElement(By.id("totalNominalPinjaman")).getAttribute("value").replace("Rp.", "").replace(".", "").trim();
            String coll_loanAmountApproved = null, loanAmountApproved;
            try {
                while (true) {
                    for (int idx = 1; idx <= 25; idx++) {
                        if (driver.findElement(By.xpath("//*[@id='channelingTable']/tr[" + idx + "]/td[4]/a")).isDisplayed()) {
                            loanAmountApproved = driver.findElement(By.xpath("//*[@id='channelingTable']/tr[" + idx + "]/td[7]")).getText();
                            coll_loanAmountApproved = coll_loanAmountApproved + "|" + loanAmountApproved;

                            takeSreenshot.capture(driver);
                        }
                        if (idx == 25) {

                            takeSreenshot.capture(driver);
                            scrollIntoView(driver, next_page);
                            driver.findElement(next_page).click();
                        }
                    }
                }
            } catch (Exception ignored) {
                String[] str_loanAmountApproved = coll_loanAmountApproved.split("\\|");
                if (i == 1) {
                    codeBlockText("Actual status Approved " + coll_loanAmountApproved.replace("null", ""));
                    for (int x = 1; x < str_loanAmountApproved.length; x++) {
                        totalLoanAmountApproved = totalLoanAmountApproved + Integer.parseInt(str_loanAmountApproved[x].replace("Rp ", "").replace(".", "").replace(",00", "").trim());
                    }
                    System.out.println(totalLoanAmountApproved);
                } else {
                    codeBlockText("Actual status PBR " + coll_loanAmountApproved.replace("null", ""));
                    for (int x = 1; x < str_loanAmountApproved.length; x++) {
                        totalLoanAmountPBR = totalLoanAmountPBR + Integer.parseInt(str_loanAmountApproved[x].replace("Rp ", "").replace(".", "").replace(",00", "").trim());
                    }
                    System.out.println(totalLoanAmountPBR);
                }
            }
        }

        int total = totalLoanAmountApproved + totalLoanAmountPBR;
        boolean _sts = LibUtils.verifyValue(totalAllNominalApproved, String.valueOf(total));

        LibUtils.capture.log(Status.PASS, MarkupHelper.createUnorderedList(List.of(
                "Jumlah status approved: " + totalLoanAmountApproved,
                "Jumlah status approved: " + totalLoanAmountPBR,
                "Total: " + total,
                "Total Nominal Approved: " + totalAllNominalApproved,
                "Status validasi: " + String.valueOf(_sts))).getMarkup());


        takeSreenshot.capture(driver);
        status_testCase(iRowPictName, _sts, "Total Nominal Approved: " + totalAllNominalApproved + " >>>>> " + "Total penjumlahan loan amount di table: " + total);
    }
}
