package elementsPage;

import org.openqa.selenium.By;

public class Const {
    public static final String e2e_environment = "E2E";
    public static final String uat_environment = "UAT";
    public static final String urlE2E = "https://10.27.62.156:7053/login";//e2e
    public static final String urlUAT = "https://10.27.62.157:7053/login";
    public static final String e2e_userMaker = "automationMaker";
    public static final String e2e_userApprover = "automationApprover";
    public static final String e2e_password = "123456789";
    public static final String uat_userMaker = "u_maker";
    public static final String uat_userApprover = "u_approver";
    public static final String uat_password = "1234567890";
    public static final String e2e_user_ldap_maker = "automationMaker";
    public static final String e2e_pwd_ldap_maker = "123456789";
    public static final String e2e_user_ldap_approveRecomend = "surya.simarmata";
    public static final String e2e_pwd_ldap_approveRecomend = "Surya2022";
    public static final String e2e_user_ldap_approveRealisasi = "weike.phinjaya";
    public static final String e2e_pwd_ldap_approveRealisasi = "Weike1234";

    public static final String noMatch = "No matching records found";
    public static final String noData = "No data available in table";
    public static final String pthFileValidation = "./data-test/";
    public static final String resultDataDummyFile = "./resultDataDummyFile/";
    public static final String pthResultScenario = "./resultScenario/";
    public static final String pthDummyZIP = "./dummyZIP/";
    public static final String pthTestData = "./data-test/testData.xlsx";
    public static final String pthFileImage = "./extent-report/";
    public static final String pthTemp = "./temp/";
    public static final String pthImages = "./temp/captureImages/";
    public static final String fileNMScenario = "Individu.xlsx";
    public static final String nameFileScenario = "Individu ";
    public static final String sheetTestData = "Data Testing";
    public static final String scSheetApp = "Approval File";
    public static final String scSheetRea = "Realisasi File";
    public static final String scSheetPengurus = "Pengurus File";
    public static final String scSheetPercentages = "Persentase";
    public static final String nFileScenario = "Negatif";
    public static final String pFileScenario = "Individu";
    public static final String sheetImages = "Capture Images";
    public static final String pPattern = "ddMMyyy-HHmmss";
    public static final String extPict = ".png";
    public static final String glPattern = "ddMMyyyy";
    public static final String extCSV = ".csv";
    public static final String extExcel = ".xlsx";
    public static final String appFile = "APPFILEIDV";
    public static final String reaFile = "REAFILEIDV";
    public static final String txtAppIDV = "Berhasil_AppIDV";
    public static final String txtReaIDV = "Berhasil_RealisasiIDV";
    public static final String rFileGagalApp = "Gagal_AppIDV";
    public static final String rFileGagalRea = "Gagal_RealisasiIDV";
    public static final String csvApp = "APP";
    public static final String csvRea = "REA";
    public static final String txtRea = "Realisasi";
    public static final String txtApp = "App";
    public static final String chartTitleP = "Perseorangan";
    public static final String chartTitleB = "Badan Usaha";
    public static final String tStatus = "Passed";
    public static final String fStatus = "Failed";
    public static final String sNull = "null";
    public static final String rBerhasil = "BERHASIL";
    public static final String yessBulkApp = "Yes Bulk Approval";
    public static final String noBulkApp = "No Bulk Approval";
    public static final String yessBulkRea = "Yes Bulk Realisasi";
    public static final String noBulkRea = "No Bulk Realisasi";
    public static final String typ_CORP = "CORP";
    public static final String typ_IDV = "IDV";
    public static final String typ_Kopnus = "KOPNUS";
    public static final String typ_NikelMora = "NIKEL MORA";
    public static final String typ_NikelICL = "NIKEL ICL";
    public static final String typ_NikelIPL = "NIKEL IPL";
    public static final String typ_IndodanaICL = "INDODANA ICL";
    public static final String typ_IndodanaIPL = "INDODANA IPL";
    public static final String typ_Amartha = "AMARTHA";
    public static final String typ_SeaMoneyLDN = "SEAMONEY LDN";
    public static final String typ_SeaMoneyCFinance = "SEAMONEY CFINANCE";// harus sama dengan test data
    public static final String sheetCaptureApproval = "Capture Approval IDV";
    public static final String sheetCaptureRealization = "Capture Realisasi IDV";
    public static final String sheetCaptureApprovalFileIDV = "Capture Approval File IDV";
    public static final String sheetCaptureRealisasiFileIDV = "Capture Realisasi File IDV";
    public static final String attachmentName = "Cek screenshoot di sheet (Capture Approval IDV)";
    public static final String attachmentNameRea = "Cek screenshoot di sheet (Capture Realisasi IDV)";
    public static final String attachmentNameValidateAppFile = "Cek screenshoot di sheet (Capture Approval File IDV)";
    public static final String attachmentNameValidateReaFile = "Cek screenshoot di sheet (Capture Realisasi File IDV)";
    public static final String sheetApprovalFormIDV = "Approval Form IDV";
    public static final String sheetRealizationFormIDV = "Realisasi Form IDV";
    public static final String sheetApprovalFileIDV = "Approval File IDV";
    public static final String sheetRealisasiFileIDV = "Realisasi File IDV";
    //common
    public static final int delay = 1500;
    public static final int seconds = 60;
    public static final String scrollIntoview = "arguments[0].scrollIntoView();";
    public static final String pthFolderSuppDocOld = "./data-test/extract zip file/old/";
    public static final String pthFolderSuppDocNew = "./data-test/extract zip file/new/";
    public static final String pthFolderSuppDocSample = "./data-test/suppdoc-file-sample/";
    public static final String pthFolderDownload = "C:/Users/" + System.getProperty("user.name") + "/Downloads/";

}
