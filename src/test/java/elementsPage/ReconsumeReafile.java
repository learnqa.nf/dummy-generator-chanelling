package elementsPage;

import applos.ConsumeData;
import applos.ExecutionJob;
import capture.TakeSreenshot;
import com.aventstack.extentreports.Status;
import createDataCSV.DataCSV;
import individu.LibUtils;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import readFile.ReadCSVFormRealization;
import runner.FormRealisasiRunner;
import testData.ReadTestData;
import writeFile.UtilsFile;

import static elementsPage.ApprovalFormApprover.loginApprover;
import static elementsPage.ApprovalFormMaker.loginMaker;
import static elementsPage.ApprovalFormMaker.logout;
import static individu.ChangesStatus.changedApproveAsApprover_rea;
import static individu.ChangesStatus.changedRecomendationAsMaker_rea;
import static individu.LibUtils.*;
import static readFile.UpdateCSV.updateFieldCSV;
import static runner.FormRealisasiRunner.extent;
import static runner.FormRealisasiRunner.extent_test_case;

public class ReconsumeReafile extends BaseAction {

    By menuDahsboard = By.xpath("//span[normalize-space()='Dashboard']");
    By processMonitoring = By.xpath("//div[contains(text(),'Process Monitoring')]");
    By menuRealization = By.xpath("//div[normalize-space()='Realization']");
    By bulk_approve = By.xpath("//a[normalize-space()='Bulk Approve']");
    By btn_bulkApprove = By.xpath("//*[@id=\"btnSubmitRealization\"]");
    By bulk_linkDebtorName = By.xpath("//*[@id=\"channelingTable\"]/tr[1]/td[5]/a");
    By txt_Search = By.xpath("//*[@id=\"myInput\"]");
    By txt_search_frommonitoring = By.xpath("//*[@id=\"search-filter\"]");
    By txt_search_bulkApprove = By.xpath("//input[@id=\"searchInput\"]");
    By btn_radioFirst_Bulk = By.xpath("//*[@id=\"channelingTable\"]/tr[1]/td[1]/div/label");
    By dashboard_rea_waitingApproval = By.xpath("//a[@class='totalRealizationWaitingForApprovalDetailLink']");
    By dashboard_rea_rejectBySystem = By.xpath("//a[@id='totalRealizationRejectedBySystemDetailLink']");
    By dashboard_repayment_waitingApproval = By.xpath("//*[@id=\"totalRepaymentWaitingForApprovalDetailLink\"]/img");
    By dashboard_title_repaymentProcess = By.xpath("//h1[normalize-space()='Repayment Process']");
    By monitor_waitingApproval_debtorname = By.xpath("//*[@id=\"realizationDataTables\"]/tbody/tr[1]/td[4]/a");
    By tbl_realizationForm_debtorname = By.xpath("//*[@id='channelingTable']/tr[1]/td[4]/a");
    By tbl_realizationForm_debtorname_2nd = By.xpath("//*[@id='channelingTable']/tr[2]/td[4]/a");
    By btn_approve_detail_formRealisasi3 = By.xpath("//*[@id='buttonApprove']");
    By popup_ya = By.xpath("//*[@id='formSubmit']");
    By pop_tidak = By.xpath("//*[@id='modal-cancel']");
    By btn_reject_form = By.xpath("//*[@id=\"rejected-form\"]");
    By btn_reject_detail_formRealisasi = By.xpath("//*[@id='buttonReject']");
    By popup_submit = By.xpath("(//button[@id='formSubmit'])[2]");
    By popup_cancel = By.xpath("//button[@id='modal-cancel-reject']");
    By popup_reject_reason = By.xpath("//input[@id='rejectReason']");
    By popup_select_option = By.xpath("//*[@id='select2-rejectDescription-container']");
    By popup_select_value_option1 = By.xpath("//*[starts-with(@id, 'select2-rejectDescription-result-') and contains(@id,'-Dokumen pendukung tidak sesuai')]");
    By popup_select_value_option2 = By.xpath("//*[starts-with(@id, 'select2-rejectDescription-result-') and contains(@id,'-Pengajuan ditolak')]");

    int iRowPictName; // row number scenario
    WebDriver driver = FormRealisasiRunner.driver;
    ConsumeData consumeData = new ConsumeData();
    ReadCSVFormRealization readCSVFormRealization = new ReadCSVFormRealization();
    TakeSreenshot takeSreenshot = new TakeSreenshot();
    ReadTestData readTestData = new ReadTestData();
    UtilsFile cleanFolder = new UtilsFile();

    public void reconsumeReafileUpdateAlamatWaitingApproval() {

        iRowPictName = 113;
        createTest(iRowPictName, extent_test_case, extent);
        try {

            readTestData.testData();
            UtilsFile cleanFolder = new UtilsFile();
            cleanFolder.dltDummyExist();

            DataCSV dataCSV = new DataCSV(); // generate data
            dataCSV.dataDummy(Const.appFile, Const.reaFile, "1");

            consumeData.serverAkses(); // consume data approval file csv
            // change status to waiting approval for
            logout(driver);
            loginMaker(driver);
            changedRecomendationAsMaker_rea(driver, 1);
            loginApprover(driver, Const.e2e_user_ldap_approveRecomend, Const.e2e_pwd_ldap_approveRecomend);
            changedApproveAsApprover_rea(driver, 1);
            consumeData.serverAksesRea(); // consume realisasi file csv

            loginApprover(driver, Const.e2e_user_ldap_approveRealisasi, Const.e2e_pwd_ldap_approveRealisasi);
            Thread.sleep(5000);
            takeSreenshot.capture(driver);
            LibUtils.moveTo(driver, dashboard_rea_waitingApproval);

            click(driver, dashboard_rea_waitingApproval);
            String[] arr = readCSVFormRealization.fileCSVRea();
            System.out.println(arr[55]);
            writeText(driver, txt_search_frommonitoring, arr[55]);
            LibUtils.waitElementVisible(driver, monitor_waitingApproval_debtorname, Const.seconds);

            takeSreenshot.capture(driver);
            LibUtils.capture.log(Status.INFO, arr[55] + ": Data dashboard realisasi");

            click(driver, monitor_waitingApproval_debtorname);
            takeSreenshot.capture(driver);
            LibUtils.capture.log(Status.INFO, arr[55] + ": Detail data dashboard realisasi");

            click(driver, menuRealization);
            writeText(driver, txt_Search, arr[55]);
            LibUtils.waitElementVisible(driver, tbl_realizationForm_debtorname, Const.seconds);
            takeSreenshot.capture(driver);
            LibUtils.capture.log(Status.INFO, arr[55] + ": Data menu realisasi");
            click(driver, tbl_realizationForm_debtorname);
            takeSreenshot.capture(driver);
            LibUtils.capture.log(Status.INFO, arr[55] + ": Detail data menu realisasi");

            String alamat1 = arr[61]; // old string
            updateFieldCSV(alamat1, "New alamat status waitingApproval "); // update csv alamat
            consumeData.serverAksesRea(); //reconsume file realisasi csv

            click(driver, menuDahsboard);
            click(driver, processMonitoring);
            Thread.sleep(3000);
            LibUtils.moveTo(driver, dashboard_rea_waitingApproval);
            takeSreenshot.capture(driver);
            click(driver, dashboard_rea_waitingApproval);
            writeText(driver, txt_search_frommonitoring, arr[55]);
            LibUtils.waitElementVisible(driver, monitor_waitingApproval_debtorname, Const.seconds);
            takeSreenshot.capture(driver);
            LibUtils.capture.log(Status.INFO, arr[55] + ": Data dashboard setelah update");
            click(driver, monitor_waitingApproval_debtorname);
            takeSreenshot.capture(driver);
            LibUtils.capture.log(Status.INFO, arr[55] + ": Detail data dashboard setelah update");

            click(driver, menuRealization);
            writeText(driver, txt_Search, arr[55]);
            LibUtils.waitElementVisible(driver, tbl_realizationForm_debtorname, Const.seconds);
            takeSreenshot.capture(driver);
            LibUtils.capture.log(Status.INFO, arr[55] + ": Data menu realisasi setelah update");
            click(driver, tbl_realizationForm_debtorname);
            takeSreenshot.capture(driver);
            LibUtils.capture.log(Status.INFO, arr[55] + ": Detail data menu realisasi setelah update");
            status_testCase(iRowPictName, true, "");

        } catch (Exception e) {


            takeSreenshot.capture(driver);
            LibUtils.capture.log(Status.FAIL, e);
            status_testCase(iRowPictName, false, e.getLocalizedMessage());
        }
    }

    public void reconsumeReafileUpdateAlamatRejected() {

        iRowPictName = 118;
        createTest(iRowPictName, extent_test_case, extent);
        try {

            readTestData.testData();
            cleanFolder.dltDummyExist();

            DataCSV dataCSV = new DataCSV(); // generate data
            dataCSV.dataDummy(Const.appFile, Const.reaFile, "1");
            consumeData.serverAkses(); // consume data approval file csv
            // change status to waiting approval for realisasi
            logout(driver);
            loginMaker(driver);
            changedRecomendationAsMaker_rea(driver, 1);
            loginApprover(driver, Const.e2e_user_ldap_approveRecomend, Const.e2e_pwd_ldap_approveRecomend);
            changedApproveAsApprover_rea(driver, 1);

            consumeData.serverAksesRea(); // consume realisasi file csv
            loginApprover(driver, Const.e2e_user_ldap_approveRealisasi, Const.e2e_pwd_ldap_approveRealisasi);

            click(driver, menuRealization);
            String[] arr = readCSVFormRealization.fileCSVRea();
            System.out.println(arr[55]);
            writeText(driver, txt_Search, arr[55]);
            click(driver, tbl_realizationForm_debtorname);
            click(driver, btn_reject_detail_formRealisasi);
            click(driver, popup_select_option);
            click(driver, popup_select_value_option1);
            writeText(driver, popup_reject_reason, "Rejected");
            click(driver, popup_submit);

            writeText(driver, txt_Search, arr[55]);
            LibUtils.waitElementVisible(driver, tbl_realizationForm_debtorname, Const.seconds);
            takeSreenshot.capture(driver);
            LibUtils.capture.log(Status.INFO, arr[55] + ": Data lama status rejected");
            click(driver, tbl_realizationForm_debtorname);
            takeSreenshot.capture(driver);
            LibUtils.capture.log(Status.INFO, arr[55] + ": Detail data lama status rejected");

            arr = readCSVFormRealization.fileCSVRea();
            String alamat2 = arr[61];
            updateFieldCSV(alamat2, "New alamat status rejected ");
            consumeData.serverAksesRea(); //reconsume file realisasi csv

            click(driver, menuRealization);
            writeText(driver, txt_Search, arr[55]);
            LibUtils.waitElementVisible(driver, tbl_realizationForm_debtorname, Const.seconds);
            takeSreenshot.capture(driver);
            LibUtils.capture.log(Status.INFO, arr[55] + ": Data baru status waiting approval");
            click(driver, tbl_realizationForm_debtorname);
            takeSreenshot.capture(driver);
            LibUtils.capture.log(Status.INFO, arr[55] + ": Detail data baru status waiting approval");

            click(driver, menuRealization);
            writeText(driver, txt_Search, arr[55]);
            LibUtils.waitElementVisible(driver, tbl_realizationForm_debtorname_2nd, Const.seconds);
            takeSreenshot.capture(driver);
            LibUtils.capture.log(Status.INFO, arr[55] + ": Data lama status rejected");
            click(driver, tbl_realizationForm_debtorname_2nd);
            takeSreenshot.capture(driver);
            LibUtils.capture.log(Status.INFO, arr[55] + ": Detail data lama status rejected");

            status_testCase(iRowPictName, true, "");

        } catch (Exception e) {
            e.printStackTrace();

            takeSreenshot.capture(driver);
            LibUtils.capture.log(Status.FAIL, e);
            status_testCase(iRowPictName, false, e.getLocalizedMessage());
        }
    }

    public void reconsumeReafileUpdateAlamatPendingBatchRun() {

        iRowPictName = 119;
        createTest(iRowPictName, extent_test_case, extent);
        try {

            readTestData.testData();
            UtilsFile cleanFolder = new UtilsFile();
            cleanFolder.dltDummyExist();

            DataCSV dataCSV = new DataCSV(); // generate data
            dataCSV.dataDummy(Const.appFile, Const.reaFile, "1");
            consumeData.serverAkses(); // consume data approval file csv
            // change status to waiting approval for realisasi
            logout(driver);
            loginMaker(driver);
            changedRecomendationAsMaker_rea(driver, 1);
            loginApprover(driver, Const.e2e_user_ldap_approveRecomend, Const.e2e_pwd_ldap_approveRecomend);
            changedApproveAsApprover_rea(driver, 1);

            consumeData.serverAksesRea(); // consume realisasi file csv
            loginApprover(driver, Const.e2e_user_ldap_approveRealisasi, Const.e2e_pwd_ldap_approveRealisasi);

            click(driver, menuRealization);
            click(driver, bulk_approve);
            String[] arr = readCSVFormRealization.fileCSVRea();
            writeText(driver, txt_search_bulkApprove, arr[55]);
            LibUtils.waitElementVisible(driver, bulk_linkDebtorName, Const.seconds);
//           takeSreenshot.capture(driver);
//            System.out.println(arr[55]);
            click(driver, btn_radioFirst_Bulk);
            click(driver, btn_bulkApprove);
            Thread.sleep(3000);
            writeText(driver, txt_search_bulkApprove, arr[55]);
            LibUtils.waitElementVisible(driver, bulk_linkDebtorName, Const.seconds);
            takeSreenshot.capture(driver);
            LibUtils.capture.log(Status.INFO, arr[55] + ": Data lama status PBR");
            click(driver, bulk_linkDebtorName);
            takeSreenshot.capture(driver);
            LibUtils.capture.log(Status.INFO, arr[55] + ": Detail data lama status PBR");

            String alamat2 = arr[61];
            updateFieldCSV(alamat2, "New alamat status PBR ");
            consumeData.serverAksesRea(); //reconsume file realisasi csv

            click(driver, menuDahsboard);
            click(driver, processMonitoring);
            moveTo(driver, dashboard_rea_rejectBySystem);
            click(driver, dashboard_rea_rejectBySystem);
            writeText(driver, txt_search_frommonitoring, arr[55]);
            LibUtils.waitElementVisible(driver, monitor_waitingApproval_debtorname, Const.seconds);
            takeSreenshot.capture(driver);
            LibUtils.capture.log(Status.INFO, arr[55] + ": Data baru reject by system");
            click(driver, monitor_waitingApproval_debtorname);
            takeSreenshot.capture(driver);
            LibUtils.capture.log(Status.INFO, arr[55] + ": Detail data baru reject by system");

            click(driver, menuRealization);
            writeText(driver, txt_Search, arr[55]);
            LibUtils.waitElementVisible(driver, tbl_realizationForm_debtorname, Const.seconds);
            takeSreenshot.capture(driver);
            LibUtils.capture.log(Status.INFO, arr[55] + ": Data lama status PBR");
            click(driver, tbl_realizationForm_debtorname);
            takeSreenshot.capture(driver);
            LibUtils.capture.log(Status.INFO, arr[55] + ": Detail data lama status PBR");

            status_testCase(iRowPictName, true, "");

        } catch (Exception e) {
            // after
            takeSreenshot.capture(driver);
            LibUtils.capture.log(Status.FAIL, e);
            status_testCase(iRowPictName, false, e.getLocalizedMessage());
        }
    }

    public void reconsumeReafileUpdateAlamatApproved() {

        iRowPictName = 120;
        createTest(iRowPictName, extent_test_case, extent);
        try {

            readTestData.testData();
            UtilsFile cleanFolder = new UtilsFile();
            cleanFolder.dltDummyExist();

            DataCSV dataCSV = new DataCSV(); // generate data
            dataCSV.dataDummy(Const.appFile, Const.reaFile, "1");
            consumeData.serverAkses(); // consume data approval file csv
            // change status to waiting approval for realisasi
            logout(driver);
            loginMaker(driver);
            changedRecomendationAsMaker_rea(driver, 1);
            loginApprover(driver, Const.e2e_user_ldap_approveRecomend, Const.e2e_pwd_ldap_approveRecomend);
            changedApproveAsApprover_rea(driver, 1);
            consumeData.serverAksesRea(); // consume realisasi file csv
            loginApprover(driver, Const.e2e_user_ldap_approveRealisasi, Const.e2e_pwd_ldap_approveRealisasi);

            click(driver, menuRealization);
            String[] arr = readCSVFormRealization.fileCSVRea();
            writeText(driver, txt_Search, arr[55]);
            click(driver, tbl_realizationForm_debtorname);
            click(driver, btn_approve_detail_formRealisasi3);
            click(driver, popup_ya);
            writeText(driver, txt_Search, arr[55]);
            takeSreenshot.capture(driver);
            LibUtils.capture.log(Status.INFO, arr[55] + ": Data lama");
            click(driver, tbl_realizationForm_debtorname);
            takeSreenshot.capture(driver);
            LibUtils.capture.log(Status.INFO, arr[55] + ": Detail data lama");

            String alamat2 = arr[61];
            updateFieldCSV(alamat2, "New alamat status Approved ");
            consumeData.serverAksesRea(); //reconsume file realisasi csv

            click(driver, menuDahsboard);
            click(driver, processMonitoring);
            moveTo(driver, dashboard_rea_rejectBySystem);
            click(driver, dashboard_rea_rejectBySystem);
            writeText(driver, txt_search_frommonitoring, arr[55]);
            LibUtils.waitElementVisible(driver, monitor_waitingApproval_debtorname, Const.seconds);
            takeSreenshot.capture(driver);
            LibUtils.capture.log(Status.INFO, arr[55] + ": Data baru reject by system");
            click(driver, monitor_waitingApproval_debtorname);
            takeSreenshot.capture(driver);
            LibUtils.capture.log(Status.INFO, arr[55] + ": Detail data baru reject by system");

            click(driver, menuRealization);
            writeText(driver, txt_Search, arr[55]);
            LibUtils.waitElementVisible(driver, tbl_realizationForm_debtorname, Const.seconds);
            takeSreenshot.capture(driver);
            LibUtils.capture.log(Status.INFO, arr[55] + ": Data lama status approved");
            click(driver, tbl_realizationForm_debtorname);
            takeSreenshot.capture(driver);
            LibUtils.capture.log(Status.INFO, arr[55] + ": Detail data lama status approved");
            status_testCase(iRowPictName, true, "");

        } catch (Exception e) {

            takeSreenshot.capture(driver);

            status_testCase(iRowPictName, false, e.getLocalizedMessage());
        }
    }

    public void reconsumeReafileUpdateAlamatApprovalExpired() {

        iRowPictName = 117;
        createTest(iRowPictName, extent_test_case, extent);
        try {

            readTestData.testData();
            UtilsFile cleanFolder = new UtilsFile();
            cleanFolder.dltDummyExist();

            DataCSV dataCSV = new DataCSV(); // generate data
            dataCSV.dataDummy(Const.appFile, Const.reaFile, "1");

            consumeData.serverAkses(); // consume data approval file csv
            // change status to waiting approval for
            logout(driver);
            loginMaker(driver);
            changedRecomendationAsMaker_rea(driver, 1);
            loginApprover(driver, Const.e2e_user_ldap_approveRecomend, Const.e2e_pwd_ldap_approveRecomend);
            changedApproveAsApprover_rea(driver, 1);
            consumeData.serverAksesRea(); // consume realisasi file csv

            loginApprover(driver, Const.e2e_user_ldap_approveRealisasi, Const.e2e_pwd_ldap_approveRealisasi);
            ExecutionJob.jobReaExpired(); //job realisasi expired
            takeSreenshot.capture(driver);

            String[] arr = readCSVFormRealization.fileCSVRea();
            click(driver, menuRealization);
            writeText(driver, txt_Search, arr[55]);
            LibUtils.waitElementVisible(driver, tbl_realizationForm_debtorname, Const.seconds);
            takeSreenshot.capture(driver);
            LibUtils.capture.log(Status.INFO, arr[55] + ": Data lama");
            click(driver, tbl_realizationForm_debtorname);
            takeSreenshot.capture(driver);
            LibUtils.capture.log(Status.INFO, arr[55] + ": Detail data lama");

            String alamat1 = arr[61]; // old string
            updateFieldCSV(alamat1, "New alamat status expired "); // update csv alamat
            consumeData.serverAksesRea(); //reconsume file realisasi csv

            click(driver, menuRealization);
            writeText(driver, txt_Search, arr[55]);
            LibUtils.waitElementVisible(driver, tbl_realizationForm_debtorname, Const.seconds);
            takeSreenshot.capture(driver);
            LibUtils.capture.log(Status.INFO, arr[55] + ": Muncul data baru dan data lama");
            click(driver, tbl_realizationForm_debtorname);
            takeSreenshot.capture(driver);
            LibUtils.capture.log(Status.INFO, arr[55] + ": Detail data baru status waiting approval");

            click(driver, menuRealization);
            writeText(driver, txt_Search, arr[55]);
            LibUtils.waitElementVisible(driver, tbl_realizationForm_debtorname_2nd, Const.seconds);
//           takeSreenshot.capture(driver);
//            LibUtils.capture.log(Status.INFO, arr[55] + ": Muncul data baru dan data lama");
            click(driver, tbl_realizationForm_debtorname_2nd);
            takeSreenshot.capture(driver);
            LibUtils.capture.log(Status.INFO, arr[55] + ": Detail data lama status expired");
            status_testCase(iRowPictName, true, "");

        } catch (Exception e) {


            takeSreenshot.capture(driver);
            LibUtils.capture.log(Status.FAIL, e);
            status_testCase(iRowPictName, false, e.getLocalizedMessage());
        }
    }

    public void reconsumeReafileUpdateAlamatPending() {
        iRowPictName = 124;
        createTest(iRowPictName, extent_test_case, extent);
        LibUtils.capture.log(Status.SKIP, "SKIP");
        takeSreenshot.capture(driver);
        status_testCase(iRowPictName, false, "SKIP");
    }
}
