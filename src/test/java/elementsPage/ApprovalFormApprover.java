package elementsPage;

import applos.ConsumeData;
import capture.TakeSreenshot;
import com.aventstack.extentreports.Status;
import createDataCSV.DataCSV;
import individu.AdditionalProcess;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import randomValue.GenerateRandomDataApp;
import runner.FormApprovalRunner;
import testData.ReadTestData;
import writeFile.UtilsFile;

import java.util.List;

import static elementsPage.ApprovalFormMaker.*;
import static individu.ChangesStatus.changedApproveAsApprover;
import static individu.ChangesStatus.changedRecomendationAsMaker;
import static individu.LibUtils.*;
import static runner.FormApprovalRunner.extent;
import static runner.FormApprovalRunner.extent_test_case;

public class ApprovalFormApprover extends BaseAction {

    static WebDriver driver = FormApprovalRunner.driver;
    public int iShoot, iRowPictName = 67, iSeq = 1;

    static ReadTestData readTestData = new ReadTestData();
    TakeSreenshot takeSreenshot = new TakeSreenshot();
    String notes = "";

    public static void loginApprover(WebDriver conn, String user, String pass) {

        driver = conn;
        readTestData.testData();
        try {
            if (readTestData.environment.equals(Const.e2e_environment)) {
                try {
                    driver.get(Const.urlE2E);
                    driver.manage().window().maximize();
                } catch (Exception ignored) {
                }
                try {
                    click(driver, btnDetails);
                    click(driver, btnUnsafe);
                } catch (Exception ignored) {
                }
                writeText(driver, userId, user);
                writeText(driver, pwd, pass);

            } else {
                try {
                    driver.get(Const.urlUAT);
                    driver.manage().window().maximize();
                } catch (Exception ignored) {
                }
                try {
                    click(driver, btnDetails);
                    click(driver, btnUnsafe);
                } catch (Exception ignored) {
                }
                writeText(driver, userId, Const.uat_userApprover);
                writeText(driver, pwd, Const.uat_password);
            }
            click(driver, btnLogin);
        } catch (Exception ignored) {
        }
    }

    public void bulkCheckBeberapaStatusWaitingApproval() {
        //67
        try {
            AdditionalProcess.provideDataApprovalFormUserApprover();

            iRowPictName = 67;
            createTest(iRowPictName, extent_test_case, extent);

            readTestData.testData();
            click(driver, menuApproval);
            waitElementVisible(driver, debtorName_firstAppForm, Const.seconds);
            click(driver, btnBulkApprove);
            waitElementVisible(driver, txtSearch, Const.seconds);
            writeText(driver, txtSearch, readTestData.timeStamp);
            click(driver, bulk_statusWaitingApproval);
            click(driver, list_source);
            scrollIntoView(driver, bulkList_sourceKopnus);
            click(driver, bulkList_sourceKopnus);
            waitElementVisible(driver, radioButtonFirst, Const.seconds);
            for (int x = 1; x <= 5; x++) {
                click(driver, By.xpath("//*[@id='channelingApprovalDataTable']/tbody/tr[" + x + "]/td[1]/div/label"));

            }
            actualLogTrue(String.valueOf(boolean_isDisplayed(driver, radioButtonFirst)));
        } catch (Exception e) {
            capture.log(Status.FAIL, e);
            e.getCause();
        }

        takeSreenshot.capture(driver);
        status_testCase(iRowPictName, true, notes);
    }

    public void bulkKlikUncheckBeberapaStatusWaitingApproval() {
        //68
        try {
            iRowPictName = iRowPictName + iSeq;
            createTest(iRowPictName, extent_test_case, extent);
            takeSreenshot.capture(driver);
            for (int x = 1; x <= 3; x++) {
                click(driver, By.xpath("//*[@id='channelingApprovalDataTable']/tbody/tr[" + x + "]/td[1]/div/label"));
            }
            actualLogTrue(String.valueOf(boolean_isDisplayed(driver, radioButtonFirst)));
        } catch (Exception e) {
            e.getCause();
        }

        takeSreenshot.capture(driver);
        status_testCase(iRowPictName, true, notes);
    }

    public void klikBackSelection() {
        //69
        iRowPictName = iRowPictName + iSeq;
        createTest(iRowPictName, extent_test_case, extent);
        takeSreenshot.capture(driver);
        click(driver, btn_cancelSelection);
        actualLogTrue(String.valueOf(boolean_isDisplayed(driver, firstNumberTableForm)));
        waitElementVisible(driver, firstNumberTableForm, Const.seconds);

        takeSreenshot.capture(driver);
        status_testCase(iRowPictName, true, notes);
    }

    public void bulkKlikCheckALl() {
        //70
        iRowPictName = iRowPictName + iSeq;
        createTest(iRowPictName, extent_test_case, extent);

        click(driver, btnBulkApprove);
        waitElementVisible(driver, radioButtonFirst, Const.seconds);
        click(driver, bulk_statusWaitingApproval);
        waitElementVisible(driver, radioButtonFirst, Const.seconds);
        actualLogTrue(String.valueOf(boolean_isDisplayed(driver, radiobtnAppAll)));
        click(driver, radiobtnAppAll);

        takeSreenshot.capture(driver);
        status_testCase(iRowPictName, true, notes);

    }

    public void bulkKlikCheckStatusApproved() {
        //71
        iRowPictName = iRowPictName + iSeq;
        createTest(iRowPictName, extent_test_case, extent);
        click(driver, bulk_statusApproved);
        waitElementVisible(driver, radioButtonFirst, Const.seconds);
        actualLogTrue(String.valueOf(boolean_isDisplayed(driver, radioButtonFirst)));
        click(driver, radioButtonFirst);

        takeSreenshot.capture(driver);
        status_testCase(iRowPictName, true, notes);
    }

    public void bulkKlikCheckStatusRejected() {
        //72
        iRowPictName = iRowPictName + iSeq;
        createTest(iRowPictName, extent_test_case, extent);

        click(driver, bulk_statusRejected);
        waitElementVisible(driver, radioButtonFirst, Const.seconds);
        actualLogTrue(String.valueOf(boolean_isDisplayed(driver, radioButtonFirst)));
        click(driver, radioButtonFirst);

        takeSreenshot.capture(driver);
        status_testCase(iRowPictName, true, notes);
    }

    public void bulkKlikCheckStatusWaitingApproval() {
        //73
        iRowPictName = iRowPictName + iSeq;
        createTest(iRowPictName, extent_test_case, extent);

        click(driver, bulk_statusWaitingApproval);
        waitElementVisible(driver, radioButtonFirst, Const.seconds);
        writeText(driver, txtSearch, readTestData.timeStamp);
        actualLogTrue(String.valueOf(boolean_isDisplayed(driver, radioButtonFirst)));
        click(driver, radioButtonFirst);

        takeSreenshot.capture(driver);
        status_testCase(iRowPictName, true, notes);
    }

    public void bulkKlikCheckStatusExpired() {
        //74
        iRowPictName = iRowPictName + iSeq;
        createTest(iRowPictName, extent_test_case, extent);

        driver.navigate().refresh();
        click(driver, list_status);
        scrollIntoView(driver, list_statusApprovalExpired2);
        click(driver, list_statusApprovalExpired2);
        try {

            waitElementVisible(driver, radioButtonFirst, Const.seconds);
            click(driver, radioButtonFirst);
            actualLogTrue(String.valueOf(boolean_isDisplayed(driver, radioButtonFirst)));
            takeSreenshot.capture(driver);
            status_testCase(iRowPictName, true, "");

        } catch (Exception e) {
            capture.log(Status.SKIP, e);
            status_testCase(iRowPictName, false, "Not ready/Pending test");
        }
    }

    public void bulkKlikCheckStatusWaitingApprovalKlikreject() {
        //75
        try {
            String coll_noAplikasi = null;
            String namaDebRecomended;

            iRowPictName = iRowPictName + iSeq;
            createTest(iRowPictName, extent_test_case, extent);

            click(driver, menuApproval);
            waitElementVisible(driver, debtorName_firstAppForm, Const.seconds);
            click(driver, btnBulkApprove);
            waitElementVisible(driver, txtSearch, Const.seconds);
            writeText(driver, txtSearch, readTestData.timeStamp);
            click(driver, bulk_statusWaitingApproval);

            click(driver, list_source);
            scrollIntoView(driver, bulkList_sourceKopnus);
            click(driver, bulkList_sourceKopnus);
            waitElementVisible(driver, bulkFilter_rwDatafirst, Const.seconds);
            waitElementVisible(driver, radioButtonFirst, Const.seconds);
            for (int x = 1; x <= 2; x++) {
                click(driver, By.xpath("//*[@id='channelingApprovalDataTable']/tbody/tr[" + x + "]/td[1]/div/label"));
                namaDebRecomended = getText(driver, By.xpath("//*[@id='channelingApprovalDataTable']/tbody/tr[" + x + "]/td[3]/a"));
                coll_noAplikasi = coll_noAplikasi + "|" + namaDebRecomended;
            }

            takeSreenshot.capture(driver);
            String[] str_loanAmount = coll_noAplikasi.split("\\|");

            click(driver, notRecomended);

            for (int i = 1; i < str_loanAmount.length; i++) {
                Thread.sleep(Const.delay);
                click(driver, menuApproval);
                waitElementVisible(driver, firstNumberTableForm, Const.seconds);
                writeText(driver, txtSearch, str_loanAmount[i]);
                waitElementVisible(driver, debtorName_firstAppForm, Const.seconds);
                verifyElementExist(driver, debtorName_firstAppForm);
                takeSreenshot.capture(driver);
            }
            Thread.sleep(Const.delay);
            status_testCase(iRowPictName, true, notes);
        } catch (Exception e) {

            takeSreenshot.capture(driver);
            status_testCase(iRowPictName, false, notes);
        }
    }

    public void bulkKlikCheckStatusWaitingApprovalKlikApproveRecomendation() {
        //76
        try {
            String coll_noAplikasi = null;
            String namaDebRecomended;

            iRowPictName = iRowPictName + iSeq;
            createTest(iRowPictName, extent_test_case, extent);

            click(driver, menuApproval);
            waitElementVisible(driver, debtorName_firstAppForm, Const.seconds);
            click(driver, btnBulkApprove);
            waitElementVisible(driver, txtSearch, Const.seconds);
            writeText(driver, txtSearch, readTestData.timeStamp);
            click(driver, bulk_statusWaitingApproval);

            click(driver, list_source);
            scrollIntoView(driver, bulkList_sourceKopnus);
            click(driver, bulkList_sourceKopnus);
            waitElementVisible(driver, bulkFilter_rwDatafirst, Const.seconds);
            waitElementVisible(driver, radioButtonFirst, Const.seconds);

            for (int x = 1; x <= 2; x++) {
                click(driver, By.xpath("//*[@id='channelingApprovalDataTable']/tbody/tr[" + x + "]/td[1]/div/label"));
                namaDebRecomended = getText(driver, By.xpath("//*[@id='channelingApprovalDataTable']/tbody/tr[" + x + "]/td[3]/a"));
                coll_noAplikasi = coll_noAplikasi + "|" + namaDebRecomended;
            }

            takeSreenshot.capture(driver);
            String[] str_loanAmount = coll_noAplikasi.split("\\|");

            click(driver, recomended);

            for (int i = 1; i < str_loanAmount.length; i++) {
                Thread.sleep(Const.delay);
                click(driver, menuApproval);
                waitElementVisible(driver, firstNumberTableForm, Const.seconds);
                writeText(driver, txtSearch, str_loanAmount[i]);
                waitElementVisible(driver, debtorName_firstAppForm, Const.seconds);
                verifyElementExist(driver, debtorName_firstAppForm);
                takeSreenshot.capture(driver);
            }
            Thread.sleep(Const.delay);
            status_testCase(iRowPictName, true, notes);
        } catch (Exception e) {

            takeSreenshot.capture(driver);
            status_testCase(iRowPictName, false, notes);
        }
    }

    public void debtorNameWaitingApproval() {
        //77
        iRowPictName = iRowPictName + iSeq;
        createTest(iRowPictName, extent_test_case, extent);

        driver.navigate().refresh();
        click(driver, menuApproval);
        waitElementVisible(driver, debtorName_firstAppForm, Const.seconds);
        click(driver, btnBulkApprove);
        waitElementVisible(driver, txtSearch, Const.seconds);
        writeText(driver, txtSearch, readTestData.timeStamp);

        click(driver, bulk_statusWaitingApproval);
        click(driver, list_source);
        scrollIntoView(driver, bulkList_sourceKopnus);
        click(driver, bulkList_sourceKopnus);
        waitElementVisible(driver, radioButtonFirst, Const.seconds);
        click(driver, bulkFilter_rwDatafirst1);
        waitElementVisible(driver, txt_approvalFormDetail, Const.seconds);

        takeSreenshot.capture(driver);
        scrollIntoView(driver, txtFieldNotes);
        takeSreenshot.capture(driver);
        click(driver, pilih_recomendation);
        takeSreenshot.capture(driver);
        actualLogTrue(String.valueOf(boolean_isDisplayed(driver, pilih_recomendation)));
        status_testCase(iRowPictName, true, notes);
    }

    public void debtorNameStatusAppvalExpired() throws InterruptedException {
        //78
        iRowPictName = iRowPictName + iSeq;
        createTest(iRowPictName, extent_test_case, extent);

        driver.navigate().refresh();
        click(driver, btn_backApprovalFormDetail);
        waitElementVisible(driver, firstNumberTableForm, Const.seconds);
        click(driver, bulk_statusApprovalExpired);
        Thread.sleep(5000);
        Thread.sleep(Const.delay);
        takeSreenshot.capture(driver);
        waitElementVisible(driver, debtorName_firstAppForm, Const.seconds);
        boolean exist = boolean_isDisplayed(driver, debtorName_firstAppForm);
        click(driver, debtorName_firstAppForm);
        actualLogTrue(String.valueOf(exist));
        takeSreenshot.capture(driver);
        status_testCase(iRowPictName, false, notes);
    }

    public void debtorNameStatusApprovalApproved() {
        //79
        iRowPictName = iRowPictName + iSeq;
        createTest(iRowPictName, extent_test_case, extent);

        click(driver, menuApproval);
        waitElementVisible(driver, debtorName_firstAppForm, Const.seconds);
        click(driver, bulk_statusApproved);
        click(driver, list_partner);
        scrollIntoView(driver, By.xpath("//*[starts-with(@id, 'select2-select-partner-result-') and contains(@id,'-" + readTestData.companyName + "')]"));
        click(driver, By.xpath("//*[starts-with(@id, 'select2-select-partner-result-') and contains(@id,'-" + readTestData.companyName + "')]"));
        writeText(driver, txtSearch, readTestData.timeStamp);
        waitElementVisible(driver, debtorName_firstAppForm, Const.seconds);
        boolean exist = boolean_isDisplayed(driver, debtorName_firstAppForm);
        click(driver, debtorName_firstAppForm);
        actualLogTrue(String.valueOf(exist));
        waitElementVisible(driver, txt_approvalFormDetail, Const.seconds);

        takeSreenshot.capture(driver);
        status_testCase(iRowPictName, true, notes);
    }

    public void debtorNameStatusRejected() {
        //80
        iRowPictName = iRowPictName + iSeq;
        createTest(iRowPictName, extent_test_case, extent);

        driver.navigate().refresh();
        click(driver, btn_backApprovalFormDetail);
        waitElementVisible(driver, firstNumberTableForm, Const.seconds);
        click(driver, bulk_statusRejected);
        click(driver, list_partner);
        boolean exist1 = boolean_isDisplayed(driver, list_partnerKopnus);
        scrollIntoView(driver, list_partnerKopnus);
        click(driver, list_partnerKopnus);
        writeText(driver, txtSearch, readTestData.timeStamp);
        waitElementVisible(driver, debtorName_firstAppForm, Const.seconds);
        boolean exist = boolean_isDisplayed(driver, debtorName_firstAppForm);
        click(driver, debtorName_firstAppForm);


        actualLogTrue(String.valueOf(exist));
        waitElementVisible(driver, txt_approvalFormDetail, Const.seconds);

        takeSreenshot.capture(driver);
        actualLogTrue(String.valueOf(exist));
        actualLogTrue(String.valueOf(exist1));
        status_testCase(iRowPictName, true, notes);
    }

    public void statusWaitingApprovalTidakKeputusanDanKeterangan() {
        //81
        try {
            iRowPictName = iRowPictName + iSeq;
            createTest(iRowPictName, extent_test_case, extent);

            driver.navigate().refresh();
            click(driver, btn_backApprovalFormDetail);
            click(driver, bulk_statusWaitingApproval);
            click(driver, list_partner);
            scrollIntoView(driver, list_partnerKopnus);
            click(driver, list_partnerKopnus);
            writeText(driver, txtSearch, readTestData.timeStamp);
            Thread.sleep(Const.delay);
            waitElementVisible(driver, debtorName_firstAppForm, Const.seconds);
            click(driver, debtorName_firstAppForm);
            waitElementVisible(driver, txt_approvalFormDetail, Const.seconds);

            takeSreenshot.capture(driver);
            click(driver, btnSubmitApp);
            Thread.sleep(5000);
            scrollIntoView(driver, txtDitolak);
            actualLogTrue(String.valueOf(boolean_isDisplayed(driver, txtDitolak)));
            takeSreenshot.capture(driver);
            status_testCase(iRowPictName, true, notes);
        } catch (Exception e) {
            e.printStackTrace();

            takeSreenshot.capture(driver);
            status_testCase(iRowPictName, false, notes);
        }
    }

    public void statusWaitingApprovalMengisiKeputusanDanTidakIsiKeterangan() {
        //82
        try {
            iRowPictName = iRowPictName + iSeq;
            createTest(iRowPictName, extent_test_case, extent);

            driver.navigate().refresh();
            waitElementVisible(driver, btn_backApprovalFormDetail, Const.seconds);

            takeSreenshot.capture(driver);
            scrollIntoView(driver, txtDitolak);
            click(driver, pilih_recomendation);
            click(driver, list_valueReject);
            takeSreenshot.capture(driver);
            scrollIntoView(driver, btnSubmitApp);
            Thread.sleep(Const.delay);
            takeSreenshot.capture(driver);
            Thread.sleep(Const.delay);
            click(driver, btnSubmitApp);
            Thread.sleep(5000);
            scrollIntoView(driver, txtDitolak);
            Thread.sleep(Const.delay);
            actualLogTrue(String.valueOf(boolean_isDisplayed(driver, txtDitolak)));
            takeSreenshot.capture(driver);
            status_testCase(iRowPictName, true, notes);
        } catch (Exception e) {
            e.printStackTrace();

            takeSreenshot.capture(driver);
            status_testCase(iRowPictName, false, notes);
        }

    }

    public void statusWaitingApprovalIsiKeputusanDanTidakIsiKeterangan() {
        //83
        try {
            iRowPictName = iRowPictName + iSeq;
            createTest(iRowPictName, extent_test_case, extent);

            driver.navigate().refresh();
            waitElementVisible(driver, btn_backApprovalFormDetail, Const.seconds);
            takeSreenshot.capture(driver);
            scrollIntoView(driver, txtDitolak);
            click(driver, pilih_recomendation);
            click(driver, list_valueApprove);

            takeSreenshot.capture(driver);
            scrollIntoView(driver, btnSubmitApp);
            Thread.sleep(Const.delay);
            takeSreenshot.capture(driver);
            Thread.sleep(Const.delay);
            actualLogTrue(String.valueOf(boolean_isDisplayed(driver, btnSubmitApp)));
            click(driver, btnSubmitApp);
            Thread.sleep(5000);
            Thread.sleep(Const.delay);

            takeSreenshot.capture(driver);
            status_testCase(iRowPictName, true, notes);
        } catch (Exception e) {
            e.printStackTrace();

            takeSreenshot.capture(driver);
            status_testCase(iRowPictName, false, notes);
        }

    }

    public void statusWaitingApprovalTidakIsiKeputusanDanIsiKeterangan() throws InterruptedException {
        //84
        iRowPictName = iRowPictName + iSeq;
        createTest(iRowPictName, extent_test_case, extent);

        click(driver, menuApproval);
        waitElementVisible(driver, debtorName_firstAppForm, Const.seconds);

        writeText(driver, txtSearch, readTestData.timeStamp);
        click(driver, bulk_statusWaitingApproval);
        waitElementVisible(driver, debtorName_firstAppForm, Const.seconds);
        click(driver, debtorName_firstAppForm);
        waitElementVisible(driver, txt_approvalFormDetail, Const.seconds);

        scrollIntoView(driver, txtDitolak);
        writeText(driver, txtFieldNotes, "Data Recommended tidak pilih keputusan");
        takeSreenshot.capture(driver);
        scrollIntoView(driver, btnSubmitApp);
        takeSreenshot.capture(driver);
        Thread.sleep(Const.delay);
        click(driver, btnSubmitApp);
        Thread.sleep(5000);

        scrollIntoView(driver, txtDitolak);
        actualLogTrue(String.valueOf(boolean_isDisplayed(driver, txtDitolak)));
        takeSreenshot.capture(driver);
        status_testCase(iRowPictName, true, notes);
    }

    public void statusWaitingApprovalIsiKeputusanDanIsiKeterangan(List<String> list_case) throws InterruptedException {
        //85-86
        for (int i = 1; i <= list_case.size(); i++) {
            click(driver, menuApproval);
            waitElementVisible(driver, debtorName_firstAppForm, Const.seconds);
            click(driver, btnBulkApprove);
            waitElementVisible(driver, txtTblBulk_debtorName, Const.seconds);
            writeText(driver, txtSearch, readTestData.timeStamp);
            click(driver, bulk_statusWaitingApproval);
            click(driver, list_source);
            scrollIntoView(driver, bulkList_sourceKopnus);
            click(driver, txtTblBulk_debtorName);
            waitElementVisible(driver, txt_approvalFormDetail, Const.seconds);
            String rdmtext;
            String debtorName = getText(driver, txtDetail_namaDebitur); //get debtor name detail
            scrollIntoView(driver, txtDitolak);
            click(driver, pilih_recomendation);
            click(driver, valueApprove);
            if (i == 1) {
                rdmtext = GenerateRandomDataApp.rndmText() + " 5000 karakter";
                writeText(driver, txtFieldNotes, rdmtext);
                iRowPictName = iRowPictName + iSeq;
                createTest(iRowPictName, extent_test_case, extent);
                takeSreenshot.capture(driver);
            } else {
                rdmtext = GenerateRandomDataApp.rndmText() + " 4999karakter";
                writeText(driver, txtFieldNotes, rdmtext);
                iRowPictName = iRowPictName + iSeq;
                createTest(iRowPictName, extent_test_case, extent);
                takeSreenshot.capture(driver);
            }
            Thread.sleep(3000);
            scrollIntoView(driver, btnSubmitApp);
            click(driver, btnSubmitApp);
            waitElementVisible(driver, txtSearch, Const.seconds);
            writeText(driver, txtSearch, debtorName);
            waitElementVisible(driver, tableFormFilter_debtNameFirstAPP, Const.seconds);
            click(driver, tableFormFilter_debtNameFirstAPP);
            waitElementVisible(driver, txt_approvalFormDetail, Const.seconds);
            codeBlockText("Jumlah char: " + rdmtext.length() + " - " + rdmtext);
            actualLogTrue("true");
            takeSreenshot.capture(driver);
            status_testCase(iRowPictName, true, notes);
        }
    }

    public static void main(String[] args) throws InterruptedException {
        String total = "null|Rp. 1.161.237|Rp. 1.161.237|Rp. 1.161.237|Rp. 1.161.237|Rp. 3.480.465|Rp. 2.320.851|Rp. 1.161.237|Rp. 1.161.237|Rp. 1.161.237|Rp. 1.161.237|Rp. 1.161.237|Rp. 1.161.237|Rp. 1.161.237|Rp. 6.000.790|Rp. 4.640.078|Rp. 1.161.237|Rp. 1.161.237|Rp. 1.161.237|Rp. 6.000.790|Rp. 4.640.078|Rp. 2.320.851|Rp. 1.161.237|Rp. 1.161.237|Rp. 1.161.237|Rp. 8.403.307|Rp. 1.161.237|Rp. 4.640.078|Rp. 1.161.237|Rp. 2.320.851|Rp. 1.161.237|Rp. 1.161.237|Rp. 2.320.851|Rp. 1.161.237|Rp. 1.161.237|Rp. 2.320.851|Rp. 1.161.237|Rp. 6.000.790|Rp. 1.161.237|Rp. 1.161.237|Rp. 3.480.465|Rp. 6.000.790|Rp. 2.320.851|Rp. 1.161.237|Rp. 6.000.790|Rp. 1.161.237|Rp. 7.200.214|Rp. 1.161.237|Rp. 4.640.078|Rp. 2.320.851|Rp. 3.480.465|Rp. 1.161.237|Rp. 3.480.465|Rp. 7.200.214|Rp. 7.200.214|Rp. 1.161.237|Rp. 1.161.237|Rp. 3.480.465|Rp. 4.640.078|Rp. 1.161.237|Rp. 6.000.790|Rp. 1.161.237|Rp. 4.640.078|Rp. 3.480.465|Rp. 1.161.237|Rp. 1.161.237|Rp. 1.161.237|Rp. 6.000.790|Rp. 3.480.465|Rp. 1.161.237|Rp. 6.000.790|Rp. 3.480.465|Rp. 1.161.237|Rp. 6.000.790|Rp. 4.640.078|Rp. 1.161.237|Rp. 2.320.851|Rp. 4.640.078|Rp. 1.161.237|Rp. 4.640.078|Rp. 1.161.237|Rp. 3.480.465|Rp. 3.480.465|Rp. 6.000.790|Rp. 1.161.237|Rp. 2.320.851|Rp. 1.161.237|Rp. 4.640.078|Rp. 1.161.237|Rp. 1.161.237|Rp. 4.640.078|Rp. 3.480.465|Rp. 1.161.237|Rp. 1.161.237|Rp. 2.320.851|Rp. 7.200.214|Rp. 1.161.237|Rp. 6.000.790|Rp. 1.161.237|Rp. 1.161.237|Rp. 2.320.851|Rp. 6.000.790|Rp. 2.320.851|Rp. 1.161.237|Rp. 6.000.790|Rp. 6.000.790|Rp. 6.000.790|Rp. 6.000.790|Rp. 1.161.237|Rp. 1.161.237|Rp. 2.320.851|Rp. 3.480.465|Rp. 3.480.465|Rp. 7.200.214|Rp. 1.161.237|Rp. 1.161.237|Rp. 1.161.237|Rp. 3.480.465|Rp. 1.161.237|Rp. 2.320.851|Rp. 1.161.237|Rp. 1.161.237|Rp. 1.161.237|Rp. 3.480.465|Rp. 1.161.237|Rp. 1.161.237|Rp. 1.161.237|Rp. 1.161.237|Rp. 6.000.790|Rp. 4.640.078|Rp. 9.602.731|Rp. 9.602.731|Rp. 6.000.790|Rp. 1.161.237|Rp. 4.640.078|Rp. 2.320.851|Rp. 2.320.851|Rp. 1.161.237|Rp. 4.640.078|Rp. 4.640.078|Rp. 4.640.078|Rp. 2.320.851|Rp. 4.640.078|Rp. 1.161.237|Rp. 1.161.237|Rp. 1.161.237|Rp. 3.480.465|Rp. 6.000.790|Rp. 1.161.237|Rp. 1.161.237|Rp. 9.602.731|Rp. 4.640.078|Rp. 1.161.237|Rp. 1.161.237|Rp. 1.161.237|Rp. 1.161.237|Rp. 1.161.237|Rp. 1.161.237|Rp. 6.000.790|Rp. 1.161.237|Rp. 1.161.237|Rp. 1.161.237|Rp. 3.480.465|Rp. 4.640.078|Rp. 1.161.237|Rp. 1.161.237|Rp. 2.320.851|Rp. 1.161.237|Rp. 3.480.465|Rp. 1.161.237|Rp. 6.000.790|Rp. 2.320.851|Rp. 1.161.237|Rp. 6.000.790|Rp. 1.161.237|Rp. 1.161.237|Rp. 4.640.078|Rp. 9.602.731|Rp. 1.161.237|Rp. 1.161.237|Rp. 1.161.237|Rp. 1.161.237|Rp. 1.161.237|Rp. 1.161.237|Rp. 1.161.237|Rp. 1.161.237|Rp. 2.320.851|Rp. 2.320.851|Rp. 1.161.237|Rp. 1.161.237|Rp. 1.161.237|Rp. 6.000.790|Rp. 1.161.237|Rp. 1.161.237|Rp. 4.640.078|Rp. 1.161.237|Rp. 1.161.237|Rp. 4.640.078|Rp. 4.640.078|Rp. 1.161.237|Rp. 4.640.078|Rp. 3.480.465|Rp. 4.640.078|Rp. 1.161.237|Rp. 4.640.078|Rp. 3.480.465|Rp. 1.161.237|Rp. 6.000.790|Rp. 2.320.851|Rp. 1.161.237|Rp. 11.501.041|Rp. 1.161.237|Rp. 1.161.237|Rp. 2.320.851|Rp. 1.161.237|Rp. 1.161.237|Rp. 1.161.237|Rp. 4.640.078|Rp. 1.161.237|Rp. 6.000.790|Rp. 4.640.078|Rp. 2.320.851|Rp. 1.161.237|Rp. 1.200.000|Rp. 1.700.000|Rp. 1.200.000|Rp. 1.100.000|Rp. 1.100.000|Rp. 1.100.000|Rp. 1.100.000|Rp. 1.161.237|Rp. 3.480.465|Rp. 1.161.237|Rp. 3.480.465|Rp. 3.480.465|Rp. 6.000.790|Rp. 3.480.465|Rp. 6.000.790|Rp. 4.640.078|Rp. 1.161.237|Rp. 1.161.237|Rp. 1.161.237|Rp. 4.640.078|Rp. 4.640.078|Rp. 1.161.237|Rp. 1.161.237|Rp. 3.480.465|Rp. 1.161.237|Rp. 3.480.465|Rp. 1.161.237|Rp. 1.161.237|Rp. 6.000.790|Rp. 1.161.237|Rp. 1.161.237|Rp. 2.320.851|Rp. 1.161.237|Rp. 1.161.237|Rp. 1.161.237|Rp. 9.602.731|Rp. 6.000.790|Rp. 4.640.078|Rp. 3.480.465|Rp. 6.000.790|Rp. 1.161.237|Rp. 1.161.237|Rp. 1.161.237|Rp. 2.320.851|Rp. 1.161.237|Rp. 1.161.237|Rp. 4.640.078|Rp. 1.161.237|Rp. 4.640.078|Rp. 4.640.078|Rp. 1.161.237|Rp. 4.640.078|Rp. 1.161.237|Rp. 3.480.465|Rp. 4.640.078|Rp. 1.161.237|Rp. 1.161.237|Rp. 4.640.078|Rp. 2.320.851|Rp. 1.161.237|Rp. 1.161.237|Rp. 1.161.237|Rp. 1.161.237|Rp. 1.161.237|Rp. 1.161.237|Rp. 1.161.237|Rp. 11.501.041|Rp. 1.161.237|Rp. 1.161.237|Rp. 1.161.237|Rp. 4.640.078|Rp. 4.640.078|Rp. 1.161.237|Rp. 3.480.465|Rp. 4.640.078|Rp. 1.161.237|Rp. 6.000.790|Rp. 7.200.214|Rp. 4.640.078|Rp. 6.000.790|Rp. 3.480.465|Rp. 3.480.465|Rp. 1.161.237|Rp. 1.161.237|Rp. 1.161.237|Rp. 6.000.790|Rp. 1.161.237|Rp. 1.161.237|Rp. 1.161.237|Rp. 1.161.237|Rp. 1.161.237|Rp. 4.640.078|Rp. 4.640.078|Rp. 1.161.237|Rp. 1.161.237|Rp. 1.161.237|Rp. 1.161.237|Rp. 1.161.237|Rp. 1.161.237|Rp. 4.640.078|Rp. 1.161.237|Rp. 1.161.237|Rp. 1.161.237|Rp. 6.000.790|Rp. 3.480.465|Rp. 1.161.237|Rp. 2.320.851|Rp. 4.640.078|Rp. 6.000.790|Rp. 4.640.078|Rp. 4.640.078|Rp. 6.000.790|Rp. 4.640.078|Rp. 4.640.078|Rp. 6.000.790|Rp. 3.480.465|Rp. 4.640.078|Rp. 1.161.237|Rp. 6.000.790|Rp. 1.161.237|Rp. 1.161.237|Rp. 1.161.237|Rp. 1.161.237|Rp. 7.200.214|Rp. 1.161.237|Rp. 1.161.237|Rp. 1.161.237|Rp. 4.640.078|Rp. 1.161.237|Rp. 2.320.851|Rp. 6.000.790|Rp. 2.320.851|Rp. 6.000.790|Rp. 4.640.078|Rp. 4.640.078|Rp. 1.161.237|Rp. 1.161.237|Rp. 3.480.465|Rp. 1.161.237|Rp. 1.161.237|Rp. 4.640.078|Rp. 4.640.078|Rp. 1.161.237|Rp. 3.480.465|Rp. 1.161.237|Rp. 1.161.237|Rp. 1.161.237|Rp. 6.000.790|Rp. 1.161.237|Rp. 1.161.237|Rp. 3.480.465|Rp. 6.000.790|Rp. 3.480.465|Rp. 1.161.237|Rp. 1.161.237|Rp. 1.161.237|Rp. 6.000.790|Rp. 1.161.237|Rp. 1.161.237|Rp. 1.161.237|Rp. 1.161.237|Rp. 1.161.237|Rp. 2.320.851|Rp. 3.480.465|Rp. 2.320.851|Rp. 1.161.237|Rp. 3.480.465|Rp. 1.161.237|Rp. 1.161.237|Rp. 6.000.790|Rp. 6.000.790|Rp. 4.640.078|Rp. 1.161.237|Rp. 1.161.237|Rp. 1.161.237|Rp. 1.161.237|Rp. 1.161.237|Rp. 3.480.465|Rp. 8.403.307|Rp. 6.000.790|Rp. 3.480.465|Rp. 2.320.851|Rp. 7.200.214|Rp. 1.161.237|Rp. 4.640.078|Rp. 2.320.851|Rp. 4.640.078|Rp. 1.161.237|Rp. 1.161.237|Rp. 1.161.237|Rp. 3.480.465|Rp. 3.480.465|Rp. 3.480.465|Rp. 6.000.790|Rp. 1.161.237|Rp. 4.640.078|Rp. 4.640.078|Rp. 1.161.237|Rp. 1.161.237|Rp. 4.640.078|Rp. 1.161.237|Rp. 1.161.237|Rp. 4.640.078|Rp. 1.161.237|Rp. 1.161.237|Rp. 2.320.851|Rp. 1.161.237|Rp. 4.640.078|Rp. 1.161.237|Rp. 3.480.465|Rp. 1.161.237|Rp. 1.161.237|Rp. 6.000.790|Rp. 1.161.237|Rp. 1.161.237|Rp. 1.161.237|Rp. 3.480.465|Rp. 1.161.237|Rp. 3.480.465|Rp. 1.161.237|Rp. 3.480.465|Rp. 1.161.237|Rp. 4.640.078|Rp. 1.161.237|Rp. 1.161.237|Rp. 3.480.465|Rp. 2.320.851|Rp. 6.000.790|Rp. 2.320.851|Rp. 1.161.237|Rp. 1.161.237|Rp. 3.480.465|Rp. 1.161.237|Rp. 3.480.465|Rp. 3.480.465|Rp. 1.161.237|Rp. 3.480.465|Rp. 4.640.078|Rp. 4.640.078|Rp. 1.161.237|Rp. 1.161.237|Rp. 9.602.731|Rp. 1.161.237|Rp. 1.161.237|Rp. 2.320.851|Rp. 3.480.465|Rp. 4.640.078|Rp. 1.161.237|Rp. 1.161.237|Rp. 1.161.237|Rp. 1.161.237|Rp. 1.161.237|Rp. 3.480.465|Rp. 1.161.237|Rp. 1.161.237|Rp. 1.161.237|Rp. 1.161.237|Rp. 4.640.078|Rp. 4.640.078|Rp. 4.640.078|Rp. 2.320.851|Rp. 1.161.237|Rp. 3.480.465|Rp. 1.161.237|Rp. 1.161.237|Rp. 1.161.237|Rp. 7.200.214|Rp. 4.640.078|Rp. 4.640.078|Rp. 1.161.237|Rp. 1.161.237|Rp. 9.602.731|Rp. 7.200.214|Rp. 1.161.237|Rp. 4.640.078|Rp. 3.480.465|Rp. 1.161.237|Rp. 4.640.078|Rp. 1.161.237|Rp. 1.161.237|Rp. 4.640.078|Rp. 1.161.237|Rp. 7.200.214|Rp. 6.000.790|Rp. 1.161.237|Rp. 1.161.237|Rp. 1.161.237|Rp. 1.161.237|Rp. 3.480.465|Rp. 1.161.237|Rp. 2.320.851|Rp. 1.161.237|Rp. 6.000.790|Rp. 1.161.237|Rp. 1.161.237|Rp. 2.320.851|Rp. 1.161.237|Rp. 2.320.851|Rp. 1.161.237|Rp. 6.000.790|Rp. 1.161.237|Rp. 7.200.214|Rp. 1.161.237|Rp. 6.000.790|Rp. 1.161.237|Rp. 7.200.214|Rp. 1.161.237|Rp. 1.161.237|Rp. 1.161.237|Rp. 4.640.078|Rp. 1.161.237|Rp. 3.480.465|Rp. 3.480.465|Rp. 1.161.237|Rp. 1.161.237|Rp. 1.161.237|Rp. 1.161.237|Rp. 1.161.237|Rp. 4.640.078|Rp. 6.000.790|Rp. 2.320.851|Rp. 1.161.237|Rp. 1.161.237|Rp. 1.161.237|Rp. 2.320.851|Rp. 3.480.465|Rp. 3.480.465|Rp. 1.161.237|Rp. 1.161.237|Rp. 3.480.465|Rp. 1.161.237|Rp. 4.640.078|Rp. 1.161.237|Rp. 4.640.078|Rp. 1.161.237|Rp. 4.640.078|Rp. 4.640.078|Rp. 1.161.237|Rp. 1.161.237|Rp. 1.161.237|Rp. 3.480.465|Rp. 4.640.078|Rp. 1.161.237|Rp. 4.640.078|Rp. 1.161.237|Rp. 3.480.465|Rp. 2.320.851|Rp. 6.000.790|Rp. 1.161.237|Rp. 1.161.237|Rp. 2.320.851|Rp. 1.161.237|Rp. 1.161.237|Rp. 1.161.237|Rp. 1.161.237|Rp. 7.200.214|Rp. 3.480.465|Rp. 1.161.237|Rp. 1.161.237|Rp. 1.161.237|Rp. 4.640.078|Rp. 3.480.465|Rp. 1.161.237|Rp. 1.161.237|Rp. 3.480.465|Rp. 9.602.731|Rp. 4.640.078|Rp. 1.161.237|Rp. 1.161.237|Rp. 1.161.237|Rp. 1.161.237|Rp. 4.640.078|Rp. 1.161.237|Rp. 1.161.237|Rp. 4.640.078|Rp. 4.640.078|Rp. 1.161.237|Rp. 4.640.078|Rp. 1.161.237|Rp. 4.640.078|Rp. 3.480.465|Rp. 1.161.237|Rp. 6.000.790|Rp. 1.161.237|Rp. 6.000.790|Rp. 2.320.851|Rp. 3.480.465|Rp. 3.480.465|Rp. 9.602.731|Rp. 2.320.851|Rp. 1.161.237|Rp. 4.640.078|Rp. 1.161.237|Rp. 1.161.237|Rp. 1.161.237|Rp. 4.640.078|Rp. 1.161.237|Rp. 3.480.465|Rp. 1.161.237|Rp. 3.480.465|Rp. 1.161.237|Rp. 4.640.078|Rp. 1.161.237|Rp. 4.640.078|Rp. 4.640.078|Rp. 1.161.237|Rp. 1.161.237|Rp. 4.640.078|Rp. 1.161.237|Rp. 1.161.237|Rp. 1.161.237|Rp. 1.161.237|Rp. 4.640.078|Rp. 1.161.237|Rp. 6.000.790|Rp. 1.161.237|Rp. 1.161.237|Rp. 1.161.237|Rp. 1.161.237|Rp. 4.640.078|Rp. 2.320.851|Rp. 1.161.237|Rp. 7.200.214|Rp. 4.640.078|Rp. 2.320.851|Rp. 1.161.237|Rp. 4.640.078|Rp. 1.161.237|Rp. 1.161.237|Rp. 3.480.465|Rp. 1.161.237|Rp. 6.000.790|Rp. 1.161.237|Rp. 7.200.214|Rp. 6.000.790|Rp. 1.161.237|Rp. 1.161.237|Rp. 1.161.237|Rp. 1.161.237|Rp. 4.640.078|Rp. 2.320.851|Rp. 2.320.851|Rp. 6.000.790|Rp. 1.161.237|Rp. 1.161.237|Rp. 4.640.078|Rp. 2.320.851|Rp. 1.161.237|Rp. 1.161.237|Rp. 6.000.790|Rp. 3.480.465|Rp. 4.640.078|Rp. 1.161.237|Rp. 4.640.078|Rp. 4.640.078|Rp. 2.320.851|Rp. 1.161.237|Rp. 4.640.078|Rp. 1.161.237|Rp. 4.640.078|Rp. 1.161.237|Rp. 3.480.465|Rp. 6.000.790|Rp. 2.320.851|Rp. 6.000.790|Rp. 1.161.237|Rp. 6.000.790|Rp. 4.640.078|Rp. 2.320.851|Rp. 1.161.237|Rp. 4.640.078|Rp. 1.161.237|Rp. 2.320.851|Rp. 4.640.078|Rp. 1.161.237|Rp. 1.161.237|Rp. 1.161.237|Rp. 3.480.465|Rp. 1.161.237|Rp. 3.480.465|Rp. 1.161.237|Rp. 3.480.465|Rp. 1.161.237|Rp. 1.161.237|Rp. 1.161.237|Rp. 3.480.465|Rp. 6.000.790|Rp. 6.000.790|Rp. 2.320.851|Rp. 4.640.078|Rp. 6.000.790|Rp. 1.161.237|Rp. 4.640.078|Rp. 1.161.237|Rp. 4.640.078|Rp. 1.161.237|Rp. 4.640.078|Rp. 1.161.237|Rp. 4.640.078|Rp. 6.000.790|Rp. 6.000.790|Rp. 1.161.237|Rp. 3.480.465|Rp. 1.161.237|Rp. 1.161.237|Rp. 6.000.790|Rp. 1.161.237|Rp. 1.161.237|Rp. 4.640.078|Rp. 1.161.237|Rp. 1.161.237|Rp. 1.161.237|Rp. 6.000.790|Rp. 1.161.237|Rp. 3.480.465|Rp. 4.640.078|Rp. 1.161.237|Rp. 4.640.078|Rp. 1.161.237|Rp. 6.000.790|Rp. 7.200.214|Rp. 4.640.078|Rp. 6.000.790|Rp. 3.480.465|Rp. 1.161.237|Rp. 6.000.790|Rp. 1.161.237|Rp. 7.200.214|Rp. 1.161.237|Rp. 3.480.465|Rp. 1.161.237|Rp. 1.161.237|Rp. 1.161.237|Rp. 1.161.237|Rp. 1.161.237|Rp. 1.161.237|Rp. 6.000.790|Rp. 1.161.237|Rp. 4.640.078|Rp. 4.640.078|Rp. 1.161.237|Rp. 6.000.790|Rp. 6.000.790|Rp. 4.640.078|Rp. 4.640.078|Rp. 7.503.000|Rp. 1.161.237|Rp. 1.161.237|Rp. 1.161.237|Rp. 2.320.851|Rp. 1.161.237|Rp. 1.161.237|Rp. 4.640.078|Rp. 4.640.078|Rp. 1.161.237|Rp. 2.320.851|Rp. 1.161.237|Rp. 1.161.237|Rp. 2.320.851|Rp. 3.480.465|Rp. 1.161.237|Rp. 1.161.237|Rp. 4.640.078|Rp. 1.161.237|Rp. 1.161.237|Rp. 1.161.237|Rp. 1.161.237|Rp. 3.480.465|Rp. 1.161.237|Rp. 1.161.237|Rp. 1.161.237|Rp. 2.320.851|Rp. 2.320.851|Rp. 4.640.078|Rp. 1.161.237|Rp. 1.161.237|Rp. 1.161.237|Rp. 3.480.465|Rp. 1.161.237|Rp. 1.161.237|Rp. 2.320.851|Rp. 3.480.465|Rp. 4.640.078|Rp. 4.640.078|Rp. 7.200.214|Rp. 1.161.237|Rp. 1.161.237|Rp. 1.161.237|Rp. 3.480.465|Rp. 9.602.731|Rp. 2.320.851|Rp. 3.480.465|Rp. 1.161.237|Rp. 1.161.237|Rp. 3.480.465|Rp. 1.161.237|Rp. 1.161.237|Rp. 3.480.465|Rp. 6.000.790|Rp. 1.161.237|Rp. 6.000.790|Rp. 1.161.237|Rp. 1.161.237|Rp. 1.161.237|Rp. 6.000.790|Rp. 1.161.237|Rp. 3.480.465|Rp. 4.640.078|Rp. 1.161.237|Rp. 1.161.237|Rp. 1.161.237|Rp. 1.161.237|Rp. 6.000.790|Rp. 1.161.237|Rp. 6.000.790|Rp. 4.640.078|Rp. 3.480.465|Rp. 3.480.465|Rp. 3.480.465|Rp. 3.480.465|Rp. 1.161.237|Rp. 3.480.465|Rp. 6.000.790|Rp. 1.161.237|Rp. 1.161.237|Rp. 1.161.237|Rp. 6.000.790|Rp. 1.161.237|Rp. 3.480.465|Rp. 7.200.214|Rp. 1.161.237|Rp. 4.640.078|Rp. 1.161.237|Rp. 3.480.465|Rp. 1.161.237|Rp. 1.161.237|Rp. 1.161.237|Rp. 1.161.237|Rp. 3.480.465|Rp. 1.161.237|Rp. 1.161.237|Rp. 1.161.237|Rp. 3.480.465|Rp. 1.161.237|Rp. 4.640.078|Rp. 1.161.237|Rp. 1.161.237|Rp. 1.161.237|Rp. 4.640.078|Rp. 6.000.790|Rp. 2.320.851|Rp. 3.480.465|Rp. 1.161.237|Rp. 1.161.237|Rp. 3.480.465|Rp. 1.161.237|Rp. 1.161.237|Rp. 1.161.237|Rp. 4.640.078|Rp. 1.161.237|Rp. 1.161.237|Rp. 2.320.851|Rp. 3.480.465|Rp. 2.320.851|Rp. 1.161.237|Rp. 2.320.851|Rp. 1.161.237|Rp. 6.000.790|Rp. 3.480.465|Rp. 2.320.851|Rp. 4.640.078|Rp. 4.640.078|Rp. 4.640.078|Rp. 1.161.237|Rp. 1.161.237|Rp. 1.161.237|Rp. 2.320.851|Rp. 1.161.237|Rp. 3.480.465|Rp. 1.161.237|Rp. 4.640.078|Rp. 6.000.790|Rp. 9.602.731|Rp. 1.161.237|Rp. 4.640.078|Rp. 1.161.237|Rp. 1.161.237|Rp. 4.640.078|Rp. 1.161.237|Rp. 1.161.237|Rp. 2.320.851|Rp. 1.161.237|Rp. 1.161.237|Rp. 1.161.237|Rp. 1.161.237|Rp. 1.161.237|Rp. 1.161.237|Rp. 3.480.465|Rp. 1.161.237|Rp. 1.161.237|Rp. 3.480.465|Rp. 1.161.237|Rp. 1.161.237|Rp. 1.161.237|Rp. 4.640.078|Rp. 4.640.078|Rp. 4.640.078|Rp. 6.000.790|Rp. 3.480.465|Rp. 4.640.078|Rp. 1.161.237|Rp. 3.480.465|Rp. 1.161.237|Rp. 1.161.237|Rp. 1.161.237|Rp. 1.161.237|Rp. 4.640.078|Rp. 3.480.465|Rp. 4.640.078|Rp. 1.161.237|Rp. 1.161.237|Rp. 3.480.465|Rp. 2.320.851|Rp. 2.320.851|Rp. 2.320.851|Rp. 4.640.078|Rp. 4.640.078|Rp. 6.000.790|Rp. 2.320.851|Rp. 1.161.237|Rp. 4.640.078|Rp. 4.640.078|Rp. 2.320.851|Rp. 2.320.851|Rp. 2.320.851|Rp. 1.161.237|Rp. 1.161.237|Rp. 4.640.078|Rp. 6.000.790|Rp. 1.161.237|Rp. 6.000.790|Rp. 1.161.237|Rp. 1.161.237|Rp. 4.640.078|Rp. 6.000.790|Rp. 6.000.790|Rp. 1.161.237|Rp. 2.320.851|Rp. 1.161.237|Rp. 1.161.237|Rp. 3.480.465|Rp. 1.161.237|Rp. 1.161.237|Rp. 1.161.237|Rp. 3.480.465|Rp. 1.161.237|Rp. 1.161.237|Rp. 1.161.237|Rp. 2.320.851|Rp. 6.000.790|Rp. 1.161.237|Rp. 3.480.465|Rp. 3.480.465|Rp. 4.640.078|Rp. 1.161.237|Rp. 1.161.237|Rp. 6.000.790|Rp. 4.640.078|Rp. 1.161.237|Rp. 3.480.465|Rp. 2.320.851|Rp. 7.200.214|Rp. 3.480.465|Rp. 1.161.237|Rp. 1.161.237|Rp. 1.161.237|Rp. 1.161.237|Rp. 1.161.237|Rp. 6.000.790|Rp. 4.640.078|Rp. 4.640.078|Rp. 4.640.078|Rp. 1.161.237|Rp. 1.161.237|Rp. 1.161.237|Rp. 6.000.790|Rp. 4.640.078|Rp. 2.320.851|Rp. 4.640.078|Rp. 1.161.237|Rp. 1.161.237|Rp. 6.000.790|Rp. 3.480.465|Rp. 1.161.237|Rp. 3.480.465|Rp. 1.161.237|Rp. 1.161.237|Rp. 4.640.078|Rp. 6.000.790|Rp. 3.480.465|Rp. 2.320.851|Rp. 1.161.237|Rp. 4.640.078|Rp. 4.640.078|Rp. 3.480.465|Rp. 4.640.078|Rp. 4.640.078|Rp. 1.161.237|Rp. 4.640.078|Rp. 3.480.465|Rp. 6.000.790|Rp. 6.000.790|Rp. 1.161.237|Rp. 1.161.237|Rp. 1.161.237|Rp. 4.640.078|Rp. 1.161.237|Rp. 2.320.851|Rp. 6.000.790|Rp. 1.161.237|Rp. 2.320.851|Rp. 4.640.078|Rp. 6.000.790|Rp. 1.161.237|Rp. 6.000.790|Rp. 1.161.237|Rp. 1.161.237|Rp. 1.161.237|Rp. 2.320.851|Rp. 3.480.465|Rp. 1.161.237|Rp. 3.480.465|Rp. 3.480.465|Rp. 7.200.214|Rp. 4.640.078|Rp. 4.640.078|Rp. 1.161.237|Rp. 4.640.078|Rp. 11.501.041|Rp. 1.161.237|Rp. 2.320.851|Rp. 1.161.237|Rp. 7.200.214|Rp. 6.000.790|Rp. 1.161.237|Rp. 1.161.237|Rp. 1.161.237|Rp. 6.000.790|Rp. 1.161.237|Rp. 3.480.465|Rp. 4.640.078|Rp. 1.700.000|Rp. 1.900.000|Rp. 1.200.000|Rp. 1.400.000|Rp. 1.600.000|Rp. 1.161.237|Rp. 1.161.237|Rp. 3.480.465|Rp. 1.161.237|Rp. 3.480.465|Rp. 4.640.078|Rp. 2.320.851|Rp. 3.480.465|Rp. 1.161.237|Rp. 1.161.237|Rp. 1.161.237|Rp. 1.161.237|Rp. 4.640.078|Rp. 3.480.465|Rp. 6.000.790|Rp. 6.000.790|Rp. 4.640.078|Rp. 6.000.790|Rp. 4.640.078|Rp. 4.640.078|Rp. 1.161.237|Rp. 1.161.237|Rp. 4.640.078|Rp. 1.161.237|Rp. 1.161.237|Rp. 2.320.851|Rp. 1.161.237|Rp. 4.640.078|Rp. 2.320.851|Rp. 4.640.078|Rp. 1.161.237|Rp. 2.320.851|Rp. 6.000.790|Rp. 3.480.465|Rp. 2.320.851|Rp. 1.161.237|Rp. 2.320.851|Rp. 9.602.731|Rp. 1.161.237|Rp. 3.480.465|Rp. 4.640.078|Rp. 4.640.078|Rp. 1.161.237|Rp. 6.000.790|Rp. 4.640.078|Rp. 1.161.237|Rp. 1.161.237|Rp. 1.161.237|Rp. 3.480.465|Rp. 2.320.851|Rp. 4.640.078|Rp. 6.000.790|Rp. 4.640.078|Rp. 1.161.237|Rp. 4.640.078|Rp. 1.161.237|Rp. 3.480.465|Rp. 3.480.465|Rp. 4.640.078|Rp. 1.161.237|Rp. 4.640.078|Rp. 4.640.078|Rp. 2.320.851|Rp. 1.161.237|Rp. 4.640.078|Rp. 4.640.078|Rp. 3.480.465|Rp. 6.000.790|Rp. 2.320.851|Rp. 9.602.731|Rp. 2.320.851|Rp. 2.320.851|Rp. 6.000.790|Rp. 2.320.851|Rp. 4.640.078|Rp. 1.161.237|Rp. 3.480.465|Rp. 6.000.790|Rp. 1.161.237|Rp. 2.320.851|Rp. 6.000.790|Rp. 1.161.237|Rp. 1.161.237|Rp. 1.161.237|Rp. 1.161.237|Rp. 3.480.465|Rp. 3.480.465|Rp. 1.161.237|Rp. 4.640.078|Rp. 9.602.731|Rp. 1.161.237|Rp. 1.161.237|Rp. 1.161.237|Rp. 1.161.237|Rp. 1.161.237|Rp. 4.640.078|Rp. 1.161.237|Rp. 3.480.465|Rp. 1.161.237|Rp. 1.161.237|Rp. 1.161.237|Rp. 1.161.237|Rp. 2.320.851|Rp. 6.000.790|Rp. 1.161.237|Rp. 6.000.790|Rp. 1.161.237|Rp. 6.000.790|Rp. 1.161.237|Rp. 1.161.237|Rp. 2.320.851|Rp. 4.640.078|Rp. 6.000.790|Rp. 4.640.078|Rp. 4.640.078|Rp. 1.161.237|Rp. 2.320.851|Rp. 2.320.851|Rp. 2.320.851|Rp. 2.320.851|Rp. 1.161.237|Rp. 6.000.790|Rp. 1.161.237|Rp. 3.480.465|Rp. 1.161.237|Rp. 7.200.214|Rp. 3.480.465|Rp. 4.640.078|Rp. 3.480.465|Rp. 1.161.237|Rp. 1.161.237|Rp. 7.200.214|Rp. 1.161.237|Rp. 4.640.078|Rp. 1.161.237|Rp. 1.161.237|Rp. 4.640.078|Rp. 1.161.237|Rp. 1.161.237|Rp. 9.602.731|Rp. 1.161.237|Rp. 6.000.790|Rp. 1.161.237|Rp. 1.161.237|Rp. 4.640.078|Rp. 1.161.237|Rp. 1.161.237|Rp. 1.161.237|Rp. 1.161.237|Rp. 1.161.237|Rp. 1.161.237|Rp. 4.640.078|Rp. 4.640.078|Rp. 1.161.237|Rp. 3.480.465|Rp. 1.161.237|Rp. 4.640.078|Rp. 1.161.237|Rp. 1.161.237|Rp. 3.480.465|Rp. 1.161.237|Rp. 4.640.078|Rp. 3.480.465|Rp. 1.161.237|Rp. 4.640.078|Rp. 1.161.237|Rp. 3.480.465|Rp. 6.000.790|Rp. 1.161.237|Rp. 4.640.078|Rp. 1.161.237|Rp. 3.480.465|Rp. 1.161.237|Rp. 4.640.078|Rp. 1.161.237|Rp. 1.161.237|Rp. 1.161.237|Rp. 1.161.237|Rp. 4.640.078|Rp. 4.640.078|Rp. 1.161.237|Rp. 1.161.237|Rp. 1.161.237|Rp. 1.161.237|Rp. 9.602.731|Rp. 1.161.237|Rp. 3.480.465|Rp. 1.161.237|Rp. 1.161.237|Rp. 2.320.851|Rp. 2.320.851|Rp. 1.161.237|Rp. 4.640.078|Rp. 2.320.851|Rp. 3.480.465|Rp. 2.320.851|Rp. 1.161.237|Rp. 2.320.851|Rp. 4.640.078|Rp. 4.640.078|Rp. 3.480.465|Rp. 1.161.237|Rp. 1.161.237|Rp. 2.320.851|Rp. 4.640.078|Rp. 6.000.790|Rp. 7.200.214|Rp. 1.161.237|Rp. 1.161.237|Rp. 2.320.851|Rp. 2.320.851|Rp. 1.161.237|Rp. 3.480.465|Rp. 1.161.237|Rp. 6.000.790|Rp. 1.161.237|Rp. 7.200.214|Rp. 6.000.790|Rp. 1.161.237|Rp. 1.161.237|Rp. 1.161.237|Rp. 6.000.790|Rp. 4.640.078|Rp. 2.320.851|Rp. 1.161.237|Rp. 1.161.237|Rp. 1.161.237|Rp. 1.161.237|Rp. 6.000.790|Rp. 4.640.078|Rp. 6.000.790|Rp. 4.640.078|Rp. 2.320.851|Rp. 1.161.237|Rp. 4.640.078|Rp. 3.480.465|Rp. 2.320.851|Rp. 3.480.465|Rp. 1.161.237|Rp. 4.640.078|Rp. 1.161.237|Rp. 4.640.078|Rp. 1.161.237|Rp. 1.161.237|Rp. 4.640.078|Rp. 3.480.465|Rp. 2.320.851|Rp. 4.640.078|Rp. 1.161.237|Rp. 1.161.237|Rp. 4.640.078|Rp. 1.161.237|Rp. 6.000.790|Rp. 1.161.237|Rp. 1.161.237|Rp. 1.161.237|Rp. 6.000.790|Rp. 1.161.237|Rp. 1.161.237|Rp. 6.000.790|Rp. 2.320.851|Rp. 1.161.237|Rp. 1.161.237|Rp. 1.161.237|Rp. 3.480.465|Rp. 1.161.237|Rp. 1.161.237|Rp. 1.161.237|Rp. 4.640.078|Rp. 1.161.237|Rp. 1.161.237|Rp. 3.480.465|Rp. 1.161.237|Rp. 6.000.790|Rp. 6.000.790|Rp. 1.161.237|Rp. 3.480.465|Rp. 1.161.237|Rp. 6.000.790|Rp. 1.161.237|Rp. 4.640.078|Rp. 1.161.237|Rp. 2.320.851|Rp. 3.480.465|Rp. 2.320.851|Rp. 1.161.237|Rp. 1.161.237|Rp. 6.000.790|Rp. 1.161.237|Rp. 3.480.465|Rp. 1.161.237|Rp. 1.161.237|Rp. 4.640.078|Rp. 1.161.237|Rp. 4.640.078|Rp. 1.161.237|Rp. 1.161.237|Rp. 2.320.851|Rp. 4.640.078|Rp. 2.320.851|Rp. 1.161.237|Rp. 3.480.465|Rp. 1.161.237|Rp. 1.161.237|Rp. 4.640.078|Rp. 4.640.078|Rp. 4.640.078|Rp. 3.480.465|Rp. 7.200.214|Rp. 6.000.790|Rp. 6.000.790|Rp. 1.161.237|Rp. 1.161.237|Rp. 1.161.237|Rp. 1.161.237|Rp. 2.320.851|Rp. 1.161.237|Rp. 1.161.237|Rp. 1.161.237|Rp. 6.000.790|Rp. 6.000.790|Rp. 1.161.237|Rp. 2.320.851|Rp. 6.000.790|Rp. 1.161.237|Rp. 1.161.237|Rp. 3.480.465|Rp. 3.480.465|Rp. 1.161.237|Rp. 3.480.465|Rp. 6.000.790|Rp. 1.161.237|Rp. 1.161.237|Rp. 1.161.237|Rp. 2.320.851|Rp. 1.161.237|Rp. 1.161.237|Rp. 1.161.237|Rp. 4.640.078|Rp. 3.480.465|Rp. 1.161.237|Rp. 1.161.237|Rp. 1.161.237|Rp. 1.161.237|Rp. 1.161.237|Rp. 4.640.078|Rp. 3.480.465|Rp. 1.161.237|Rp. 1.161.237|Rp. 4.640.078|Rp. 2.320.851|Rp. 1.161.237|Rp. 2.320.851|Rp. 1.161.237|Rp. 6.000.790|Rp. 6.000.790|Rp. 1.161.237|Rp. 1.161.237|Rp. 1.161.237|Rp. 3.480.465|Rp. 1.161.237|Rp. 1.161.237|Rp. 1.161.237|Rp. 1.161.237|Rp. 6.000.790|Rp. 4.640.078|Rp. 9.602.731|Rp. 4.640.078|Rp. 1.161.237|Rp. 6.000.790|Rp. 1.161.237|Rp. 4.640.078|Rp. 1.161.237|Rp. 3.480.465|Rp. 1.161.237|Rp. 6.000.790|Rp. 2.320.851|Rp. 1.161.237|Rp. 4.640.078|Rp. 4.640.078|Rp. 4.640.078|Rp. 3.480.465|Rp. 4.640.078|Rp. 1.161.237|Rp. 1.161.237|Rp. 1.161.237|Rp. 3.480.465|Rp. 8.403.307|Rp. 4.640.078|Rp. 7.200.214|Rp. 2.320.851|Rp. 1.161.237|Rp. 1.161.237|Rp. 6.000.790|Rp. 6.000.790|Rp. 4.640.078|Rp. 1.161.237|Rp. 1.161.237|Rp. 1.161.237|Rp. 1.161.237|Rp. 3.480.465|Rp. 1.161.237|Rp. 1.161.237|Rp. 6.000.790|Rp. 6.000.790|Rp. 2.320.851|Rp. 1.161.237|Rp. 2.320.851|Rp. 1.161.237|Rp. 6.000.790|Rp. 1.161.237|Rp. 6.000.790|Rp. 3.480.465|Rp. 1.161.237|Rp. 4.640.078|Rp. 1.161.237|Rp. 4.640.078|Rp. 1.161.237|Rp. 1.161.237|Rp. 1.161.237|Rp. 1.161.237|Rp. 8.403.307|Rp. 1.161.237|Rp. 1.161.237|Rp. 1.161.237|Rp. 1.161.237|Rp. 1.161.237|Rp. 1.161.237";
        String[] str_loanAmount = total.split("\\|");
        long totalLoanAmount = 0;
        for (String ttl : str_loanAmount) {

            if (!ttl.equals("null")) {
                totalLoanAmount = totalLoanAmount + Integer.parseInt(ttl.replace("Rp. ", "").replace(".", ""));
            }
        }
        System.out.println(totalLoanAmount);
    }

    @SuppressWarnings("BusyWait")
    public void nominalApproveAndSelectAllVerifyTotalNominalApprove(List<String> list_case) {
        //87-88
        try {
            for (int count = 1; count <= list_case.size(); count++) {
                String totalNominalApprove = null;
                boolean sts = true;
                long totalLoanAmount = 0;
                String loanAmount;
                String coll_loanAmount = null;

                click(driver, menuApproval);
                waitElementVisible(driver, debtorName_firstAppForm, Const.seconds);
                if (count == 1) {
                    iRowPictName = 87;
                    createTest(iRowPictName, extent_test_case, extent);

                    click(driver, bulk_statusApproved);
                    waitElementVisible(driver, debtorName_firstAppForm, Const.seconds);
                    Thread.sleep(Const.delay);
                    Thread.sleep(Const.delay);
                    totalNominalApprove = driver.findElement(By.id("totalNominalPinjaman")).getAttribute("value").replace("Rp.", "").replace(".", "").trim();

                    takeSreenshot.capture(driver);
                } else {
                    iRowPictName = iRowPictName + iSeq;
                    createTest(iRowPictName, extent_test_case, extent);

                    click(driver, btnBulkRecomend);
                    click(driver, bulk_statusForReview);
                    waitElementVisible(driver, radioButtonFirst, Const.seconds);
                    click(driver, radiobtnAppAll);
                    Thread.sleep(Const.delay);
                    Thread.sleep(Const.delay);
                    totalNominalApprove = driver.findElement(By.id("totalNominalApproved")).getAttribute("value").replace("Rp.", "").replace(".", "").trim();

                    takeSreenshot.capture(driver);
                    scrollIntoView(driver, tableBulk_nextPage);
                }
                try {
                    while (sts) {

                        takeSreenshot.capture(driver);

                        if (count == 1) {
                            for (int idx = 1; idx <= 10; idx++) {
                                waitElementVisible(driver, By.xpath("//*[@id='dataTable']/tbody/tr[" + idx + "]/td[7]"), Const.seconds);
                                if (driver.findElement(By.xpath("//*[@id='dataTable']/tbody/tr[" + idx + "]/td[7]")).isDisplayed()) {
                                    loanAmount = driver.findElement(By.xpath("//*[@id='dataTable']/tbody/tr[" + idx + "]/td[7]")).getText();
                                    coll_loanAmount = coll_loanAmount + "|" + loanAmount;
                                }

                                if (idx == 10) {
                                    scrollIntoView(driver, By.xpath("//*[@id='dataTable_next']/a"));
                                    click(driver, nextPage);
                                }
                            }
                            Thread.sleep(Const.delay);
                        } else {
                            Thread.sleep(1000);
                            for (int idx = 1; idx <= 25; idx++) {
                                loanAmount = getText(driver, By.xpath("//*[@id='channelingApprovalDataTable']/tbody/tr[" + idx + "]/td[6]"));
                                coll_loanAmount = coll_loanAmount + "|" + loanAmount;

                                if (idx == 25) {
                                    Thread.sleep(1000);
                                    scrollIntoView(driver, tableBulk_nextPage);
                                    takeSreenshot.capture(driver);
                                    click(driver, tableBulk_nextPage);
                                }
                            }
                        }
                    }
                } catch (Exception ignored) {
                    String[] str_loanAmount = coll_loanAmount.split("\\|");
                    codeBlockText(coll_loanAmount.replace("null", ""));

                    for(String ttl : str_loanAmount){
                        if(!ttl.equals("null")){
                            totalLoanAmount = totalLoanAmount + Integer.parseInt(ttl.replace("Rp.", "").replace(".", "").trim());
                        }

                    }
                    if (count == 2) {
                        click(driver, radiobtnAppAll);
                        Thread.sleep(Const.delay);
                        click(driver, radiobtnAppAll);
                        Thread.sleep(Const.delay);
                        click(driver, radiobtnAppAll);
                        takeSreenshot.capture(driver);
                    }

                    takeSreenshot.capture(driver);
                    status_testCase(iRowPictName, verifyValue(totalNominalApprove, String.valueOf(totalLoanAmount)), notes);
                    System.out.println("Total Nominal Approver >>>>>>>>>>>>>>>>>>>> " + totalNominalApprove);
                    System.out.println("Total penjumlahan loan amount di table >>>> " + totalLoanAmount);
                }
            }
        } catch (Exception ignored) {

            takeSreenshot.capture(driver);
            status_testCase(iRowPictName, false, notes);
        }
    }

    public void verifyBulkRecomendTotalAndUncheckAndCancelSelection(List<String> list_case) throws InterruptedException {
        //89-90-91
        for (int count = 1; count <= list_case.size(); count++) {
            String totalNominalApprove = null;
            String loanAmount;
            String coll_loanAmount = null;
            long totalLoanAmount = 0;

            if (count == 1) {
                iRowPictName = 89;
                createTest(iRowPictName, extent_test_case, extent);

                driver.navigate().refresh();
                click(driver, bulk_statusForReview);
                waitElementVisible(driver, radioButtonFirst, Const.seconds);
                Thread.sleep(Const.delay);
                Thread.sleep(Const.delay);
                try {
                    for (int idx = 1; idx <= 5; idx++) {
                        click(driver, By.xpath("//*[@id='channelingApprovalDataTable']/tbody/tr[" + idx + "]/td[1]/div/label"));
                        loanAmount = getText(driver, By.xpath("//*[@id='channelingApprovalDataTable']/tbody/tr[" + idx + "]/td[6]"));
                        coll_loanAmount = coll_loanAmount + "|" + loanAmount;
                    }

                    takeSreenshot.capture(driver);
                    totalNominalApprove = driver.findElement(By.id("totalNominalApproved")).getAttribute("value").replace("Rp.", "").replace(".", "").trim();

                    String[] str_loanAmount = coll_loanAmount.split("\\|");

                    for(String ttl : str_loanAmount){
                        if(!ttl.equals("null")){
                            totalLoanAmount = totalLoanAmount + Integer.parseInt(ttl.replace("Rp.", "").replace(".", "").trim());
                        }
                    }

                    codeBlockText(coll_loanAmount);
                    takeSreenshot.capture(driver);
                    status_testCase(iRowPictName, verifyValue(totalNominalApprove, String.valueOf(totalLoanAmount)), notes);
                    System.out.println("Total Nominal Approver >>>>>>>>>>>>>>>>>>>> " + totalNominalApprove);
                    System.out.println("Total penjumlahan loan amount di table >>>> " + totalLoanAmount);
                } catch (Exception e) {
                    e.getCause();
                    takeSreenshot.capture(driver);
                    status_testCase(iRowPictName, verifyValue(totalNominalApprove, String.valueOf(totalLoanAmount)), notes);
                }
            } else if (count == 2) {
                iRowPictName = iRowPictName + iSeq;
                createTest(iRowPictName, extent_test_case, extent);

                driver.navigate().refresh();
                click(driver, bulk_statusForReview);
                waitElementVisible(driver, radioButtonFirst, Const.seconds);
                for (int idx = 1; idx <= 5; idx++) {
                    click(driver, By.xpath("//*[@id='channelingApprovalDataTable']/tbody/tr[" + idx + "]/td[1]/div/label"));
                }
                takeSreenshot.capture(driver);
                for (int idx = 5; idx >= 4; idx--) {

                    click(driver, By.xpath("//*[@id='channelingApprovalDataTable']/tbody/tr[" + idx + "]/td[1]/div/label"));
                }

                takeSreenshot.capture(driver);
                for (int idx = 1; idx <= 3; idx++) {

                    loanAmount = getText(driver, By.xpath("//*[@id='channelingApprovalDataTable']/tbody/tr[" + idx + "]/td[6]"));
                    coll_loanAmount = coll_loanAmount + "|" + loanAmount;
                }
                takeSreenshot.capture(driver);
                totalNominalApprove = driver.findElement(By.id("totalNominalApproved")).getAttribute("value").replace("Rp.", "").replace(".", "").trim();
                String[] str_loanAmount = coll_loanAmount.split("\\|");

                for(String ttl : str_loanAmount){
                    if(!ttl.equals("null")){
                        totalLoanAmount = totalLoanAmount + Integer.parseInt(ttl.replace("Rp.", "").replace(".", "").trim());
                    }
                }

                codeBlockText(coll_loanAmount);
                takeSreenshot.capture(driver);
                status_testCase(iRowPictName, verifyValue(totalNominalApprove, String.valueOf(totalLoanAmount)), notes);
                System.out.println("Total Nominal Approver >>>>>>>>>>>>>>>>>>>> " + totalNominalApprove);
                System.out.println("Total penjumlahan loan amount di table >>>> " + totalLoanAmount);
            } else {
                iRowPictName = iRowPictName + iSeq;
                createTest(iRowPictName, extent_test_case, extent);

                click(driver, menuApproval);
                waitElementVisible(driver, debtorName_firstAppForm, Const.seconds);
                String totalNominalApprove1 = driver.findElement(By.id("totalNominalPinjaman")).getAttribute("value").replace("Rp.", "").replace(".", "").trim();
                click(driver, btnBulkRecomend);
                click(driver, bulk_statusForReview);
                waitElementVisible(driver, radioButtonFirst, Const.seconds);
                Thread.sleep(Const.delay);
                Thread.sleep(Const.delay);
                takeSreenshot.capture(driver);
                for (int idx = 1; idx <= 3; idx++) {
                    Thread.sleep(500);
                    click(driver, By.xpath("//*[@id='channelingApprovalDataTable']/tbody/tr[" + idx + "]/td[1]/div/label"));
                    loanAmount = getText(driver, By.xpath("//*[@id='channelingApprovalDataTable']/tbody/tr[" + idx + "]/td[6]"));
                    coll_loanAmount = coll_loanAmount + "|" + loanAmount;
                }
                takeSreenshot.capture(driver);
                click(driver, btn_cancelSelection);
                Thread.sleep(5000);
                totalNominalApprove = driver.findElement(By.id("totalNominalPinjaman")).getAttribute("value").replace("Rp.", "").replace(".", "").trim();
                String[] str_loanAmount = coll_loanAmount.split("\\|");

                for(String ttl : str_loanAmount){
                    if(!ttl.equals("null")){
                        totalLoanAmount = totalLoanAmount + Integer.parseInt(ttl.replace("Rp.", "").replace(".", "").trim());
                    }
                }
                takeSreenshot.capture(driver);
                status_testCase(iRowPictName, verifyValue(totalNominalApprove1, totalNominalApprove), notes);
                System.out.println("Total Nominal Approver >>>>>>>>>>>>>>>>>>>> " + totalNominalApprove1);
                System.out.println("Total penjumlahan loan amount di table >>>> " + totalNominalApprove);
            }
        }
    }

    public void verifyBulkApproveSelectAllAndCheckBeberapaAndCancelSelection(List<String> list_case) {
        //92-93-94
        for (int count = 1; count <= list_case.size(); count++) {
            iRowPictName = iRowPictName + iSeq;
            createTest(iRowPictName, extent_test_case, extent);

            String totalNominalApprove = null;
            boolean sts = true;
            long totalLoanAmount = 0;
            String loanAmount;
            String coll_loanAmount = null;
            if (count == 1) {
                click(driver, menuApproval);
                waitElementVisible(driver, debtorName_firstAppForm, Const.seconds);
                click(driver, btnBulkApprove);
                waitElementVisible(driver, txtTblBulk_debtorName, Const.seconds);
                click(driver, bulk_statusWaitingApproval);
                click(driver, radiobtnAppAll);
                totalNominalApprove = driver.findElement(By.id("totalNominalApproved")).getAttribute("value").replace("Rp.", "").replace(".", "").trim();
                takeSreenshot.capture(driver);
            } else {
                driver.navigate().refresh();
                click(driver, bulk_statusWaitingApproval);
                writeText(driver, txtSearch, readTestData.timeStamp);
                waitElementVisible(driver, radioButtonFirst, Const.seconds);
            }
            try {
                while (sts) {
                    if (count == 1) {
                        for (int idx = 1; idx <= 25; idx++) {
                            loanAmount = getText(driver, By.xpath("//*[@id='channelingApprovalDataTable']/tbody/tr[" + idx + "]/td[6]"));
                            coll_loanAmount = coll_loanAmount + "|" + loanAmount;

                            if (idx == 25) {
                                scrollIntoView(driver, tableBulk_nextPage);
                                click(driver, tableBulk_nextPage);
                            }
                            takeSreenshot.capture(driver);
                        }
                    } else if (count == 2) {
                        for (int idx = 1; idx <= 5; idx++) {
                            click(driver, By.xpath("//*[@id='channelingApprovalDataTable']/tbody/tr[" + idx + "]/td[1]/div/label"));
                            loanAmount = getText(driver, By.xpath("//*[@id='channelingApprovalDataTable']/tbody/tr[" + idx + "]/td[6]"));
                            coll_loanAmount = coll_loanAmount + "|" + loanAmount;
                        }

                        totalNominalApprove = driver.findElement(By.id("totalNominalApproved")).getAttribute("value").replace("Rp.", "").replace(".", "").trim();
                        click(driver, tableBulk_nextPage);

                        takeSreenshot.capture(driver);
                    } else {
                        for (int idx = 1; idx <= 5; idx++) {
                            click(driver, By.xpath("//*[@id='channelingApprovalDataTable']/tbody/tr[" + idx + "]/td[1]/div/label"));
                        }
                        takeSreenshot.capture(driver);
                        for (int idx = 5; idx >= 4; idx--) {
                            click(driver, By.xpath("//*[@id='channelingApprovalDataTable']/tbody/tr[" + idx + "]/td[1]/div/label"));
                        }
                        takeSreenshot.capture(driver);
                        for (int idx = 1; idx <= 3; idx++) {
                            loanAmount = getText(driver, By.xpath("//*[@id='channelingApprovalDataTable']/tbody/tr[" + idx + "]/td[6]"));
                            coll_loanAmount = coll_loanAmount + "|" + loanAmount;
                        }
                        totalNominalApprove = driver.findElement(By.id("totalNominalApproved")).getAttribute("value").replace("Rp.", "").replace(".", "").trim();
                        click(driver, tableBulk_nextPage);
                    }
                }
            } catch (Exception ignored) {
                String[] str_loanAmount = coll_loanAmount.split("\\|");
                codeBlockText(coll_loanAmount);

                for(String ttl : str_loanAmount){
                    if(!ttl.equals("null")){
                        totalLoanAmount = totalLoanAmount + Integer.parseInt(ttl.replace("Rp.", "").replace(".", "").trim());
                    }
                }
                takeSreenshot.capture(driver);
                status_testCase(iRowPictName, verifyValue(totalNominalApprove, String.valueOf(totalLoanAmount)), notes);
                System.out.println("Total Nominal Approver >>>>>>>>>>>>>>>>>>>> " + totalNominalApprove);
                System.out.println("Total penjumlahan loan amount di table >>>> " + totalLoanAmount);
            }
        }
    }


    public void verifyBulkApproveCheckBeberapaCancel() throws InterruptedException {
        //95
        iRowPictName = iRowPictName + iSeq;
        createTest(iRowPictName, extent_test_case, extent);

        String loanAmount;
        String coll_loanAmount = null;

        click(driver, menuApproval);
        waitElementVisible(driver, debtorName_firstAppForm, Const.seconds);
        String totalNominalApprove1 = driver.findElement(By.id("totalNominalPinjaman")).getAttribute("value").replace("Rp.", "").replace(".", "").trim();
        click(driver, btnBulkApprove);
        Thread.sleep(3000);

        click(driver, bulk_statusWaitingApproval);
        waitElementVisible(driver, radioButtonFirst, Const.seconds);

        for (int idx = 1; idx <= 3; idx++) {
            click(driver, By.xpath("//*[@id='channelingApprovalDataTable']/tbody/tr[" + idx + "]/td[1]/div/label"));
            loanAmount = getText(driver, By.xpath("//*[@id='channelingApprovalDataTable']/tbody/tr[" + idx + "]/td[6]"));
            coll_loanAmount = coll_loanAmount + "|" + loanAmount;
        }
        takeSreenshot.capture(driver);
        click(driver, btn_cancelSelection);
        Thread.sleep(4000);
        String totalNominalApprove2 = driver.findElement(By.id("totalNominalPinjaman")).getAttribute("value").replace("Rp.", "").replace(".", "").trim();
        takeSreenshot.capture(driver);
        status_testCase(iRowPictName, verifyValue(totalNominalApprove1, totalNominalApprove2), notes);
        System.out.println("Total Nominal Approver >>>>>>>>>>>>>>>>>>>> " + totalNominalApprove1);
        System.out.println("Total penjumlahan loan amount di table >>>> " + totalNominalApprove2);
    }

    public void bulkApproveCheckAllRejectApprove(List<String> list_case) {
        //96-97
        for (int i = 1; i <= list_case.size(); i++) {
            try {
                click(driver, btnProfile);
                Thread.sleep(Const.delay);
                Thread.sleep(Const.delay);
                click(driver, btnLogout);
                //generate data
                iRowPictName = iRowPictName + iSeq;
                createTest(iRowPictName, extent_test_case, extent);

                UtilsFile utilsFile = new UtilsFile();
                utilsFile.dltDummyExist();

                DataCSV dataCSV = new DataCSV();
                dataCSV.dataDummy(Const.appFile, Const.reaFile, "3");

                //hit server
                ConsumeData connServer = new ConsumeData();
                connServer.serverAkses();

                //changes status
                loginMaker(driver);
                changedRecomendationAsMaker(driver, 3);
                loginApprover(driver, "surya.simarmata", "Surya2022");
                changedApproveAsApprover(driver, 2);
                loginApprover(driver, "surya.simarmata", "Surya2022");

                click(driver, menuApproval);
                waitElementVisible(driver, debtorName_firstAppForm, Const.seconds);
                click(driver, btnBulkApprove);
                waitElementVisible(driver, radioButtonFirst, Const.seconds);
                click(driver, bulk_statusWaitingApproval);
                waitElementVisible(driver, radioButtonFirst, Const.seconds);
                click(driver, radiobtnAppAll);

                takeSreenshot.capture(driver);
                if (i == 1) {
                    click(driver, notRecomended);
                    Thread.sleep(5000);
                    click(driver, menuApproval);
                    waitElementVisible(driver, debtorName_firstAppForm, Const.seconds);
                    readTestData.testData();
                    writeText(driver, txtSearch, readTestData.timeStamp);
                    Thread.sleep(5000);
                    click(driver, bulk_statusRejected);
                    waitElementVisible(driver, txtTblBulk_debtorName, Const.seconds);
                    actualLogTrue(String.valueOf(boolean_isDisplayed(driver, txtTblBulk_debtorName)));

                } else {
                    click(driver, recomended);
                    Thread.sleep(5000);
                    click(driver, menuApproval);
                    waitElementVisible(driver, debtorName_firstAppForm, Const.seconds);
                    readTestData.testData();
                    writeText(driver, txtSearch, readTestData.timeStamp);
                    Thread.sleep(5000);
                    click(driver, bulk_statusApproved);
                    waitElementVisible(driver, txtTblBulk_debtorName, Const.seconds);
                    actualLogTrue(String.valueOf(boolean_isDisplayed(driver, txtTblBulk_debtorName)));
                }

                takeSreenshot.capture(driver);
                status_testCase(iRowPictName, true, notes);
            } catch (Exception e) {

                takeSreenshot.capture(driver);
                status_testCase(iRowPictName, false, notes);
            }
        }
    }
}
