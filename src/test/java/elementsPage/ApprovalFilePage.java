package elementsPage;

import applos.ConsumeData;
import capture.TakeSreenshot;
import createDataCSV.DataValidationCSVFileDifferentPartner;
import individu.LibUtils;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import readFile.ReadCSVFormApproval;
import runner.ValidateFileApprovalRunner;
import testData.ReadTestData;
import writeFile.UtilsFile;

import java.io.IOException;

import static elementsPage.ApprovalFormApprover.loginApprover;
import static elementsPage.ApprovalFormMaker.loginMaker;
import static elementsPage.ApprovalFormMaker.logout;
import static individu.LibUtils.*;
import static runner.ValidateFileApprovalRunner.extent;
import static runner.ValidateFileApprovalRunner.extent_test_case;

public class ApprovalFilePage extends BaseAction {
    By menuRealization = By.xpath("//div[normalize-space()='Realization']");
    By menuApproval = By.xpath("//div[normalize-space()='Approval Form']");
    By menuExposureDebtor = By.xpath("//div[normalize-space()='Exposure Debtor']");
    By txtSearch = By.id("search-filter");
    By linktxtTbl_namaDebitur = By.xpath("//*[@id='dataTable']/tbody/tr[1]/td[4]/a");
    By noApplication_detail = By.xpath("/html/body/div[2]/form/div[2]/div/div[2]/div/div/div[8]");
    By nik_detail = By.xpath("/html/body/div[2]/form/div[2]/div/div[2]/div/div/div[20]");
    By npwp_detail = By.xpath("/html/body/div[2]/form/div[2]/div/div[2]/div/div/div[22]");
    By jenisDebitur_detail = By.xpath("/html/body/div[2]/form/div[2]/div/div[2]/div/div/div[12]");
    By namaDebitur_detail = By.xpath("/html/body/div[2]/form/div[2]/div/div[2]/div/div/div[4]");
    By jangkaWaktu_detail = By.xpath("/html/body/div[2]/form/div[2]/div/div[2]/div/div/div[16]");
    By interestRate_detail = By.xpath("/html/body/div[2]/form/div[2]/div/div[2]/div/div/div[18]");
    By plafon_detail = By.xpath("/html/body/div[2]/form/div[2]/div/div[2]/div/div/div[14]");
    By phoneNUmber_detail = By.xpath("/html/body/div[2]/form/div[2]/div/div[2]/div/div/div[24]");
    By remarks_detail = By.xpath("/html/body/div[2]/form/div[2]/div/div[2]/div/div/div[26]");
    By exposureAmountLimit = By.xpath("//*[@id='data-use']/table/tbody/tr[3]/td[3]");
    By label_exposureAmountLimit = By.xpath("//*[@id='data-use']/table/tbody/tr[3]/td[1]");
    By icon_notif = By.xpath("//*[@id='notificationIcon']/img");
    By seeAllNotif = By.xpath("//span[@class='See-All-Notification']");

    private static WebDriver driver = ValidateFileApprovalRunner.driver;
    private String[] arr;
    int iSeq = 1;
    int iShoot = 1; // row number picture
    int iRowCapture = 55; // jarak antar picture
    int iRowPictName; // row number scenario

    TakeSreenshot takeSreenshot = new TakeSreenshot();
    ReadCSVFormApproval readCSVFormApproval = new ReadCSVFormApproval();
    ReadTestData readTestData = new ReadTestData();
    ConsumeData consumeData = new ConsumeData();
    ProductProfilePage productProfilePage = new ProductProfilePage();

    public void login() {
        loginMaker(driver);

    }

    public void validationNoApplication17Varchar() throws IOException {
        iRowPictName = iRowPictName + iSeq;
        createTest(iRowPictName, extent_test_case, extent);

        ExposureDebtorPage.changeExposureActive(driver);// cek exposure
        productProfilePage.productProfile(driver);
        arr = readCSVFormApproval.fileCSVApp();

        click(driver, menuApproval);
        writeText(driver, txtSearch, arr[10]);// first no application
        click(driver, linktxtTbl_namaDebitur);

        String no_application = getText(driver, noApplication_detail);
        verifyValue(arr[10], no_application);

        takeSreenshot.capture(driver);
        status_testCase(iRowPictName, true, "");
    }

    public void validationNIK16numerik() {
        iRowPictName = iRowPictName + iSeq;
        createTest(iRowPictName, extent_test_case, extent);

        String nik = getText(driver, nik_detail);
        verifyValue(arr[11], nik);
        takeSreenshot.capture(driver);
        status_testCase(iRowPictName, true, "");
    }

    public void validationNPWP15KarakterNumerik() throws IOException {
        iRowPictName = iRowPictName + iSeq;
        createTest(iRowPictName, extent_test_case, extent);

        arr = readCSVFormApproval.fileCSVApp();
        click(driver, menuApproval);
        writeText(driver, txtSearch, arr[20]);
        click(driver, linktxtTbl_namaDebitur);
        String npwp = getText(driver, npwp_detail);
        verifyValue(arr[22], npwp);
        takeSreenshot.capture(driver);
        status_testCase(iRowPictName, true, "");
    }

    public void validationNPWPKosongPlafonKurang50Juta() throws IOException {
        iRowPictName = iRowPictName + iSeq;
        createTest(iRowPictName, extent_test_case, extent);

        arr = readCSVFormApproval.fileCSVApp();
        click(driver, menuApproval);
        writeText(driver, txtSearch, arr[10]);
        click(driver, linktxtTbl_namaDebitur);
        String npwp = getText(driver, npwp_detail);
        verifyValue(arr[12], npwp);
        takeSreenshot.capture(driver);
        status_testCase(iRowPictName, true, "");
    }

    public void validationJenisDebitur() {
        iRowPictName = iRowPictName + iSeq;
        createTest(iRowPictName, extent_test_case, extent);

        String jenis_debitur = getText(driver, jenisDebitur_detail);
        verifyValue(arr[13], jenis_debitur);
        takeSreenshot.capture(driver);
        status_testCase(iRowPictName, true, "");
    }

    public void validationNamaDebitur60Karakter() throws IOException {
        iRowPictName = iRowPictName + iSeq;
        createTest(iRowPictName, extent_test_case, extent);

        arr = readCSVFormApproval.fileCSVApp();
        click(driver, menuApproval);
        writeText(driver, txtSearch, arr[10]);
        click(driver, linktxtTbl_namaDebitur);
        String nama_debitur = getText(driver, namaDebitur_detail);
        verifyValue(arr[14], nama_debitur);
        takeSreenshot.capture(driver);
        status_testCase(iRowPictName, true, "");
    }

    public void validationNamaDebitur59Karakter() throws IOException {
        iRowPictName = iRowPictName + iSeq;
        createTest(iRowPictName, extent_test_case, extent);

        arr = readCSVFormApproval.fileCSVApp();
        click(driver, menuApproval);
        writeText(driver, txtSearch, arr[20]);
        click(driver, linktxtTbl_namaDebitur);
        String nama_debitur = getText(driver, namaDebitur_detail);
        verifyValue(arr[24], nama_debitur);
        takeSreenshot.capture(driver);
        status_testCase(iRowPictName, true, "");

    }

    public void validationJangkaWaktuBatasBawah() throws IOException {
        iRowPictName = iRowPictName + iSeq;
        createTest(iRowPictName, extent_test_case, extent);

        arr = readCSVFormApproval.fileCSVApp();
        click(driver, menuApproval);
        writeText(driver, txtSearch, arr[10]);
        click(driver, linktxtTbl_namaDebitur);
        String jangka_waktu = getText(driver, jangkaWaktu_detail);
        verifyValue(arr[15], jangka_waktu);
        takeSreenshot.capture(driver);
        status_testCase(iRowPictName, true, "");
    }

    public void validationJangkaWaktuBatasTengah() throws IOException {
        iRowPictName = iRowPictName + iSeq;
        createTest(iRowPictName, extent_test_case, extent);

        arr = readCSVFormApproval.fileCSVApp();
        click(driver, menuApproval);
        writeText(driver, txtSearch, arr[20]);
        click(driver, linktxtTbl_namaDebitur);
        String jangka_waktu = getText(driver, jangkaWaktu_detail);
        verifyValue(arr[25], jangka_waktu);
        takeSreenshot.capture(driver);
        status_testCase(iRowPictName, true, "");
    }

    public void validationJangkaWaktuBatasAtas() throws IOException {
        iRowPictName = iRowPictName + iSeq;
        createTest(iRowPictName, extent_test_case, extent);

        arr = readCSVFormApproval.fileCSVApp();
        click(driver, menuApproval);
        writeText(driver, txtSearch, arr[30]);
        click(driver, linktxtTbl_namaDebitur);
        String jangka_waktu = getText(driver, jangkaWaktu_detail);
        verifyValue(arr[35], jangka_waktu);
        takeSreenshot.capture(driver);
        status_testCase(iRowPictName, true, "");
    }

    public void validationInterestRateBatasBawah() throws IOException {
        iRowPictName = iRowPictName + iSeq;
        createTest(iRowPictName, extent_test_case, extent);

        arr = readCSVFormApproval.fileCSVApp();
        click(driver, menuApproval);
        writeText(driver, txtSearch, arr[10]);
        click(driver, linktxtTbl_namaDebitur);
        String interest_rate = getText(driver, interestRate_detail);
        verifyValue(arr[16], interest_rate);
        takeSreenshot.capture(driver);
        status_testCase(iRowPictName, true, "");
    }

    public void validationInterestRateBatasTengah() throws IOException {
        iRowPictName = iRowPictName + iSeq;
        createTest(iRowPictName, extent_test_case, extent);

        arr = readCSVFormApproval.fileCSVApp();
        click(driver, menuApproval);
        writeText(driver, txtSearch, arr[20]);
        click(driver, linktxtTbl_namaDebitur);
        String interest_rate = getText(driver, interestRate_detail);
        verifyValue(arr[26], interest_rate);
        takeSreenshot.capture(driver);
        status_testCase(iRowPictName, true, "");
    }

    public void validationInterestRateBatasAtas() throws IOException {
        iRowPictName = iRowPictName + iSeq;
        createTest(iRowPictName, extent_test_case, extent);

        arr = readCSVFormApproval.fileCSVApp();
        click(driver, menuApproval);
        writeText(driver, txtSearch, arr[30]);
        click(driver, linktxtTbl_namaDebitur);
        String interest_rate = getText(driver, interestRate_detail);
        verifyValue(arr[36], interest_rate);
        takeSreenshot.capture(driver);
        status_testCase(iRowPictName, true, "");
    }

    public void validationPlafonBatasBawah() throws IOException {
        iRowPictName = iRowPictName + iSeq;
        createTest(iRowPictName, extent_test_case, extent);

        arr = readCSVFormApproval.fileCSVApp();
        click(driver, menuApproval);
        writeText(driver, txtSearch, arr[60]);
        click(driver, linktxtTbl_namaDebitur);
        String plafon = getText(driver, plafon_detail);
        verifyValue(arr[67], plafon);
        takeSreenshot.capture(driver);
        status_testCase(iRowPictName, true, "");
    }

    public void validationPlafonBatasTengah() throws IOException {
        iRowPictName = iRowPictName + iSeq;
        createTest(iRowPictName, extent_test_case, extent);

        arr = readCSVFormApproval.fileCSVApp();
        click(driver, menuApproval);
        writeText(driver, txtSearch, arr[20]);
        click(driver, linktxtTbl_namaDebitur);
        String plafon = getText(driver, plafon_detail);
        verifyValue(arr[27], plafon);
        takeSreenshot.capture(driver);
        status_testCase(iRowPictName, true, "");
    }

    public void validationPlafonBatasAtas() throws IOException {
        iRowPictName = iRowPictName + iSeq;
        createTest(iRowPictName, extent_test_case, extent);

        arr = readCSVFormApproval.fileCSVApp();
        click(driver, menuApproval);
        writeText(driver, txtSearch, arr[30]);
        click(driver, linktxtTbl_namaDebitur);
        String plafon = getText(driver, plafon_detail);
        verifyValue(arr[37], plafon);
        takeSreenshot.capture(driver);
        status_testCase(iRowPictName, true, "");
    }

    public void validationPhoneNumber20Karakter() throws IOException {
        iRowPictName = iRowPictName + iSeq;
        createTest(iRowPictName, extent_test_case, extent);

        arr = readCSVFormApproval.fileCSVApp();
        click(driver, menuApproval);
        writeText(driver, txtSearch, arr[10]);
        click(driver, linktxtTbl_namaDebitur);
        String phone_number = getText(driver, phoneNUmber_detail);
        verifyValue(arr[18], phone_number);
        takeSreenshot.capture(driver);
        status_testCase(iRowPictName, true, "");
    }

    public void validationPhoneNumber19Karakter() throws IOException {
        iRowPictName = iRowPictName + iSeq;
        createTest(iRowPictName, extent_test_case, extent);

        arr = readCSVFormApproval.fileCSVApp();
        click(driver, menuApproval);
        writeText(driver, txtSearch, arr[20]);
        click(driver, linktxtTbl_namaDebitur);
        String phone_number = getText(driver, phoneNUmber_detail);
        verifyValue(arr[28], phone_number);
        takeSreenshot.capture(driver);
        status_testCase(iRowPictName, true, "");
    }

    public void validationRemarks40Karakter() throws IOException {
        iRowPictName = iRowPictName + iSeq;
        createTest(iRowPictName, extent_test_case, extent);

        arr = readCSVFormApproval.fileCSVApp();
        click(driver, menuApproval);
        writeText(driver, txtSearch, arr[10]);
        click(driver, linktxtTbl_namaDebitur);
        String remarks = getText(driver, remarks_detail);
        verifyValue(arr[19], remarks);
        takeSreenshot.capture(driver);
        status_testCase(iRowPictName, true, "");
    }

    public void validationRemarksKosong() throws IOException {
        iRowPictName = iRowPictName + iSeq;
        createTest(iRowPictName, extent_test_case, extent);

        arr = readCSVFormApproval.fileCSVApp();
        click(driver, menuApproval);
        writeText(driver, txtSearch, arr[20]);
        click(driver, linktxtTbl_namaDebitur);
        String remarks = getText(driver, remarks_detail);
        verifyValue(arr[29], remarks);
        takeSreenshot.capture(driver);
        status_testCase(iRowPictName, true, "");
    }

    public void validationDuaDataNIKSatuPartner() {
        iRowPictName = iRowPictName + iSeq;
        createTest(iRowPictName, extent_test_case, extent);

        int x = 0;
        long totalPlafon = 0;
        String plafon;
        String ttl_plafon = null;

        for (int i = 2; i <= 3; i++) {
            click(driver, menuApproval);
            writeText(driver, txtSearch, arr[40 + x]);
            click(driver, linktxtTbl_namaDebitur);
            plafon = getText(driver, By.xpath("/html/body/div[2]/form/div[2]/div/div[" + i + "]/div/div/div[14]"));
            ttl_plafon = ttl_plafon + "|" + plafon;
            takeSreenshot.capture(driver);
            x = 10;
        }
        String[] str_loanAmount = ttl_plafon.split("\\|");
        codeBlockText(ttl_plafon.replace("null", ""));

        for (String ttl : str_loanAmount) {
            if (!ttl.equals("null")) {
                totalPlafon = totalPlafon + Integer.parseInt(ttl.replace("Rp.", "").replace(".", "").trim());
            }
        }

        click(driver, menuExposureDebtor);
        String amountExposureLimit = getText(driver, exposureAmountLimit);

        verifyValue(amountExposureLimit, String.valueOf(totalPlafon));
        LibUtils.scrollIntoView(driver, label_exposureAmountLimit);
        takeSreenshot.capture(driver);
        status_testCase(iRowPictName, true, "");
    }

    public void validationDuaDataNIKDuaPartner() throws IOException {

        iRowPictName = iRowPictName + iSeq;
        createTest(iRowPictName, extent_test_case, extent);
        readTestData.testData();
        //clean folder
        UtilsFile cleanFolder = new UtilsFile();
        cleanFolder.dltDummyExist();
        //create csv
        DataValidationCSVFileDifferentPartner dataValidationCSVFileDifferentPartner = new DataValidationCSVFileDifferentPartner();
        dataValidationCSVFileDifferentPartner.dataDummy(Const.appFile, Const.reaFile, "1");
        //consume to LOS
        consumeData.serverAkses();
        //read new csv
        arr = readCSVFormApproval.fileCSVApp();
        long totalPlafon = 0;
        String plafon;
        String ttl_plafon = null;

        for (int i = 2; i <= 3; i++) {
            click(driver, menuApproval);
            if (i == 2) {
                writeText(driver, txtSearch, readTestData.noAppp);
            } else {
                writeText(driver, txtSearch, arr[10]);
            }
            click(driver, linktxtTbl_namaDebitur);
            plafon = getText(driver, By.xpath("/html/body/div[2]/form/div[2]/div/div[" + i + "]/div/div/div[14]"));
            String nik = getText(driver, By.xpath("/html/body/div[2]/form/div[2]/div/div[" + i + "]/div/div/div[20]"));
            ttl_plafon = ttl_plafon + "|" + plafon;

            takeSreenshot.capture(driver);
        }
        String[] str_loanAmount = ttl_plafon.split("\\|");
        codeBlockText(ttl_plafon.replace("null", ""));
        for (String ttl : str_loanAmount) {
            if (!ttl.equals("null")) {
                totalPlafon = totalPlafon + Integer.parseInt(ttl.replace("Rp.", "").replace(".", "").trim());
            }
        }

        click(driver, menuExposureDebtor);

        String amountExposureLimit = getText(driver, exposureAmountLimit);
        LibUtils.scrollIntoView(driver, label_exposureAmountLimit);
        verifyValue(amountExposureLimit, String.valueOf(totalPlafon));
        takeSreenshot.capture(driver);
        status_testCase(iRowPictName, true, "");
    }

    public void validationNotifSetelahProsesApproval() throws InterruptedException {

        logout(driver);
        loginApprover(driver, Const.e2e_user_ldap_approveRecomend, Const.e2e_pwd_ldap_approveRecomend);
        iRowPictName = iRowPictName + iSeq;
        createTest(iRowPictName, extent_test_case, extent);

        click(driver, icon_notif);
        click(driver, seeAllNotif);

        boolean sts = true;
        int i = 1;
        String getMessage = null;
        while (sts) {
            getMessage = getText(driver, By.xpath("//tbody[@id='tBody-55-89']/tr[" + i + "]/td[3]"));
            if (getMessage.contains("Terdapat file approval baru dari partner " + readTestData.companyName)) {
                LibUtils.verifyElementExist(driver, By.xpath("//tbody[@id='tBody-55-89']/tr[" + i + "]/td[3]"));
                actualLogTrue(String.valueOf(boolean_isDisplayed(driver, By.xpath("//tbody[@id='tBody-55-89']/tr[" + i + "]/td[3]"))));
                sts = false;
            }
            i++;
        }
        takeSreenshot.capture(driver);
        status_testCase(iRowPictName, true, "");

    }
}
