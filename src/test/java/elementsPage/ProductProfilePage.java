package elementsPage;

import capture.TakeSreenshot;
import individu.LibUtils;
import io.github.bonigarcia.wdm.WebDriverManager;
import org.openqa.selenium.By;
import org.openqa.selenium.TakesScreenshot;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.edge.EdgeDriver;
import org.testng.annotations.Test;
import testData.ReadTestData;

import java.util.concurrent.TimeUnit;

import static elementsPage.ApprovalFormMaker.loginMaker;

public class ProductProfilePage extends BaseAction {

    public static final By menuProductProfile = By.xpath("//div[normalize-space()='Product Profile']");
    public static final By columnSearch = By.xpath("//input[@id='search']");
    public static final By lblCompanyProfile = By.xpath("//h5[normalize-space()='Company Partner Profile']");
    public static final By titlerRejectReason = By.xpath("//h5[normalize-space()='Rejected Reason']");

    ReadTestData readTestData = new ReadTestData();

    public void productProfile(WebDriver driver) {

        readTestData.testData();
        click(driver, menuProductProfile);
        writeText(driver, columnSearch, readTestData.companyName);
        click(driver, By.xpath("//a[normalize-space()='" + readTestData.kode_produk + "']"));
        LibUtils.boolean_isDisplayed(driver, lblCompanyProfile);
        takeSreenshot.capture(driver);
    }
}
