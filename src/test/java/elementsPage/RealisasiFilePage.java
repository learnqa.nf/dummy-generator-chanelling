package elementsPage;

import capture.TakeSreenshot;
import com.aventstack.extentreports.Status;
import individu.LibUtils;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import readFile.ReadCSVFormRealization;
import runner.ValidateFileRealisasiRunner;

import java.io.IOException;

import static elementsPage.ApprovalFormApprover.loginApprover;
import static individu.LibUtils.*;
import static runner.ValidateFileRealisasiRunner.extent;
import static runner.ValidateFileRealisasiRunner.extent_test_case;

public class RealisasiFilePage extends BaseAction {

    //data debitur
    public static final By no_aplikasi = By.xpath("/html/body/div[2]/form/div[2]/div/div[1]/div/div[2]/div/table/tbody/tr[1]/td[2]");
    public static final By nama_debitur = By.xpath("/html/body/div[2]/form/div[2]/div/div[1]/div/div[2]/div/table/tbody/tr[2]/td[2]");
    public static final By jenis_debitur = By.xpath("/html/body/div[2]/form/div[2]/div/div[1]/div/div[2]/div/table/tbody/tr[3]/td[2]");
    public static final By gender_code = By.xpath("/html/body/div[2]/form/div[2]/div/div[1]/div/div[2]/div/table/tbody/tr[4]/td[2]");
    public static final By nik = By.xpath("/html/body/div[2]/form/div[2]/div/div[1]/div/div[2]/div/table/tbody/tr[5]/td[2]");
    public static final By npwp = By.xpath("/html/body/div[2]/form/div[2]/div/div[1]/div/div[2]/div/table/tbody/tr[6]/td[2]");
    public static final By alamat_berdasarkan_identitas = By.xpath("/html/body/div[2]/form/div[2]/div/div[1]/div/div[2]/div/table/tbody/tr[7]/td[2]");
    public static final By kelurahan = By.xpath("/html/body/div[2]/form/div[2]/div/div[1]/div/div[2]/div/table/tbody/tr[8]/td[2]");
    public static final By kecamatan = By.xpath("/html/body/div[2]/form/div[2]/div/div[1]/div/div[2]/div/table/tbody/tr[9]/td[2]");
    public static final By kode_dati_II = By.xpath("/html/body/div[2]/form/div[2]/div/div[1]/div/div[2]/div/table/tbody/tr[10]/td[2]");
    public static final By province = By.xpath("/html/body/div[2]/form/div[2]/div/div[1]/div/div[2]/div/table/tbody/tr[11]/td[2]");
    public static final By kode_pos = By.xpath("/html/body/div[2]/form/div[2]/div/div[1]/div/div[2]/div/table/tbody/tr[12]/td[2]");
    public static final By no_telepon = By.xpath("/html/body/div[2]/form/div[2]/div/div[1]/div/div[2]/div/table/tbody/tr[14]/td[2]");
    public static final By mobile_phone_number = By.xpath("/html/body/div[2]/form/div[2]/div/div[1]/div/div[2]/div/table/tbody/tr[15]/td[2]");
    public static final By email = By.xpath("/html/body/div[2]/form/div[2]/div/div[1]/div/div[2]/div/table/tbody/tr[16]/td[2]");
    public static final By place_of_birth = By.xpath("/html/body/div[2]/form/div[2]/div/div[1]/div/div[2]/div/table/tbody/tr[17]/td[2]");
    public static final By date_of_birth = By.xpath("/html/body/div[2]/form/div[2]/div/div[1]/div/div[2]/div/table/tbody/tr[18]/td[2]");
    public static final By last_education_code = By.xpath("/html/body/div[2]/form/div[2]/div/div[1]/div/div[2]/div/table/tbody/tr[19]/td[2]");
    public static final By employer = By.xpath("/html/body/div[2]/form/div[2]/div/div[1]/div/div[2]/div/table/tbody/tr[20]/td[2]");
    public static final By employer_address = By.xpath("/html/body/div[2]/form/div[2]/div/div[1]/div/div[2]/div/table/tbody/tr[21]/td[2]");
    //detail pinjaman
    public static final By plafon = By.xpath("/html/body/div[2]/form/div[2]/div/div[2]/div/div[1]/div/table/tbody/tr[1]/td[2]");
    public static final By interest_rate = By.xpath("/html/body/div[2]/form/div[2]/div/div[2]/div/div[1]/div/table/tbody/tr[2]/td[2]");
    public static final By nomor_pk = By.xpath("/html/body/div[2]/form/div[2]/div/div[2]/div/div[1]/div/table/tbody/tr[3]/td[2]");
    public static final By tanggal_akad = By.xpath("/html/body/div[2]/form/div[2]/div/div[2]/div/div[1]/div/table/tbody/tr[4]/td[2]");
    public static final By tanggal_angsuran_1 = By.xpath("/html/body/div[2]/form/div[2]/div/div[2]/div/div[1]/div/table/tbody/tr[5]/td[2]");
    public static final By jenis_penggunaan = By.xpath("/html/body/div[2]/form/div[2]/div/div[2]/div/div[1]/div/table/tbody/tr[6]/td[2]");
    public static final By sektor_ekonomi = By.xpath("/html/body/div[2]/form/div[2]/div/div[2]/div/div[1]/div/table/tbody/tr[7]/td[2]");
    public static final By omzet = By.xpath("/html/body/div[2]/form/div[2]/div/div[2]/div/div[1]/div/table/tbody/tr[8]/td[2]");
    public static final By go_public = By.xpath("/html/body/div[2]/form/div[2]/div/div[2]/div/div[1]/div/table/tbody/tr[9]/td[2]");
    public static final By sandi_golongan_debitur = By.xpath("/html/body/div[2]/form/div[2]/div/div[2]/div/div[1]/div/table/tbody/tr[10]/td[2]");
    public static final By penghasilan_kotor_per_tahun = By.xpath("/html/body/div[2]/form/div[2]/div/div[2]/div/div[1]/div/table/tbody/tr[11]/td[2]");
    public static final By bentuk_badan_usaha = By.xpath("/html/body/div[2]/form/div[2]/div/div[2]/div/div[1]/div/table/tbody/tr[12]/td[2]");
    public static final By tempat_berdiri_badan_usaha = By.xpath("/html/body/div[2]/form/div[2]/div/div[2]/div/div[1]/div/table/tbody/tr[13]/td[2]");
    public static final By disbursement_date = By.xpath("/html/body/div[2]/form/div[2]/div/div[2]/div/div[1]/div/table/tbody/tr[15]/td[2]");
    public static final By tenor = By.xpath("/html/body/div[2]/form/div[2]/div/div[2]/div/div[1]/div/table/tbody/tr[16]/td[2]");
    public static final By segmentasi_debitur = By.xpath("/html/body/div[2]/form/div[2]/div/div[2]/div/div[1]/div/table/tbody/tr[17]/td[2]");
    public static final By kode_pekerjaan = By.xpath("/html/body/div[2]/form/div[2]/div/div[2]/div/div[1]/div/table/tbody/tr[18]/td[2]");
    //data tambahan
    public static final By mother_maiden_name = By.xpath("/html/body/div[2]/form/div[2]/div/div[2]/div/div[2]/div/table/tbody/tr[1]/td[2]");
    public static final By religion = By.xpath("/html/body/div[2]/form/div[2]/div/div[2]/div/div[2]/div/table/tbody/tr[2]/td[2]");
    public static final By penghasilan_kotor_per_bulan = By.xpath("/html/body/div[2]/form/div[2]/div/div[2]/div/div[2]/div/table/tbody/tr[3]/td[2]");
    public static final By marital_status = By.xpath("/html/body/div[2]/form/div[2]/div/div[2]/div/div[2]/div/table/tbody/tr[4]/td[2]");
    public static final By jumlah_tanggungan = By.xpath("/html/body/div[2]/form/div[2]/div/div[2]/div/div[2]/div/table/tbody/tr[5]/td[2]");
    public static final By nama_pasangan = By.xpath("/html/body/div[2]/form/div[2]/div/div[2]/div/div[2]/div/table/tbody/tr[6]/td[2]");
    public static final By tanggal_lahir_pasangan = By.xpath("/html/body/div[2]/form/div[2]/div/div[2]/div/div[2]/div/table/tbody/tr[7]/td[2]");
    public static final By perjanjian_pisah_harta = By.xpath("/html/body/div[2]/form/div[2]/div/div[2]/div/div[2]/div/table/tbody/tr[8]/td[2]");
    public static final By no_akte = By.xpath("/html/body/div[2]/form/div[2]/div/div[2]/div/div[2]/div/table/tbody/tr[9]/td[2]");
    public static final By tanggal_berdiri = By.xpath("/html/body/div[2]/form/div[2]/div/div[2]/div/div[2]/div/table/tbody/tr[10]/td[2]");
    public static final By no_akte_terakhir = By.xpath("/html/body/div[2]/form/div[2]/div/div[2]/div/div[2]/div/table/tbody/tr[11]/td[2]");
    public static final By tanggal_akte_terkhir = By.xpath("/html/body/div[2]/form/div[2]/div/div[2]/div/div[2]/div/table/tbody/tr[12]/td[2]");
    public static final By bidang_usaha = By.xpath("/html/body/div[2]/form/div[2]/div/div[2]/div/div[2]/div/table/tbody/tr[13]/td[2]");
    public static final By jangka_waktu = By.xpath("/html/body/div[2]/form/div[2]/div/div[2]/div/div[2]/div/table/tbody/tr[14]/td[2]");
    public static final By jenis_kredit = By.xpath("/html/body/div[2]/form/div[2]/div/div[2]/div/div[2]/div/table/tbody/tr[15]/td[2]");
    public static final By debtor_category = By.xpath("/html/body/div[2]/form/div[2]/div/div[2]/div/div[2]/div/table/tbody/tr[16]/td[2]");
    public static final By income_source = By.xpath("/html/body/div[2]/form/div[2]/div/div[2]/div/div[2]/div/table/tbody/tr[17]/td[2]");

    private static final WebDriver driver = ValidateFileRealisasiRunner.driver;
    String[] arr;
    int iSeq = 1;
    int iRowPictName; // row number scenario
    String value, no_app1, no_app2, no_app3, no_app4, no_app5, no_app6, no_app7, no_app8, no_app9, no_app10;

    TakeSreenshot takeSreenshot = new TakeSreenshot();
    ReadCSVFormRealization readCSVFormRealization = new ReadCSVFormRealization();

    public void login() throws IOException {
        loginApprover(driver, Const.e2e_user_ldap_approveRealisasi, Const.e2e_pwd_ldap_approveRealisasi);// realisasi file

       arr = readCSVFormRealization.fileCSVRea();
        no_app1 = arr[55];
        no_app2 = arr[110];
        no_app3 = arr[165];
        no_app4 = arr[220];
        no_app5 = arr[275];
        no_app6 = arr[385];
        no_app7 = arr[440];
        no_app8 = arr[495];
        no_app9 = arr[550];
        no_app10 = arr[605];

    }

    public void noAplikasi17karakter() {
        iRowPictName = 1;
        createTest(iRowPictName, extent_test_case, extent);

        click(driver, DebtorDetailPage.menuRealization);
        writeText(driver, DebtorDetailPage.txt_Search, no_app1);
        click(driver, DebtorDetailPage.tbl_realizationForm_debtorname);
        value = getText(driver, no_aplikasi);
        verifyValue(no_app1, value);
        takeSreenshot.capture(driver);
        status_testCase(iRowPictName, true, "");
    }

    public void namaDebitur40Karakter() {
        iRowPictName = iRowPictName + iSeq;
        createTest(iRowPictName, extent_test_case, extent);

        click(driver, DebtorDetailPage.menuRealization);
        writeText(driver, DebtorDetailPage.txt_Search, no_app1);
        click(driver, DebtorDetailPage.tbl_realizationForm_debtorname);
        value = getText(driver, nama_debitur);
        verifyValue(arr[56], value);
        takeSreenshot.capture(driver);
        status_testCase(iRowPictName, true, "");
    }

    public void namaDebitur39Karakter() {
        iRowPictName = iRowPictName + iSeq;
        createTest(iRowPictName, extent_test_case, extent);

        click(driver, DebtorDetailPage.menuRealization);
        writeText(driver, DebtorDetailPage.txt_Search, no_app10);
        click(driver, DebtorDetailPage.tbl_realizationForm_debtorname);
        value = getText(driver, nama_debitur);
        verifyValue(arr[606], value);
        takeSreenshot.capture(driver);
        status_testCase(iRowPictName, true, "");
    }

    public void jenisDebitur() {
        iRowPictName = iRowPictName + iSeq;
        createTest(iRowPictName, extent_test_case, extent);

        value = getText(driver, jenis_debitur);
        verifyValue(arr[607], value);
        takeSreenshot.capture(driver);
        status_testCase(iRowPictName, true, "");
    }

    public void genderCodeL() {
        iRowPictName = iRowPictName + iSeq;
        createTest(iRowPictName, extent_test_case, extent);

        click(driver, DebtorDetailPage.menuRealization);
        writeText(driver, DebtorDetailPage.txt_Search, no_app6);
        click(driver, DebtorDetailPage.tbl_realizationForm_debtorname);
        value = getText(driver, gender_code);
        verifyValue(arr[388], value);
        takeSreenshot.capture(driver);
        status_testCase(iRowPictName, true, "");
    }

    public void genderCodeP() {
        iRowPictName = iRowPictName + iSeq;
        createTest(iRowPictName, extent_test_case, extent);

        click(driver, DebtorDetailPage.menuRealization);
        writeText(driver, DebtorDetailPage.txt_Search, no_app7);
        click(driver, DebtorDetailPage.tbl_realizationForm_debtorname);
        value = getText(driver, gender_code);
        verifyValue(arr[443], value);
        takeSreenshot.capture(driver);
        status_testCase(iRowPictName, true, "");
    }

    public void nik16Karakter() {
        iRowPictName = iRowPictName + iSeq;
        createTest(iRowPictName, extent_test_case, extent);

        click(driver, DebtorDetailPage.menuRealization);
        writeText(driver, DebtorDetailPage.txt_Search, no_app1);
        click(driver, DebtorDetailPage.tbl_realizationForm_debtorname);
        value = getText(driver, nik);
        verifyValue(arr[59], value);
        takeSreenshot.capture(driver);
        status_testCase(iRowPictName, true, "");
    }

    public void npwp15Karakter() {
        iRowPictName = iRowPictName + iSeq;
        createTest(iRowPictName, extent_test_case, extent);

        click(driver, DebtorDetailPage.menuRealization);
        writeText(driver, DebtorDetailPage.txt_Search, no_app6);
        click(driver, DebtorDetailPage.tbl_realizationForm_debtorname);
        value = getText(driver, npwp);
        verifyValue(arr[390], value);
        takeSreenshot.capture(driver);
        status_testCase(iRowPictName, true, "");
    }

    public void npwpKosong() {
        iRowPictName = iRowPictName + iSeq;
        createTest(iRowPictName, extent_test_case, extent);

        click(driver, DebtorDetailPage.menuRealization);
        writeText(driver, DebtorDetailPage.txt_Search, no_app1);
        click(driver, DebtorDetailPage.tbl_realizationForm_debtorname);
        value = getText(driver, npwp);
        verifyValue(arr[60], value);
        takeSreenshot.capture(driver);
        status_testCase(iRowPictName, true, "");
    }

    public void alamat50Karakter() {
        iRowPictName = iRowPictName + iSeq;
        createTest(iRowPictName, extent_test_case, extent);

        value = getText(driver, alamat_berdasarkan_identitas);
        verifyValue(arr[61], value);
        takeSreenshot.capture(driver);
        status_testCase(iRowPictName, true, "");
    }

    public void alamat49Karakter() {
        iRowPictName = iRowPictName + iSeq;
        createTest(iRowPictName, extent_test_case, extent);

        click(driver, DebtorDetailPage.menuRealization);
        writeText(driver, DebtorDetailPage.txt_Search, no_app6);
        click(driver, DebtorDetailPage.tbl_realizationForm_debtorname);
        value = getText(driver, alamat_berdasarkan_identitas);
        verifyValue(arr[391], value);
        takeSreenshot.capture(driver);
        status_testCase(iRowPictName, true, "");
    }

    public void alamatKelurahan40Karakter() {
        iRowPictName = iRowPictName + iSeq;
        createTest(iRowPictName, extent_test_case, extent);

        click(driver, DebtorDetailPage.menuRealization);
        writeText(driver, DebtorDetailPage.txt_Search, no_app1);
        click(driver, DebtorDetailPage.tbl_realizationForm_debtorname);
        value = getText(driver, kelurahan);
        verifyValue(arr[62], value);
        takeSreenshot.capture(driver);
        status_testCase(iRowPictName, true, "");
    }

    public void alamatKelurahan39Karakter() {
        iRowPictName = iRowPictName + iSeq;
        createTest(iRowPictName, extent_test_case, extent);

        click(driver, DebtorDetailPage.menuRealization);
        writeText(driver, DebtorDetailPage.txt_Search, no_app6);
        click(driver, DebtorDetailPage.tbl_realizationForm_debtorname);
        value = getText(driver, kelurahan);
        verifyValue(arr[392], value);
        takeSreenshot.capture(driver);
        status_testCase(iRowPictName, true, "");
    }

    public void alamatKecamatan40Karakter() {
        iRowPictName = iRowPictName + iSeq;
        createTest(iRowPictName, extent_test_case, extent);

        click(driver, DebtorDetailPage.menuRealization);
        writeText(driver, DebtorDetailPage.txt_Search, no_app1);
        click(driver, DebtorDetailPage.tbl_realizationForm_debtorname);
        value = getText(driver, kecamatan);
        verifyValue(arr[63], value);
        takeSreenshot.capture(driver);
        status_testCase(iRowPictName, true, "");
    }

    public void alamatKecamatan39Karakter() {
        iRowPictName = iRowPictName + iSeq;
        createTest(iRowPictName, extent_test_case, extent);

        click(driver, DebtorDetailPage.menuRealization);
        writeText(driver, DebtorDetailPage.txt_Search, no_app6);
        click(driver, DebtorDetailPage.tbl_realizationForm_debtorname);
        value = getText(driver, kecamatan);
        verifyValue(arr[393], value);
        takeSreenshot.capture(driver);
        status_testCase(iRowPictName, true, "");
    }

    public void alamatKodepos() {
        iRowPictName = iRowPictName + iSeq;
        createTest(iRowPictName, extent_test_case, extent);

        click(driver, DebtorDetailPage.menuRealization);
        writeText(driver, DebtorDetailPage.txt_Search, no_app1);
        click(driver, DebtorDetailPage.tbl_realizationForm_debtorname);
        value = getText(driver, kode_pos);
        verifyValue(arr[64], value);
        takeSreenshot.capture(driver);
        status_testCase(iRowPictName, true, "");
    }

    public void alamatDatiII() {
        iRowPictName = iRowPictName + iSeq;
        createTest(iRowPictName, extent_test_case, extent);

        value = getText(driver, kode_dati_II);
        verifyValue(arr[65], value);
        takeSreenshot.capture(driver);
        status_testCase(iRowPictName, true, "");
    }

    public void province25Karakter() {
        iRowPictName = iRowPictName + iSeq;
        createTest(iRowPictName, extent_test_case, extent);

        value = getText(driver, province);
        verifyValue(arr[66], value);
        takeSreenshot.capture(driver);
        status_testCase(iRowPictName, true, "");
    }

    public void province24Karakter() {
        iRowPictName = iRowPictName + iSeq;
        createTest(iRowPictName, extent_test_case, extent);

        click(driver, DebtorDetailPage.menuRealization);
        writeText(driver, DebtorDetailPage.txt_Search, no_app6);
        click(driver, DebtorDetailPage.tbl_realizationForm_debtorname);
        value = getText(driver, province);
        verifyValue(arr[396], value);
        takeSreenshot.capture(driver);
        status_testCase(iRowPictName, true, "");
    }

    public void noTelepon13Karakter() {
        iRowPictName = iRowPictName + iSeq;
        createTest(iRowPictName, extent_test_case, extent);

        click(driver, DebtorDetailPage.menuRealization);
        writeText(driver, DebtorDetailPage.txt_Search, no_app1);
        click(driver, DebtorDetailPage.tbl_realizationForm_debtorname);
        value = getText(driver, no_telepon);
        verifyValue(arr[67], value);
        takeSreenshot.capture(driver);
        status_testCase(iRowPictName, true, "");
    }

    public void noTelepon12Karakter() {
        iRowPictName = iRowPictName + iSeq;
        createTest(iRowPictName, extent_test_case, extent);

        click(driver, DebtorDetailPage.menuRealization);
        writeText(driver, DebtorDetailPage.txt_Search, no_app6);
        click(driver, DebtorDetailPage.tbl_realizationForm_debtorname);
        value = getText(driver, no_telepon);
        verifyValue(arr[397], value);
        takeSreenshot.capture(driver);
        status_testCase(iRowPictName, true, "");
    }

    public void MobilePhoneNumber20Karakter() {
        iRowPictName = iRowPictName + iSeq;
        createTest(iRowPictName, extent_test_case, extent);

        click(driver, DebtorDetailPage.menuRealization);
        writeText(driver, DebtorDetailPage.txt_Search, no_app1);
        click(driver, DebtorDetailPage.tbl_realizationForm_debtorname);
        value = getText(driver, mobile_phone_number);
        verifyValue(arr[68], value);
        takeSreenshot.capture(driver);
        status_testCase(iRowPictName, true, "");
    }

    public void mobilePhoneNumber19Karakter() {
        iRowPictName = iRowPictName + iSeq;
        createTest(iRowPictName, extent_test_case, extent);

        click(driver, DebtorDetailPage.menuRealization);
        writeText(driver, DebtorDetailPage.txt_Search, no_app6);
        click(driver, DebtorDetailPage.tbl_realizationForm_debtorname);
        value = getText(driver, mobile_phone_number);
        verifyValue(arr[398], value);
        takeSreenshot.capture(driver);
        status_testCase(iRowPictName, true, "");
    }

    public void email50Karakter() {
        iRowPictName = iRowPictName + iSeq;
        createTest(iRowPictName, extent_test_case, extent);

        click(driver, DebtorDetailPage.menuRealization);
        writeText(driver, DebtorDetailPage.txt_Search, no_app6);
        click(driver, DebtorDetailPage.tbl_realizationForm_debtorname);
        value = getText(driver, email);
        LibUtils.scrollIntoView(driver, income_source);
        verifyValue(arr[399], value);
        takeSreenshot.capture(driver);
        status_testCase(iRowPictName, true, "");
    }

    public void email49Karakter() {
        iRowPictName = iRowPictName + iSeq;
        createTest(iRowPictName, extent_test_case, extent);

        click(driver, DebtorDetailPage.menuRealization);
        writeText(driver, DebtorDetailPage.txt_Search, no_app7);
        click(driver, DebtorDetailPage.tbl_realizationForm_debtorname);
        value = getText(driver, email);
        LibUtils.scrollIntoView(driver, income_source);
        verifyValue(arr[454], value);
        takeSreenshot.capture(driver);
        status_testCase(iRowPictName, true, "");
    }

    public void placeOfBirth50Karakter() {
        iRowPictName = iRowPictName + iSeq;
        createTest(iRowPictName, extent_test_case, extent);

        click(driver, DebtorDetailPage.menuRealization);
        writeText(driver, DebtorDetailPage.txt_Search, no_app6);
        click(driver, DebtorDetailPage.tbl_realizationForm_debtorname);
        value = getText(driver, place_of_birth);
        LibUtils.scrollIntoView(driver, income_source);
        verifyValue(arr[400], value);
        takeSreenshot.capture(driver);
        status_testCase(iRowPictName, true, "");
    }

    public void placeOfBirth49Karakter() {
        iRowPictName = iRowPictName + iSeq;
        createTest(iRowPictName, extent_test_case, extent);

        click(driver, DebtorDetailPage.menuRealization);
        writeText(driver, DebtorDetailPage.txt_Search, no_app7);
        click(driver, DebtorDetailPage.tbl_realizationForm_debtorname);
        value = getText(driver, place_of_birth);
        LibUtils.scrollIntoView(driver, income_source);
        verifyValue(arr[455], value);
        takeSreenshot.capture(driver);
        status_testCase(iRowPictName, true, "");
    }

    public void tanggalLahirDebitur() {
        iRowPictName = iRowPictName + iSeq;
        createTest(iRowPictName, extent_test_case, extent);

        click(driver, DebtorDetailPage.menuRealization);
        writeText(driver, DebtorDetailPage.txt_Search, no_app1);
        click(driver, DebtorDetailPage.tbl_realizationForm_debtorname);
        value = getText(driver, date_of_birth);
        LibUtils.scrollIntoView(driver, income_source);
        verifyValue(arr[71], value);
        takeSreenshot.capture(driver);
        status_testCase(iRowPictName, true, "");
    }

    public void lastEducation2Karakter() {
        iRowPictName = iRowPictName + iSeq;
        createTest(iRowPictName, extent_test_case, extent);

        click(driver, DebtorDetailPage.menuRealization);
        writeText(driver, DebtorDetailPage.txt_Search, no_app1);
        click(driver, DebtorDetailPage.tbl_realizationForm_debtorname);
        value = getText(driver, last_education_code);
        LibUtils.scrollIntoView(driver, income_source);
        verifyValue(arr[72], value);
        takeSreenshot.capture(driver);
        status_testCase(iRowPictName, true, "");
    }

    public void lastEducationKosong() {
        iRowPictName = iRowPictName + iSeq;
        createTest(iRowPictName, extent_test_case, extent);

        click(driver, DebtorDetailPage.menuRealization);
        writeText(driver, DebtorDetailPage.txt_Search, no_app6);
        click(driver, DebtorDetailPage.tbl_realizationForm_debtorname);
        value = getText(driver, last_education_code);
        LibUtils.scrollIntoView(driver, income_source);
        verifyValue(arr[402], value);
        takeSreenshot.capture(driver);
        status_testCase(iRowPictName, true, "");
    }

    public void employer50Karakter() {
        iRowPictName = iRowPictName + iSeq;
        createTest(iRowPictName, extent_test_case, extent);

        click(driver, DebtorDetailPage.menuRealization);
        writeText(driver, DebtorDetailPage.txt_Search, no_app9);
        click(driver, DebtorDetailPage.tbl_realizationForm_debtorname);
        value = getText(driver, employer);
        LibUtils.scrollIntoView(driver, income_source);
        verifyValue(arr[568], value);
        takeSreenshot.capture(driver);
        status_testCase(iRowPictName, true, "");
    }

    public void employer49Karakter() {
        iRowPictName = iRowPictName + iSeq;
        createTest(iRowPictName, extent_test_case, extent);

        click(driver, DebtorDetailPage.menuRealization);
        writeText(driver, DebtorDetailPage.txt_Search, no_app10);
        click(driver, DebtorDetailPage.tbl_realizationForm_debtorname);
        value = getText(driver, employer);
        LibUtils.scrollIntoView(driver, income_source);
        verifyValue(arr[623], value);
        takeSreenshot.capture(driver);
        status_testCase(iRowPictName, true, "");
    }

    public void employerAddress200Karakter() {
        iRowPictName = iRowPictName + iSeq;
        createTest(iRowPictName, extent_test_case, extent);

        click(driver, DebtorDetailPage.menuRealization);
        writeText(driver, DebtorDetailPage.txt_Search, no_app7);
        click(driver, DebtorDetailPage.tbl_realizationForm_debtorname);
        value = getText(driver, employer_address);
        LibUtils.scrollIntoView(driver, income_source);
        verifyValue(arr[514], value);
        takeSreenshot.capture(driver);
        status_testCase(iRowPictName, true, "");
    }

    public void employerAddress199Karakter() {
        iRowPictName = iRowPictName + iSeq;
        createTest(iRowPictName, extent_test_case, extent);

        click(driver, DebtorDetailPage.menuRealization);
        writeText(driver, DebtorDetailPage.txt_Search, no_app6);
        click(driver, DebtorDetailPage.tbl_realizationForm_debtorname);
        value = getText(driver, employer_address);
        LibUtils.scrollIntoView(driver, income_source);
        verifyValue(arr[459], value);
        takeSreenshot.capture(driver);
        status_testCase(iRowPictName, true, "");
    }

    public void motherMaiden50Karakter() {
        iRowPictName = iRowPictName + iSeq;
        createTest(iRowPictName, extent_test_case, extent);

        click(driver, DebtorDetailPage.menuRealization);
        writeText(driver, DebtorDetailPage.txt_Search, no_app7);
        click(driver, DebtorDetailPage.tbl_realizationForm_debtorname);
        value = getText(driver, mother_maiden_name);
        LibUtils.scrollIntoView(driver, income_source);
        verifyValue(arr[460], value);
        takeSreenshot.capture(driver);
        status_testCase(iRowPictName, true, "");
    }

    public void motherMaiden49Karakter() {
        iRowPictName = iRowPictName + iSeq;
        createTest(iRowPictName, extent_test_case, extent);

        click(driver, DebtorDetailPage.menuRealization);
        writeText(driver, DebtorDetailPage.txt_Search, no_app8);
        click(driver, DebtorDetailPage.tbl_realizationForm_debtorname);
        value = getText(driver, mother_maiden_name);
        LibUtils.scrollIntoView(driver, income_source);
        verifyValue(arr[515], value);
        takeSreenshot.capture(driver);
        status_testCase(iRowPictName, true, "");
    }

    public void religion1Karakter() {
        iRowPictName = iRowPictName + iSeq;
        createTest(iRowPictName, extent_test_case, extent);

        click(driver, DebtorDetailPage.menuRealization);
        writeText(driver, DebtorDetailPage.txt_Search, no_app1);
        click(driver, DebtorDetailPage.tbl_realizationForm_debtorname);
        value = getText(driver, religion);
        LibUtils.scrollIntoView(driver, income_source);
        verifyValue(arr[76], value);
        takeSreenshot.capture(driver);
        status_testCase(iRowPictName, true, "");
    }

    public void penghasilanKotorPerBulan() {
        iRowPictName = iRowPictName + iSeq;
        createTest(iRowPictName, extent_test_case, extent);

        click(driver, DebtorDetailPage.menuRealization);
        writeText(driver, DebtorDetailPage.txt_Search, no_app10);
        click(driver, DebtorDetailPage.tbl_realizationForm_debtorname);
        value = getText(driver, penghasilan_kotor_per_bulan);
        LibUtils.scrollIntoView(driver, income_source);
        verifyValue(arr[627], value);
        takeSreenshot.capture(driver);
        status_testCase(iRowPictName, true, "");
    }

    public void maritalStatus1Karakter() {
        iRowPictName = iRowPictName + iSeq;
        createTest(iRowPictName, extent_test_case, extent);

        click(driver, DebtorDetailPage.menuRealization);
        writeText(driver, DebtorDetailPage.txt_Search, no_app10);
        click(driver, DebtorDetailPage.tbl_realizationForm_debtorname);
        value = getText(driver, marital_status);
        LibUtils.scrollIntoView(driver, income_source);
        verifyValue(arr[628], value);
        takeSreenshot.capture(driver);
        status_testCase(iRowPictName, true, "");
    }

    public void namaPasangan40Karakter() {
        iRowPictName = iRowPictName + iSeq;
        createTest(iRowPictName, extent_test_case, extent);

        click(driver, DebtorDetailPage.menuRealization);
        writeText(driver, DebtorDetailPage.txt_Search, no_app8);
        click(driver, DebtorDetailPage.tbl_realizationForm_debtorname);
        value = getText(driver, nama_pasangan);
        LibUtils.scrollIntoView(driver, income_source);
        verifyValue(arr[519], value);
        takeSreenshot.capture(driver);
        status_testCase(iRowPictName, true, "");
    }

    public void namaPasangan39Karakter() {
        iRowPictName = iRowPictName + iSeq;
        createTest(iRowPictName, extent_test_case, extent);

        click(driver, DebtorDetailPage.menuRealization);
        writeText(driver, DebtorDetailPage.txt_Search, no_app7);
        click(driver, DebtorDetailPage.tbl_realizationForm_debtorname);
        value = getText(driver, nama_pasangan);
        LibUtils.scrollIntoView(driver, income_source);
        verifyValue(arr[464], value);
        takeSreenshot.capture(driver);
        status_testCase(iRowPictName, true, "");
    }

    public void namaPasanganKosong() {
        iRowPictName = iRowPictName + iSeq;
        createTest(iRowPictName, extent_test_case, extent);

        click(driver, DebtorDetailPage.menuRealization);
        writeText(driver, DebtorDetailPage.txt_Search, no_app6);
        click(driver, DebtorDetailPage.tbl_realizationForm_debtorname);
        value = getText(driver, nama_pasangan);
        LibUtils.scrollIntoView(driver, income_source);
        verifyValue(arr[409], value);
        takeSreenshot.capture(driver);
        status_testCase(iRowPictName, true, "");
    }

    public void tanggalLahirPasangan8Karakter() {
        iRowPictName = iRowPictName + iSeq;
        createTest(iRowPictName, extent_test_case, extent);

        click(driver, DebtorDetailPage.menuRealization);
        writeText(driver, DebtorDetailPage.txt_Search, no_app10);
        click(driver, DebtorDetailPage.tbl_realizationForm_debtorname);
        value = getText(driver, tanggal_lahir_pasangan);
        LibUtils.scrollIntoView(driver, income_source);
        verifyValue(arr[630], value);
        takeSreenshot.capture(driver);
        status_testCase(iRowPictName, true, "");
    }

    public void tanggalLahirPasanganKosong() {
        iRowPictName = iRowPictName + iSeq;
        createTest(iRowPictName, extent_test_case, extent);

        click(driver, DebtorDetailPage.menuRealization);
        writeText(driver, DebtorDetailPage.txt_Search, no_app6);
        click(driver, DebtorDetailPage.tbl_realizationForm_debtorname);
        value = getText(driver, tanggal_lahir_pasangan);
        LibUtils.scrollIntoView(driver, income_source);
        verifyValue(arr[410], value);
        takeSreenshot.capture(driver);
        status_testCase(iRowPictName, true, "");
    }

    public void perjanjianPisahHarta1Karakter() {
        iRowPictName = iRowPictName + iSeq;
        createTest(iRowPictName, extent_test_case, extent);

        click(driver, DebtorDetailPage.menuRealization);
        writeText(driver, DebtorDetailPage.txt_Search, no_app1);
        click(driver, DebtorDetailPage.tbl_realizationForm_debtorname);
        value = getText(driver, perjanjian_pisah_harta);
        LibUtils.scrollIntoView(driver, income_source);
        verifyValue(arr[81], value);
        takeSreenshot.capture(driver);
        status_testCase(iRowPictName, true, "");
    }

    public void perjanjianPisahHarta1Kosong() {
        iRowPictName = iRowPictName + iSeq;
        createTest(iRowPictName, extent_test_case, extent);

        click(driver, DebtorDetailPage.menuRealization);
        writeText(driver, DebtorDetailPage.txt_Search, no_app6);
        click(driver, DebtorDetailPage.tbl_realizationForm_debtorname);
        value = getText(driver, perjanjian_pisah_harta);
        LibUtils.scrollIntoView(driver, income_source);
        verifyValue(arr[411], value);
        takeSreenshot.capture(driver);
        status_testCase(iRowPictName, true, "");
    }

    public void noAkte30Karakter() {
        iRowPictName = iRowPictName + iSeq;
        createTest(iRowPictName, extent_test_case, extent);

        click(driver, DebtorDetailPage.menuRealization);
        writeText(driver, DebtorDetailPage.txt_Search, no_app10);
        click(driver, DebtorDetailPage.tbl_realizationForm_debtorname);
        value = getText(driver, no_akte);
        LibUtils.scrollIntoView(driver, income_source);
        verifyValue(arr[632], value);
        takeSreenshot.capture(driver);
        status_testCase(iRowPictName, true, "");
    }

    public void noAkte29Karakter() {
        iRowPictName = iRowPictName + iSeq;
        createTest(iRowPictName, extent_test_case, extent);

        click(driver, DebtorDetailPage.menuRealization);
        writeText(driver, DebtorDetailPage.txt_Search, no_app9);
        click(driver, DebtorDetailPage.tbl_realizationForm_debtorname);
        value = getText(driver, no_akte);
        LibUtils.scrollIntoView(driver, income_source);
        verifyValue(arr[577], value);
        takeSreenshot.capture(driver);
        status_testCase(iRowPictName, true, "");
    }

    public void noAkteKosong() {
        iRowPictName = iRowPictName + iSeq;
        createTest(iRowPictName, extent_test_case, extent);

        click(driver, DebtorDetailPage.menuRealization);
        writeText(driver, DebtorDetailPage.txt_Search, no_app8);
        click(driver, DebtorDetailPage.tbl_realizationForm_debtorname);
        value = getText(driver, no_akte);
        LibUtils.scrollIntoView(driver, income_source);
        verifyValue(arr[522], value);
        takeSreenshot.capture(driver);
        status_testCase(iRowPictName, true, "");
    }

    public void tanggalBerdiri8Karakter() {
        iRowPictName = iRowPictName + iSeq;
        createTest(iRowPictName, extent_test_case, extent);

        click(driver, DebtorDetailPage.menuRealization);
        writeText(driver, DebtorDetailPage.txt_Search, no_app10);
        click(driver, DebtorDetailPage.tbl_realizationForm_debtorname);
        value = getText(driver, tanggal_berdiri);
        LibUtils.scrollIntoView(driver, income_source);
        verifyValue(arr[633], value);
        takeSreenshot.capture(driver);
        status_testCase(iRowPictName, true, "");
    }

    public void tanggalBerdiriKosong() {
        iRowPictName = iRowPictName + iSeq;
        createTest(iRowPictName, extent_test_case, extent);

        click(driver, DebtorDetailPage.menuRealization);
        writeText(driver, DebtorDetailPage.txt_Search, no_app8);
        click(driver, DebtorDetailPage.tbl_realizationForm_debtorname);
        value = getText(driver, tanggal_berdiri);
        LibUtils.scrollIntoView(driver, income_source);
        verifyValue(arr[523], value);
        takeSreenshot.capture(driver);
        status_testCase(iRowPictName, true, "");
    }

    public void noAkteTerakhir30Karakter() {
        iRowPictName = iRowPictName + iSeq;
        createTest(iRowPictName, extent_test_case, extent);

        click(driver, DebtorDetailPage.menuRealization);
        writeText(driver, DebtorDetailPage.txt_Search, no_app10);
        click(driver, DebtorDetailPage.tbl_realizationForm_debtorname);
        value = getText(driver, no_akte_terakhir);
        LibUtils.scrollIntoView(driver, income_source);
        verifyValue(arr[634], value);
        takeSreenshot.capture(driver);
        status_testCase(iRowPictName, true, "");
    }

    public void noAkteTerakhir29Karakter() {
        iRowPictName = iRowPictName + iSeq;
        createTest(iRowPictName, extent_test_case, extent);

        click(driver, DebtorDetailPage.menuRealization);
        writeText(driver, DebtorDetailPage.txt_Search, no_app9);
        click(driver, DebtorDetailPage.tbl_realizationForm_debtorname);
        value = getText(driver, no_akte_terakhir);
        LibUtils.scrollIntoView(driver, income_source);
        verifyValue(arr[579], value);
        takeSreenshot.capture(driver);
        status_testCase(iRowPictName, true, "");
    }

    public void noAkteTerakhirKosong() {
        iRowPictName = iRowPictName + iSeq;
        createTest(iRowPictName, extent_test_case, extent);

        click(driver, DebtorDetailPage.menuRealization);
        writeText(driver, DebtorDetailPage.txt_Search, no_app8);
        click(driver, DebtorDetailPage.tbl_realizationForm_debtorname);
        value = getText(driver, no_akte_terakhir);
        LibUtils.scrollIntoView(driver, income_source);
        verifyValue(arr[524], value);
        takeSreenshot.capture(driver);
        status_testCase(iRowPictName, true, "");
    }

    public void tanggalAkteTerakhir8Karakter() {
        iRowPictName = iRowPictName + iSeq;
        createTest(iRowPictName, extent_test_case, extent);

        click(driver, DebtorDetailPage.menuRealization);
        writeText(driver, DebtorDetailPage.txt_Search, no_app10);
        click(driver, DebtorDetailPage.tbl_realizationForm_debtorname);
        value = getText(driver, tanggal_akte_terkhir);
        LibUtils.scrollIntoView(driver, income_source);
        verifyValue(arr[635], value);
        takeSreenshot.capture(driver);
        status_testCase(iRowPictName, true, "");
    }

    public void tanggalAkteTerakhirKosong() {
        iRowPictName = iRowPictName + iSeq;
        createTest(iRowPictName, extent_test_case, extent);

        click(driver, DebtorDetailPage.menuRealization);
        writeText(driver, DebtorDetailPage.txt_Search, no_app8);
        click(driver, DebtorDetailPage.tbl_realizationForm_debtorname);
        value = getText(driver, tanggal_akte_terkhir);
        LibUtils.scrollIntoView(driver, income_source);
        verifyValue(arr[525], value);
        takeSreenshot.capture(driver);
        status_testCase(iRowPictName, true, "");
    }

    public void bidangUsaha6Karakter() {
        iRowPictName = iRowPictName + iSeq;
        createTest(iRowPictName, extent_test_case, extent);

        click(driver, DebtorDetailPage.menuRealization);
        writeText(driver, DebtorDetailPage.txt_Search, no_app10);
        click(driver, DebtorDetailPage.tbl_realizationForm_debtorname);
        value = getText(driver, bidang_usaha);
        LibUtils.scrollIntoView(driver, income_source);
        verifyValue(arr[636], value);
        takeSreenshot.capture(driver);
        status_testCase(iRowPictName, true, "");
    }

    public void jangkaWaktu3Karakter() {
        iRowPictName = iRowPictName + iSeq;
        createTest(iRowPictName, extent_test_case, extent);

        click(driver, DebtorDetailPage.menuRealization);
        writeText(driver, DebtorDetailPage.txt_Search, no_app9);
        click(driver, DebtorDetailPage.tbl_realizationForm_debtorname);
        value = getText(driver, jangka_waktu);
        LibUtils.scrollIntoView(driver, income_source);
        verifyValue(arr[582], value);
        takeSreenshot.capture(driver);
        status_testCase(iRowPictName, true, "");
    }

    public void jenisKredit3Karakter() {
        iRowPictName = iRowPictName + iSeq;
        createTest(iRowPictName, extent_test_case, extent);

        click(driver, DebtorDetailPage.menuRealization);
        writeText(driver, DebtorDetailPage.txt_Search, no_app8);
        click(driver, DebtorDetailPage.tbl_realizationForm_debtorname);
        value = getText(driver, jenis_kredit);
        LibUtils.scrollIntoView(driver, income_source);
        verifyValue(arr[528], value);
        takeSreenshot.capture(driver);
        status_testCase(iRowPictName, true, "");
    }

    public void plafon15Karakter() {
        iRowPictName = iRowPictName + iSeq;
        createTest(iRowPictName, extent_test_case, extent);

        click(driver, DebtorDetailPage.menuRealization);
        writeText(driver, DebtorDetailPage.txt_Search, no_app7);
        click(driver, DebtorDetailPage.tbl_realizationForm_debtorname);
        value = getText(driver, plafon);
        LibUtils.scrollIntoView(driver, income_source);
        verifyValue(arr[474], value);
        takeSreenshot.capture(driver);
        status_testCase(iRowPictName, true, "");
    }

    public void interestRate5Karakter() {
        iRowPictName = iRowPictName + iSeq;
        createTest(iRowPictName, extent_test_case, extent);

        click(driver, DebtorDetailPage.menuRealization);
        writeText(driver, DebtorDetailPage.txt_Search, no_app6);
        click(driver, DebtorDetailPage.tbl_realizationForm_debtorname);
        value = getText(driver, interest_rate);
        LibUtils.scrollIntoView(driver, income_source);
        verifyValue(arr[420], value);
        takeSreenshot.capture(driver);
        status_testCase(iRowPictName, true, "");
    }

    public void noPk100Karakter() {
        iRowPictName = iRowPictName + iSeq;
        createTest(iRowPictName, extent_test_case, extent);

        click(driver, DebtorDetailPage.menuRealization);
        writeText(driver, DebtorDetailPage.txt_Search, no_app6);
        click(driver, DebtorDetailPage.tbl_realizationForm_debtorname);
        value = getText(driver, nomor_pk);
        LibUtils.scrollIntoView(driver, income_source);
        verifyValue(arr[421], value);
        takeSreenshot.capture(driver);
        status_testCase(iRowPictName, true, "");
    }

    public void noPk99Karakter() {
        iRowPictName = iRowPictName + iSeq;
        createTest(iRowPictName, extent_test_case, extent);

        click(driver, DebtorDetailPage.menuRealization);
        writeText(driver, DebtorDetailPage.txt_Search, no_app7);
        click(driver, DebtorDetailPage.tbl_realizationForm_debtorname);
        value = getText(driver, nomor_pk);
        LibUtils.scrollIntoView(driver, income_source);
        verifyValue(arr[476], value);
        takeSreenshot.capture(driver);
        status_testCase(iRowPictName, true, "");
    }

    public void tanggalAkad8Karakter() {
        iRowPictName = iRowPictName + iSeq;
        createTest(iRowPictName, extent_test_case, extent);

        value = getText(driver, tanggal_akad);
        LibUtils.scrollIntoView(driver, income_source);
        verifyValue(arr[477], value);
        takeSreenshot.capture(driver);
        status_testCase(iRowPictName, true, "");
    }

    public void tanggalAngsuran1_8Karakter() {
        iRowPictName = iRowPictName + iSeq;
        createTest(iRowPictName, extent_test_case, extent);

        value = getText(driver, tanggal_angsuran_1);
        LibUtils.scrollIntoView(driver, income_source);
        verifyValue(arr[478], value);
        takeSreenshot.capture(driver);
        status_testCase(iRowPictName, true, "");
    }

    public void jenisPenggunaan1Karakter() {
        iRowPictName = iRowPictName + iSeq;
        createTest(iRowPictName, extent_test_case, extent);

        value = getText(driver, jenis_penggunaan);
        LibUtils.scrollIntoView(driver, income_source);
        verifyValue(arr[479], value);
        takeSreenshot.capture(driver);
        status_testCase(iRowPictName, true, "");
    }

    public void sektorEkonomi6Karakter() {
        iRowPictName = iRowPictName + iSeq;
        createTest(iRowPictName, extent_test_case, extent);

        value = getText(driver, sektor_ekonomi);
        LibUtils.scrollIntoView(driver, income_source);
        verifyValue(arr[480], value);
        takeSreenshot.capture(driver);
        status_testCase(iRowPictName, true, "");
    }

    public void omzet17Karakter() {
        iRowPictName = iRowPictName + iSeq;
        createTest(iRowPictName, extent_test_case, extent);

        click(driver, DebtorDetailPage.menuRealization);
        writeText(driver, DebtorDetailPage.txt_Search, no_app1);
        click(driver, DebtorDetailPage.tbl_realizationForm_debtorname);
        value = getText(driver, omzet);
        LibUtils.scrollIntoView(driver, income_source);
        verifyValue(arr[96], value);
        takeSreenshot.capture(driver);
        status_testCase(iRowPictName, true, "");
    }

    public void omzetKosong() {
        iRowPictName = iRowPictName + iSeq;
        createTest(iRowPictName, extent_test_case, extent);

        click(driver, DebtorDetailPage.menuRealization);
        writeText(driver, DebtorDetailPage.txt_Search, no_app10);
        click(driver, DebtorDetailPage.tbl_realizationForm_debtorname);
        value = getText(driver, omzet);
        LibUtils.scrollIntoView(driver, income_source);
        verifyValue(arr[646], value);
        takeSreenshot.capture(driver);
        status_testCase(iRowPictName, true, "");
    }

    public void goPublic1Karakter() {
        iRowPictName = iRowPictName + iSeq;
        createTest(iRowPictName, extent_test_case, extent);

        click(driver, DebtorDetailPage.menuRealization);
        writeText(driver, DebtorDetailPage.txt_Search, no_app1);
        click(driver, DebtorDetailPage.tbl_realizationForm_debtorname);
        value = getText(driver, go_public);
        LibUtils.scrollIntoView(driver, income_source);
        verifyValue(arr[97], value);
        takeSreenshot.capture(driver);
        status_testCase(iRowPictName, true, "");
    }

    public void goPublicKosong() {
        iRowPictName = iRowPictName + iSeq;
        createTest(iRowPictName, extent_test_case, extent);

        click(driver, DebtorDetailPage.menuRealization);
        writeText(driver, DebtorDetailPage.txt_Search, no_app10);
        click(driver, DebtorDetailPage.tbl_realizationForm_debtorname);
        value = getText(driver, go_public);
        LibUtils.scrollIntoView(driver, income_source);
        verifyValue(arr[647], value);
        takeSreenshot.capture(driver);
        status_testCase(iRowPictName, true, "");
    }

    public void sandiGolongan() {
        iRowPictName = iRowPictName + iSeq;
        createTest(iRowPictName, extent_test_case, extent);

        value = getText(driver, sandi_golongan_debitur);
        LibUtils.scrollIntoView(driver, income_source);
        verifyValue(arr[648], value);
        takeSreenshot.capture(driver);
        status_testCase(iRowPictName, true, "");
    }

    public void penghasilanKotorPerTahun() {
        iRowPictName = iRowPictName + iSeq;
        createTest(iRowPictName, extent_test_case, extent);

        value = getText(driver, penghasilan_kotor_per_tahun);
        LibUtils.scrollIntoView(driver, income_source);
        verifyValue(arr[649], value);
        takeSreenshot.capture(driver);
        status_testCase(iRowPictName, true, "");
    }

    public void bentukBadanUsahaSesuaiList() {
        iRowPictName = iRowPictName + iSeq;
        createTest(iRowPictName, extent_test_case, extent);

        value = getText(driver, bentuk_badan_usaha);
        LibUtils.scrollIntoView(driver, income_source);
        verifyValue(arr[650], value);
        takeSreenshot.capture(driver);
        status_testCase(iRowPictName, true, "");
    }

    public void bentukBadanUsahaKosong() {
        iRowPictName = iRowPictName + iSeq;
        createTest(iRowPictName, extent_test_case, extent);

        click(driver, DebtorDetailPage.menuRealization);
        writeText(driver, DebtorDetailPage.txt_Search, no_app6);
        click(driver, DebtorDetailPage.tbl_realizationForm_debtorname);
        value = getText(driver, bentuk_badan_usaha);
        LibUtils.scrollIntoView(driver, income_source);
        verifyValue(arr[430], value);
        takeSreenshot.capture(driver);
        status_testCase(iRowPictName, true, "");
    }

    public void tempatBerdiriBadanUsaha30Karakter() {
        iRowPictName = iRowPictName + iSeq;
        createTest(iRowPictName, extent_test_case, extent);

        click(driver, DebtorDetailPage.menuRealization);
        writeText(driver, DebtorDetailPage.txt_Search, no_app6);
        click(driver, DebtorDetailPage.tbl_realizationForm_debtorname);
        value = getText(driver, tempat_berdiri_badan_usaha);
        LibUtils.scrollIntoView(driver, income_source);
        verifyValue(arr[431], value);
        takeSreenshot.capture(driver);
        status_testCase(iRowPictName, true, "");
    }

    public void tempatBerdiriBadanUsaha29Karakter() {
        iRowPictName = iRowPictName + iSeq;
        createTest(iRowPictName, extent_test_case, extent);

        click(driver, DebtorDetailPage.menuRealization);
        writeText(driver, DebtorDetailPage.txt_Search, no_app7);
        click(driver, DebtorDetailPage.tbl_realizationForm_debtorname);
        value = getText(driver, tempat_berdiri_badan_usaha);
        LibUtils.scrollIntoView(driver, income_source);
        verifyValue(arr[486], value);
        takeSreenshot.capture(driver);
        status_testCase(iRowPictName, true, "");
    }

    public void tempatBerdiriBadanUsahaKosong() {
        iRowPictName = iRowPictName + iSeq;
        createTest(iRowPictName, extent_test_case, extent);

        click(driver, DebtorDetailPage.menuRealization);
        writeText(driver, DebtorDetailPage.txt_Search, no_app8);
        click(driver, DebtorDetailPage.tbl_realizationForm_debtorname);
        value = getText(driver, tempat_berdiri_badan_usaha);
        LibUtils.scrollIntoView(driver, income_source);
        verifyValue(arr[541], value);
        takeSreenshot.capture(driver);
        status_testCase(iRowPictName, true, "");
    }

    public void disbursement8KarakterCurrentDate() {
        iRowPictName = iRowPictName + iSeq;
        createTest(iRowPictName, extent_test_case, extent);

        click(driver, DebtorDetailPage.menuRealization);
        writeText(driver, DebtorDetailPage.txt_Search, no_app7);
        click(driver, DebtorDetailPage.tbl_realizationForm_debtorname);
        value = getText(driver, disbursement_date);
        LibUtils.scrollIntoView(driver, income_source);
        verifyValue(arr[488], value);
        takeSreenshot.capture(driver);
        status_testCase(iRowPictName, true, "");
    }

    public void disbursement8KarakterPlus1Hari() {//not test skip
        iRowPictName = iRowPictName + iSeq;
        createTest(iRowPictName, extent_test_case, extent);
        try {

//            click(driver, DebtorDetailPage.menuRealization);
            writeText(driver, DebtorDetailPage.txt_Search, no_app8);
            click(driver, DebtorDetailPage.tbl_realizationForm_debtorname);
            value = getText(driver, disbursement_date);
            verifyValue(arr[543], value);
            takeSreenshot.capture(driver);
            status_testCase(iRowPictName, true, "");
        } catch (Exception ee) {
            LibUtils.capture.log(Status.FAIL, ee);

            takeSreenshot.capture(driver);
            status_testCase(iRowPictName, false, "SKIP TEST/ NOT TEST");
        }

    }

    public void tenor3Karakter() {
        iRowPictName = iRowPictName + iSeq;
        createTest(iRowPictName, extent_test_case, extent);

        click(driver, DebtorDetailPage.menuRealization);
        writeText(driver, DebtorDetailPage.txt_Search, no_app1);
        click(driver, DebtorDetailPage.tbl_realizationForm_debtorname);
        value = getText(driver, tenor);
        LibUtils.scrollIntoView(driver, income_source);
        verifyValue(arr[104], value);
        takeSreenshot.capture(driver);
        status_testCase(iRowPictName, true, "");
    }

    public void segmentasiDebitur2Karakter() {
        iRowPictName = iRowPictName + iSeq;
        createTest(iRowPictName, extent_test_case, extent);

        click(driver, DebtorDetailPage.menuRealization);
        writeText(driver, DebtorDetailPage.txt_Search, no_app10);
        click(driver, DebtorDetailPage.tbl_realizationForm_debtorname);
        value = getText(driver, segmentasi_debitur);
        LibUtils.scrollIntoView(driver, income_source);
        verifyValue(arr[710], value);
        takeSreenshot.capture(driver);
        status_testCase(iRowPictName, true, "");
    }

    public void kodePekerjaan3Karakter() {
        iRowPictName = iRowPictName + iSeq;
        createTest(iRowPictName, extent_test_case, extent);

        value = getText(driver, kode_pekerjaan);
        LibUtils.scrollIntoView(driver, income_source);
        verifyValue(arr[711], value);
        takeSreenshot.capture(driver);
        status_testCase(iRowPictName, true, "");
    }

    public void debtorCategory2Karakter() {
        iRowPictName = iRowPictName + iSeq;
        createTest(iRowPictName, extent_test_case, extent);

        value = getText(driver, debtor_category);
        LibUtils.scrollIntoView(driver, income_source);
        verifyValue(arr[712], value);
        takeSreenshot.capture(driver);
        status_testCase(iRowPictName, true, "");
    }

    public void incomeSource1Karakter() {
        iRowPictName = iRowPictName + iSeq;
        createTest(iRowPictName, extent_test_case, extent);

        value = getText(driver, income_source);
        LibUtils.scrollIntoView(driver, income_source);
        verifyValue(arr[713], value);
        takeSreenshot.capture(driver);
        status_testCase(iRowPictName, true, "");
    }

    public void jumlahTanggungan2Karakter() {
        iRowPictName = iRowPictName + iSeq;
        createTest(iRowPictName, extent_test_case, extent);

        click(driver, DebtorDetailPage.menuRealization);
        writeText(driver, DebtorDetailPage.txt_Search, no_app7);
        click(driver, DebtorDetailPage.tbl_realizationForm_debtorname);
        value = getText(driver, jumlah_tanggungan);
        LibUtils.scrollIntoView(driver, income_source);
        verifyValue(arr[549], value);
        takeSreenshot.capture(driver);
        status_testCase(iRowPictName, true, "");
    }

    public void jumlahTanggunganKosong() {
        iRowPictName = iRowPictName + iSeq;
        createTest(iRowPictName, extent_test_case, extent);

        click(driver, DebtorDetailPage.menuRealization);
        writeText(driver, DebtorDetailPage.txt_Search, no_app6);
        click(driver, DebtorDetailPage.tbl_realizationForm_debtorname);
        value = getText(driver, jumlah_tanggungan);
        LibUtils.scrollIntoView(driver, income_source);
        verifyValue(arr[494], value);
        takeSreenshot.capture(driver);
        status_testCase(iRowPictName, true, "");
    }
}
