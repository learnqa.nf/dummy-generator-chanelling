package elementsPage;

import applos.ConsumeData;
import capture.TakeSreenshot;
import com.aventstack.extentreports.Status;
import com.aventstack.extentreports.markuputils.MarkupHelper;
import createDataCSV.DataCSV;
import formatFile.ZipFolder;
import individu.LibUtils;
import io.github.bonigarcia.wdm.WebDriverManager;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.edge.EdgeDriver;
import org.openqa.selenium.interactions.Actions;
import org.testng.Assert;
import org.testng.annotations.Test;
import randomValue.GenerateRandomDataApp;
import readFile.ReadCSV;
import readFile.ReadCSVFormApproval;
import runner.FormApprovalRunner;
import testData.ReadTestData;
import writeFile.UtilsFile;

import java.io.File;
import java.io.IOException;
import java.util.List;
import java.util.concurrent.TimeUnit;

import static elementsPage.ApprovalFormApprover.loginApprover;
import static elementsPage.Const.*;
import static individu.ChangesStatus.changedApproveAsApprover;
import static individu.ChangesStatus.changedRecomendationAsMaker;
import static individu.LibUtils.*;
import static randomValue.GenerateRandomDataApp.dateNow;
import static readFile.ReadCSV.readAllLineAppCSV;
import static runner.FormApprovalRunner.extent;
import static runner.FormApprovalRunner.extent_test_case;
import static writeFile.UtilsFile.unzip;

public class ApprovalFormMaker extends BaseAction {

    public static final By menuApproval = By.xpath("//div[normalize-space()='Approval Form']");
    public static final By lbl_filter_source = By.xpath("//label[normalize-space()='Source']");
    public static final By lbl_filter_status = By.xpath("//label[normalize-space()='Status']");
    public static final By lbl_filter_partner = By.xpath("//label[normalize-space()='Partner']");
    public static final By lbl_no = By.xpath("//th[@aria-label='No.']");
    public static final By lbl_source = By.xpath("//th[@aria-label='Source']");
    public static final By lbl_application_no = By.xpath("//th[@aria-label='Application No']");
    public static final By lbl_debtor_name = By.xpath("//th[@aria-label='Debtor Name: activate to sort column descending']");
    public static final By lbl_partner = By.xpath("//th[@aria-label='Partner: activate to sort column ascending']");
    public static final By lbl_date = By.xpath("//th[@aria-label='Date: activate to sort column ascending']");
    public static final By lbl_loan_amount = By.xpath("//th[@aria-label='Loan Amount: activate to sort column ascending']");
    public static final By lbl_rate = By.xpath("//th[@aria-label='Rate']");
    public static final By lbl_status = By.xpath("//th[@aria-label='Status: activate to sort column ascending']");
    public static final By lbl_reason = By.xpath("//th[@aria-label='Reason']");
    public static final By lbl_hasil_rekomendasi = By.xpath("//th[@aria-label='Hasil Rekomendasi']");
    public static final By list_source_all = By.xpath("//li[normalize-space()='All']");
    public static final By list_source_los = By.xpath("//*[starts-with(@id, 'select2-select-source-result-') and contains(@id,'-LOS')]");
    public static final By list_source_nikel = By.xpath("//*[starts-with(@id, 'select2-select-source-result-') and contains(@id,'-NIKEL')]");

    public static final By title_approval_form = By.xpath("//h1[normalize-space()='Approval Form']");
    public static final By txtSearch = By.id("search-filter");
    public static final By btnDetails = By.id("details-button");
    public static final By btnUnsafe = By.id("proceed-link");
    public static final By userId = By.id("username");
    public static final By pwd = By.id("password");
    public static final By btnLogin = By.xpath("//button[contains(.,'Sign In')]");
    public static final By btnProfile = By.xpath("//a[@id='header-foto-profil']");
    public static final By btnLogout = By.linkText("Logout");
    public static final By firstNumberTableForm = By.xpath("//*[@id='dataTable']/tbody/tr[1]/td[1]");
    public static final By btnBulkRecomend = By.linkText("Bulk Recommendation");
    public static final By btn_cancelSelection = By.xpath("/html/body/div[2]/div[1]/div/div[1]/a");
    public static final By btn_backApprovalFormDetail = By.xpath("/html/body/div[2]/form/div[1]/h1/a");
    public static final By tableBulk_nextPage = By.xpath("//*[@id='channelingApprovalDataTable_next']/a");
    public static final By radiobtnAppAll = By.xpath("//table[@id='channelingApprovalDataTable']/thead/tr/th/div/label");
    public static final By recomended = By.xpath("//button[@id='recommended']");
    public static final By notRecomended = By.xpath("//button[@id='notRecommended']");
    public static final By noDataTable = By.xpath("//td[contains(.,'No matching records found')]");
    public static final By noDataAvailable = By.xpath("//td[@class='dataTables_empty']");
    public static final By bulk_statusForReview = By.xpath("//*[@id='select-status']/option[3]");
    public static final By bulk_statusWaitingApproval = By.xpath("//*[@id='select-status']/option[4]");
    public static final By bulk_statusApproved = By.xpath("//*[@id='select-status']/option[2]");
    public static final By bulk_statusRejected = By.xpath("//*[@id='select-status']/option[5]");
    public static final By bulk_statusApprovalExpired = By.xpath("//*[@id='select-status']/option[6]");
    public static final By debtorName_firstAppForm = By.xpath("//*[@id='dataTable']/tbody/tr[1]/td[4]/a");
    public static final By tableFormFilter_debtNameFirstAPP = By.xpath("//*[@id='dataTable']/tbody/tr/td[4]/a");
    public static final By txt_approvalFormDetail = By.xpath("/html/body/div[2]/form/div[1]/h1/span");
    public static final By bulk_rwDatafirstApp = By.xpath("//*[@id='channelingApprovalDataTable']/tbody/tr[1]/td[3]/a");
    public static final By txtTbl_nomorAplikasi = By.xpath("//*[@id='dataTable']/tbody/tr/td[3]");
    public static final By txtTbl_namaDebitur = By.xpath("//*[@id='dataTable']/tbody/tr/td[4]/a");
    public static final By txtTblForm_noDebitur = By.xpath("//*[@id='dataTable']/tbody/tr[1]/td[1]");
    public static final By txtTbl_partner = By.xpath("//*[@id='dataTable']/tbody/tr/td[5]");
    public static final By txtTbl_date = By.xpath("//*[@id='dataTable']/tbody/tr/td[6]");
    public static final By txtTbl_palfon = By.xpath("//*[@id='dataTable']/tbody/tr/td[7]");
    public static final By txtTbl_interestRate = By.xpath("//*[@id='dataTable']/tbody/tr/td[8]");
    public static final By txtTblBulk_debtorName = By.xpath("//*[@id='channelingApprovalDataTable']/tbody/tr[1]/td[3]/a");
    public static final By txtDetail_partner = By.xpath("/html/body/div[2]/form/div[2]/div/div[2]/div/div/div[2]");
    public static final By txtDetail_namaDebitur = By.xpath("/html/body/div[2]/form/div[2]/div/div[2]/div/div/div[4]");
    public static final By txtDetail_status = By.xpath("/html/body/div[2]/form/div[2]/div/div[2]/div/div/div[6]/div");
    public static final By txtDetail_nomorAplikasi = By.xpath("/html/body/div[2]/form/div[2]/div/div[2]/div/div/div[8]");
    public static final By txtDetail_tglPengajuan = By.xpath("/html/body/div[2]/form/div[2]/div/div[2]/div/div/div[10]");
    public static final By txtDetail_jenisDebitur = By.xpath("/html/body/div[2]/form/div[2]/div/div[2]/div/div/div[12]");
    public static final By txtDetail_plafon = By.xpath("/html/body/div[2]/form/div[2]/div/div[2]/div/div/div[14]");
    public static final By txtDetail_jangkaWaktu = By.xpath("/html/body/div[2]/form/div[2]/div/div[2]/div/div/div[16]");
    public static final By txtDetail_interestRate = By.xpath("/html/body/div[2]/form/div[2]/div/div[2]/div/div/div[18]");
    public static final By txtDetail_nik = By.xpath("/html/body/div[2]/form/div[2]/div/div[2]/div/div/div[20]");
    public static final By txtDetail_npwp = By.xpath("/html/body/div[2]/form/div[2]/div/div[2]/div/div/div[22]");
    public static final By txtDetail_phoneNumber = By.xpath("/html/body/div[2]/form/div[2]/div/div[2]/div/div/div[24]");
    public static final By txtDetail_remark = By.xpath("/html/body/div[2]/form/div[2]/div/div[2]/div/div/div[26]");
    public static final By txtDetail_suppDoc = By.xpath("/html/body/div[2]/form/div[2]/div/div[2]/div/div/div[28]");
    public static final By linkText_suppdoc = By.xpath("/html/body/div[2]/form/div[2]/div/div[2]/div/div/div[28]/a");
    public static final By list_source = By.xpath("//span[@id='select2-select-source-container']");
    public static final By list_sourceLOS = By.xpath("//*[starts-with(@id, 'select2-select-source-result-') and contains(@id,'-LOS')]");
    public static final By bulkList_sourceKopnus = By.xpath("//*[starts-with(@id, 'select2-select-source-result-') and contains(@id,'-KOPNUS')]");
    public static final By list_status = By.id("select2-select-status-container");
    public static final By list_statusApprovalExpired = By.xpath("//*[@id='select2-select-status-results']/li[6]");
    public static final By list_statusApprovalExpired2 = By.xpath("//*[starts-with(@id, 'select2-select-status-result-') and contains(@id,'-Approval Expired')]");
    public static final By list_statusApproved = By.xpath("//*[starts-with(@id, 'select2-select-status-result-') and contains(@id,'-Approved')]");
    public static final By list_statusWaitingForReview = By.xpath("//*[starts-with(@id, 'select2-select-status-result-') and contains(@id,'-Waiting for Review')]");
    public static final By list_statusRejected = By.xpath("//*[starts-with(@id, 'select2-select-status-result-') and contains(@id,'-Rejected')]");
    public static final By list_statusWaitingApproval = By.xpath("//*[starts-with(@id, 'select2-select-status-result-') and contains(@id,'-Waiting Approval')]");
    public static final By list_partner = By.xpath("//span[@id='select2-select-partner-container']");
    public static final By list_partnerKopnus = By.xpath("//*[starts-with(@id, 'select2-select-partner-result-') and contains(@id,'-KOPNUS')]");
    public static final By list_partnerAll = By.xpath("//*[@id='select2-select-partner-results']/li[1]");
    public static final By pageNumber = By.xpath("//*[@id='dataTable_paginate']");
    public static final By nextPage = By.xpath("//*[@id='dataTable_next']/a");
    public static final By txt_totalNominalPinjaman = By.xpath("//*[@id='totalNominalPinjamanWrapper']/span");
    public static final By previousPage = By.xpath("//*[@id='dataTable_previous']/a");
    public static final By specificPage = By.xpath("//*[@id='dataTable_paginate']/ul/li[4]/a");
    public static final By shortColumn_debtorName = By.xpath("//*[@id='dataTable']/thead/tr/th[4]");
    public static final By shortColumn_partner = By.xpath("//*[@id='dataTable']/thead/tr/th[5]");
    public static final By shortColumn_date = By.xpath("//*[@id='dataTable']/thead/tr/th[6]");
    public static final By shortLoanAmmount = By.xpath("//*[@id='dataTable']/thead/tr/th[7]");
    public static final By shortColumn_status = By.xpath("//*[@id='dataTable']/thead/tr/th[9]");
    public static final By radioButtonFirst = By.xpath("//*[@id='channelingApprovalDataTable']/tbody/tr[1]/td[1]/div/label");
    public static final By rwDatafirstApp = By.xpath("//*[@id='dataTable']/tbody/tr[1]/td[3]/a");
    public static final By bulkFilter_radioFirstData = By.xpath("//*[@id='channelingApprovalDataTable']/tbody/tr/td[1]/div/label");
    public static final By bulkFilter_rwDatafirst = By.xpath("//*[@id='channelingApprovalDataTable']/tbody/tr/td[3]/a");
    public static final By bulkFilter_rwDatafirst1 = By.xpath("//*[@id='channelingApprovalDataTable']/tbody/tr[1]/td[3]/a");
    public static final By pilih_recomendation = By.xpath("//*[@id='select2-dropdown-choice-container']");
    public static final By dropDwRecomendation = By.xpath("//*[@id='dropdown-choice']/option[2]");
    public static final By txtFieldRecomendation = By.xpath("//*[@id='recomendation-field']");
    public static final By txtDitolak = By.xpath("/html/body/div[2]/form/div[2]/div/div[3]/div[2]/div/div[4]/span");
    public static final String textRecomendation = "Recomended Automation";
    public static final By btnSubmitApp = By.xpath("//*[@id='btn-submit-recomendation']");
    public static final By btnSubmitApp1 = By.id("btn-submit-recomendation");
    public static final By valueApprove = By.xpath("//*[starts-with(@id, 'select2-dropdown-choice-result-') and contains(@id, '-1')]");
    public static final By valueNotApprove = By.xpath("//*[starts-with(@id, 'select2-dropdown-choice-result-') and contains(@id, '-0')]");
    public static final By list_valueReject = By.xpath("//*[starts-with(@id, 'select2-dropdown-choice-result-') and contains(@id, '--2')]");
    public static final By list_valueApprove = By.xpath("//*[starts-with(@id, 'select2-dropdown-choice-result-') and contains(@id, '-1')]");
    public static final By txtFieldNotes = By.xpath("//*[@id='notes-field']");
    //rea
    public static final By menuRealization = By.xpath("//li[4]/a/div");
    public static final By btnBulkApprove = By.linkText("Bulk Approve");
    public static final By rwDataFirstRea = By.xpath("//*[@id='channelingTable']/tr[1]/td[4]/a");
    public static final By btnApproveRea = By.xpath("//*[@id='approved']");
    public static final By btnYa = By.id("formSubmit");
    public static final By txtSearchRea = By.id("myInput");
    public static final By filterStatusRea = By.xpath("//*[@id='filterStatus']/option[2]");
    public static final By radiobtnReaAll = By.id("selectAll");
    public static final By btnSubmitApprove = By.id("btnSubmitRealization");

    private static WebDriver driver = FormApprovalRunner.driver;
    int iSeq = 1;
    public static int iRowPictName = 1; // row number scenario
    public static String nomor_aplikasi;

    String str_rndm;
    String nama_debitur, jenis_debitur, plafon, jangka_waktu, interest_rate, nik, npwp;
    static String notes = "";
    Actions builder;
    String[] arr;
    WebElement moveTo;
    boolean sts = true;
    int index = 0;

    static ReadTestData readTestData = new ReadTestData();
    static TakeSreenshot takeSreenshot = new TakeSreenshot();
    static ReadCSVFormApproval readCSVFormApproval = new ReadCSVFormApproval();

    static ConsumeData consumeData = new ConsumeData();

    // Login Maker
    public static void loginMaker(WebDriver conn) {
        driver = conn;
        readTestData.testData();
        if (readTestData.environment.equals(Const.e2e_environment)) {
            try {
                driver.get(Const.urlE2E);
                driver.manage().window().maximize();
            } catch (Exception ignored) {
            }
            try {
                click(driver, btnDetails);
                click(driver, btnUnsafe);
            } catch (Exception ignored) {
            }
            writeText(driver, userId, Const.e2e_userMaker);
            writeText(driver, pwd, Const.e2e_password);

        } else {
            try {
                driver.get(Const.urlUAT);
                driver.manage().window().maximize();
            } catch (Exception ignored) {
            }
            try {
                click(driver, btnDetails);
                click(driver, btnUnsafe);
            } catch (Exception ignored) {
            }
            writeText(driver, userId, Const.uat_userMaker);
            writeText(driver, pwd, Const.uat_password);
        }
        click(driver, btnLogin);
    }

    public void setApproval() throws IOException {
        arr = readCSVFormApproval.fileCSVApp();

    }

    public void verifyTampilanLosApprovalForm() {
        //1
        try {
            createTest(iRowPictName, extent_test_case, extent);

            click(driver, menuApproval);
            waitElementVisible(driver, debtorName_firstAppForm, Const.seconds);
            writeText(driver, txtSearch, readTestData.timeStamp);
            if (driver.getPageSource().contains(Const.noMatch)) {
                takeSreenshot.capture(driver);

            } else {
                waitElementVisible(driver, debtorName_firstAppForm, Const.seconds);
                verifyElementExist(driver, lbl_filter_source);
                verifyElementExist(driver, lbl_filter_status);
                verifyElementExist(driver, lbl_filter_partner);
                boolean kolomSearch = boolean_isDisplayed(driver, txtSearch);
                actualLogDoubleOrder(String.valueOf(kolomSearch), readTestData.timeStamp);
                verifyElementExist(driver, lbl_no);
                verifyElementExist(driver, lbl_source);
                verifyElementExist(driver, lbl_application_no);
                verifyElementExist(driver, lbl_debtor_name);
                verifyElementExist(driver, lbl_partner);
                verifyElementExist(driver, lbl_date);
                verifyElementExist(driver, lbl_loan_amount);
                verifyElementExist(driver, lbl_rate);
                verifyElementExist(driver, lbl_status);
                verifyElementExist(driver, lbl_reason);
                verifyElementExist(driver, lbl_hasil_rekomendasi);

                takeSreenshot.capture(driver);
                status_testCase(iRowPictName, true, notes);
                driver.findElement(txtSearch).clear();
            }
        } catch (Exception e) {
            takeSreenshot.capture(driver);
        }
    }

    public void verifySemuaDataListApprovalForm() throws InterruptedException {
        //2
        iRowPictName = iRowPictName + iSeq;
        createTest(iRowPictName, extent_test_case, extent);
        codeBlockText(readAllLineAppCSV());

        for (int i = 1; i <= arr.length / 10; i++) {
            if (arr.length - (14 + index + 1) > 0) {
                writeText(driver, txtSearch, arr[14 + index]);
                waitElementVisible(driver, tableFormFilter_debtNameFirstAPP, Const.seconds);
                if (boolean_isDisplayed(driver, txtTbl_nomorAplikasi)) {
                    nomor_aplikasi = getText(driver, txtTbl_nomorAplikasi);
                    String[] arrayExpected = {arr[10 + index]};
                    String[] arrayActual = {nomor_aplikasi};

                    for (int x = 0; x < arrayExpected.length; x++) {
                        Thread.sleep(Const.delay);
                        verifyValue(arrayExpected[x], arrayActual[x]);
                        takeSreenshot.capture(driver);
                    }
                } else {
                    capture.log(Status.FAIL, "No matching records found");
                    takeSreenshot.capture(driver);
                }
                index = index + 10;
                driver.findElement(txtSearch).clear();
            }
        }
        status_testCase(iRowPictName, true, notes);
        index = 0;
    }

    public void changeStatus() throws InterruptedException {
        // changes status
        changedRecomendationAsMaker(driver, 10);
        loginApprover(driver, "surya.simarmata", "Surya2022");
        changedApproveAsApprover(driver, 9);
        loginMaker(driver);
    }

    public void verifyFilterDropDown(List<String> list_case) {
        //3-5
        iRowPictName = iRowPictName + iSeq;
        createTest(iRowPictName, extent_test_case, extent);
        click(driver, menuApproval);
        // filter source
        waitElementVisible(driver, firstNumberTableForm, Const.seconds);
        click(driver, list_source);
        verifyElementExist(driver, list_source_all);
        verifyElementExist(driver, list_source_los);
        verifyElementExist(driver, list_source_nikel);
        takeSreenshot.capture(driver);
        status_testCase(iRowPictName, true, notes);

        for (int idx = 1; idx < list_case.size(); idx++) {
            try {
                if (idx == 1) {
                    iRowPictName = iRowPictName + iSeq;
                    createTest(iRowPictName, extent_test_case, extent);
                    driver.navigate().refresh();
                    waitElementVisible(driver, debtorName_firstAppForm, Const.seconds);
                    actualLogTrue(String.valueOf(boolean_isDisplayed(driver, debtorName_firstAppForm)));
                    takeSreenshot.capture(driver);
                    click(driver, list_source);
                    click(driver, list_source_los);
                    actualLogTrue(String.valueOf(boolean_isDisplayed(driver, debtorName_firstAppForm)));

                    takeSreenshot.capture(driver);
                    status_testCase(iRowPictName, true, notes);
                } else {
                    driver.navigate().refresh();
                    iRowPictName = iRowPictName + iSeq;
                    createTest(iRowPictName, extent_test_case, extent);
                    click(driver, list_source);
                    click(driver, list_source_nikel);
                    scrollIntoView(driver, By.xpath("//*[contains(text(), 'No data available in table')]"));
                    verifyElementExist(driver, noDataAvailable);

                    takeSreenshot.capture(driver);
                    status_testCase(iRowPictName, true, notes);
                }

            } catch (Exception e) {

                takeSreenshot.capture(driver);
                status_testCase(iRowPictName, false, notes);
            }
        }
    }

    public void verifyFilterDropdownStatus(List<String> list_case) throws InterruptedException {
        //6-12
        try {
            iRowPictName = iRowPictName + iSeq;
            createTest(iRowPictName, extent_test_case, extent);

            click(driver, menuApproval);
            waitElementVisible(driver, firstNumberTableForm, Const.seconds);
            //Filter Status
            click(driver, list_status);
            verifyElementExist(driver, By.xpath("//li[normalize-space()='All']"));
            verifyElementExist(driver, list_statusApproved);
            verifyElementExist(driver, list_statusWaitingForReview);
            takeSreenshot.capture(driver);
            Thread.sleep(Const.delay);
            WebElement moveToExpired = driver.findElement(list_statusApprovalExpired2);
            builder = new Actions(driver);
            builder.moveToElement(moveToExpired).perform();

            verifyElementExist(driver, list_statusWaitingApproval);
            verifyElementExist(driver, list_statusRejected);
            verifyElementExist(driver, list_statusApprovalExpired);
            takeSreenshot.capture(driver);
            status_testCase(iRowPictName, true, notes);
        } catch (Exception e) {

            takeSreenshot.capture(driver);
            status_testCase(iRowPictName, false, notes);
        }

        for (int iStatus = 1; iStatus <= list_case.size() - 1; iStatus++) {
            iRowPictName = iRowPictName + iSeq;
            createTest(iRowPictName, extent_test_case, extent);

            if (iStatus == 1) {
                driver.navigate().refresh();
                waitElementVisible(driver, debtorName_firstAppForm, Const.seconds);
                actualLogTrue(String.valueOf(boolean_isDisplayed(driver, debtorName_firstAppForm)));
//                moveTo = driver.findElement(By.xpath("//*[@id='select2-select-status-results']/li[" + iStatus + "]"));
//                builder = new Actions(driver);
//                builder.moveToElement(moveTo).perform();
//                verifyElementExist(driver, By.xpath("//*[@id='select2-select-status-results']/li[" + iStatus + "]"));
            } else {
                click(driver, list_status);
                if (iStatus == 2 || iStatus == 3 || iStatus == 4) {
//                    moveTo = driver.findElement(list_statusApprovalExpired);
//                    builder = new Actions(driver);
//                    builder.moveToElement(moveTo).perform();
                    waitElementVisible(driver, debtorName_firstAppForm, Const.seconds);
                    driver.findElement(By.xpath("//*[@id='select2-select-status-results']/li[" + iStatus + "]")).click();
                    actualLogTrue(String.valueOf(boolean_isDisplayed(driver, debtorName_firstAppForm)));
                } else if (iStatus == 5) {
                    driver.navigate().refresh();
                    click(driver, list_status);
                    Thread.sleep(Const.delay);
                    moveTo = driver.findElement(list_statusApprovalExpired2);
                    builder = new Actions(driver);
                    builder.moveToElement(moveTo).perform();

                    click(driver, list_statusRejected);
                    waitElementVisible(driver, debtorName_firstAppForm, Const.seconds);
                    actualLogTrue(String.valueOf(boolean_isDisplayed(driver, debtorName_firstAppForm)));
                } else {
                    driver.navigate().refresh();
                    click(driver, list_status);
                    Thread.sleep(Const.delay);
                    moveTo = driver.findElement(list_statusApprovalExpired2);
                    builder = new Actions(driver);
                    builder.moveToElement(moveTo).perform();

                    click(driver, list_statusApprovalExpired2);
                    waitElementVisible(driver, debtorName_firstAppForm, Const.seconds);
                    actualLogTrue(String.valueOf(boolean_isDisplayed(driver, debtorName_firstAppForm)));
                }
            }
            takeSreenshot.capture(driver);
            status_testCase(iRowPictName, true, notes);
        }
    }

    public void verifyFilterDropdwonPartner(List<String> list_case) {
        //13-15
        iRowPictName = iRowPictName + iSeq;
        createTest(iRowPictName, extent_test_case, extent);

        click(driver, menuApproval);
        waitElementVisible(driver, debtorName_firstAppForm, Const.seconds);
        //Filter Partner
        click(driver, list_partner);
        actualLogTrue("true");
        takeSreenshot.capture(driver);
        status_testCase(iRowPictName, true, notes);

        iRowPictName = iRowPictName + iSeq;
        createTest(iRowPictName, extent_test_case, extent);
        actualLogTrue(String.valueOf(boolean_isDisplayed(driver, list_partnerAll)));
        click(driver, list_partnerAll);
        waitElementVisible(driver, debtorName_firstAppForm, Const.seconds);
        takeSreenshot.capture(driver);
        status_testCase(iRowPictName, true, notes);

        iRowPictName = iRowPictName + iSeq;
        createTest(iRowPictName, extent_test_case, extent);
        click(driver, list_partner);
        scrollIntoView(driver, By.xpath("//*[starts-with(@id, 'select2-select-partner-result-') and contains(@id,'-" + readTestData.companyName + "')]"));
        verifyElementExist(driver, By.xpath("//*[starts-with(@id, 'select2-select-partner-result-') and contains(@id,'-" + readTestData.companyName + "')]"));
        driver.findElement(By.xpath("//*[starts-with(@id, 'select2-select-partner-result-') and contains(@id,'-" + readTestData.companyName + "')]")).click();
        waitElementVisible(driver, debtorName_firstAppForm, Const.seconds);
        takeSreenshot.capture(driver);
        status_testCase(iRowPictName, true, notes);
    }

    public void verifyFungsiSearch() throws InterruptedException {
        //16
        iRowPictName = iRowPictName + iSeq;
        createTest(iRowPictName, extent_test_case, extent);

        click(driver, menuApproval);
        Thread.sleep(Const.delay);
        waitElementVisible(driver, debtorName_firstAppForm, Const.seconds);
        writeText(driver, txtSearch, readTestData.timeStamp);

        takeSreenshot.capture(driver);
        logInfo("Search : " + readTestData.timeStamp);
        actualLogTrue(String.valueOf(boolean_isDisplayed(driver, txtSearch)));
        status_testCase(iRowPictName, true, notes);
    }

    public void verifyNextPage() throws InterruptedException {
        //17
        iRowPictName = iRowPictName + iSeq;
        createTest(iRowPictName, extent_test_case, extent);
        click(driver, menuApproval);
        waitElementVisible(driver, debtorName_firstAppForm, Const.seconds);
        Thread.sleep(Const.delay);
        moveToEnd(driver, nextPage);
        waitElementVisible(driver, nextPage, Const.seconds);
        actualLogTrue(String.valueOf(boolean_isDisplayed(driver, nextPage)));
        Thread.sleep(Const.delay);
        takeSreenshot.capture(driver);
        click(driver, nextPage);//*[@id="dataTable_next"]/a

        takeSreenshot.capture(driver);
        status_testCase(iRowPictName, true, notes);
    }

    public void verifyPrevPage() {
        //18
        iRowPictName = iRowPictName + iSeq;
        createTest(iRowPictName, extent_test_case, extent);

        moveToEnd(driver, nextPage);
        waitElementVisible(driver, nextPage, Const.seconds);
        takeSreenshot.capture(driver);
        actualLogTrue(String.valueOf(boolean_isDisplayed(driver, previousPage)));
        click(driver, previousPage);
        takeSreenshot.capture(driver);
        status_testCase(iRowPictName, true, notes);
    }

    public void verifySpecificPage() {
        //19
        iRowPictName = iRowPictName + iSeq;
        createTest(iRowPictName, extent_test_case, extent);
        takeSreenshot.capture(driver);
        actualLogTrue(String.valueOf(boolean_isDisplayed(driver, specificPage)));
        click(driver, specificPage);

        takeSreenshot.capture(driver);
        status_testCase(iRowPictName, true, notes);
    }

    public void verifySortDebtorName() {
        //20
        iRowPictName = iRowPictName + iSeq;
        createTest(iRowPictName, extent_test_case, extent);
        driver.navigate().refresh();
        String data;
        String strDataColl = null;
        String[] dataColl;

        click(driver, shortColumn_debtorName);
        try {
            for (int i = 1; i <= 10; i++) {
                data = getText(driver, By.xpath("//*[@id='dataTable']/tbody/tr[" + i + "]/td[4]/a"));
                strDataColl = strDataColl + "|" + data;
            }
        } catch (Exception ignored) {
        }
        //validasi sort ascending
        moveToEnd(driver, nextPage);
        takeSreenshot.capture(driver);
        codeBlockText(
                "Actual Descending Debtor Name: "
                        + strDataColl.replace("null", ""));
        actualLogTrue(String.valueOf(LibUtils.verifyAscending(strDataColl)));
        scrollIntoView(driver, title_approval_form);
        click(driver, shortColumn_debtorName);

        strDataColl = null;
        try {
            for (int i = 1; i <= 10; i++) {
                data = getText(driver, By.xpath("//*[@id='dataTable']/tbody/tr[" + i + "]/td[4]/a"));
                strDataColl = strDataColl + "|" + data;
            }
        } catch (Exception ignored) {
        }
        //validasi sort descending
        moveToEnd(driver, nextPage);
        takeSreenshot.capture(driver);
        codeBlockText(
                "Ascending Debtor Name: "
                        + strDataColl.replace("null", ""));
        actualLogTrue(String.valueOf(LibUtils.verifyDescending(strDataColl)));
        scrollIntoView(driver, title_approval_form);

        status_testCase(iRowPictName, true, "");
    }

    public void verifySortpartner() {
        //21
        iRowPictName = iRowPictName + iSeq;
        createTest(iRowPictName, extent_test_case, extent);
        driver.navigate().refresh();
        String data;
        String strDataColl = null;
        String[] dataColl;

        click(driver, shortColumn_partner);
        try {
            for (int i = 1; i <= 10; i++) {
                data = getText(driver, By.xpath("//*[@id='dataTable']/tbody/tr[" + i + "]/td[5]"));
                strDataColl = strDataColl + "|" + data;
            }
        } catch (Exception ignored) {
        }
        //validasi sort ascending
        moveToEnd(driver, nextPage);
        takeSreenshot.capture(driver);
        codeBlockText(
                "Actual Ascending partner: "
                        + strDataColl.replace("null", ""));
        actualLogTrue(String.valueOf(LibUtils.verifyAscending(strDataColl)));
        scrollIntoView(driver, title_approval_form);
        click(driver, shortColumn_partner);

        strDataColl = null;
        try {
            for (int i = 1; i <= 10; i++) {
                data = getText(driver, By.xpath("//*[@id='dataTable']/tbody/tr[" + i + "]/td[5]"));
                strDataColl = strDataColl + "|" + data;
            }
        } catch (Exception ignored) {
        }
        //validasi sort descending
        moveToEnd(driver, nextPage);
        takeSreenshot.capture(driver);
        codeBlockText(
                "Descending partner: "
                        + strDataColl.replace("null", ""));
        actualLogTrue(String.valueOf(LibUtils.verifyDescending(strDataColl)));
        scrollIntoView(driver, title_approval_form);

        status_testCase(iRowPictName, true, "");
    }

    public void verifySortDate() {
        //22
        iRowPictName = iRowPictName + iSeq;
        createTest(iRowPictName, extent_test_case, extent);
        driver.navigate().refresh();
        String data;
        String strDataColl = null;
        String[] dataColl;

        click(driver, shortColumn_date);
        try {
            for (int i = 1; i <= 10; i++) {
                data = getText(driver, By.xpath("//*[@id='dataTable']/tbody/tr[" + i + "]/td[6]"));
                strDataColl = strDataColl + "|" + data;
            }
        } catch (Exception ignored) {
        }
        //validasi sort ascending
        moveToEnd(driver, nextPage);
        takeSreenshot.capture(driver);
        codeBlockText(
                "Actual Ascending date: "
                        + strDataColl.replace("null", ""));
        actualLogTrue(String.valueOf(LibUtils.verifyAscending(strDataColl)));
        scrollIntoView(driver, title_approval_form);
        click(driver, shortColumn_date);

        strDataColl = null;
        try {
            for (int i = 1; i <= 10; i++) {
                data = getText(driver, By.xpath("//*[@id='dataTable']/tbody/tr[" + i + "]/td[6]"));
                strDataColl = strDataColl + "|" + data;
            }
        } catch (Exception ignored) {
        }
        //validasi sort descending
        moveToEnd(driver, nextPage);
        takeSreenshot.capture(driver);
        codeBlockText(
                "Descending date: "
                        + strDataColl.replace("null", ""));
        actualLogTrue(String.valueOf(LibUtils.verifyDescending(strDataColl)));
        scrollIntoView(driver, title_approval_form);

        status_testCase(iRowPictName, true, "");
    }

    public void verifySortLoanAmount() {
        //23
        iRowPictName = iRowPictName + iSeq;
        createTest(iRowPictName, extent_test_case, extent);
        driver.navigate().refresh();
        String data;
        String strDataColl = null;
        String[] dataColl;

        click(driver, shortLoanAmmount);
        try {
            for (int i = 1; i <= 10; i++) {
                data = getText(driver, By.xpath("//*[@id='dataTable']/tbody/tr[" + i + "]/td[7]"));
                strDataColl = strDataColl + "|" + data;
            }
        } catch (Exception ignored) {
        }
        //validasi sort ascending
        moveToEnd(driver, nextPage);
        takeSreenshot.capture(driver);
        codeBlockText(
                "Actual Ascending loan amount: "
                        + strDataColl.replace("null", ""));
        actualLogTrue(String.valueOf(LibUtils.verifyAscending(strDataColl)));
        scrollIntoView(driver, title_approval_form);
        click(driver, shortLoanAmmount);

        strDataColl = null;
        try {
            for (int i = 1; i <= 10; i++) {
                data = getText(driver, By.xpath("//*[@id='dataTable']/tbody/tr[" + i + "]/td[7]"));
                strDataColl = strDataColl + "|" + data;
            }
        } catch (Exception ignored) {
        }
        //validasi sort descending
        moveToEnd(driver, nextPage);
        takeSreenshot.capture(driver);
        codeBlockText(
                "Descending loan amount: "
                        + strDataColl.replace("null", ""));
        actualLogTrue(String.valueOf(LibUtils.verifyDescending(strDataColl)));
        scrollIntoView(driver, title_approval_form);

        status_testCase(iRowPictName, true, "");
    }

    public void verifySortStatus() {
        //24
        iRowPictName = iRowPictName + iSeq;
        createTest(iRowPictName, extent_test_case, extent);
        driver.navigate().refresh();
        String data;
        String strDataColl = null;
        String[] dataColl;

        click(driver, shortColumn_status);
        try {
            for (int i = 1; i <= 10; i++) {
                data = getText(driver, By.xpath("//*[@id='dataTable']/tbody/tr[" + i + "]/td[9]"));
                strDataColl = strDataColl + "|" + data;
            }
        } catch (Exception ignored) {
        }
        //validasi sort ascending
        moveToEnd(driver, nextPage);
        takeSreenshot.capture(driver);
        codeBlockText(
                "Actual Ascending status: "
                        + strDataColl.replace("null", ""));
        actualLogTrue(String.valueOf(LibUtils.verifyAscending(strDataColl)));
        scrollIntoView(driver, title_approval_form);
        click(driver, shortColumn_status);

        strDataColl = null;
        try {
            for (int i = 1; i <= 10; i++) {
                data = getText(driver, By.xpath("//*[@id='dataTable']/tbody/tr[" + i + "]/td[9]"));
                strDataColl = strDataColl + "|" + data;
            }
        } catch (Exception ignored) {
        }
        //validasi sort descending
        moveToEnd(driver, nextPage);
        takeSreenshot.capture(driver);
        codeBlockText(
                "Descending status: "
                        + strDataColl.replace("null", ""));
        actualLogTrue(String.valueOf(LibUtils.verifyDescending(strDataColl)));
        scrollIntoView(driver, title_approval_form);

        status_testCase(iRowPictName, true, "");
    }

    public void verifyDataSource() {
        //25
        iRowPictName = iRowPictName + iSeq;
        createTest(iRowPictName, extent_test_case, extent);

        click(driver, list_source);
        verifyElementExist(driver, list_sourceLOS);
        click(driver, list_sourceLOS);
        takeSreenshot.capture(driver);
        status_testCase(iRowPictName, true, notes);
    }

    public void verifyDataApprovalForm(List<String> list_case) throws InterruptedException {
        //26-31

        boolean sts = true;
        while (sts) {
            writeText(driver, txtSearch, arr[14 + index]);
            click(driver, bulk_statusForReview);
            if (boolean_isDisplayed(driver, txtTbl_nomorAplikasi)) {
                sts = false;
            } else {
                clearText(driver, txtSearch);
                index = index + 10;
            }
        }
        Thread.sleep(Const.delay);
        nomor_aplikasi = getText(driver, txtTbl_nomorAplikasi);
        nama_debitur = getText(driver, txtTbl_namaDebitur);
        String partner = getText(driver, txtTbl_partner);
        String date = getText(driver, txtTbl_date);
        plafon = "000000" + getText(driver, txtTbl_palfon).replace(".", "").replace("Rp", "").trim() + "00";
        interest_rate = "0" + getText(driver, txtTbl_interestRate).replace("%", "").replace(".", "");

        String[] arrayExpected = {arr[10 + index], arr[14 + index], readTestData.companyName, dateNow("dd/MM/yyyy"), arr[17 + index], arr[16 + index]};
        String[] arrayActual = {nomor_aplikasi, nama_debitur, partner, date.substring(0, 10), plafon, interest_rate};

        for (int x = 0; x < list_case.size(); x++) {
            iRowPictName = iRowPictName + iSeq;
            createTest(iRowPictName, extent_test_case, extent);

            takeSreenshot.capture(driver);
            status_testCase(iRowPictName, verifyValue(arrayExpected[x], arrayActual[x]), "");
        }
    }

    public void verifyDataApprovalFormDetail(List<String> list_case) throws InterruptedException {
        //32-44
        index = 0;

        click(driver, menuApproval);
        boolean sts = true;
        while (sts) {
            writeText(driver, txtSearch, arr[14 + index]);
            click(driver, bulk_statusForReview);
            waitElementVisible(driver, debtorName_firstAppForm, Const.seconds);
            if (boolean_isDisplayed(driver, debtorName_firstAppForm)) {
                sts = false;
            } else {
                clearText(driver, txtSearch);
                index = index + 10;
            }
        }
        click(driver, debtorName_firstAppForm);
        Thread.sleep(Const.delay);
        String partner = getText(driver, txtDetail_partner);
        nama_debitur = getText(driver, txtDetail_namaDebitur);
        String c_status = getText(driver, txtDetail_status);
        nomor_aplikasi = getText(driver, txtDetail_nomorAplikasi);
        String tglPengajuan = getText(driver, txtDetail_tglPengajuan);
        jenis_debitur = getText(driver, txtDetail_jenisDebitur).substring(0, 1);
        plafon = "000000" + getText(driver, txtDetail_plafon).replace(".", "").replace("Rp", "").trim() + "00";
        jangka_waktu = "0" + getText(driver, txtDetail_jangkaWaktu).replace("Bulan", "").trim();
        interest_rate = "0" + getText(driver, txtDetail_interestRate).replace("%", "").replace(".", "");
        nik = getText(driver, txtDetail_nik);
        npwp = getText(driver, txtDetail_npwp).replace(".", "").replace("-", "");
        String mobilePhoneNumber = getText(driver, txtDetail_phoneNumber);
        String remark = getText(driver, txtDetail_remark);

        String[] arrayExpected = {readTestData.companyName, arr[14 + index], "Waiting for review", arr[10 + index], tglPengajuan, arr[13 + index], arr[17 + index], arr[15 + index], arr[16 + index], arr[11 + index], arr[12 + index], arr[18 + index], arr[19 + index]};
        String[] arrayActual = {partner, nama_debitur, c_status, nomor_aplikasi, tglPengajuan, jenis_debitur, plafon, jangka_waktu, interest_rate, nik, npwp, mobilePhoneNumber, remark};


        for (int i = 0; i < list_case.size(); i++) {

            iRowPictName = iRowPictName + iSeq;
            createTest(iRowPictName, extent_test_case, extent);

            codeBlockText(ReadCSV.readAllLineAppCSV());
            takeSreenshot.capture(driver);
            status_testCase(iRowPictName, verifyValue(arrayExpected[i], arrayActual[i]), "");
        }
    }

    public void verifyDetailRemark(List<String> list_case) throws InterruptedException {
        //45-46
        click(driver, menuApproval);
        waitElementVisible(driver, debtorName_firstAppForm, Const.seconds);
        index = 90;
        boolean sts = true;
        while (sts) {
            writeText(driver, txtSearch, arr[14 + index]);
            click(driver, bulk_statusForReview);
            if (boolean_isDisplayed(driver, tableFormFilter_debtNameFirstAPP)) {
                sts = false;
            } else {
                clearText(driver, txtSearch);
                index = index + 10;
            }
        }

        click(driver, tableFormFilter_debtNameFirstAPP);
        Thread.sleep(Const.delay);
        String remark = getText(driver, txtDetail_remark);
        String suppdoc = getText(driver, txtDetail_suppDoc);

        String[] arrayExpected2 = {"", ""};
        String[] arrayActual2 = {remark, suppdoc};

        for (int x = 0; x < list_case.size(); x++) {

            iRowPictName = iRowPictName + iSeq;
            createTest(iRowPictName, extent_test_case, extent);
            takeSreenshot.capture(driver);
            if (x == 0) {
                status_testCase(iRowPictName, verifyValue(arrayExpected2[x], arrayActual2[x]), "");
            } else {
                status_testCase(iRowPictName, verifyValue(arrayExpected2[x], arrayActual2[x]), "");
            }
        }
    }

    public void klikBulkRecomendation(List<String> list_case) throws InterruptedException {
        //47-48
        click(driver, menuApproval);
        waitElementVisible(driver, debtorName_firstAppForm, Const.seconds);
        click(driver, btnBulkRecomend);
        waitElementVisible(driver, radioButtonFirst, Const.seconds);
        click(driver, list_source);
        scrollIntoView(driver, bulkList_sourceKopnus);
        click(driver, bulkList_sourceKopnus);
        click(driver, bulk_statusForReview);
        Thread.sleep(Const.delay);
        try {
            for (int x = 1; x <= list_case.size(); x++) {
                iRowPictName = iRowPictName + iSeq;
                createTest(iRowPictName, extent_test_case, extent);
                if (x == 1) {
                    int i = 1;
                    while (i <= 5) {
                        driver.findElement(By.xpath("//*[@id='channelingApprovalDataTable']/tbody/tr[" + i + "]/td[1]/div/label")).click();
                        i = i + 1;
                    }
                } else {
                    takeSreenshot.capture(driver);
                    int i = 1;
                    while (i <= 5) {
                        driver.findElement(By.xpath("//*[@id='channelingApprovalDataTable']/tbody/tr[" + i + "]/td[1]/div/label")).click();
                        i = i + 2;
                    }
                }
                actualLogTrue(String.valueOf(boolean_isDisplayed(driver, By.xpath("//*[@id='channelingApprovalDataTable']/tbody/tr[1]/td[1]/div/label"))));
                takeSreenshot.capture(driver);
                status_testCase(iRowPictName, true, notes);
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public void klikButtonBackCancel() throws InterruptedException {
        //49
        iRowPictName = iRowPictName + iSeq;
        createTest(iRowPictName, extent_test_case, extent);

        click(driver, btn_cancelSelection);
        waitElementVisible(driver, shortColumn_debtorName, Const.seconds);
        actualLogTrue(String.valueOf(boolean_isDisplayed(driver, shortColumn_debtorName)));
        Thread.sleep(3000);

        takeSreenshot.capture(driver);
        status_testCase(iRowPictName, true, notes);
    }

    public void klikBulkCheckAll() {
        //50
        iRowPictName = iRowPictName + iSeq;
        createTest(iRowPictName, extent_test_case, extent);

        click(driver, btnBulkRecomend);
        waitElementVisible(driver, radioButtonFirst, Const.seconds);
        click(driver, bulk_statusForReview);
        actualLogTrue(String.valueOf(boolean_isDisplayed(driver, radiobtnAppAll)));
        click(driver, radiobtnAppAll);

        takeSreenshot.capture(driver);
        status_testCase(iRowPictName, true, notes);
    }

    public void klikCheckPadaStatusApproved() throws InterruptedException {
        //51
        iRowPictName = iRowPictName + iSeq;
        createTest(iRowPictName, extent_test_case, extent);

        click(driver, bulk_statusApproved);
        Thread.sleep(Const.delay);
        if (driver.getPageSource().contains(Const.noMatch)) {

            LibUtils.capture.log(Status.FAIL, Const.noMatch);
            takeSreenshot.capture(driver);
            status_testCase(iRowPictName, false, notes);
        } else {

            takeSreenshot.capture(driver);
            actualLogTrue(String.valueOf(boolean_isDisplayed(driver, radioButtonFirst)));
            click(driver, radioButtonFirst);
            status_testCase(iRowPictName, true, notes);
        }

    }

    public void klikCheckPadaStatusRejected() throws InterruptedException {
        //52
        iRowPictName = iRowPictName + iSeq;
        createTest(iRowPictName, extent_test_case, extent);
        click(driver, bulk_statusRejected);
        Thread.sleep(Const.delay);
        if (driver.getPageSource().contains(Const.noMatch)) {
            LibUtils.capture.log(Status.FAIL, Const.noMatch);
            takeSreenshot.capture(driver);
            status_testCase(iRowPictName, false, notes);
        } else {

            takeSreenshot.capture(driver);
            actualLogTrue(String.valueOf(boolean_isDisplayed(driver, radioButtonFirst)));
            click(driver, radioButtonFirst);
            status_testCase(iRowPictName, true, notes);
        }
    }

    public void klikCheckPadaStatusWaitingApproval() throws InterruptedException {
        //53

        iRowPictName = iRowPictName + iSeq;
        createTest(iRowPictName, extent_test_case, extent);
        driver.navigate().refresh();
        click(driver, menuApproval);
        click(driver, btnBulkApprove);
        click(driver, list_status);
        Thread.sleep(Const.delay);
        LibUtils.scrollIntoView(driver, list_statusApprovalExpired2);
        Thread.sleep(Const.delay);
        click(driver, list_statusWaitingApproval);

        Thread.sleep(Const.delay);
        if (driver.getPageSource().contains(Const.noMatch)) {
            LibUtils.capture.log(Status.FAIL, Const.noMatch);
            status_testCase(iRowPictName, false, notes);
        } else {
            LibUtils.waitElementVisible(driver, radioButtonFirst, Const.seconds);
            actualLogTrue(String.valueOf(boolean_isDisplayed(driver, radioButtonFirst)));
            click(driver, radioButtonFirst);
            takeSreenshot.capture(driver);
            status_testCase(iRowPictName, true, notes);
        }

    }

    public void klikBeberapaWaitingForReviewKlikRecomendation() throws InterruptedException {
        //54
        String coll_noAplikasi = null;
        String namaDebRecomended;

        iRowPictName = iRowPictName + iSeq;
        createTest(iRowPictName, extent_test_case, extent);

        click(driver, menuApproval);
        click(driver, btnBulkRecomend);
        click(driver, bulk_statusForReview);
        click(driver, list_source);
        scrollIntoView(driver, bulkList_sourceKopnus);
        click(driver, bulkList_sourceKopnus);
        writeText(driver, txtSearch, readTestData.timeStamp);
        Thread.sleep(Const.delay);
        waitElementVisible(driver, radioButtonFirst, Const.seconds);
        for (int i = 1; i <= 2; i++) {
            driver.findElement(By.xpath("//*[@id='channelingApprovalDataTable']/tbody/tr[" + i + "]/td[1]/div/label")).click();
            namaDebRecomended = getText(driver, By.xpath("//*[@id='channelingApprovalDataTable']/tbody/tr[" + i + "]/td[3]/a"));
            coll_noAplikasi = coll_noAplikasi + "|" + namaDebRecomended;
        }
        actualLogTrue(String.valueOf(boolean_isDisplayed(driver, By.xpath("//*[@id='channelingApprovalDataTable']/tbody/tr[1]/td[1]/div/label"))));
        takeSreenshot.capture(driver);
        String[] str_loanAmount = coll_noAplikasi.split("\\|");

        Thread.sleep(Const.delay);
        Thread.sleep(Const.delay);
        click(driver, recomended);
        Thread.sleep(Const.delay);

        for (int i = 1; i < str_loanAmount.length; i++) {
            Thread.sleep(Const.delay);
            click(driver, menuApproval);
            waitElementVisible(driver, firstNumberTableForm, Const.seconds);
            writeText(driver, txtSearch, str_loanAmount[i]);
            click(driver, bulk_statusWaitingApproval);

            Thread.sleep(Const.delay);
            takeSreenshot.capture(driver);

            waitElementVisible(driver, tableFormFilter_debtNameFirstAPP, Const.seconds);
            verifyElementExist(driver, tableFormFilter_debtNameFirstAPP);
            click(driver, tableFormFilter_debtNameFirstAPP);
            Thread.sleep(3000);
            takeSreenshot.capture(driver);
        }
        scrollIntoView(driver, txtFieldNotes);
        Thread.sleep(Const.delay);
        status_testCase(iRowPictName, true, notes);

    }

    public void klikBeberapaWaitingForReviewKlikNotRecomendation() throws InterruptedException {
        //55 klik lebih dari satu and cek satu persatu
        String coll_noAplikasi = null;
        String namaDebRecomended;

        iRowPictName = iRowPictName + iSeq;
        createTest(iRowPictName, extent_test_case, extent);

        click(driver, menuApproval);
        Thread.sleep(5000);
        click(driver, btnBulkRecomend);
        Thread.sleep(5000);
        click(driver, bulk_statusForReview);
        click(driver, list_source);
        scrollIntoView(driver, bulkList_sourceKopnus);
        click(driver, bulkList_sourceKopnus);
        writeText(driver, txtSearch, readTestData.timeStamp);
        Thread.sleep(Const.delay);
        waitElementVisible(driver, radioButtonFirst, Const.seconds);
        for (int i = 1; i <= 2; i++) {
            driver.findElement(By.xpath("//*[@id='channelingApprovalDataTable']/tbody/tr[" + i + "]/td[1]/div/label")).click();
            namaDebRecomended = getText(driver, By.xpath("//*[@id='channelingApprovalDataTable']/tbody/tr[" + i + "]/td[3]/a"));
            coll_noAplikasi = coll_noAplikasi + "|" + namaDebRecomended;
        }

        takeSreenshot.capture(driver);
        String[] str_loanAmount = coll_noAplikasi.split("\\|");

        Thread.sleep(Const.delay);
        Thread.sleep(Const.delay);
        click(driver, notRecomended);
        Thread.sleep(Const.delay);

        for (int i = 1; i < str_loanAmount.length; i++) {
            Thread.sleep(Const.delay);
            click(driver, menuApproval);
            waitElementVisible(driver, firstNumberTableForm, Const.seconds);
            writeText(driver, txtSearch, str_loanAmount[i]);
            click(driver, bulk_statusWaitingApproval);
            Thread.sleep(Const.delay);

            takeSreenshot.capture(driver);
            waitElementVisible(driver, tableFormFilter_debtNameFirstAPP, Const.seconds);
            verifyElementExist(driver, tableFormFilter_debtNameFirstAPP);
            click(driver, tableFormFilter_debtNameFirstAPP);
            Thread.sleep(3000);

            takeSreenshot.capture(driver);
        }
        scrollIntoView(driver, txtFieldNotes);
        Thread.sleep(Const.delay);
        status_testCase(iRowPictName, true, notes);
    }

    public void klikDebtorNameWaitingForReview() {
        //56
        iRowPictName = iRowPictName + iSeq;
        createTest(iRowPictName, extent_test_case, extent);
        click(driver, menuApproval);
        waitElementVisible(driver, debtorName_firstAppForm, Const.seconds);
        click(driver, bulk_statusForReview);
        click(driver, list_partner);
        scrollIntoView(driver, list_partnerKopnus);
        click(driver, list_partnerKopnus);
        writeText(driver, txtSearch, readTestData.timeStamp);
        waitElementVisible(driver, debtorName_firstAppForm, Const.seconds);
        click(driver, debtorName_firstAppForm);
        waitElementVisible(driver, txt_approvalFormDetail, Const.seconds);
        scrollIntoView(driver, txtFieldRecomendation);

        takeSreenshot.capture(driver);
        actualLogTrue(String.valueOf(boolean_isDisplayed(driver, pilih_recomendation)));
        click(driver, pilih_recomendation);
        takeSreenshot.capture(driver);
        status_testCase(iRowPictName, true, notes);
    }

    public void debtorNameWaitingForReviewTidakIsiRekomenDanCatatan() throws InterruptedException {
        //57

        iRowPictName = iRowPictName + iSeq;
        createTest(iRowPictName, extent_test_case, extent);

        click(driver, menuApproval);
        waitElementVisible(driver, debtorName_firstAppForm, Const.seconds);
        click(driver, btnBulkRecomend);
        waitElementVisible(driver, radioButtonFirst, Const.seconds);
        click(driver, bulk_statusForReview);
        click(driver, list_source);
        scrollIntoView(driver, bulkList_sourceKopnus);
        click(driver, bulkList_sourceKopnus);
        writeText(driver, txtSearch, readTestData.timeStamp);
        waitElementVisible(driver, bulk_rwDatafirstApp, Const.seconds);
        click(driver, bulk_rwDatafirstApp);
        waitElementVisible(driver, btnSubmitApp, Const.seconds);
        Thread.sleep(Const.delay);
        click(driver, btnSubmitApp);

        Thread.sleep(6000);
        scrollIntoView(driver, txtFieldRecomendation);// hasrus nya ada error
        actualLogTrue(String.valueOf(boolean_isDisplayed(driver, txtFieldRecomendation)));
        takeSreenshot.capture(driver);
        status_testCase(iRowPictName, true, notes);
    }

    public void debtorNameWaitingForReviewIsiRekomenDanTidakISiCatatan() throws InterruptedException {
        //58

        iRowPictName = iRowPictName + iSeq;
        createTest(iRowPictName, extent_test_case, extent);

        click(driver, pilih_recomendation);
        click(driver, valueApprove);
        takeSreenshot.capture(driver);
        scrollIntoView(driver, btnSubmitApp);
        click(driver, btnSubmitApp);
        Thread.sleep(6000);
        scrollIntoView(driver, txtFieldRecomendation);
        String err = getText(driver, By.xpath("/html/body/div[2]/form/div[2]/div/div[3]/div/div[2]/div/span[2]/strong"));
        actualLogDoubleOrder(String.valueOf(boolean_isDisplayed(driver, txtFieldRecomendation)), err);
        takeSreenshot.capture(driver);
        status_testCase(iRowPictName, true, notes);
    }

    public void debtorNameWaitingForReviewTidakIsiRekomenDanISiCatatan() throws InterruptedException {//59

        iRowPictName = iRowPictName + iSeq;
        createTest(iRowPictName, extent_test_case, extent);
        driver.navigate().refresh();
        scrollIntoView(driver, txtFieldRecomendation);
        writeText(driver, txtFieldRecomendation, "Data Recommended");

        takeSreenshot.capture(driver);
        scrollIntoView(driver, btnSubmitApp);
        click(driver, btnSubmitApp);

        Thread.sleep(6000);
        scrollIntoView(driver, txtFieldRecomendation);//tidak ada error message-ok
        actualLogTrue(String.valueOf(boolean_isDisplayed(driver, txtFieldRecomendation)));
        takeSreenshot.capture(driver);
        status_testCase(iRowPictName, true, "cek error message");
    }

    public void waitingForReviewIsiRekomendasidanIsiCatatan5000Karakter() throws InterruptedException {
        //60

        iRowPictName = iRowPictName + iSeq;
        createTest(iRowPictName, extent_test_case, extent);

        driver.navigate().refresh();
        String debtorName = getText(driver, txtDetail_namaDebitur);
        scrollIntoView(driver, txtFieldRecomendation);
        click(driver, pilih_recomendation);
        click(driver, valueApprove);
        str_rndm = GenerateRandomDataApp.rndmText() + " 5000 karakter";
        writeText(driver, txtFieldRecomendation, str_rndm);

        takeSreenshot.capture(driver);
        Thread.sleep(3000);
        scrollIntoView(driver, btnSubmitApp);
        click(driver, btnSubmitApp);
        waitElementVisible(driver, txtSearch, Const.seconds);
        writeText(driver, txtSearch, debtorName);

        waitElementVisible(driver, tableFormFilter_debtNameFirstAPP, Const.seconds);
        takeSreenshot.capture(driver);
        click(driver, tableFormFilter_debtNameFirstAPP);
        waitElementVisible(driver, txt_approvalFormDetail, Const.seconds);
        takeSreenshot.capture(driver);
        moveToEnd(driver, txtDitolak);

        logInfo(debtorName);
        actualLogTrue(String.valueOf(boolean_isDisplayed(driver, txtDitolak)));
        takeSreenshot.capture(driver);

        Thread.sleep(Const.delay);
        status_testCase(iRowPictName, true, notes);
    }

    public void waitingForReviewIsiRekomendasidanIsiCatatan4999Karakter() throws InterruptedException {
        //61
        iRowPictName = iRowPictName + iSeq;
        createTest(iRowPictName, extent_test_case, extent);
        click(driver, menuApproval);
        waitElementVisible(driver, debtorName_firstAppForm, Const.seconds);
        click(driver, btnBulkRecomend);
        waitElementVisible(driver, radioButtonFirst, Const.seconds);
        click(driver, bulk_statusForReview);
        click(driver, list_source);
        scrollIntoView(driver, bulkList_sourceKopnus);
        click(driver, bulkList_sourceKopnus);
        writeText(driver, txtSearch, readTestData.timeStamp);
        Thread.sleep(Const.delay);
        click(driver, bulk_rwDatafirstApp);
        waitElementVisible(driver, btnSubmitApp, Const.seconds);
        String debtorName = getText(driver, txtDetail_namaDebitur);
        scrollIntoView(driver, txtFieldRecomendation);
        click(driver, pilih_recomendation);
        click(driver, valueNotApprove);//*[@id="select2-dropdown-choice-result-hvlh-0"]
        str_rndm = GenerateRandomDataApp.rndmText() + " 4999karakter";
        writeText(driver, txtFieldRecomendation, str_rndm);
        takeSreenshot.capture(driver);

        Thread.sleep(3000);
        scrollIntoView(driver, btnSubmitApp);
        click(driver, btnSubmitApp);
        waitElementVisible(driver, txtSearch, Const.seconds);
        writeText(driver, txtSearch, debtorName);
        waitElementVisible(driver, tableFormFilter_debtNameFirstAPP, Const.seconds);
        click(driver, tableFormFilter_debtNameFirstAPP);
        waitElementVisible(driver, txt_approvalFormDetail, Const.seconds);

        takeSreenshot.capture(driver);
        scrollIntoView(driver, txtDitolak);
        logInfo(debtorName);
        actualLogTrue(String.valueOf(boolean_isDisplayed(driver, txtDitolak)));
        takeSreenshot.capture(driver);

        Thread.sleep(Const.delay);
        status_testCase(iRowPictName, true, notes);
    }

    public void dataDetailDokumenPendukungUploadFileZip() throws IOException {
        //62
        iRowPictName = iRowPictName + iSeq;
        createTest(iRowPictName, extent_test_case, extent);

        String[] get_file_name_sample, get_file_name_new;
        String[] arr = readCSVFormApproval.fileCSVApp();
        consumeData.consumeSuppdocApproval(arr[10]);

        click(driver, menuApproval);
        writeText(driver, txtSearch, arr[10]);
        click(driver, debtorName_firstAppForm);
        click(driver, linkText_suppdoc);
        {
            UtilsFile.deleteInFolder(pthFolderSuppDocOld);//delete all file existing
            String filePathDownload = UtilsFile.getFileNameZip(pthFolderDownload, arr[10]);//get path file zip
            unzip(new File(filePathDownload), pthFolderSuppDocOld); //unzip old
            get_file_name_sample = UtilsFile.arrGetFileName(pthFolderSuppDocSample);//get all file name
            get_file_name_new = UtilsFile.arrGetFileName(pthFolderSuppDocOld);//get all file name
            UtilsFile.deleteInFolderContainsFileName(pthFolderDownload, arr[10]);
        }
        try {
            Assert.assertEquals(get_file_name_sample, get_file_name_new);
            assert get_file_name_sample != null;
            capture.log(Status.PASS, MarkupHelper.createUnorderedList(List.of("Expected file: true", get_file_name_sample)).getMarkup());
            assert get_file_name_new != null;
            capture.log(Status.PASS, MarkupHelper.createUnorderedList(List.of("Actual file: true", get_file_name_new)).getMarkup());
        } catch (AssertionError e) {
            capture.log(Status.FAIL, e.getLocalizedMessage());
        }

        takeSreenshot.capture(driver);
        status_testCase(iRowPictName, true, notes);
    }

    public void dataDetailDokumenPendukungReupload() throws IOException {
        //63
        iRowPictName = iRowPictName + iSeq;
        createTest(iRowPictName, extent_test_case, extent);

        String[] get_file_name_old, get_file_name_new;
        String[] arr = readCSVFormApproval.fileCSVApp();
        {
            UtilsFile.deleteInFolder(pthFolderSuppDocNew);//delete all existing file
            UtilsFile.copyRenameFile(pthFolderSuppDocSample + "bpkb.jpg", pthFolderSuppDocSample + "bpkb2.jpg");//copy and rename file
            UtilsFile.renameFile(pthFolderSuppDocSample + "bpkb.jpg", pthFolderSuppDocSample + "bpkbRename.jpg");//rename file
            ZipFolder.zipFolder(pthFolderSuppDocSample, "./dummy_suppDoc/" + "IDV_" + arr[10] + "_" + dateNow(glPattern) + ".zip");//reZip new content
            consumeData.consumeSuppdocApproval(arr[10]);//reconsume suppdocZip
        }
        click(driver, menuApproval);
        writeText(driver, txtSearch, arr[10]);
        click(driver, debtorName_firstAppForm);
        click(driver, linkText_suppdoc);//download
        {
            String filePathDownload = UtilsFile.getFileNameZip(pthFolderDownload, arr[10]);//get path file zip latest(1)
            unzip(new File(filePathDownload), pthFolderSuppDocNew); //unzip old
            get_file_name_old = UtilsFile.arrGetFileName(pthFolderSuppDocOld);//get all file name
            get_file_name_new = UtilsFile.arrGetFileName(pthFolderSuppDocNew);//get all file name
            UtilsFile.deleteFile(pthFolderSuppDocSample + "bpkbRename.jpg");
            UtilsFile.renameFile(pthFolderSuppDocSample + "bpkb2.jpg", pthFolderSuppDocSample + "bpkb.jpg");//rename file
            UtilsFile.deleteInFolderContainsFileName(pthFolderDownload, arr[10]);
        }

        try {
            Assert.assertEquals(get_file_name_old, get_file_name_new);
        } catch (AssertionError e) {
            assert get_file_name_old != null;
            capture.log(Status.PASS, MarkupHelper.createUnorderedList(List.of("Expected file: true", get_file_name_old)).getMarkup());
            assert get_file_name_new != null;
            capture.log(Status.PASS, MarkupHelper.createUnorderedList(List.of("Actual file: true", get_file_name_new)).getMarkup());
        }

        takeSreenshot.capture(driver);
        status_testCase(iRowPictName, true, notes);
    }

    public void checkDataStatusApprovalExpired() throws InterruptedException {
        //64
        iRowPictName = iRowPictName + iSeq;
        createTest(iRowPictName, extent_test_case, extent);

        click(driver, menuApproval);
        waitElementVisible(driver, debtorName_firstAppForm, Const.seconds);
        click(driver, btnBulkRecomend);
        waitElementVisible(driver, radioButtonFirst, Const.seconds);
        click(driver, bulk_statusApprovalExpired);
        Thread.sleep(Const.delay);
        if (driver.getPageSource().contains(Const.noMatch)) {
            LibUtils.capture.log(Status.FAIL, Const.noMatch);
            takeSreenshot.capture(driver);
            status_testCase(iRowPictName, false, notes);
        } else {
            click(driver, radioButtonFirst);
            actualLogTrue(String.valueOf(boolean_isDisplayed(driver, radioButtonFirst)));
            takeSreenshot.capture(driver);
            status_testCase(iRowPictName, true, notes);
        }
    }

    public void klikBulkRekomendasiCheck(List<String> list_case) throws IOException, InterruptedException {
        //65 -66
        iRowPictName = 64;
        for (int x = 1; x <= list_case.size(); x++) {
            iRowPictName = iRowPictName + iSeq;
            createTest(iRowPictName, extent_test_case, extent);

            UtilsFile utilsFile = new UtilsFile();
            utilsFile.dltDummyExist();
            //generate data //hit server
            DataCSV dataCSV = new DataCSV();
            dataCSV.dataDummy(Const.appFile, Const.reaFile, "2");

            ConsumeData connServer = new ConsumeData();
            connServer.serverAkses();

            click(driver, menuApproval);
            waitElementVisible(driver, debtorName_firstAppForm, Const.seconds);
            click(driver, btnBulkRecomend);
            waitElementVisible(driver, radioButtonFirst, Const.seconds);
            click(driver, radiobtnAppAll);
            takeSreenshot.capture(driver);

            scrollIntoView(driver, recomended);
            if (x == 1) {
                click(driver, recomended);
                Thread.sleep(5000);
                click(driver, menuApproval);
                readTestData.testData();
                writeText(driver, txtSearch, readTestData.timeStamp);
                waitElementVisible(driver, debtorName_firstAppForm, Const.seconds);
            } else {
                click(driver, notRecomended);
                Thread.sleep(5000);
                click(driver, menuApproval);
                readTestData.testData();
                writeText(driver, txtSearch, readTestData.timeStamp);
                waitElementVisible(driver, debtorName_firstAppForm, Const.seconds);
            }
            takeSreenshot.capture(driver);
            scrollIntoView(driver, By.xpath("//*[@id='dataTable']/tbody/tr[1]/td[11]"));

            takeSreenshot.capture(driver);
            status_testCase(iRowPictName, true, notes);
        }
    }

    public static void logout(WebDriver conn) throws InterruptedException {
        driver = conn;
        click(driver, btnProfile);
        Thread.sleep(3000);
        click(driver, btnLogout);
    }
}