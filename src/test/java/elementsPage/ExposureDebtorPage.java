package elementsPage;

import individu.LibUtils;
import io.github.bonigarcia.wdm.WebDriverManager;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.edge.EdgeDriver;
import org.testng.annotations.Test;
import runner.ValidateFileApprovalRunner;

import java.util.concurrent.TimeUnit;

import static elementsPage.ApprovalFormMaker.loginMaker;

public class ExposureDebtorPage extends BaseAction {

    public static final By menuExposureDebtor = By.xpath("//div[normalize-space()='Exposure Debtor']");
    public static final By titleExposureActive = By.xpath("//h5[normalize-space()='Exposure Active']");
    public static final By dropDownExposureActive = By.xpath("//span[@id='select2-exposureCode-container']");
    public static final By valueExposureIDV = By.xpath("//li[text()='EX0006 - Automation Chanelling IDV' or text()='EX0009 - Automation Chanelling IDV']");
//    public static final By valueExposureIDV = By.xpath("//*[starts-with(@id, 'EX') and contains(@id,' - Automation Chanelling IDV')]");
    public static final By btnUse = By.xpath("//button[normalize-space()='Use']");
    public static final By lblExposureLimit = By.xpath("//td[normalize-space()='Exposure Amount Limit']");

    public static void changeExposureActive(WebDriver driver) {

        click(driver, menuExposureDebtor);
        click(driver, dropDownExposureActive);
        LibUtils.scrollIntoView(driver, valueExposureIDV);
        click(driver, valueExposureIDV);
        click(driver,btnUse);

    }

}
