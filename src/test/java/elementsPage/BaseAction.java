package elementsPage;

import capture.TakeSreenshot;
import individu.LibUtils;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import runner.FormRealisasiRunner;

public class BaseAction {

    TakeSreenshot takeSreenshot = new TakeSreenshot();

    public static void click(WebDriver driver, By locator) {
        LibUtils.waitElementVisible(driver, locator, Const.seconds);
        driver.findElement(locator).click();
    }

    public static void writeText(WebDriver driver, By locator, String value) {
        LibUtils.waitElementVisible(driver, locator, Const.seconds);
        driver.findElement(locator).sendKeys(value);
    }

    public static String getText(WebDriver driver, By locator) {
        LibUtils.waitElementVisible(driver, locator, Const.seconds);
        return driver.findElement(locator).getText();
    }
    public static void clearText(WebDriver driver, By locator){
        LibUtils.waitElementVisible(driver, locator, Const.seconds);
        driver.findElement(locator).clear();
    }
}
