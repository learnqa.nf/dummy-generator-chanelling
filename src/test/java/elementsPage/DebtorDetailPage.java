package elementsPage;

import capture.ImageExcel;
import capture.TakeSreenshot;
import com.aventstack.extentreports.Status;
import com.aventstack.extentreports.markuputils.MarkupHelper;
import individu.LibUtils;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import randomValue.GenerateRandomDataApp;
import readFile.ReadCSVFormRealization;
import runner.FormRealisasiRunner;
import testData.ReadTestData;

import java.io.IOException;
import java.util.Arrays;
import java.util.List;

import static individu.LibUtils.*;
import static runner.FormRealisasiRunner.extent;
import static runner.FormRealisasiRunner.extent_test_case;

public class DebtorDetailPage extends BaseAction {

    public static final By menuRealization = By.xpath("//div[normalize-space()='Realization']");
    public static final By txt_Search = By.xpath("//*[@id='myInput']");
    public static final By tbl_realizationForm_filter_applicationNo = By.xpath("//*[@id='channelingTable']/tr/td[3]");
    public static final By tbl_realizationForm_debtorname = By.xpath("//*[@id='channelingTable']/tr[1]/td[4]/a");
    public static final By txt_RealizationDetail = By.xpath("/html/body/div[2]/form/div[1]/h1/span");

    public static final By popup_select_option = By.xpath("//*[@id='select2-rejectDescription-container']");
    public static final By popup_select_value_option1 = By.xpath("//*[starts-with(@id, 'select2-rejectDescription-result-') and contains(@id,'-Dokumen pendukung tidak sesuai')]");
    public static final By popup_select_value_option2 = By.xpath("//*[starts-with(@id, 'select2-rejectDescription-result-') and contains(@id,'-Pengajuan ditolak')]");

    public static final By btn_approve_detail_formRealisasi3 = By.xpath("//*[@id='buttonApprove']");
    public static final By popup_ya = By.xpath("//*[@id='formSubmit']");
    public static final By pop_tidak = By.xpath("//*[@id='modal-cancel']");
    public static final By btn_reject_detail_formRealisasi = By.xpath("//*[@id='buttonReject']");
    public static final By popup_submit = By.xpath("(//button[@id='formSubmit'])[2]");
    public static final By popup_cancel = By.xpath("//button[@id='modal-cancel-reject']");

    public static final By popup_reject_reason = By.xpath("//input[@id='rejectReason']");
    public static final By icon_notif = By.xpath("//*[@id='notificationIcon']/img");
    public static final By seeAllNotif = By.xpath("//span[@class='See-All-Notification']");
    public static final By title_notif_page = By.xpath("//h5[normalize-space()=Notifications']");
    public static final By messageNotif = By.xpath("//*[@id='tBody-55-89']/tr[2]/td[3]/text()");

    public static final By list_status = By.xpath("//*[@id='select2-filterStatus-container']");
    public static final By list_statusPending = By.xpath("//*[starts-with(@id, 'select2-filterStatus-result-') and contains(@id,'-Pending')]");
    public static final By list_statusWaitingApproval = By.xpath("//*[starts-with(@id, 'select2-filterStatus-result-') and contains(@id,'-Waiting Approval')]");
    public static final By list_statusApproved = By.xpath("//*[starts-with(@id, 'select2-filterStatus-result-') and contains(@id,'-Approved')]");
    public static final By list_statusRejected = By.xpath("//*[starts-with(@id, 'select2-filterStatus-result-') and contains(@id,'-Rejected')]");
    public static final By list_statusApprovalExpired = By.xpath("//*[starts-with(@id, 'select2-filterStatus-result-') and contains(@id,'-Approval Expired')]");
    public static final By detail_statusApproved = By.xpath("//a[normalize-space()='Approved']");
    public static final By detail_statusRejected = By.xpath("//a[normalize-space()='Rejected']");
    public static final By detail_statusPending = By.xpath("//a[normalize-space()='Pending']");
    public static final By detail_statusApprovalExpired = By.xpath("//a[normalize-space()='Approval Expired']");
    public static final By detail_statusExpired = By.xpath("//a[normalize-space()='Expired']");
    public static final By field_income_source = By.xpath("/html/body/div[2]/form/div[2]/div/div[2]/div/div[2]/div/table/tbody/tr[17]/td[1]");

    public int iSeq = 1;
    private int iShoot = 2201; // row number picture
    public int iRowCapture = 55; // jarak antar picture
    private int iRowPictName = 33; // row number scenario

    TakeSreenshot takeSreenshot = new TakeSreenshot();
    ReadCSVFormRealization readCSVFormRealization = new ReadCSVFormRealization();
    ReadTestData readTestData = new ReadTestData();
    WebDriver driver = FormRealisasiRunner.driver;
    String[] arr;

    public void verifyDataRalizationFormDetail(List<String> dataDetailRealization) throws IOException {
        arr = readCSVFormRealization.fileCSVRea();

        String appNo = driver.findElement(tbl_realizationForm_filter_applicationNo).getText();
        driver.findElement(tbl_realizationForm_debtorname).click();
//        LibUtils.waitElementVisible(driver, txt_RealizationDetail, Const.seconds);

        String data_detail;
        String collDataDetail = "Waiting Approval";
        for (int i = 0; i < 3; i++) {
            if (i == 0) {
                for (int indexNol = 0; indexNol < 21; indexNol++) {
                    int j = indexNol + 1;
                    data_detail = driver.findElement(By.xpath("/html/body/div[2]/form/div[2]/div/div[1]/div/div[2]/div/table/tbody/tr[" + j + "]/td[2]")).getText();
                    collDataDetail = collDataDetail + "|" + data_detail;
                }
            } else if (i == 1) {
                for (int indexSatu = 0; indexSatu < 18; indexSatu++) {
                    int j = indexSatu + 1;
                    data_detail = driver.findElement(By.xpath("/html/body/div[2]/form/div[2]/div/div[2]/div/div[1]/div/table/tbody/tr[" + j + "]/td[2]")).getText();
                    collDataDetail = collDataDetail + "|" + data_detail;
                }
            } else {
                for (int indexDua = 0; indexDua < 17; indexDua++) {
                    int j = indexDua + 1;
                    data_detail = driver.findElement(By.xpath("/html/body/div[2]/form/div[2]/div/div[2]/div/div[2]/div/table/tbody/tr[" + j + "]/td[2]")).getText();
                    collDataDetail = collDataDetail + "|" + data_detail;
                }
            }
        }
        /**
         * search data array csv realisasi
         */
        String[] arrayActual = new String[0];
        String[] arrayExpected = new String[0];


        for (int i = 0; i < arr.length; i++) {
            if (arr[i].equals(appNo)) {
                System.out.println(i);
                System.out.println(arr[55 + i - 55]);

                arrayExpected = new String[]{"Waiting Approval", arr[55 + i - 55], arr[56 + i - 55], arr[57 + i - 55], arr[58 + i - 55], arr[59 + i - 55], arr[60 + i - 55], arr[61],
                        arr[62 + i - 55], arr[63 + i - 55], arr[65 + i - 55], arr[66 + i - 55], arr[64 + i - 55], arr[65 + i - 55], arr[67 + i - 55], arr[68 + i - 55], arr[69 + i - 55], arr[70 + i - 55], arr[71],
                        arr[72 + i - 55], arr[73 + i - 55], arr[74 + i - 55], arr[89 + i - 55], arr[90 + i - 55], arr[91 + i - 55], arr[92 + i - 55], arr[93 + i - 55], arr[94 + i - 55], arr[95 + i - 55], arr[96],
                        arr[97 + i - 55], arr[98 + i - 55], arr[99 + i - 55], arr[100 + i - 55], arr[101 + i - 55], arr[102 + i - 55], arr[103 + i - 55], arr[104 + i - 55], arr[105 + i - 55], arr[106],
                        arr[75 + i - 55], arr[76 + i - 55], arr[77 + i - 55], arr[78 + i - 55], arr[109 + i - 55], arr[79 + i - 55], arr[80 + i - 55], arr[81 + i - 55], arr[82 + i - 55], arr[83 + i - 55], arr[84],
                        arr[85 + i - 55], arr[86 + i - 55], arr[87 + i - 55], arr[88 + i - 55], arr[107 + i - 55], arr[108]};
            }
        }
        String[] arrDataActual = collDataDetail.split("\\|");

        arrayActual = new String[]{
                arrDataActual[0], arrDataActual[1], arrDataActual[2], arrDataActual[3], arrDataActual[4], arrDataActual[5],
                arrDataActual[6], arrDataActual[7], arrDataActual[8], arrDataActual[9], arrDataActual[10], arrDataActual[11],
                arrDataActual[12], arrDataActual[13], arrDataActual[14], arrDataActual[15], arrDataActual[16], arrDataActual[17],
                arrDataActual[18], arrDataActual[19], arrDataActual[20], arrDataActual[21], arrDataActual[22], arrDataActual[23],
                arrDataActual[24], arrDataActual[25], arrDataActual[26], arrDataActual[27], arrDataActual[28], arrDataActual[29],
                arrDataActual[30], arrDataActual[31], arrDataActual[32], arrDataActual[33], arrDataActual[34], arrDataActual[35],
                arrDataActual[36], arrDataActual[37], arrDataActual[38], arrDataActual[39], arrDataActual[40], arrDataActual[41],
                arrDataActual[42], arrDataActual[43], arrDataActual[44], arrDataActual[45], arrDataActual[46], arrDataActual[47],
                arrDataActual[48], arrDataActual[49], arrDataActual[50], arrDataActual[51], arrDataActual[52], arrDataActual[53],
                arrDataActual[54], arrDataActual[55], arrDataActual[56]};

        //insert to excel scenario
        iRowPictName = 30;
        for (int i = 0; i < dataDetailRealization.size(); i++) {

            createTest(iRowPictName, extent_test_case, extent);
            LibUtils.capture.log(Status.PASS, MarkupHelper.createUnorderedList(Arrays.asList("Expected: " + arrayExpected[i], "Actual: " + arrDataActual[i])).getMarkup());

            if (i == 16) {
                LibUtils.scrollIntoView(driver, field_income_source);
            } else if (i == 21) {
                LibUtils.scrollIntoView(driver, txt_RealizationDetail);
            } else if (i == 39) {
                LibUtils.scrollIntoView(driver, field_income_source);
            }

            takeSreenshot.capture(driver);

            status_testCase(iRowPictName, verifyValue(arrayExpected[i], arrDataActual[i]), "Expected: " + arrayExpected[i] + " ====================> Actual: " + arrDataActual[i]);
            iRowPictName = iRowPictName + iSeq;
        }
    }

    public void klikDebtorNameVerifyDataWaitingApproval() {
        iRowPictName = 97;

        createTest(iRowPictName, extent_test_case, extent);
        readTestData.testData();

        click(driver, menuRealization);
        driver.findElement(list_status).click();
        LibUtils.waitElementVisible(driver, list_statusWaitingApproval, Const.seconds);
        driver.findElement(list_statusWaitingApproval).click();
        driver.findElement(txt_Search).sendKeys(readTestData.timeStamp);
        driver.findElement(tbl_realizationForm_debtorname).click();
        LibUtils.actualLogTrue(String.valueOf(LibUtils.boolean_isDisplayed(driver, txt_RealizationDetail)));
        takeSreenshot.capture(driver);
        status_testCase(iRowPictName, true, "");

    }


    public void klikDebtorNameVerifyDataApprovalExpired() throws InterruptedException {

        iRowPictName = 115;
        createTest(iRowPictName, extent_test_case, extent);

        click(driver, menuRealization);
        readTestData.testData();
        writeText(driver, txt_Search, readTestData.timeStamp);
        click(driver, list_status);
        LibUtils.scrollIntoView(driver, list_statusApprovalExpired);
        LibUtils.waitElementVisible(driver, list_statusApprovalExpired, Const.seconds);
        click(driver, list_statusApprovalExpired);
        Thread.sleep(Const.delay);
        if (driver.getPageSource().contains(Const.noMatch)) {
            LibUtils.capture.log(Status.FAIL, "Data not exist");
            takeSreenshot.capture(driver);
            status_testCase(iRowPictName, false, "Data not exist");
        } else {
            try {

                click(driver, tbl_realizationForm_debtorname);
                LibUtils.waitElementVisible(driver, detail_statusExpired, Const.seconds);

                LibUtils.verifyElementExist(driver, detail_statusExpired);
                takeSreenshot.capture(driver);
                LibUtils.scrollIntoView(driver, field_income_source);

                takeSreenshot.capture(driver);
                status_testCase(iRowPictName, true, "");
            } catch (Exception ee) {

                takeSreenshot.capture(driver);
                LibUtils.capture.log(Status.FAIL, ee);
                status_testCase(iRowPictName, false, ee.getMessage());
            }
        }
    }

    public void klikDebtorNameVerifyDataApproved() throws InterruptedException {
        readTestData.testData();
        iRowPictName = 98;
        createTest(iRowPictName, extent_test_case, extent);

        click(driver, menuRealization);
        driver.findElement(list_status).click();
        driver.findElement(list_statusWaitingApproval).click();
        driver.findElement(txt_Search).sendKeys(readTestData.timeStamp);
        driver.findElement(tbl_realizationForm_debtorname).click();
        String noApplication = driver.findElement(By.xpath("/html/body/div[2]/form/div[2]/div/div[1]/div/div[2]/div/table/tbody/tr[1]/td[2]")).getText();
        driver.findElement(btn_approve_detail_formRealisasi3).click();
        driver.findElement(popup_ya).click();
        Thread.sleep(2000);
        driver.findElement(txt_Search).sendKeys(noApplication);

        Thread.sleep(Const.delay);
        if (driver.getPageSource().contains(Const.noMatch)) {

            takeSreenshot.capture(driver);
            status_testCase(iRowPictName, false, "Data not exist");
        } else {
            driver.findElement(tbl_realizationForm_debtorname).click();
            Thread.sleep(Const.delay);
            LibUtils.actualLogTrue(String.valueOf(LibUtils.boolean_isDisplayed(driver, txt_RealizationDetail)));
            takeSreenshot.capture(driver);
            LibUtils.scrollIntoView(driver, field_income_source);

            takeSreenshot.capture(driver);
            status_testCase(iRowPictName, true, "");
        }
    }

    public void klikDebtorNameVerifyDataRejected() throws InterruptedException {

        iRowPictName = 99;
        createTest(iRowPictName, extent_test_case, extent);

        click(driver, menuRealization);
        driver.findElement(list_status).click();
        driver.findElement(list_statusWaitingApproval).click();
        driver.findElement(txt_Search).sendKeys(readTestData.timeStamp);
        driver.findElement(tbl_realizationForm_debtorname).click();
        String noApplication = driver.findElement(By.xpath("/html/body/div[2]/form/div[2]/div/div[1]/div/div[2]/div/table/tbody/tr[1]/td[2]")).getText();
        driver.findElement(btn_reject_detail_formRealisasi).click();
        driver.findElement(popup_select_option).click();
        driver.findElement(popup_select_value_option1).click();
        driver.findElement(popup_reject_reason).sendKeys("data not valid");
        driver.findElement(popup_submit).click();
        Thread.sleep(2000);
        driver.findElement(txt_Search).sendKeys(noApplication);
        driver.findElement(list_status).click();
        LibUtils.scrollIntoView(driver, list_statusApprovalExpired);
        driver.findElement(list_statusRejected).click();

        Thread.sleep(Const.delay);
        if (driver.getPageSource().contains(Const.noMatch)) {

            takeSreenshot.capture(driver);
            status_testCase(iRowPictName, false, "Data not exist");
        } else {
            driver.findElement(tbl_realizationForm_debtorname).click();
            Thread.sleep(Const.delay);
            LibUtils.actualLogTrue(String.valueOf(LibUtils.boolean_isDisplayed(driver, txt_RealizationDetail)));
            takeSreenshot.capture(driver);
            LibUtils.scrollIntoView(driver, field_income_source);

            takeSreenshot.capture(driver);
            status_testCase(iRowPictName, true, "");
        }
    }

    public void klikDebtorNameVerifyDataPending() throws InterruptedException {

        iRowPictName = 122;
        createTest(iRowPictName, extent_test_case, extent);

        click(driver, menuRealization);
        driver.findElement(list_status).click();
        driver.findElement(list_statusPending).click();
        driver.findElement(txt_Search).sendKeys(readTestData.timeStamp);
        Thread.sleep(Const.delay);
        if (driver.getPageSource().contains(Const.noMatch)) {
            LibUtils.capture.log(Status.SKIP, "Data not exist");
            takeSreenshot.capture(driver);
            status_testCase(iRowPictName, false, "Data not exist");
        } else {
            try {

                driver.findElement(tbl_realizationForm_debtorname).click();
                LibUtils.waitElementVisible(driver, detail_statusPending, Const.seconds);

                LibUtils.verifyElementExist(driver, detail_statusPending);
                takeSreenshot.capture(driver);
                LibUtils.scrollIntoView(driver, field_income_source);


                takeSreenshot.capture(driver);
                status_testCase(iRowPictName, false, "");
            } catch (Exception ee) {
                LibUtils.capture.log(Status.FAIL, ee);
                takeSreenshot.capture(driver);
                status_testCase(iRowPictName, false, ee.getMessage());
            }
        }
    }

    public void klikDebtorNameApproveVerifyPopUp() {
        iRowPictName = 100;
        createTest(iRowPictName, extent_test_case, extent);

        click(driver, menuRealization);
        driver.findElement(list_status).click();
        LibUtils.waitElementVisible(driver, list_statusWaitingApproval, Const.seconds);
        driver.findElement(list_statusWaitingApproval).click();
        readTestData.testData();
        driver.findElement(txt_Search).sendKeys(readTestData.timeStamp);
        LibUtils.waitElementVisible(driver, tbl_realizationForm_debtorname, Const.seconds);

        driver.findElement(tbl_realizationForm_debtorname).click();
        LibUtils.waitElementVisible(driver, btn_approve_detail_formRealisasi3, Const.seconds);
        driver.findElement(btn_approve_detail_formRealisasi3).click();
        LibUtils.waitElementVisible(driver, pop_tidak, Const.seconds);

        LibUtils.actualLogTrue(String.valueOf(LibUtils.boolean_isDisplayed(driver, pop_tidak)));
        takeSreenshot.capture(driver);
        status_testCase(iRowPictName, true, "");

        driver.findElement(pop_tidak).click();
    }

    public void klikDebtorNameApproveVerifyPopUpTidak() {
        iRowPictName = 101;
        createTest(iRowPictName, extent_test_case, extent);

        click(driver, menuRealization);
        click(driver, list_status);
        LibUtils.waitElementVisible(driver, list_statusWaitingApproval, Const.seconds);
        click(driver, list_statusWaitingApproval);
        readTestData.testData();
        writeText(driver, txt_Search, readTestData.timeStamp);
        LibUtils.waitElementVisible(driver, tbl_realizationForm_debtorname, Const.seconds);

        click(driver, tbl_realizationForm_debtorname);
        LibUtils.waitElementVisible(driver, btn_approve_detail_formRealisasi3, Const.seconds);
        click(driver, btn_approve_detail_formRealisasi3);
        LibUtils.waitElementVisible(driver, pop_tidak, Const.seconds);
        takeSreenshot.capture(driver);
        LibUtils.actualLogTrue(String.valueOf(LibUtils.boolean_isDisplayed(driver, pop_tidak)));
        click(driver, pop_tidak);

        takeSreenshot.capture(driver);
        status_testCase(iRowPictName, true, "");

    }

    public void klikDebtorNameApproveVerifyPopUpYa() {
        iRowPictName = 102;
        createTest(iRowPictName, extent_test_case, extent);

        click(driver, menuRealization);
        click(driver, list_status);
        LibUtils.waitElementVisible(driver, list_statusWaitingApproval, Const.seconds);
        click(driver, list_statusWaitingApproval);
        readTestData.testData();
        writeText(driver, txt_Search, readTestData.timeStamp);
        LibUtils.waitElementVisible(driver, tbl_realizationForm_debtorname, Const.seconds);
        takeSreenshot.capture(driver);

        click(driver, tbl_realizationForm_debtorname);
        LibUtils.waitElementVisible(driver, btn_approve_detail_formRealisasi3, Const.seconds);
        click(driver, btn_approve_detail_formRealisasi3);
        LibUtils.waitElementVisible(driver, popup_ya, Const.seconds);
        takeSreenshot.capture(driver);
        LibUtils.actualLogTrue(String.valueOf(LibUtils.boolean_isDisplayed(driver, popup_ya)));
        click(driver, popup_ya);
        LibUtils.waitElementVisible(driver, tbl_realizationForm_debtorname, Const.seconds);

        takeSreenshot.capture(driver);
        status_testCase(iRowPictName, true, "");

    }

    public void klikDebtorNameRejectVerifyPopUp() throws InterruptedException {
        iRowPictName = 103;
        createTest(iRowPictName, extent_test_case, extent);

        click(driver, menuRealization);
        click(driver, list_status);
        LibUtils.waitElementVisible(driver, list_statusWaitingApproval, Const.seconds);
        click(driver, list_statusWaitingApproval);
        readTestData.testData();
        writeText(driver, txt_Search, readTestData.timeStamp);
        LibUtils.waitElementVisible(driver, tbl_realizationForm_debtorname, Const.seconds);

        click(driver, tbl_realizationForm_debtorname);
        LibUtils.waitElementVisible(driver, btn_reject_detail_formRealisasi, Const.seconds);
        click(driver, btn_reject_detail_formRealisasi);
        LibUtils.waitElementVisible(driver, popup_select_option, Const.seconds);
        takeSreenshot.capture(driver);

        driver.findElement(popup_select_option).click();
        Thread.sleep(3000);
        LibUtils.actualLogTrue(String.valueOf(LibUtils.boolean_isDisplayed(driver, popup_cancel)));
        takeSreenshot.capture(driver);
        status_testCase(iRowPictName, true, "");

        driver.findElement(popup_cancel).click();
    }

    public void klikDebtorNameReject100Char() throws InterruptedException {
        iRowPictName = 104;
        createTest(iRowPictName, extent_test_case, extent);

        click(driver, menuRealization);
        click(driver, list_status);
        LibUtils.waitElementVisible(driver, list_statusWaitingApproval, Const.seconds);
        click(driver, list_statusWaitingApproval);
        readTestData.testData();
        writeText(driver, txt_Search, readTestData.timeStamp);
        LibUtils.waitElementVisible(driver, tbl_realizationForm_debtorname, Const.seconds);
        takeSreenshot.capture(driver);
        String debtorname = getText(driver, tbl_realizationForm_debtorname);
        click(driver, tbl_realizationForm_debtorname);
        LibUtils.waitElementVisible(driver, btn_reject_detail_formRealisasi, Const.seconds);
        click(driver, btn_reject_detail_formRealisasi);
        takeSreenshot.capture(driver);

        click(driver, popup_select_option);
        click(driver, popup_select_value_option1);
        String rdm_str = "100 character ASFVDFDF SFSDFSFGGF DRTYUTYIUUIPYUUF TTSDDFSDFSGDFGDFGE TETWTEFGHKKFGNC VFSDFDHGKJHKKP";
        writeText(driver, popup_reject_reason, rdm_str);
        takeSreenshot.capture(driver);

        try {
            click(driver, popup_submit);
        } catch (Exception ignored) {
        }

        LibUtils.waitElementVisible(driver, tbl_realizationForm_debtorname, Const.seconds);
        takeSreenshot.capture(driver);
        writeText(driver, txt_Search, debtorname);
        LibUtils.capture.log(Status.INFO, MarkupHelper.createCodeBlock(rdm_str));
        LibUtils.actualLogTrue(debtorname);
        takeSreenshot.capture(driver);
        status_testCase(iRowPictName, true, "");
    }

    public void klikDebtorNameReject99Char() throws InterruptedException {
        iRowPictName = 105;
        createTest(iRowPictName, extent_test_case, extent);

        click(driver, menuRealization);
        click(driver, list_status);
        LibUtils.waitElementVisible(driver, list_statusWaitingApproval, Const.seconds);
        click(driver, list_statusWaitingApproval);
        readTestData.testData();
        writeText(driver, txt_Search, readTestData.timeStamp);
        LibUtils.waitElementVisible(driver, tbl_realizationForm_debtorname, Const.seconds);
        String debtorname = driver.findElement(tbl_realizationForm_debtorname).getText();

        click(driver, tbl_realizationForm_debtorname);
        LibUtils.waitElementVisible(driver, btn_reject_detail_formRealisasi, Const.seconds);
        click(driver, btn_reject_detail_formRealisasi);
        takeSreenshot.capture(driver);

        click(driver, popup_select_option);
        click(driver, popup_select_value_option1);
        String rdm_str = "99 character ASFVDFDF SFSDFSFGGF DRTYUTYIUUIPYUUF TTSDDFSDFSGDFGDFGE TETWTEFGHKKFGNC VFSDFDHGKJHKKP";

        LibUtils.waitElementVisible(driver, popup_reject_reason, Const.seconds);
        writeText(driver, popup_reject_reason, rdm_str);
        takeSreenshot.capture(driver);

        try {
            driver.findElement(popup_submit).click();
        } catch (Exception ignored) {
        }
        LibUtils.waitElementVisible(driver, tbl_realizationForm_debtorname, Const.seconds);

        takeSreenshot.capture(driver);
        writeText(driver, txt_Search, debtorname);
        LibUtils.capture.log(Status.INFO, MarkupHelper.createCodeBlock(rdm_str));
        LibUtils.actualLogTrue(debtorname);
        takeSreenshot.capture(driver);
        status_testCase(iRowPictName, true, "");
    }

    public void klikDebtorNameRejectEmptyChar() throws InterruptedException {
        iRowPictName = 106;
        createTest(iRowPictName, extent_test_case, extent);

        click(driver, menuRealization);
        click(driver, list_status);
        LibUtils.waitElementVisible(driver, list_statusWaitingApproval, Const.seconds);
        click(driver, list_statusWaitingApproval);
        readTestData.testData();
        writeText(driver, txt_Search, readTestData.timeStamp);
        LibUtils.waitElementVisible(driver, tbl_realizationForm_debtorname, Const.seconds);
        String debtorname = getText(driver, tbl_realizationForm_debtorname);

        click(driver, tbl_realizationForm_debtorname);
        LibUtils.waitElementVisible(driver, btn_reject_detail_formRealisasi, Const.seconds);
        click(driver, btn_reject_detail_formRealisasi);
        takeSreenshot.capture(driver);

        click(driver, popup_select_option);
        click(driver, popup_select_value_option1);
        LibUtils.waitElementVisible(driver, popup_reject_reason, Const.seconds);
        writeText(driver, popup_reject_reason, "");

        takeSreenshot.capture(driver);

        try {
            driver.findElement(popup_submit).click();
        } catch (Exception ignored) {
        }
        LibUtils.waitElementVisible(driver, tbl_realizationForm_debtorname, Const.seconds);

        takeSreenshot.capture(driver);
        writeText(driver, txt_Search, debtorname);
        LibUtils.actualLogTrue(debtorname);
        takeSreenshot.capture(driver);
        status_testCase(iRowPictName, true, "");
    }

    public void reject() throws InterruptedException {

        click(driver, menuRealization);
        driver.findElement(list_status).click();
        LibUtils.waitElementVisible(driver, list_statusWaitingApproval, Const.seconds);
        driver.findElement(list_statusWaitingApproval).click();
        readTestData.testData();
        driver.findElement(txt_Search).sendKeys(readTestData.timeStamp);
        LibUtils.waitElementVisible(driver, tbl_realizationForm_debtorname, Const.seconds);
        String debtorname = driver.findElement(tbl_realizationForm_debtorname).getText();

        driver.findElement(tbl_realizationForm_debtorname).click();
        LibUtils.waitElementVisible(driver, btn_reject_detail_formRealisasi, Const.seconds);
        driver.findElement(btn_reject_detail_formRealisasi).click();
        driver.findElement(popup_select_option).click();
        driver.findElement(popup_select_value_option1).click();
        LibUtils.waitElementVisible(driver, popup_reject_reason, Const.seconds);
        driver.findElement(popup_reject_reason).sendKeys("");

        try {
            driver.findElement(popup_submit).click();
        } catch (Exception ignored) {
        }
    }

    public void verifyNotification() {
        iRowPictName = 107;
        createTest(iRowPictName, extent_test_case, extent);

        click(driver, menuRealization);
        LibUtils.waitElementVisible(driver, tbl_realizationForm_debtorname, Const.seconds);
        driver.findElement(icon_notif).click();
        LibUtils.waitElementVisible(driver, seeAllNotif, Const.seconds);
        takeSreenshot.capture(driver);

        driver.findElement(seeAllNotif).click();

        boolean sts = true;
        int i = 1;
        String getMessage = null;
        while (sts) {
            getMessage = driver.findElement(By.xpath("//tbody[@id='tBody-55-89']/tr[" + i + "]/td[3]")).getText();
            if (getMessage.contains("Terdapat file realisasi baru dari partner " + readTestData.companyName)) {
                LibUtils.verifyElementExist(driver, By.xpath("//tbody[@id='tBody-55-89']/tr[" + i + "]/td[3]"));
                sts = false;
            }
            i++;
        }
        takeSreenshot.capture(driver);
        status_testCase(iRowPictName, true, "");
    }
}
