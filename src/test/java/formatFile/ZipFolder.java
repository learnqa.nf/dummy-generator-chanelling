package formatFile;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.zip.ZipEntry;
import java.util.zip.ZipOutputStream;

public class ZipFolder {
    public static void zipFolder(String pthFolder, String suppDocName) {

        try {
            File source = new File(pthFolder);
            List<String> filesListInDir = new ArrayList<>();
            File[] files = source.listFiles();

            for (File file : files) {
                if (file.isFile()) {
                    filesListInDir.add(file.getAbsolutePath());
                }
            }
            //create ZipOutputStream to write to the zip file zip
            FileOutputStream fos = new FileOutputStream(suppDocName);
            ZipOutputStream zos = new ZipOutputStream(fos);

            for (String filePath : filesListInDir) {

                ZipEntry ze = new ZipEntry(filePath.substring(source.getAbsolutePath().length() + 1));
                zos.putNextEntry(ze);

                FileInputStream fis = new FileInputStream(filePath);
                byte[] buffer = new byte[1024];
                int len;
                while ((len = fis.read(buffer)) > 0) {
                    zos.write(buffer, 0, len);
                }
                zos.closeEntry();
                fis.close();
            }

            zos.close();
            fos.close();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
}