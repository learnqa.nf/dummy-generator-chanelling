package formatFile;

import java.io.*;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Base64;
import java.util.Date;

import elementsPage.Const;
import testData.ReadTestData;
import org.openqa.selenium.io.Zip;

public class CSVFileName {
    private String nomorAplikasi;
    private String kodeProduk;
    ReadTestData readTestData = new ReadTestData();


    public void parseData(String case1, String case2, String nomor_aplikasi, String kode_produk, String tanggal, String namaDebitur, String companyName) {

        String resultDataDummyFile = Const.resultDataDummyFile;
        this.nomorAplikasi = nomor_aplikasi;
        this.kodeProduk = kode_produk;
        try {
            if (companyName.equals(Const.typ_NikelMora) || companyName.equals(Const.typ_NikelICL) || companyName.equals(Const.typ_NikelIPL)) {
                File fileApp = new File(resultDataDummyFile + case1.substring(0, 7) + Const.extCSV);
                File fileRea = new File(resultDataDummyFile + case2.substring(0, 7) + Const.extCSV);

                fileApp.renameTo(new File(resultDataDummyFile + case1.substring(0, 7) + "_" + nomor_aplikasi + "_" + kode_produk + "_" + tanggal + Const.extCSV));
                fileRea.renameTo(new File(resultDataDummyFile + case2.substring(0, 7) + "_" + nomor_aplikasi + "_" + kode_produk + "_" + tanggal + Const.extCSV));

            } else {
                File fileApp = new File(resultDataDummyFile + case1 + Const.extCSV);
                File fileRea = new File(resultDataDummyFile + case2 + Const.extCSV);

                fileApp.renameTo(new File(resultDataDummyFile + case1 + "_" + nomor_aplikasi + "_" + kode_produk + "_" + tanggal + Const.extCSV));
                fileRea.renameTo(new File(resultDataDummyFile + case2 + "_" + nomor_aplikasi + "_" + kode_produk + "_" + tanggal + Const.extCSV));
            }
        } catch (Exception ex) {
            ex.getCause();
        }
    }

    public void zipDataDummy(String companyName, ArrayList<String[]> arrayData) {

        readTestData.testData();
        String pattern = Const.glPattern;
        SimpleDateFormat simpleDateFormat = new SimpleDateFormat(pattern);
        String date = simpleDateFormat.format(new Date());

        BufferedOutputStream stream = null;
        BufferedOutputStream stream2 = null;
        BufferedOutputStream streamSupDoc = null;
        companyName = companyName.toUpperCase().replace(" ", "");

        try {
            String zip = Zip.zip(new File(Const.resultDataDummyFile));
            String zipSupDoc = Zip.zip(new File("./dummy_SuppDoc/"));
            stream = new BufferedOutputStream(new FileOutputStream("./dummy_suppDoc/" + "DATADUMMY_" + nomorAplikasi + companyName + "_" + kodeProduk + "_" + date + "_" + readTestData.timeStamp + ".zip"));

            byte[] decode = Base64.getDecoder().decode(zip);
            stream.write(decode);
            stream.close();

            //create support document
            String supdocFile = Zip.zip(new File("./data-test/suppdoc-file-sample/"));
            for (String[] reaData : arrayData) {
                if (reaData[0] != "Nomor_Aplikasi") {
                    stream2 = new BufferedOutputStream(new FileOutputStream("./dummy_suppDoc/" + "IDV_" + reaData[0] + "_" + date + ".zip"));
                    byte[] decode2 = Base64.getDecoder().decode(supdocFile);
                    stream2.write(decode2);
                    stream2.close();
                }
            }

//            File source = new File("./dummy_SuppDoc/");
            ZipFolder.zipFolder("./dummy_SuppDoc/", "./dummy_suppDocZip/" + "DATADUMMY_SUPPDOC_" + nomorAplikasi + companyName + "_" + kodeProduk + "_" + date + "_" + readTestData.timeStamp + ".zip");


        } catch (
                Exception e) {
            e.getCause();
        }
    }
}
