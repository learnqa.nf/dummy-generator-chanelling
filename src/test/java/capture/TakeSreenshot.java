package capture;

import com.aventstack.extentreports.MediaEntityBuilder;
import com.aventstack.extentreports.Status;
import elementsPage.Const;
import individu.LibUtils;
import org.apache.commons.io.FileUtils;
import org.apache.commons.io.IOUtils;
import org.openqa.selenium.OutputType;
import org.openqa.selenium.TakesScreenshot;
import org.openqa.selenium.WebDriver;
import testData.ReadTestData;

import javax.imageio.ImageIO;
import java.awt.*;
import java.awt.image.BufferedImage;
import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.Date;

import static com.extentreports.ExtentReportsFile.folderReport;

public class TakeSreenshot {
    static String pthFileImageName = Const.pthFileImage;
    ImageExcel imageExcel = new ImageExcel();
    ReadTestData readTestData = new ReadTestData();

    public void capture(WebDriver driver) {
        readTestData.testData();

        try {

            String pattern = Const.pPattern;
            SimpleDateFormat simpleDateFormat = new SimpleDateFormat(pattern);
            String formattedToday = simpleDateFormat.format(new Date());

            String pictName = readTestData.tagName + formattedToday.substring(0, 6) + "/" + "" + formattedToday + Const.extPict;
            String fullPathName = folderReport + "/" + pictName;
            Thread.sleep(Const.delay);
            File src_screenshot = ((TakesScreenshot) driver).getScreenshotAs(OutputType.FILE);
            File Dest = new File(fullPathName);
            FileUtils.copyFile(src_screenshot, Dest);

            LibUtils.capture.log(Status.INFO, MediaEntityBuilder.createScreenCaptureFromPath(pictName).build());
//            LibUtils.capture.log(Status.INFO, MediaEntityBuilder.createScreenCaptureFromBase64String(Base64.getEncoder().encodeToString(imgBytes)).build());

        } catch (InterruptedException | IOException e) {
            e.printStackTrace();
        }
    }

    public static void captureRobot(boolean exist, String pictName, String sheet) {

        try {
            Robot robotObject = new Robot();
            Rectangle screenSize = new Rectangle(Toolkit.getDefaultToolkit().getScreenSize());
            BufferedImage tmp = robotObject.createScreenCapture(screenSize);

            String pattern = Const.pPattern;
            SimpleDateFormat simpleDateFormat = new SimpleDateFormat(pattern);
            String formattedToday = simpleDateFormat.format(new Date());

            String path = "src/." + Const.pthFileImage + formattedToday + Const.extPict;
            ImageIO.write(tmp, "png", new File(path));
            File Dest = new File(path);


            if (exist) {

//                childLog.log(Status.PASS, pictName, MediaEntityBuilder.createScreenCaptureFromPath(Dest.getAbsolutePath()).build());
            } else {

//                childLog.log(Status.FAIL, pictName, MediaEntityBuilder.createScreenCaptureFromPath(Dest.getAbsolutePath()).build());

            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public static void main(String[] args) {
        captureRobot(true, "", "");
    }
}
